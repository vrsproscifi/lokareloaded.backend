﻿using Loka.Common.Authentication;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;
using Loka.Common.Orleans.Grains;
using Loka.MatchMaking.DataBase;
using Loka.MatchMaking.DataBase.Entities.Queue;
using Loka.MatchMaking.Interfaces;
using Loka.MatchMaking.Interfaces.Forms.Queue;
using Microsoft.EntityFrameworkCore;

namespace Loka.MatchMaking.Grains.Grains.Queue;

public class MatchMakingQueueGrain : Grain, IMatchMakingQueueGrain, IWithPlayerActor
{
    public PlayerActor Actor { get; set; }
    private MatchMakingDbContext MatchMakingDbContext { get; set; }

    public MatchMakingQueueGrain(MatchMakingDbContext matchMakingDbContext)
    {
        MatchMakingDbContext = matchMakingDbContext;
    }

    public async Task<OperationResult<JoinToMatchMakingQueueResponse>> JoinToMatchMakingQueue(JoinToMatchMakingQueueRequest request)
    {
        var queue = await MatchMakingDbContext.PlayerQueueEntity
            .Include(entity => entity.Members)
            .FirstAsync(entity => entity.EntityId == Actor.PlayerId);

        if (queue.State != PlayerQueueState.None)
        {
            return OperationResultCode.MembersAlreadyInMatch;
        }

        if (queue.IsSquadLeader())
        {
            if (!queue.IsMembersReady())
            {
                return OperationResultCode.MembersInSquadNotReady;
            }

            var targetServerVersionId = request.TargetServerVersionId;
            if (!queue.IsEqualTargetServerVersion(targetServerVersionId))
            {
                return OperationResultCode.MembersHasDifferentVersions;
            }

            queue.StartSearch(new PlayerQueueOptions()
            {
                GameMapsIds = request.GameMapsIds,
                GameModesIds = request.GameMapsIds,
                RegionsIds = request.GameMapsIds,
                TargetServerVersionId = request.TargetServerVersionId,
            });
        }
        else
        {
            queue.State = PlayerQueueState.Ready;
            queue.Options.TargetServerVersionId = request.TargetServerVersionId;
        }

        await MatchMakingDbContext.SaveChangesAsync();

        return new JoinToMatchMakingQueueResponse()
        {
        };
    }

    public async Task<OperationResult> LeaveFromMatchMakingQueue()
    {
        var queue = await MatchMakingDbContext.PlayerQueueEntity
            .Include(entity => entity.Members)
            .FirstAsync(entity => entity.EntityId == Actor.PlayerId);

        if (queue.State != PlayerQueueState.Playing)
        {
            queue.StopSearch();
        }

        await MatchMakingDbContext.SaveChangesAsync();

        throw new System.NotImplementedException();
    }

    public async Task<OperationResult<GetMatchMakingQueueInformationResponse>> GetMatchMakingQueueInformation()
    {
        throw new System.NotImplementedException();
    }
}