﻿using AutoMapper;
using Loka.Admin.Api.Features.Storages;

namespace Loka.Admin.Api.Mapping.Shared;

public sealed class LanguageInformationMapping : Profile
{
    public LanguageInformationMapping()
    {
        CreateMap<Common.Models.Countries.LanguageInformationModel, LanguageInformationModel>()
            .ForMember(q => q.Id, q => q.MapFrom(p => p.Id))
            .ForMember(q => q.Name, q => q.MapFrom(p => p.Name));
            
                        
        CreateMap<Common.Models.Countries.LanguageInformationModel[], GetWorldLanguagesResult>()
            .ForMember(q => q.Entries, q => q.MapFrom(p => p));
    }
}