﻿using System.Diagnostics;

namespace Loka.Identity.Interfaces.Models;

[DebuggerDisplay("Issuer: {Issuer}, Audience: {Audience}, Authority: {Authority}, SigningKey: {SigningKey}")]
public class JwtBearerAuthenticationSettings
{
    public string? Issuer { get; set; }
    public string? Audience { get; set; }
    public string? Authority { get; set; }
    public string? SigningKey { get; set; }

    public TimeSpan SessionTimeout { get; init; } = TimeSpan.FromSeconds(30);
    public TimeSpan TokenLifeTime { get; set; } = TimeSpan.FromMinutes(15);

    public void Validate()
    {
        if (string.IsNullOrWhiteSpace(Issuer))
            throw new ArgumentNullException(nameof(Issuer), $"Invalid {nameof(JwtBearerAuthenticationSettings)}.{nameof(Issuer)}");

        if (string.IsNullOrWhiteSpace(Audience))
            throw new ArgumentNullException(nameof(Audience), $"Invalid {nameof(JwtBearerAuthenticationSettings)}.{nameof(Audience)}");

        if (string.IsNullOrWhiteSpace(Authority))
            throw new ArgumentNullException(nameof(Authority), $"Invalid {nameof(JwtBearerAuthenticationSettings)}.{nameof(Authority)}");

        if (string.IsNullOrWhiteSpace(SigningKey))
            throw new ArgumentNullException(nameof(SigningKey), $"Invalid {nameof(JwtBearerAuthenticationSettings)}.{nameof(SigningKey)}");
    }
}