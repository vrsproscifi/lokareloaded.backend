﻿using Loka.MatchMaking.Interfaces;

namespace Loka.Api.Features.MatchMaking;

public class MatchMakingSquadController : MatchMakingSquad.MatchMakingSquadBase
{
    private ControllerServices<MatchMakingSquadController, IMatchMakingSquadGrain> Services { get; }

    public MatchMakingSquadController(ControllerServices<MatchMakingSquadController, IMatchMakingSquadGrain> services)
    {
        Services = services;
    }

    public override Task<SendSquadInviteQuery>SendSquadInvite(SendSquadInviteQuery request, ServerCallContext context)
    {
        return base.SendSquadInvite(request, context);
    }

    public override Task<SendSquadInviteQuery>AcceptSquadInvite(AcceptSquadInviteQuery request, ServerCallContext context)
    {
        return base.AcceptSquadInvite(request, context);
    }

    public override Task<SquadInformation>GetSquadInformation(RevokeSquadInviteQuery request, ServerCallContext context)
    {
        return base.GetSquadInformation(request, context);
    }

    public override Task<SendSquadInviteQuery>RevokeSquadInvite(RevokeSquadInviteQuery request, ServerCallContext context)
    {
        return base.RevokeSquadInvite(request, context);
    }
}