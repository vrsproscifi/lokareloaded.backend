﻿namespace Loka.MatchMaking.DataBase.Enums.Server;

public enum ServerRegionTypeId : byte
{
    None = 0,

    RuEast = 1,
    RuCentral = 2,
    RuSouth = 3,

    UsNorth = 4,
    UsSouth = 5,

    China = 6,

    Japan = 7,

    Australian = 8,
}