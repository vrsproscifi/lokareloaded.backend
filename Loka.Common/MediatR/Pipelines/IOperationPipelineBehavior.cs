﻿using MediatR;
using Loka.Common.MediatR.Requests;
using Loka.Common.Operations.Results;

namespace Loka.Common.MediatR.Pipelines;

public interface IOperationPipelineBehavior<in TRequest, TResult> : IPipelineBehavior<TRequest, OperationResult<TResult>>
    where TRequest : IOperationRequest<TResult>
{

}

public interface IOperationPipelineBehavior<in TRequest> : IPipelineBehavior<TRequest, OperationResult>
    where TRequest : IOperationRequest
{

}