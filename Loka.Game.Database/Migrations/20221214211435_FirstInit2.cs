﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Loka.Game.Database.Migrations
{
    /// <inheritdoc />
    public partial class FirstInit2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CaseItemEntity_CaseEntity_CaseId",
                schema: "System",
                table: "CaseItemEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_EveryDayAwardEntity_CaseEntity_CaseId",
                schema: "System",
                table: "EveryDayAwardEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_CurrencyId",
                schema: "System",
                table: "GameItemEntity");

            migrationBuilder.RenameColumn(
                name: "CurrencyId",
                schema: "System",
                table: "GameItemEntity",
                newName: "SubscribeCurrencyId");

            migrationBuilder.RenameColumn(
                name: "Cost",
                schema: "System",
                table: "GameItemEntity",
                newName: "SubscribePrice");

            migrationBuilder.RenameIndex(
                name: "IX_GameItemEntity_CurrencyId",
                schema: "System",
                table: "GameItemEntity",
                newName: "IX_GameItemEntity_SubscribeCurrencyId");

            migrationBuilder.RenameColumn(
                name: "CaseId",
                schema: "System",
                table: "EveryDayAwardEntity",
                newName: "ModelId");

            migrationBuilder.RenameIndex(
                name: "IX_EveryDayAwardEntity_CaseId",
                schema: "System",
                table: "EveryDayAwardEntity",
                newName: "IX_EveryDayAwardEntity_ModelId");

            migrationBuilder.RenameColumn(
                name: "CaseId",
                schema: "System",
                table: "CaseItemEntity",
                newName: "ModelId");

            migrationBuilder.RenameIndex(
                name: "IX_CaseItemEntity_CaseId",
                schema: "System",
                table: "CaseItemEntity",
                newName: "IX_CaseItemEntity_ModelId");

            migrationBuilder.AddColumn<long>(
                name: "AmountDefault",
                schema: "System",
                table: "GameItemEntity",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "AmountInStack",
                schema: "System",
                table: "GameItemEntity",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<long>(
                name: "AmountMaximum",
                schema: "System",
                table: "GameItemEntity",
                type: "bigint",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "AmountMinimum",
                schema: "System",
                table: "GameItemEntity",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<Guid>(
                name: "SellCurrencyId",
                schema: "System",
                table: "GameItemEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<int>(
                name: "SellPrice",
                schema: "System",
                table: "GameItemEntity",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "System",
                table: "GameCurrencyEntity",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "AdminComment",
                schema: "System",
                table: "AchievementEntity",
                type: "character varying(8192)",
                maxLength: 8192,
                nullable: true);

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(8396), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe602"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(9015), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe603"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(9215), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe604"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(9403), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe605"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(9586), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe100"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(9288), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe101"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(123), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe102"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(332), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe103"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(523), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe104"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(709), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe105"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(891), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe106"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(1076), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe107"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(1257), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe108"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(1436), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe109"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(1617), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe110"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(1798), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe111"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(1982), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe112"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(2163), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe113"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(2346), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe114"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(2528), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe115"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(2708), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe116"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(2890), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe117"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(3073), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe118"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(3253), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe119"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(3437), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe120"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(3692), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe121"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(3910), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe122"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(4094), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe123"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(4275), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe124"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(4454), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe125"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(4685), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe126"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(4866), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe127"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(5047), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe128"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(5229), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe129"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(5407), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe130"),
                columns: new[] { "AdminComment", "CreatedAt" },
                values: new object[] { null, new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(5588), new TimeSpan(0, 0, 0, 0, 0)) });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 70, DateTimeKind.Unspecified).AddTicks(758), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 70, DateTimeKind.Unspecified).AddTicks(1609), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 70, DateTimeKind.Unspecified).AddTicks(1818), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 70, DateTimeKind.Unspecified).AddTicks(2009), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 70, DateTimeKind.Unspecified).AddTicks(8350), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 70, DateTimeKind.Unspecified).AddTicks(9252), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 70, DateTimeKind.Unspecified).AddTicks(9460), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(3367), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(4030), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(4231), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(4418), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(4601), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(4780), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(4956), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(5135), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(5381), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.InsertData(
                schema: "System",
                table: "GameItemCategoryEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[] { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(5594), new TimeSpan(0, 0, 0, 0, 0)), null, "Resource", (byte)10 });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe100"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(8139), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(8348), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(8545), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(8737), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(8928), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(9120), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(9309), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(9496), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(9681), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe109"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(9871), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe110"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(59), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe111"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(247), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe112"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(433), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe113"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(618), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe114"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(804), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe115"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(989), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe116"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(1173), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe117"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(1357), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe118"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(1620), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe119"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(1841), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe120"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(2033), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe121"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(2222), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe122"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(2409), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe123"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(2596), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe124"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(2780), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe125"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(2963), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe126"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(3149), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe127"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(3339), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe128"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(3542), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe129"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(3737), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe130"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(3923), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe131"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(4108), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe132"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(4342), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe133"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(4526), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe134"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(4710), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe135"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(4896), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe136"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(5083), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe137"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(5268), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe138"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(5453), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe139"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(5636), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe140"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(5822), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe141"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(6059), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe142"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(6271), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe143"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(6458), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe144"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(6642), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe145"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(6828), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe147"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(7016), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe148"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(7198), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe149"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(7384), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe151"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(7567), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe152"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(7815), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe154"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(8028), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe155"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(8219), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe156"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(8405), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe157"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(8591), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe158"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(8776), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe159"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(8960), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe160"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(9168), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe161"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(9354), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe162"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(9539), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe163"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(9724), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe164"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(9908), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe165"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(94), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe166"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(278), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe167"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(458), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe168"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(645), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe170"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(829), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe171"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(1012), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe172"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(1195), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe173"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(1378), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe174"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(1565), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe175"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(1745), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe176"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(1929), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe177"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(2111), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe178"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(2293), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe179"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(2616), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe180"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(2814), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe181"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(3004), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe182"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(3195), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe183"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(3381), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe184"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(3567), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe185"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(3752), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe186"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(4006), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe187"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(4223), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe188"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(4414), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe189"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(4604), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe190"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(4818), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe191"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(5049), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe192"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(5261), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe193"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(5449), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe194"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(5633), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe196"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(5818), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe197"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(6005), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe198"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(6187), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe199"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(6372), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe200"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(6556), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe201"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(6738), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe203"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(6917), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe204"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(7099), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe205"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(7285), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe206"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(7542), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe207"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(7721), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe208"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(7900), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe209"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(8079), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe210"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(8257), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe211"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(8437), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe212"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(8615), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe213"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(8798), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe214"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(8976), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe217"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(9155), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe218"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(9335), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe220"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(9514), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe221"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(9695), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe222"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(9876), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe223"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(125), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe224"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(334), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe225"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(517), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe226"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(697), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe227"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(878), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe228"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(1056), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe229"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(1231), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe230"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(1407), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe231"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(1588), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe232"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(1765), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe233"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(1942), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe234"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(2121), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe235"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(2299), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe236"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(2475), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe237"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(2651), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe238"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(2828), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe239"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(3014), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe240"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(3191), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe241"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(3369), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe242"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(3546), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe243"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(3773), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe244"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(3974), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe245"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(4160), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe246"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(4340), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe247"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(4515), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe248"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(4690), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe249"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(4868), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe250"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(5046), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe251"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(5222), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe252"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(5416), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe253"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(5608), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe254"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(5788), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe255"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(6033), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe256"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(6249), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe257"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(6432), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe258"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(6614), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe259"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(6794), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe260"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(6970), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe261"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(7147), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe262"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(7326), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe263"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(7536), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe264"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(7741), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe265"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(7948), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe266"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(8130), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe268"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(8314), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe269"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(8495), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe270"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(8679), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe271"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(8862), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe272"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(9044), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe273"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(9225), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe274"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(9409), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe275"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(9590), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe276"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(9770), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe277"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(9952), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe278"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(168), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe279"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(368), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe280"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(553), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe281"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(736), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe282"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(930), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe283"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(1122), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe284"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(1305), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe285"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(1485), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe286"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(1667), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe287"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(1850), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe288"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(2097), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe289"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(2309), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe290"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(2537), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe291"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(2749), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe292"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(2936), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe293"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(3121), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe294"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(3303), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe295"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(3485), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe296"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(3667), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe297"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(3849), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe298"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(4034), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe299"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(4215), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe300"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(4398), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe301"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(4582), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe302"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(4765), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe303"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(4945), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe304"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(5128), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe305"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(5313), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe306"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(5496), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe307"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(5677), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe308"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(5862), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe309"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(6046), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe310"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(6231), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe311"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(6432), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe312"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(6628), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe313"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(6815), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe314"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(6999), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe315"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(7184), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe316"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(7366), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe317"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(7554), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe318"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(7758), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe319"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(7938), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe320"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(8181), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe321"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(8390), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe322"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(8576), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe323"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(8755), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe324"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(8934), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe325"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(9113), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe326"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(9291), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe327"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(9468), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe328"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(9648), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe329"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(9823), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe330"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe331"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(178), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe332"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(354), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe333"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(529), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe334"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(706), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe335"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(884), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe338"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(1061), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe339"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(1286), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe340"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(1487), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe341"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(1668), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe342"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(1847), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe343"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(6831), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(9644), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(286), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(490), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(679), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(865), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(1048), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(1232), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(1413), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldGenderEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(4759), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldGenderEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(5439), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 72, DateTimeKind.Unspecified).AddTicks(2979), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 72, DateTimeKind.Unspecified).AddTicks(2218), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 72, DateTimeKind.Unspecified).AddTicks(3182), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_SellCurrencyId",
                schema: "System",
                table: "GameItemEntity",
                column: "SellCurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_CaseItemEntity_CaseEntity_ModelId",
                schema: "System",
                table: "CaseItemEntity",
                column: "ModelId",
                principalSchema: "System",
                principalTable: "CaseEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EveryDayAwardEntity_CaseEntity_ModelId",
                schema: "System",
                table: "EveryDayAwardEntity",
                column: "ModelId",
                principalSchema: "System",
                principalTable: "CaseEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SellCurrencyId",
                schema: "System",
                table: "GameItemEntity",
                column: "SellCurrencyId",
                principalSchema: "System",
                principalTable: "GameCurrencyEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SubscribeCurrencyId",
                schema: "System",
                table: "GameItemEntity",
                column: "SubscribeCurrencyId",
                principalSchema: "System",
                principalTable: "GameCurrencyEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CaseItemEntity_CaseEntity_ModelId",
                schema: "System",
                table: "CaseItemEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_EveryDayAwardEntity_CaseEntity_ModelId",
                schema: "System",
                table: "EveryDayAwardEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SellCurrencyId",
                schema: "System",
                table: "GameItemEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SubscribeCurrencyId",
                schema: "System",
                table: "GameItemEntity");

            migrationBuilder.DropIndex(
                name: "IX_GameItemEntity_SellCurrencyId",
                schema: "System",
                table: "GameItemEntity");

            migrationBuilder.DeleteData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"));

            migrationBuilder.DropColumn(
                name: "AmountDefault",
                schema: "System",
                table: "GameItemEntity");

            migrationBuilder.DropColumn(
                name: "AmountInStack",
                schema: "System",
                table: "GameItemEntity");

            migrationBuilder.DropColumn(
                name: "AmountMaximum",
                schema: "System",
                table: "GameItemEntity");

            migrationBuilder.DropColumn(
                name: "AmountMinimum",
                schema: "System",
                table: "GameItemEntity");

            migrationBuilder.DropColumn(
                name: "SellCurrencyId",
                schema: "System",
                table: "GameItemEntity");

            migrationBuilder.DropColumn(
                name: "SellPrice",
                schema: "System",
                table: "GameItemEntity");

            migrationBuilder.DropColumn(
                name: "AdminComment",
                schema: "System",
                table: "AchievementEntity");

            migrationBuilder.RenameColumn(
                name: "SubscribePrice",
                schema: "System",
                table: "GameItemEntity",
                newName: "Cost");

            migrationBuilder.RenameColumn(
                name: "SubscribeCurrencyId",
                schema: "System",
                table: "GameItemEntity",
                newName: "CurrencyId");

            migrationBuilder.RenameIndex(
                name: "IX_GameItemEntity_SubscribeCurrencyId",
                schema: "System",
                table: "GameItemEntity",
                newName: "IX_GameItemEntity_CurrencyId");

            migrationBuilder.RenameColumn(
                name: "ModelId",
                schema: "System",
                table: "EveryDayAwardEntity",
                newName: "CaseId");

            migrationBuilder.RenameIndex(
                name: "IX_EveryDayAwardEntity_ModelId",
                schema: "System",
                table: "EveryDayAwardEntity",
                newName: "IX_EveryDayAwardEntity_CaseId");

            migrationBuilder.RenameColumn(
                name: "ModelId",
                schema: "System",
                table: "CaseItemEntity",
                newName: "CaseId");

            migrationBuilder.RenameIndex(
                name: "IX_CaseItemEntity_ModelId",
                schema: "System",
                table: "CaseItemEntity",
                newName: "IX_CaseItemEntity_CaseId");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                schema: "System",
                table: "GameCurrencyEntity",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 700, DateTimeKind.Unspecified).AddTicks(6589), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe602"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 700, DateTimeKind.Unspecified).AddTicks(7248), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe603"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 700, DateTimeKind.Unspecified).AddTicks(7487), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe604"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 700, DateTimeKind.Unspecified).AddTicks(7694), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe605"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 700, DateTimeKind.Unspecified).AddTicks(7867), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe100"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(6338), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(7086), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(7269), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(7436), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(7604), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(7768), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(7928), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(8088), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(8248), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe109"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(8409), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe110"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(8568), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe111"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(8729), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe112"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(8886), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe113"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(9044), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe114"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(9206), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe115"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(9362), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe116"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(9522), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe117"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(9679), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe118"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(9840), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe119"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(9997), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe120"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(207), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe121"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(379), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe122"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(596), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe123"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(779), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe124"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(941), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe125"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(1099), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe126"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(1258), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe127"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(1415), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe128"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(1573), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe129"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(1730), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe130"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(1887), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 697, DateTimeKind.Unspecified).AddTicks(5582), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 697, DateTimeKind.Unspecified).AddTicks(6351), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 697, DateTimeKind.Unspecified).AddTicks(6540), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 697, DateTimeKind.Unspecified).AddTicks(6709), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 698, DateTimeKind.Unspecified).AddTicks(5029), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 698, DateTimeKind.Unspecified).AddTicks(5766), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 698, DateTimeKind.Unspecified).AddTicks(5950), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 698, DateTimeKind.Unspecified).AddTicks(9118), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 698, DateTimeKind.Unspecified).AddTicks(9767), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 698, DateTimeKind.Unspecified).AddTicks(9954), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 699, DateTimeKind.Unspecified).AddTicks(122), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 699, DateTimeKind.Unspecified).AddTicks(288), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 699, DateTimeKind.Unspecified).AddTicks(451), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 699, DateTimeKind.Unspecified).AddTicks(615), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 699, DateTimeKind.Unspecified).AddTicks(778), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 699, DateTimeKind.Unspecified).AddTicks(938), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe100"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(4391), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(4575), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(4747), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(4923), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(5090), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(5252), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(5413), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(5575), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(5737), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe109"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(5899), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe110"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(6061), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe111"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(6224), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe112"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(6388), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe113"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(6550), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe114"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(6713), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe115"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(6877), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe116"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(7036), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe117"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(7267), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe118"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(7450), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe119"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(7610), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe120"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(7770), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe121"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(7930), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe122"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(8125), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe123"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(8305), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe124"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(8468), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe125"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(8628), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe126"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(8788), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe127"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(8949), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe128"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(9109), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe129"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(9273), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe130"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(9432), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe131"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(9594), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe132"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(9755), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe133"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(9914), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe134"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(74), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe135"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(233), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe136"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(391), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe137"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(549), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe138"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(709), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe139"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(867), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe140"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(1025), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe141"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(1186), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe142"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(1345), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe143"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(1503), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe144"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(1661), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe145"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(1817), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe147"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(1974), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe148"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(2134), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe149"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(2292), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe151"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(2448), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe152"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(2608), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe154"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(2766), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe155"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(2955), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe156"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(3168), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe157"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(3361), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe158"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(3525), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe159"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(3686), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe160"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(3844), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe161"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(4004), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe162"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(4208), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe163"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(4377), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe164"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(4534), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe165"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(4692), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe166"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(4847), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe167"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(5007), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe168"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(5167), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe170"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(5323), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe171"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(5479), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe172"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(5638), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe173"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(5841), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe174"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(6014), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe175"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(6171), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe176"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(6331), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe177"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(6489), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe178"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(6649), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe179"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(6809), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe180"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(6967), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe181"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(7126), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe182"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(7281), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe183"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(7439), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe184"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(7594), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe185"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(7755), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe186"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(7912), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe187"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(8072), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe188"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(8231), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe189"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(8389), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe190"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(8548), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe191"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(8707), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe192"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(8872), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe193"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(9028), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe194"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(9244), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe196"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(9418), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe197"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(9577), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe198"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(9734), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe199"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(9893), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe200"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(54), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe201"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(211), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe203"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(369), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe204"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(528), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe205"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(687), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe206"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(845), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe207"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(1001), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe208"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(1158), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe209"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(1314), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe210"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(1473), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe211"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(1631), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe212"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(1788), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe213"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(1945), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe214"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(2103), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe217"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(2261), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe218"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(2422), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe220"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(2581), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe221"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(2739), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe222"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(2896), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe223"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(3054), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe224"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(3212), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe225"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(3450), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe226"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(3629), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe227"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(3787), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe228"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(3948), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe229"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(4173), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe230"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(4358), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe231"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(4517), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe232"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(4674), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe233"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(4832), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe234"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(4988), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe235"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(5146), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe236"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(5362), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe237"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(5538), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe238"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(5696), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe239"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(5856), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe240"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(6014), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe241"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(6171), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe242"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(6330), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe243"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(6486), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe244"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(6642), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe245"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(6801), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe246"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(6962), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe247"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(7121), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe248"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(7281), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe249"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(7440), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe250"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(7599), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe251"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(7758), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe252"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(7918), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe253"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(8078), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe254"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(8234), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe255"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(8393), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe256"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(8550), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe257"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(8707), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe258"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(8866), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe259"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(9025), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe260"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(9183), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe261"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(9342), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe262"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(9498), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe263"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(9655), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe264"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(9813), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe265"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(9971), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe266"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(129), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe268"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(286), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe269"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(443), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe270"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(600), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe271"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(757), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe272"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(957), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe273"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(1177), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe274"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(1353), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe275"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(1510), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe276"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(1669), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe277"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(1828), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe278"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(1986), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe279"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(2148), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe280"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(2304), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe281"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(2463), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe282"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(2621), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe283"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(2788), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe284"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(2947), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe285"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(3106), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe286"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(3265), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe287"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(3424), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe288"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(3581), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe289"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(3738), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe290"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(3894), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe291"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(4052), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe292"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(4257), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe293"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(4422), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe294"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(4582), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe295"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(4740), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe296"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(4899), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe297"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(5057), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe298"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(5243), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe299"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(5414), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe300"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(5573), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe301"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(5730), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe302"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(5889), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe303"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(6046), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe304"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(6204), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe305"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(6362), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe306"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(6519), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe307"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(6676), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe308"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(6835), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe309"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(6990), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe310"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(7209), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe311"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(7385), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe312"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(7544), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe313"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(7704), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe314"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(7860), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe315"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(8020), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe316"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(8178), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe317"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(8335), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe318"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(8531), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe319"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(8705), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe320"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(8865), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe321"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(9026), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe322"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(9185), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe323"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(9345), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe324"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(9505), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe325"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(9662), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe326"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(9816), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe327"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(9973), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe328"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(131), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe329"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(288), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe330"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(444), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe331"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(599), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe332"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(758), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe333"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(919), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe334"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(1077), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe335"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(1236), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe338"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(1394), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe339"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(1553), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe340"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(1709), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe341"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(1868), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe342"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(2027), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe343"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(3286), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(7978), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(8550), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(8728), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(8896), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(9060), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(9271), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(9450), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(9613), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldGenderEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(4513), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldGenderEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(5118), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 701, DateTimeKind.Unspecified).AddTicks(675), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 700, DateTimeKind.Unspecified).AddTicks(9985), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 701, DateTimeKind.Unspecified).AddTicks(858), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.AddForeignKey(
                name: "FK_CaseItemEntity_CaseEntity_CaseId",
                schema: "System",
                table: "CaseItemEntity",
                column: "CaseId",
                principalSchema: "System",
                principalTable: "CaseEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_EveryDayAwardEntity_CaseEntity_CaseId",
                schema: "System",
                table: "EveryDayAwardEntity",
                column: "CaseId",
                principalSchema: "System",
                principalTable: "CaseEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_CurrencyId",
                schema: "System",
                table: "GameItemEntity",
                column: "CurrencyId",
                principalSchema: "System",
                principalTable: "GameCurrencyEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
