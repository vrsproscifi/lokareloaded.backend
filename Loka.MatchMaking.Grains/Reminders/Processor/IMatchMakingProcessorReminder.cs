﻿namespace Loka.MatchMaking.Grains.Reminders.Processor;


public interface IMatchMakingProcessorReminder : IGrainWithGuidKey, IRemindable
{
    Task Start();
    Task Stop();
}