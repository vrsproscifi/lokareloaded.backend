﻿using System.ComponentModel.DataAnnotations;
using Loka.Database.Common.Entities;
using Loka.Database.Common.Interfaces;

namespace Loka.Inventory.DataBase.Entities.Templates;

public sealed class PlayerInventoryTemplateEntity : BaseEntity, INamedEntity
{
    [Required, MaxLength(64)]
    public string Name { get; set; } = null!;

    public Guid PlayerId { get; set; }
    
    public IReadOnlyList<PlayerInventoryTemplateItemEntity>? Items { get; set; }
}