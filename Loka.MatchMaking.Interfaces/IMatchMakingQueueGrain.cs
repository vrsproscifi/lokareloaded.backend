﻿using Loka.Common.Operations.Results;
using Orleans;
using Loka.MatchMaking.Interfaces.Forms.Queue;

namespace Loka.MatchMaking.Interfaces;

public interface IMatchMakingQueueGrain : IGrainWithGuidKey
{
    Task<OperationResult<JoinToMatchMakingQueueResponse>> JoinToMatchMakingQueue(JoinToMatchMakingQueueRequest request);
    Task<OperationResult> LeaveFromMatchMakingQueue();
    Task<OperationResult<GetMatchMakingQueueInformationResponse>> GetMatchMakingQueueInformation();
}