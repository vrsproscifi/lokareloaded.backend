﻿using System;
using Loka.Database.Common.Entities;
using Microsoft.EntityFrameworkCore;

namespace Loka.Inventory.DataBase.Entities.Discounts;

[Index(nameof(PlayerId))]
public class PlayerDiscountEntity : BaseEntity, IPlayerChildEntity
{
    public Guid PlayerId { get; set; }
    public Guid ModelId { get; set; }
        
    /// <summary>
    /// Отображает информацию о последней дате использования.
    /// Null если скидка еще не использовалась
    /// </summary>
    public DateTimeOffset? LastUsingDate { get; set; }

    /// <summary>
    /// Информация о том, когда купон будет доступен снова. Если есть значение - то после этой даты скидка появится сама автоматически.
    /// Если значения нет, то скидка была одноразовая или значение устанавливается вручную
    /// </summary>
    public DateTimeOffset? NextAvailableDate { get; set; }

    /// <summary>
    /// Информация о том, когда действие акции будет окончено.
    /// Если есть значение - то после этой даты скидка будет не действительна. Если значения нет, то скидка действует бессрочно
    /// </summary>
    public DateTimeOffset? EndDiscountDate { get; set; }

}