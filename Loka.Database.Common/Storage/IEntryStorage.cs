﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Loka.Common.Operations.Results;

namespace Loka.Database.Common.Storage;

public interface IIsEntryStorage
{
        
}
    
public interface IEntryStorage<TModel> : IIsEntryStorage
{
    Task<OperationResult<TModel>> GetById(Guid id, CancellationToken cancellationToken = default);
    Task<TModel[]> GetList(CancellationToken cancellationToken = default);
}