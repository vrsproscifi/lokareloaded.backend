﻿namespace Loka.Admin.Players.Models;

public readonly struct PlayerNameObject
{
    public string Name { get; }
    public bool IsConfirmed { get; }
        
    public PlayerNameObject(string name, bool isConfirmed)
    {
        Name = name;
        IsConfirmed = isConfirmed;
    }
}