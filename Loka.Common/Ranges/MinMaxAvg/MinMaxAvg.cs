﻿using System;

namespace Loka.Common.Ranges.MinMaxAvg;

public class MinMaxAvg<T, TAvg>
    where T : IEquatable<T>, IComparable<T>, IComparable, IConvertible, IFormattable
    where TAvg : IEquatable<TAvg>, IComparable<TAvg>, IComparable, IConvertible, IFormattable
{
    public T Min { get; set; }
    public T Max { get; set; }
    public TAvg Avg { get; set; }

    public override string ToString()
    {
        return $"Min: {Min} | Max: {Max} | Avg: {Avg}";
    }
}

public class MinMaxAvg<T> : MinMaxAvg<T, T> 
    where T : IEquatable<T>, IComparable<T>, IComparable, IConvertible, IFormattable
{
}