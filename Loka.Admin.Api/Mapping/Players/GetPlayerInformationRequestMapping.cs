﻿using AutoMapper;
using Loka.Admin.Api.Features.Players;
using Loka.Admin.Players.Handlers.Players.Listing.GetPlayerInformation;

namespace Loka.Admin.Api.Mapping.Players;

public sealed class GetPlayerInformationRequestMapping : Profile
{
    public GetPlayerInformationRequestMapping()
    {
        CreateMap<GetPlayerInformationQuery, GetPlayerInformationRequest>()
            .ForMember(q => q.PlayerId, q => q.MapFrom(p => p.PlayerId));


        CreateMap<GetPlayerInformationResponse, GetPlayerInformationResult>()
            .ForMember(q => q.EntityId, q => q.MapFrom(p => p.EntityId))
            .ForMember(q => q.Login, q => q.MapFrom(p => p.Login))
            .ForMember(q => q.Email, q => q.MapFrom(p => p.Email))
            .ForMember(q => q.Statistics, q => q.MapFrom(p => p.Statistics))
            .ForMember(q => q.RegisteredAt, q => q.MapFrom(p => p.RegisteredAt))
            .ForMember(q => q.LastActivityAt, q => q.MapFrom(p => p.LastActivityAt))
            .ForMember(q => q.PremiumEndDate, q => q.MapFrom(p => p.PremiumEndDate))
            .ForMember(q => q.Country, q => q.MapFrom(p => p.Country))
            .ForMember(q => q.Language, q => q.MapFrom(p => p.Language))
            .ForMember(q => q.Role, q => q.MapFrom(p => p.Role));

    }
}