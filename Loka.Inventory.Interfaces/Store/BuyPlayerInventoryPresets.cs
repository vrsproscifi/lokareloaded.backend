﻿using Loka.Inventory.Interfaces.Presets;
using Loka.Inventory.Interfaces.Presets.Models;

namespace Loka.Inventory.Interfaces.Store;

public static class BuyPlayerInventoryPresets
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<Result>
    {
        [Id(0)]
        public Guid ModelId { get; init; }
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public PlayerInventoryPresetModel.RPC Preset { get; init; }
    }
}