﻿using System.Diagnostics;
using Loka.Common.Entry;
using Loka.Game.Interfaces.Players.Enums;

namespace Loka.Game.Interfaces.Players.Dtos;


[GenerateSerializer]
[DebuggerDisplay("Id: {Id}, Name: {Name}, Status: {Status}, LastActivityDate: {LastActivityDate}")]
public sealed class PlayerModel : IEntry, INamedEntry
{
    [Id(0)]
    public Guid Id { get; init; }
    [Id(1)]
    public string Name { get; init; } = null!;
    [Id(2)]
    public PlayerGameStatus Status { get; set; }
    [Id(3)]
    public DateTimeOffset LastActivityDate { get; set; }
}