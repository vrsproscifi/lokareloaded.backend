using Loka.Api.Authentication;
using Loka.Api.Features;
using Loka.Common;
using Microsoft.IdentityModel.Logging;

namespace Loka.Api;

public sealed class Startup
{
    public IWebHostEnvironment HostEnvironment { get; }
    public IConfiguration Configuration { get; }

    public Startup(IConfiguration configuration, IWebHostEnvironment hostEnvironment)
    {
        Configuration = configuration;
        HostEnvironment = hostEnvironment;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        IdentityModelEventSource.ShowPII = true;

        services.AddHttpsRedirection(options =>
        {
            options.RedirectStatusCode = (int)HttpStatusCode.PermanentRedirect;
            options.HttpsPort = 13852;
        });

        services.AddAutoMapperWithProfiles();
        services.AddAuthenticationServices(Configuration);
        services.AddGRPCServices(Configuration);
        services.AddIdentityApiServices(Configuration);

        services.AddScoped<PlayerActorMiddleWare>();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseAuthentication();
        app.UseHsts();
        app.UseRouting();
        app.UseMiddleware<PlayerActorMiddleWare>();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapGet("/", async context => { await context.Response.WriteAsync("Hello World!"); });
            endpoints.AddApiControllers(env);
        });
    }
}