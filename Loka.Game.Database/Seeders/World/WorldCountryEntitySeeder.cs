﻿using Loka.Game.Database.Entities.World;
using Loka.Game.Database.Enums.World;

namespace Loka.Game.Database.Seeders.World;

public static class WorldCountryEntitySeeder
{
    public static Guid Afghanistan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe100");
    public static Guid AlandIslands { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe101");
    public static Guid Albania { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe102");
    public static Guid Algeria { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe103");
    public static Guid AmericanSamoa { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe104");
    public static Guid AndorrA { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe105");
    public static Guid Angola { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe106");
    public static Guid Anguilla { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe107");
    public static Guid Antarctica { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe108");
    public static Guid AntiguaandBarbuda { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe109");
    public static Guid Argentina { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe110");
    public static Guid Armenia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe111");
    public static Guid Aruba { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe112");
    public static Guid Australia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe113");
    public static Guid Austria { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe114");
    public static Guid Azerbaijan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe115");
    public static Guid Bahamas { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe116");
    public static Guid Bahrain { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe117");
    public static Guid Bangladesh { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe118");
    public static Guid Barbados { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe119");
    public static Guid Belarus { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe120");
    public static Guid Belgium { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe121");
    public static Guid Belize { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe122");
    public static Guid Benin { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe123");
    public static Guid Bermuda { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe124");
    public static Guid Bhutan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe125");
    public static Guid Bolivia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe126");
    public static Guid BosniaandHerzegovina { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe127");
    public static Guid Botswana { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe128");
    public static Guid BouvetIsland { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe129");
    public static Guid Brazil { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe130");
    public static Guid BritishIndianOceanTerritory { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe131");
    public static Guid BruneiDarussalam { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe132");
    public static Guid Bulgaria { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe133");
    public static Guid BurkinaFaso { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe134");
    public static Guid Burundi { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe135");
    public static Guid Cambodia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe136");
    public static Guid Cameroon { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe137");
    public static Guid Canada { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe138");
    public static Guid CapeVerde { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe139");
    public static Guid CaymanIslands { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe140");
    public static Guid CentralAfricanRepublic { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe141");
    public static Guid Chad { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe142");
    public static Guid Chile { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe143");
    public static Guid China { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe144");
    public static Guid ChristmasIsland { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe145");
    public static Guid Colombia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe147");
    public static Guid Comoros { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe148");
    public static Guid Congo { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe149");
    public static Guid CookIslands { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe151");
    public static Guid CostaRica { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe152");
    public static Guid Croatia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe154");
    public static Guid Cuba { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe155");
    public static Guid Cyprus { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe156");
    public static Guid CzechRepublic { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe157");
    public static Guid Denmark { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe158");
    public static Guid Djibouti { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe159");
    public static Guid Dominica { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe160");
    public static Guid DominicanRepublic { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe161");
    public static Guid Ecuador { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe162");
    public static Guid Egypt { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe163");
    public static Guid ElSalvador { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe164");
    public static Guid EquatorialGuinea { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe165");
    public static Guid Eritrea { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe166");
    public static Guid Estonia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe167");
    public static Guid Ethiopia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe168");
    public static Guid FaroeIslands { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe170");
    public static Guid Fiji { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe171");
    public static Guid Finland { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe172");
    public static Guid France { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe173");
    public static Guid FrenchGuiana { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe174");
    public static Guid FrenchPolynesia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe175");
    public static Guid FrenchSouthernTerritories { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe176");
    public static Guid Gabon { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe177");
    public static Guid Gambia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe178");
    public static Guid Georgia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe179");
    public static Guid Germany { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe180");
    public static Guid Ghana { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe181");
    public static Guid Gibraltar { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe182");
    public static Guid Greece { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe183");
    public static Guid Greenland { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe184");
    public static Guid Grenada { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe185");
    public static Guid Guadeloupe { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe186");
    public static Guid Guam { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe187");
    public static Guid Guatemala { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe188");
    public static Guid Guernsey { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe189");
    public static Guid Guinea { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe190");
    public static Guid GuineaBissau { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe191");
    public static Guid Guyana { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe192");
    public static Guid Haiti { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe193");
    public static Guid HeardIslandandMcdonaldIslands { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe194");
    public static Guid Honduras { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe196");
    public static Guid HongKong { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe197");
    public static Guid Hungary { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe198");
    public static Guid Iceland { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe199");
    public static Guid India { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe200");
    public static Guid Indonesia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe201");
    public static Guid Iraq { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe203");
    public static Guid Ireland { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe204");
    public static Guid IsleofMan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe205");
    public static Guid Israel { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe206");
    public static Guid Italy { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe207");
    public static Guid Jamaica { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe208");
    public static Guid Japan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe209");
    public static Guid Jersey { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe210");
    public static Guid Jordan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe211");
    public static Guid Kazakhstan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe212");
    public static Guid Kenya { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe213");
    public static Guid Kiribati { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe214");
    public static Guid Kuwait { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe217");
    public static Guid Kyrgyzstan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe218");
    public static Guid Latvia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe220");
    public static Guid Lebanon { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe221");
    public static Guid Lesotho { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe222");
    public static Guid Liberia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe223");
    public static Guid LibyanArabJamahiriya { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe224");
    public static Guid Liechtenstein { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe225");
    public static Guid Lithuania { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe226");
    public static Guid Luxembourg { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe227");
    public static Guid Macao { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe228");
    public static Guid Madagascar { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe230");
    public static Guid Malawi { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe231");
    public static Guid Malaysia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe232");
    public static Guid Maldives { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe233");
    public static Guid Mali { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe234");
    public static Guid Malta { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe235");
    public static Guid MarshallIslands { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe236");
    public static Guid Martinique { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe237");
    public static Guid Mauritania { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe238");
    public static Guid Mauritius { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe239");
    public static Guid Mayotte { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe240");
    public static Guid Mexico { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe241");
    public static Guid Monaco { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe244");
    public static Guid Mongolia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe245");
    public static Guid Montserrat { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe246");
    public static Guid Morocco { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe247");
    public static Guid Mozambique { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe248");
    public static Guid Myanmar { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe249");
    public static Guid Namibia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe250");
    public static Guid Nauru { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe251");
    public static Guid Nepal { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe252");
    public static Guid Netherlands { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe253");
    public static Guid NetherlandsAntilles { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe254");
    public static Guid NewCaledonia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe255");
    public static Guid NewZealand { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe256");
    public static Guid Nicaragua { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe257");
    public static Guid Niger { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe258");
    public static Guid Nigeria { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe259");
    public static Guid Niue { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe260");
    public static Guid NorfolkIsland { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe261");
    public static Guid NorthernMarianaIslands { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe262");
    public static Guid Norway { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe263");
    public static Guid Oman { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe264");
    public static Guid Pakistan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe265");
    public static Guid Palau { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe266");
    public static Guid Panama { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe268");
    public static Guid PapuaNewGuinea { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe269");
    public static Guid Paraguay { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe270");
    public static Guid Peru { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe271");
    public static Guid Philippines { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe272");
    public static Guid Pitcairn { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe273");
    public static Guid Poland { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe274");
    public static Guid Portugal { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe275");
    public static Guid PuertoRico { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe276");
    public static Guid Qatar { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe277");
    public static Guid Reunion { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe278");
    public static Guid Romania { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe279");
    public static Guid RussianFederation { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe280");
    public static Guid RWANDA { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe281");
    public static Guid SaintHelena { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe282");
    public static Guid SaintKittsandNevis { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe283");
    public static Guid SaintLucia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe284");
    public static Guid SaintPierreandMiquelon { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe285");
    public static Guid SaintVincentandtheGrenadines { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe286");
    public static Guid Samoa { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe287");
    public static Guid SanMarino { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe288");
    public static Guid SaoTomeandPrincipe { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe289");
    public static Guid SaudiArabia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe290");
    public static Guid Senegal { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe291");
    public static Guid SerbiaandMontenegro { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe292");
    public static Guid Seychelles { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe293");
    public static Guid SierraLeone { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe294");
    public static Guid Singapore { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe295");
    public static Guid Slovakia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe296");
    public static Guid Slovenia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe297");
    public static Guid SolomonIslands { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe298");
    public static Guid Somalia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe299");
    public static Guid SouthAfrica { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe300");
    public static Guid SouthGeorgiaandtheSouthSandwichIslands { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe301");
    public static Guid Spain { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe302");
    public static Guid SriLanka { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe303");
    public static Guid Sudan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe304");
    public static Guid Suriname { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe305");
    public static Guid SvalbardandJanMayen { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe306");
    public static Guid Swaziland { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe307");
    public static Guid Sweden { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe308");
    public static Guid Switzerland { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe309");
    public static Guid SyrianArabRepublic { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe310");
    public static Guid Taiwan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe311");
    public static Guid Tajikistan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe312");
    public static Guid Tanzania { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe313");
    public static Guid Thailand { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe314");
    public static Guid TimorLeste { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe315");
    public static Guid Togo { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe316");
    public static Guid Tokelau { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe317");
    public static Guid Tonga { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe318");
    public static Guid TrinidadandTobago { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe319");
    public static Guid Tunisia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe320");
    public static Guid Turkey { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe321");
    public static Guid Turkmenistan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe322");
    public static Guid TurksandCaicosIslands { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe323");
    public static Guid Tuvalu { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe324");
    public static Guid Uganda { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe325");
    public static Guid Ukraine { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe326");
    public static Guid UnitedArabEmirates { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe327");
    public static Guid UnitedKingdom { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe328");
    public static Guid UnitedStates { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe329");
    public static Guid UnitedStatesMinorOutlyingIslands { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe330");
    public static Guid Uruguay { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe331");
    public static Guid Uzbekistan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe332");
    public static Guid Vanuatu { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe333");
    public static Guid Venezuela { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe334");
    public static Guid VietNam { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe335");
    public static Guid WallisandFutuna { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe338");
    public static Guid WesternSahara { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe339");
    public static Guid Yemen { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe340");
    public static Guid Zambia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe341");
    public static Guid Zimbabwe { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe342");
    public static Guid Macedonia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe229");
    public static Guid Micronesia { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe242");
    public static Guid Moldova { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe243");
    public static Guid Korea { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe343");


    public static WorldCountryEntity[] Entities { get; } = new[]
    {
        new WorldCountryEntity(Korea, WorldCountryTypeId.KR, "Korea"),

        new WorldCountryEntity(Afghanistan, WorldCountryTypeId.AF, "Afghanistan"),
        new WorldCountryEntity(AlandIslands, WorldCountryTypeId.AX, "Aland Islands"),
        new WorldCountryEntity(Albania, WorldCountryTypeId.AL, "Albania"),
        new WorldCountryEntity(Algeria, WorldCountryTypeId.DZ, "Algeria"),
        new WorldCountryEntity(AmericanSamoa, WorldCountryTypeId.AS, "American Samoa"),
        new WorldCountryEntity(AndorrA, WorldCountryTypeId.AD, "AndorrA"),
        new WorldCountryEntity(Angola, WorldCountryTypeId.AO, "Angola"),
        new WorldCountryEntity(Anguilla, WorldCountryTypeId.AI, "Anguilla"),
        new WorldCountryEntity(Antarctica, WorldCountryTypeId.AQ, "Antarctica"),
        new WorldCountryEntity(AntiguaandBarbuda, WorldCountryTypeId.AG, "Antigua and Barbuda"),
        new WorldCountryEntity(Argentina, WorldCountryTypeId.AR, "Argentina"),
        new WorldCountryEntity(Armenia, WorldCountryTypeId.AM, "Armenia"),
        new WorldCountryEntity(Aruba, WorldCountryTypeId.AW, "Aruba"),
        new WorldCountryEntity(Australia, WorldCountryTypeId.AU, "Australia"),
        new WorldCountryEntity(Austria, WorldCountryTypeId.AT, "Austria"),
        new WorldCountryEntity(Azerbaijan, WorldCountryTypeId.AZ, "Azerbaijan"),
        new WorldCountryEntity(Bahamas, WorldCountryTypeId.BS, "Bahamas"),
        new WorldCountryEntity(Bahrain, WorldCountryTypeId.BH, "Bahrain"),
        new WorldCountryEntity(Bangladesh, WorldCountryTypeId.BD, "Bangladesh"),
        new WorldCountryEntity(Barbados, WorldCountryTypeId.BB, "Barbados"),
        new WorldCountryEntity(Belarus, WorldCountryTypeId.BY, "Belarus"),
        new WorldCountryEntity(Belgium, WorldCountryTypeId.BE, "Belgium"),
        new WorldCountryEntity(Belize, WorldCountryTypeId.BZ, "Belize"),
        new WorldCountryEntity(Benin, WorldCountryTypeId.BJ, "Benin"),
        new WorldCountryEntity(Bermuda, WorldCountryTypeId.BM, "Bermuda"),
        new WorldCountryEntity(Bhutan, WorldCountryTypeId.BT, "Bhutan"),
        new WorldCountryEntity(Bolivia, WorldCountryTypeId.BO, "Bolivia"),
        new WorldCountryEntity(BosniaandHerzegovina, WorldCountryTypeId.BA, "Bosnia and Herzegovina"),
        new WorldCountryEntity(Botswana, WorldCountryTypeId.BW, "Botswana"),
        new WorldCountryEntity(BouvetIsland, WorldCountryTypeId.BV, "Bouvet Island"),
        new WorldCountryEntity(Brazil, WorldCountryTypeId.BR, "Brazil"),
        new WorldCountryEntity(BritishIndianOceanTerritory, WorldCountryTypeId.IO, "British Indian Ocean Territory"),
        new WorldCountryEntity(BruneiDarussalam, WorldCountryTypeId.BN, "Brunei Darussalam"),
        new WorldCountryEntity(Bulgaria, WorldCountryTypeId.BG, "Bulgaria"),
        new WorldCountryEntity(BurkinaFaso, WorldCountryTypeId.BF, "Burkina Faso"),
        new WorldCountryEntity(Burundi, WorldCountryTypeId.BI, "Burundi"),
        new WorldCountryEntity(Cambodia, WorldCountryTypeId.KH, "Cambodia"),
        new WorldCountryEntity(Cameroon, WorldCountryTypeId.CM, "Cameroon"),
        new WorldCountryEntity(Canada, WorldCountryTypeId.CA, "Canada"),
        new WorldCountryEntity(CapeVerde, WorldCountryTypeId.CV, "Cape Verde"),
        new WorldCountryEntity(CaymanIslands, WorldCountryTypeId.KY, "Cayman Islands"),
        new WorldCountryEntity(CentralAfricanRepublic, WorldCountryTypeId.CF, "Central African Republic"),
        new WorldCountryEntity(Chad, WorldCountryTypeId.TD, "Chad"),
        new WorldCountryEntity(Chile, WorldCountryTypeId.CL, "Chile"),
        new WorldCountryEntity(China, WorldCountryTypeId.CN, "China"),
        new WorldCountryEntity(ChristmasIsland, WorldCountryTypeId.CX, "Christmas Island"),
        new WorldCountryEntity(Colombia, WorldCountryTypeId.CO, "Colombia"),
        new WorldCountryEntity(Comoros, WorldCountryTypeId.KM, "Comoros"),
        new WorldCountryEntity(Congo, WorldCountryTypeId.CG, "Congo"),
        new WorldCountryEntity(CookIslands, WorldCountryTypeId.CK, "Cook Islands"),
        new WorldCountryEntity(CostaRica, WorldCountryTypeId.CR, "Costa Rica"),
        new WorldCountryEntity(Croatia, WorldCountryTypeId.HR, "Croatia"),
        new WorldCountryEntity(Cuba, WorldCountryTypeId.CU, "Cuba"),
        new WorldCountryEntity(Cyprus, WorldCountryTypeId.CY, "Cyprus"),
        new WorldCountryEntity(CzechRepublic, WorldCountryTypeId.CZ, "Czech Republic"),
        new WorldCountryEntity(Denmark, WorldCountryTypeId.DK, "Denmark"),
        new WorldCountryEntity(Djibouti, WorldCountryTypeId.DJ, "Djibouti"),
        new WorldCountryEntity(Dominica, WorldCountryTypeId.DM, "Dominica"),
        new WorldCountryEntity(DominicanRepublic, WorldCountryTypeId.DO, "Dominican Republic"),
        new WorldCountryEntity(Ecuador, WorldCountryTypeId.EC, "Ecuador"),
        new WorldCountryEntity(Egypt, WorldCountryTypeId.EG, "Egypt"),
        new WorldCountryEntity(ElSalvador, WorldCountryTypeId.SV, "El Salvador"),
        new WorldCountryEntity(EquatorialGuinea, WorldCountryTypeId.GQ, "Equatorial Guinea"),
        new WorldCountryEntity(Eritrea, WorldCountryTypeId.ER, "Eritrea"),
        new WorldCountryEntity(Estonia, WorldCountryTypeId.EE, "Estonia"),
        new WorldCountryEntity(Ethiopia, WorldCountryTypeId.ET, "Ethiopia"),
        new WorldCountryEntity(FaroeIslands, WorldCountryTypeId.FO, "Faroe Islands"),
        new WorldCountryEntity(Fiji, WorldCountryTypeId.FJ, "Fiji"),
        new WorldCountryEntity(Finland, WorldCountryTypeId.FI, "Finland"),
        new WorldCountryEntity(France, WorldCountryTypeId.FR, "France"),
        new WorldCountryEntity(FrenchGuiana, WorldCountryTypeId.GF, "French Guiana"),
        new WorldCountryEntity(FrenchPolynesia, WorldCountryTypeId.PF, "French Polynesia"),
        new WorldCountryEntity(FrenchSouthernTerritories, WorldCountryTypeId.TF, "French Southern Territories"),
        new WorldCountryEntity(Gabon, WorldCountryTypeId.GA, "Gabon"),
        new WorldCountryEntity(Gambia, WorldCountryTypeId.GM, "Gambia"),
        new WorldCountryEntity(Georgia, WorldCountryTypeId.GE, "Georgia"),
        new WorldCountryEntity(Germany, WorldCountryTypeId.DE, "Germany"),
        new WorldCountryEntity(Ghana, WorldCountryTypeId.GH, "Ghana"),
        new WorldCountryEntity(Gibraltar, WorldCountryTypeId.GI, "Gibraltar"),
        new WorldCountryEntity(Greece, WorldCountryTypeId.GR, "Greece"),
        new WorldCountryEntity(Greenland, WorldCountryTypeId.GL, "Greenland"),
        new WorldCountryEntity(Grenada, WorldCountryTypeId.GD, "Grenada"),
        new WorldCountryEntity(Guadeloupe, WorldCountryTypeId.GP, "Guadeloupe"),
        new WorldCountryEntity(Guam, WorldCountryTypeId.GU, "Guam"),
        new WorldCountryEntity(Guatemala, WorldCountryTypeId.GT, "Guatemala"),
        new WorldCountryEntity(Guernsey, WorldCountryTypeId.GG, "Guernsey"),
        new WorldCountryEntity(Guinea, WorldCountryTypeId.GN, "Guinea"),
        new WorldCountryEntity(GuineaBissau, WorldCountryTypeId.GW, "Guinea-Bissau"),
        new WorldCountryEntity(Guyana, WorldCountryTypeId.GY, "Guyana"),
        new WorldCountryEntity(Haiti, WorldCountryTypeId.HT, "Haiti"),
        new WorldCountryEntity(HeardIslandandMcdonaldIslands, WorldCountryTypeId.HM, "Heard Island and Mcdonald Islands"),
        new WorldCountryEntity(Honduras, WorldCountryTypeId.HN, "Honduras"),
        new WorldCountryEntity(HongKong, WorldCountryTypeId.HK, "Hong Kong"),
        new WorldCountryEntity(Hungary, WorldCountryTypeId.HU, "Hungary"),
        new WorldCountryEntity(Iceland, WorldCountryTypeId.IS, "Iceland"),
        new WorldCountryEntity(India, WorldCountryTypeId.IN, "India"),
        new WorldCountryEntity(Indonesia, WorldCountryTypeId.ID, "Indonesia"),
        new WorldCountryEntity(Iraq, WorldCountryTypeId.IQ, "Iraq"),
        new WorldCountryEntity(Ireland, WorldCountryTypeId.IE, "Ireland"),
        new WorldCountryEntity(IsleofMan, WorldCountryTypeId.IM, "Isle of Man"),
        new WorldCountryEntity(Israel, WorldCountryTypeId.IL, "Israel"),
        new WorldCountryEntity(Italy, WorldCountryTypeId.IT, "Italy"),
        new WorldCountryEntity(Jamaica, WorldCountryTypeId.JM, "Jamaica"),
        new WorldCountryEntity(Japan, WorldCountryTypeId.JP, "Japan"),
        new WorldCountryEntity(Jersey, WorldCountryTypeId.JE, "Jersey"),
        new WorldCountryEntity(Jordan, WorldCountryTypeId.JO, "Jordan"),
        new WorldCountryEntity(Kazakhstan, WorldCountryTypeId.KZ, "Kazakhstan"),
        new WorldCountryEntity(Kenya, WorldCountryTypeId.KE, "Kenya"),
        new WorldCountryEntity(Kiribati, WorldCountryTypeId.KI, "Kiribati"),
        new WorldCountryEntity(Kuwait, WorldCountryTypeId.KW, "Kuwait"),
        new WorldCountryEntity(Kyrgyzstan, WorldCountryTypeId.KG, "Kyrgyzstan"),
        new WorldCountryEntity(Latvia, WorldCountryTypeId.LV, "Latvia"),
        new WorldCountryEntity(Lebanon, WorldCountryTypeId.LB, "Lebanon"),
        new WorldCountryEntity(Lesotho, WorldCountryTypeId.LS, "Lesotho"),
        new WorldCountryEntity(Liberia, WorldCountryTypeId.LR, "Liberia"),
        new WorldCountryEntity(LibyanArabJamahiriya, WorldCountryTypeId.LY, "Libyan Arab Jamahiriya"),
        new WorldCountryEntity(Liechtenstein, WorldCountryTypeId.LI, "Liechtenstein"),
        new WorldCountryEntity(Lithuania, WorldCountryTypeId.LT, "Lithuania"),
        new WorldCountryEntity(Luxembourg, WorldCountryTypeId.LU, "Luxembourg"),
        new WorldCountryEntity(Macao, WorldCountryTypeId.MO, "Macao"),
        new WorldCountryEntity(Macedonia, WorldCountryTypeId.MK, "Macedonia, The Former Yugoslav Republic of"),
        new WorldCountryEntity(Madagascar, WorldCountryTypeId.MG, "Madagascar"),
        new WorldCountryEntity(Malawi, WorldCountryTypeId.MW, "Malawi"),
        new WorldCountryEntity(Malaysia, WorldCountryTypeId.MY, "Malaysia"),
        new WorldCountryEntity(Maldives, WorldCountryTypeId.MV, "Maldives"),
        new WorldCountryEntity(Mali, WorldCountryTypeId.ML, "Mali"),
        new WorldCountryEntity(Malta, WorldCountryTypeId.MT, "Malta"),
        new WorldCountryEntity(MarshallIslands, WorldCountryTypeId.MH, "Marshall Islands"),
        new WorldCountryEntity(Martinique, WorldCountryTypeId.MQ, "Martinique"),
        new WorldCountryEntity(Mauritania, WorldCountryTypeId.MR, "Mauritania"),
        new WorldCountryEntity(Mauritius, WorldCountryTypeId.MU, "Mauritius"),
        new WorldCountryEntity(Mayotte, WorldCountryTypeId.YT, "Mayotte"),
        new WorldCountryEntity(Mexico, WorldCountryTypeId.MX, "Mexico"),
        new WorldCountryEntity(Micronesia, WorldCountryTypeId.FM, "Micronesia, Federated States of"),
        new WorldCountryEntity(Moldova, WorldCountryTypeId.MD, "Moldova, Republic of"),
        new WorldCountryEntity(Monaco, WorldCountryTypeId.MC, "Monaco"),
        new WorldCountryEntity(Mongolia, WorldCountryTypeId.MN, "Mongolia"),
        new WorldCountryEntity(Montserrat, WorldCountryTypeId.MS, "Montserrat"),
        new WorldCountryEntity(Morocco, WorldCountryTypeId.MA, "Morocco"),
        new WorldCountryEntity(Mozambique, WorldCountryTypeId.MZ, "Mozambique"),
        new WorldCountryEntity(Myanmar, WorldCountryTypeId.MM, "Myanmar"),
        new WorldCountryEntity(Namibia, WorldCountryTypeId.NA, "Namibia"),
        new WorldCountryEntity(Nauru, WorldCountryTypeId.NR, "Nauru"),
        new WorldCountryEntity(Nepal, WorldCountryTypeId.NP, "Nepal"),
        new WorldCountryEntity(Netherlands, WorldCountryTypeId.NL, "Netherlands"),
        new WorldCountryEntity(NetherlandsAntilles, WorldCountryTypeId.AN, "Netherlands Antilles"),
        new WorldCountryEntity(NewCaledonia, WorldCountryTypeId.NC, "New Caledonia"),
        new WorldCountryEntity(NewZealand, WorldCountryTypeId.NZ, "New Zealand"),
        new WorldCountryEntity(Nicaragua, WorldCountryTypeId.NI, "Nicaragua"),
        new WorldCountryEntity(Niger, WorldCountryTypeId.NE, "Niger"),
        new WorldCountryEntity(Nigeria, WorldCountryTypeId.NG, "Nigeria"),
        new WorldCountryEntity(Niue, WorldCountryTypeId.NU, "Niue"),
        new WorldCountryEntity(NorfolkIsland, WorldCountryTypeId.NF, "Norfolk Island"),
        new WorldCountryEntity(NorthernMarianaIslands, WorldCountryTypeId.MP, "Northern Mariana Islands"),
        new WorldCountryEntity(Norway, WorldCountryTypeId.NO, "Norway"),
        new WorldCountryEntity(Oman, WorldCountryTypeId.OM, "Oman"),
        new WorldCountryEntity(Pakistan, WorldCountryTypeId.PK, "Pakistan"),
        new WorldCountryEntity(Palau, WorldCountryTypeId.PW, "Palau"),
        new WorldCountryEntity(Panama, WorldCountryTypeId.PA, "Panama"),
        new WorldCountryEntity(PapuaNewGuinea, WorldCountryTypeId.PG, "Papua New Guinea"),
        new WorldCountryEntity(Paraguay, WorldCountryTypeId.PY, "Paraguay"),
        new WorldCountryEntity(Peru, WorldCountryTypeId.PE, "Peru"),
        new WorldCountryEntity(Philippines, WorldCountryTypeId.PH, "Philippines"),
        new WorldCountryEntity(Pitcairn, WorldCountryTypeId.PN, "Pitcairn"),
        new WorldCountryEntity(Poland, WorldCountryTypeId.PL, "Poland"),
        new WorldCountryEntity(Portugal, WorldCountryTypeId.PT, "Portugal"),
        new WorldCountryEntity(PuertoRico, WorldCountryTypeId.PR, "Puerto Rico"),
        new WorldCountryEntity(Qatar, WorldCountryTypeId.QA, "Qatar"),
        new WorldCountryEntity(Reunion, WorldCountryTypeId.RE, "Reunion"),
        new WorldCountryEntity(Romania, WorldCountryTypeId.RO, "Romania"),
        new WorldCountryEntity(RussianFederation, WorldCountryTypeId.RU, "Russian Federation"),
        new WorldCountryEntity(RWANDA, WorldCountryTypeId.RW, "RWANDA"),
        new WorldCountryEntity(SaintHelena, WorldCountryTypeId.SH, "Saint Helena"),
        new WorldCountryEntity(SaintKittsandNevis, WorldCountryTypeId.KN, "Saint Kitts and Nevis"),
        new WorldCountryEntity(SaintLucia, WorldCountryTypeId.LC, "Saint Lucia"),
        new WorldCountryEntity(SaintPierreandMiquelon, WorldCountryTypeId.PM, "Saint Pierre and Miquelon"),
        new WorldCountryEntity(SaintVincentandtheGrenadines, WorldCountryTypeId.VC, "Saint Vincent and the Grenadines"),
        new WorldCountryEntity(Samoa, WorldCountryTypeId.WS, "Samoa"),
        new WorldCountryEntity(SanMarino, WorldCountryTypeId.SM, "San Marino"),
        new WorldCountryEntity(SaoTomeandPrincipe, WorldCountryTypeId.ST, "Sao Tome and Principe"),
        new WorldCountryEntity(SaudiArabia, WorldCountryTypeId.SA, "Saudi Arabia"),
        new WorldCountryEntity(Senegal, WorldCountryTypeId.SN, "Senegal"),
        new WorldCountryEntity(SerbiaandMontenegro, WorldCountryTypeId.CS, "Serbia and Montenegro"),
        new WorldCountryEntity(Seychelles, WorldCountryTypeId.SC, "Seychelles"),
        new WorldCountryEntity(SierraLeone, WorldCountryTypeId.SL, "Sierra Leone"),
        new WorldCountryEntity(Singapore, WorldCountryTypeId.SG, "Singapore"),
        new WorldCountryEntity(Slovakia, WorldCountryTypeId.SK, "Slovakia"),
        new WorldCountryEntity(Slovenia, WorldCountryTypeId.SI, "Slovenia"),
        new WorldCountryEntity(SolomonIslands, WorldCountryTypeId.SB, "Solomon Islands"),
        new WorldCountryEntity(Somalia, WorldCountryTypeId.SO, "Somalia"),
        new WorldCountryEntity(SouthAfrica, WorldCountryTypeId.ZA, "South Africa"),
        new WorldCountryEntity(SouthGeorgiaandtheSouthSandwichIslands, WorldCountryTypeId.GS, "South Georgia and the South Sandwich Islands"),
        new WorldCountryEntity(Spain, WorldCountryTypeId.ES, "Spain"),
        new WorldCountryEntity(SriLanka, WorldCountryTypeId.LK, "Sri Lanka"),
        new WorldCountryEntity(Sudan, WorldCountryTypeId.SD, "Sudan"),
        new WorldCountryEntity(Suriname, WorldCountryTypeId.SR, "Suriname"),
        new WorldCountryEntity(SvalbardandJanMayen, WorldCountryTypeId.SJ, "Svalbard and Jan Mayen"),
        new WorldCountryEntity(Swaziland, WorldCountryTypeId.SZ, "Swaziland"),
        new WorldCountryEntity(Sweden, WorldCountryTypeId.SE, "Sweden"),
        new WorldCountryEntity(Switzerland, WorldCountryTypeId.CH, "Switzerland"),
        new WorldCountryEntity(SyrianArabRepublic, WorldCountryTypeId.SY, "Syrian Arab Republic"),
        new WorldCountryEntity(Taiwan, WorldCountryTypeId.TW, "Taiwan, Province of China"),
        new WorldCountryEntity(Tajikistan, WorldCountryTypeId.TJ, "Tajikistan"),
        new WorldCountryEntity(Tanzania, WorldCountryTypeId.TZ, "Tanzania, United Republic of"),
        new WorldCountryEntity(Thailand, WorldCountryTypeId.TH, "Thailand"),
        new WorldCountryEntity(TimorLeste, WorldCountryTypeId.TL, "Timor-Leste"),
        new WorldCountryEntity(Togo, WorldCountryTypeId.TG, "Togo"),
        new WorldCountryEntity(Tokelau, WorldCountryTypeId.TK, "Tokelau"),
        new WorldCountryEntity(Tonga, WorldCountryTypeId.TO, "Tonga"),
        new WorldCountryEntity(TrinidadandTobago, WorldCountryTypeId.TT, "Trinidad and Tobago"),
        new WorldCountryEntity(Tunisia, WorldCountryTypeId.TN, "Tunisia"),
        new WorldCountryEntity(Turkey, WorldCountryTypeId.TR, "Turkey"),
        new WorldCountryEntity(Turkmenistan, WorldCountryTypeId.TM, "Turkmenistan"),
        new WorldCountryEntity(TurksandCaicosIslands, WorldCountryTypeId.TC, "Turks and Caicos Islands"),
        new WorldCountryEntity(Tuvalu, WorldCountryTypeId.TV, "Tuvalu"),
        new WorldCountryEntity(Uganda, WorldCountryTypeId.UG, "Uganda"),
        new WorldCountryEntity(Ukraine, WorldCountryTypeId.UA, "Ukraine"),
        new WorldCountryEntity(UnitedArabEmirates, WorldCountryTypeId.AE, "United Arab Emirates"),
        new WorldCountryEntity(UnitedKingdom, WorldCountryTypeId.GB, "United Kingdom"),
        new WorldCountryEntity(UnitedStates, WorldCountryTypeId.US, "United States"),
        new WorldCountryEntity(UnitedStatesMinorOutlyingIslands, WorldCountryTypeId.UM, "United States Minor Outlying Islands"),
        new WorldCountryEntity(Uruguay, WorldCountryTypeId.UY, "Uruguay"),
        new WorldCountryEntity(Uzbekistan, WorldCountryTypeId.UZ, "Uzbekistan"),
        new WorldCountryEntity(Vanuatu, WorldCountryTypeId.VU, "Vanuatu"),
        new WorldCountryEntity(Venezuela, WorldCountryTypeId.VE, "Venezuela"),
        new WorldCountryEntity(VietNam, WorldCountryTypeId.VN, "Viet Nam"),
        new WorldCountryEntity(WallisandFutuna, WorldCountryTypeId.WF, "Wallis and Futuna"),
        new WorldCountryEntity(WesternSahara, WorldCountryTypeId.EH, "Western Sahara"),
        new WorldCountryEntity(Yemen, WorldCountryTypeId.YE, "Yemen"),
        new WorldCountryEntity(Zambia, WorldCountryTypeId.ZM, "Zambia"),
        new WorldCountryEntity(Zimbabwe, WorldCountryTypeId.ZW, "Zimbabwe"),
    };
}