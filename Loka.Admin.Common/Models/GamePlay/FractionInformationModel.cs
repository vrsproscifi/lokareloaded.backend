﻿using System;
using Loka.Game.Database.Enums.Store;
using Loka.Store.Interfaces.Enums;

namespace Loka.Admin.Common.Models.GamePlay;

public sealed class FractionInformationModel
{
    public Guid Id { get; set; }
    public string Name { get; set; }= null!;
    public FractionTypeId TypeId { get; set; }

    public override string ToString()
    {
        return $"Id: {Id} | Name: {Name} | TypeId: {TypeId}";
    }
}