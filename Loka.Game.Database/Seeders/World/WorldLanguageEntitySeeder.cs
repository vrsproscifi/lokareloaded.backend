﻿using Loka.Game.Database.Entities.World;
using Loka.Game.Database.Enums.World;

namespace Loka.Game.Database.Seeders.World;

public static class WorldLanguageEntitySeeder
{
    public static Guid English { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe101");
    public static Guid Russian { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe102");
    public static Guid German { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe103");

    public static WorldLanguageEntity[] Entities { get; } = new[]
    {
        new WorldLanguageEntity(Russian, WorldLanguageTypeId.Russian, "ru"),
        new WorldLanguageEntity(English, WorldLanguageTypeId.English, "en"),
        new WorldLanguageEntity(German, WorldLanguageTypeId.German, "de"),
    };
}