﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Loka.Store.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class AddedBlueprintSeeders2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(5335), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(6177), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(6446), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(6695), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameItemBlueprintEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "ItemModelId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe916"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906") },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe926"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906") }
                });

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 844, DateTimeKind.Unspecified).AddTicks(9322), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(472), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(754), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(1011), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(1309), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(1581), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(1835), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(2074), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(2477), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(2893), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(9611), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 846, DateTimeKind.Unspecified).AddTicks(767), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 846, DateTimeKind.Unspecified).AddTicks(775), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 846, DateTimeKind.Unspecified).AddTicks(1112), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe202"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 846, DateTimeKind.Unspecified).AddTicks(1403), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe203"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 846, DateTimeKind.Unspecified).AddTicks(1646), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                columns: new[] { "EntityId", "Amount", "BlueprintId", "CreatedAt", "DeletedAt", "ResourceId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"), 1000, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe916"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe203") },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe911"), 50, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe916"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201") },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe912"), 50, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe916"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe202") }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe911"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe912"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemBlueprintEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe926"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemBlueprintEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe916"));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(1524), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(2408), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(2683), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(2936), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(5748), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(6948), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(7231), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(7492), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(7739), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(7979), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(8219), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(8451), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(8688), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(8923), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(5967), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(7114), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(7122), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(7507), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe202"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(7824), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe203"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(8085), new TimeSpan(0, 0, 0, 0, 0)));
        }
    }
}
