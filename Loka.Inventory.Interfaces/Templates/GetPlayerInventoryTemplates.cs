﻿namespace Loka.Inventory.Interfaces.Templates;

public static class GetPlayerInventoryTemplates
{
    public sealed class Request : IOperationRequest<Result>
    {
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public IReadOnlyList<PlayerInventoryTemplateModel> Presets { get; init; } = null!;
    }
}