﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Loka.Database.Common.Entities;
using Loka.Database.Common.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Loka.MatchMaking.DataBase.Entities.Server;

[Index(nameof(Name))]
[Index(nameof(MachineAddress), IsUnique = true)]
[Index(nameof(MachineName))]
public sealed class ClusterEntity : BaseEntity, INamedEntity
{
    [Required]
    [MaxLength(128)]
    public string Name { get; set; } = null!;
        
    [Required]
    [MaxLength(128)]
    public string MachineAddress { get; set; } = null!;

    [Required]
    [MaxLength(128)]
    public string MachineName { get; set; } = null!;

    public int LimitActiveNodes { get; set; }

    public DateTimeOffset LastActivityDate { get; set; }

    public List<ClusterRegionEntity>? Regions { get; set; }
}