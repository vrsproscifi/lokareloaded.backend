﻿using Loka.Game.Interfaces.Players;
using Loka.Game.Interfaces.Players.Dtos;
using Loka.Game.Interfaces.Players.Enums;
using Loka.Identity.Database;
using Loka.Identity.Database.Entities.Player;
using Loka.Identity.Grains.Storages.Sessions;
using Microsoft.EntityFrameworkCore;

namespace Loka.Game.Services.Players;

internal sealed class PlayersService : IPlayersService
{
    private GameIdentityDbContext IdentityDbContext { get; }
    private IPlayerSessionStorage SessionStorage { get; }

    public PlayersService
    (
        GameIdentityDbContext identityDbContext,
        IPlayerSessionStorage sessionStorage
    )
    {
        IdentityDbContext = identityDbContext;
        SessionStorage = sessionStorage;
    }

    private async Task<IReadOnlyList<PlayerModel>> SearchPlayers(Func<IQueryable<PlayerAccountEntity>, IQueryable<PlayerAccountEntity>> filter)
    {
        var query = filter(IdentityDbContext.PlayerAccountEntity.AsQueryable());

        var players = await query
            .Select(player => new PlayerModel()
            {
                Id = player.EntityId,
                Name = player.PlayerName,
                Status = PlayerGameStatus.Offline,
                LastActivityDate = player.LastActivityDate,
            }).ToArrayAsync<PlayerModel>();

        foreach (var player in players)
        {
            var hasActiveSession = await SessionStorage.HasActive(player.Id);
            player.Status = hasActiveSession ? PlayerGameStatus.Online : PlayerGameStatus.Offline;
        }

        return players;
    }

    public async Task<PlayerModel?> GetById(Guid playerId)
    {
        var players = await SearchPlayers(q => q.Where(player => player.EntityId == playerId).Take(1));
        return players.FirstOrDefault();
    }

    public Task<IReadOnlyList<PlayerModel>> GetByIds(IReadOnlyList<Guid> playerIds)
    {
        return SearchPlayers(f => f.Where(player => playerIds.Contains(player.EntityId)));
    }

    public Task<IReadOnlyList<PlayerModel>> GetByIdsAndName(IReadOnlyList<Guid> playerIds, string name)
    {
        return SearchPlayers(f => f.Where(player => playerIds.Contains(player.EntityId) && player.PlayerName.Contains(name)));
    }

    public Task<IReadOnlyList<PlayerModel>> SearchByName(string name)
    {
        return SearchPlayers(f => f.Where(player => player.PlayerName.Contains(name)));
    }
}