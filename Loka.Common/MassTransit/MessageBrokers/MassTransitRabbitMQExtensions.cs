﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Common.MassTransit.MessageBrokers;

public static class MassTransitRabbitMQExtensions
{
    public static ServiceCollectionInitializer AddMassTransitRabbitMQ
    (
        this ServiceCollectionInitializer initializer,
        Action<IBusRegistrationContext, IRabbitMqBusFactoryConfigurator>? configureRabbitMQ = default
    )
    {
        initializer.Services.AddScoped(typeof(ClusterConsumerServices<>));
        
        initializer.Services.AddMassTransit(x =>
        {
            x.AddConsumers(initializer.Assemblies);

            x.SetKebabCaseEndpointNameFormatter();

            x.UsingRabbitMq((context, cfg) =>
            {
                var config = initializer.Configuration
                    .GetRequiredSection("Application.DataBase")
                    .GetRequiredSection("MessageBroker")
                    .Get<MassTransitRabbitMQConfig>()!;

                cfg.UseDelayedMessageScheduler();
                configureRabbitMQ?.Invoke(context, cfg);

                cfg.Host(config.Host, "/", h =>
                {
                    h.Username(config.UserName);
                    h.Password(config.Password);
                    h.RequestedChannelMax(128);
                });

                cfg.ConfigureEndpoints(context);
            });
        });

        return initializer;
    }
}