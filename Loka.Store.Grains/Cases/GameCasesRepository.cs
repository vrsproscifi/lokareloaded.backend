﻿using AutoMapper;
using Loka.Common.Cache.DistributedCache;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Repositories;
using Loka.Store.DataBase;
using Loka.Store.DataBase.Entities.Cases;
using Loka.Store.Interfaces.Models.Cases;
using Loka.Store.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Loka.Store.Grains.Cases;

internal sealed class GameCasesRepository : BaseRepository<StoreDbContext, CaseEntity, StoreCaseModel>, IGameCasesRepository
{
    private GameCaseResourcesRepository CaseResourcesRepository { get; }
    private GameCaseItemsRepository CaseItemsRepository { get; }
    private IGameResourcesRepository GameResourcesRepository { get; }

    public GameCasesRepository
    (
        ICacheProvider cacheProvider,
        StoreDbContext dbContext,
        IMapper mapper,
        GameCaseResourcesRepository caseResourcesRepository,
        GameCaseItemsRepository caseItemsRepository, IGameResourcesRepository gameResourcesRepository) : base(cacheProvider, dbContext, mapper)
    {
        CaseResourcesRepository = caseResourcesRepository;
        CaseItemsRepository = caseItemsRepository;
        GameResourcesRepository = gameResourcesRepository;
    }

    protected override async Task<OperationResult> AfterMap(CaseEntity entity, StoreCaseModel model)
    {
        if (entity.SellCurrencyId.HasValue)
        {
            var getResourceResult = await GameResourcesRepository.GetById(entity.SellCurrencyId.Value);
            if (!getResourceResult.GetValueIfSucceeded(out var resource))
                return getResourceResult.Error!;

            model.SellCurrency = resource;
        }
        
        var resourceIds = await DbContext.CaseResourceEntity
            .Where(q => q.CaseId == entity.EntityId)
            .Select(q => q.EntityId)
            .ToArrayAsync();

        var itemIds = await DbContext.CaseItemEntity
            .Where(q => q.CaseId == entity.EntityId)
            .Select(q => q.EntityId)
            .ToArrayAsync();

        model.Resources = await CaseResourcesRepository.GetByIds(resourceIds);
        model.Items = await CaseItemsRepository.GetByIds(itemIds);

        return await base.AfterMap(entity, model);
    }
}