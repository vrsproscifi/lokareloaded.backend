﻿namespace Loka.Deployment.DataBase.Enums;

public enum DeployState : byte
{
    None,

    Created,
        
    Published,
    Publishing,
        
    Updated,
    Updating,
        
    Deleted,
    Obsolete
}