﻿using Orleans;

namespace Loka.MatchMaking.Interfaces;

public interface IMatchMakingProcessorGrain : IGrainWithGuidKey
{
    Task Process();
}