﻿using Loka.Api.Features;

namespace Loka.Api.IntegrationTests.Asserts;

public static class GameChatAsserts
{
    public static void AssertResponse(this AssignRoleForChannelMember.Types.Response response)
    {
        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.That(response.Member, Is.Not.Null, $"Invalid {nameof(response.Member)}");
        Assert.That(response.Channel, Is.Not.Null, $"Invalid {nameof(response.Channel)}");

        response.Member.AssertModel();
        response.Channel.AssertModel();
    }

    public static void AssertResponse(this GetChatMember.Types.Response response)
    {
        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.That(response.Member, Is.Not.Null, $"Invalid {nameof(response.Member)}");
        response.Member.AssertModel();
    }

    public static void AssertResponse(this GetChatMembers.Types.Response response)
    {
        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.That(response.Entries, Is.Not.Null, $"Invalid {nameof(response.Entries)}");
        foreach (var member in response.Entries)
        {
            member.AssertModel();
        }
    }

    public static void AssertResponse(this CreatePrivateChannel.Types.Response response)
    {
        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.That(response.Channel, Is.Not.Null, $"Invalid {nameof(response.Channel)}");
        Assert.Multiple(() =>
        {
            Assert.That(response.Channel.Id, Is.Not.Null, $"Invalid {nameof(response.Channel.Id)}");
            Assert.That(response.Channel.TypeId, Is.EqualTo(ChatChanelType.Private), $"Invalid {nameof(response.Channel.TypeId)}");
        });
    }

    public static void AssertResponse(this SendChatMessage.Types.Response response)
    {
        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.That(response.Message, Is.Not.Null, $"Invalid {nameof(response.Message)}");
        Assert.Multiple(() =>
        {
            Assert.That(response.Message.Id, Is.Not.Null, $"Invalid {nameof(response.Message.Id)}");
            Assert.That(response.Message.CreatedAt, Is.Not.Null, $"Invalid {nameof(response.Message.CreatedAt)}");
            Assert.That(response.Message.Sender, Is.Not.Null, $"Invalid {nameof(response.Message.Sender)}");
        });
    }

    public static void AssertResponse(this GetChatMessages.Types.Response response)
    {
        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.Multiple(() =>
        {
            Assert.That(response.Messages, Is.Not.Null, $"Invalid {nameof(response.Messages)}");
            Assert.That(response.Events, Is.Not.Null, $"Invalid {nameof(response.Events)}");
            Assert.That(response.EarlyThen, Is.Not.Null, $"Invalid {nameof(response.EarlyThen)}");
            Assert.That(response.LaterThen, Is.Not.Null, $"Invalid {nameof(response.LaterThen)}");
        });

        foreach (var message in response.Messages)
        {
            message.AssertModel();
        }

        foreach (var eventEntry in response.Events)
        {
            eventEntry.AssertModel();
        }
    }

    public static void AssertModel(this ChatMemberEntry entry)
    {
        Assert.That(entry, Is.Not.Null, $"Invalid {nameof(entry)}");
        Assert.Multiple(() =>
        {
            Assert.That(entry.PlayerId, Is.Not.Null, $"Invalid {nameof(entry)}.{nameof(entry.PlayerId)}");
            Assert.That(entry.PlayerName, Is.Not.Null, $"Invalid {nameof(entry)}.{nameof(entry.PlayerName)}");
        });
        entry.PlayerId.AssertModel();
    }

    public static void AssertEqualModel(this ChatMessageEntry actual, ChatMessageEntry expected)
    {
        Assert.Multiple(() =>
        {
            Assert.That(actual.Id, Is.EqualTo(expected.Id), $"Invalid {nameof(expected.Id)}");
            Assert.That(actual.Message, Is.EqualTo(expected.Message), $"Invalid {nameof(expected.Message)}");
            Assert.That(actual.CreatedAt, Is.EqualTo(expected.CreatedAt), $"Invalid {nameof(expected.CreatedAt)}");
            Assert.That(actual.Sender.PlayerId, Is.EqualTo(expected.Sender.PlayerId), $"Invalid {nameof(expected.Sender.PlayerId)}");
            Assert.That(actual.Sender.PlayerName, Is.EqualTo(expected.Sender.PlayerName), $"Invalid {nameof(expected.Sender.PlayerName)}");
        });
    }

    public static void AssertModel(this ChatMessageEntry entry)
    {
        Assert.That(entry, Is.Not.Null, $"Invalid {nameof(entry)}");
        Assert.Multiple(() =>
        {
            Assert.That(entry.Id, Is.Not.Null, $"Invalid {nameof(entry)}.{nameof(entry.Id)}");
            Assert.That(entry.Message, Is.Not.Null, $"Invalid {nameof(entry)}.{nameof(entry.Message)}");
            Assert.That(entry.Sender, Is.Not.Null, $"Invalid {nameof(entry)}.{nameof(entry.Sender)}");
            Assert.That(entry.CreatedAt, Is.Not.Null, $"Invalid {nameof(entry)}.{nameof(entry.CreatedAt)}");
        });
        entry.Id.AssertModel();
        entry.Sender.AssertModel();
    }

    public static void AssertModel(this ChatEventEntry entry)
    {
        Assert.That(entry, Is.Not.Null, $"Invalid {nameof(entry)}");
        Assert.Multiple(() =>
        {
            Assert.That(entry.Id, Is.Not.Null, $"Invalid {nameof(entry)}.{nameof(entry.Id)}");
            Assert.That(entry.Sender, Is.Not.Null, $"Invalid {nameof(entry)}.{nameof(entry.Sender)}");
            Assert.That(entry.CreatedAt, Is.Not.Null, $"Invalid {nameof(entry)}.{nameof(entry.CreatedAt)}");
        });
        entry.Id.AssertModel();
        entry.Sender.AssertModel();
    }

    public static void AssertModel(this ChannelEntry entry)
    {
        Assert.That(entry, Is.Not.Null, $"Invalid {nameof(entry)}");
        Assert.That(entry.Id, Is.Not.Null, $"Invalid {nameof(entry)}.{nameof(entry.Id)}");
        entry.Id.AssertModel();
    }
}