﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Loka.Common.Entry;
using Loka.Common.Extensions;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;

namespace Loka.Common.Operations.Mapping;

public class OperationMappingProfile<TEntity, TModel, TProperty>
    where TProperty : IEntry
    where TModel : IEntry
{
    public static IOperationMappingProfile<TEntity, TModel> Create
    (
        Func<TEntity, Guid> modelPropertyIdSelector,
        Action<TModel, TProperty> modelPropertyMap,
        Func<IReadOnlyList<Guid>, TProperty[]> entriesSource
    )
    {
        return new SyncImpl(modelPropertyIdSelector, modelPropertyMap, entriesSource);
    }

    public static IOperationMappingProfile<TEntity, TModel> Create
    (
        Func<TEntity, Guid> modelPropertyIdSelector,
        Action<TModel, TProperty> modelPropertyMap,
        Func<IReadOnlyList<Guid>, CancellationToken, Task<OperationResult<TProperty[]>>> entriesSource
    )
    {
        return new AsyncImpl(modelPropertyIdSelector, modelPropertyMap, entriesSource);
    }

    private sealed class SyncImpl : IOperationMappingProfile<TEntity, TModel>
    {
        private Action<TModel, TProperty> ModelPropertyMap { get; }
        private Func<TEntity, Guid> ModelPropertyIdSelector { get; }
        private Func<IReadOnlyList<Guid>, IReadOnlyList<TProperty>> EntriesSource { get; }

        public SyncImpl
        (
            Func<TEntity, Guid> modelPropertyIdSelector,
            Action<TModel, TProperty> modelPropertyMap,
            Func<IReadOnlyList<Guid>, IReadOnlyList<TProperty>> entriesSource
        )
        {
            ModelPropertyIdSelector = modelPropertyIdSelector;
            EntriesSource = entriesSource;
            ModelPropertyMap = modelPropertyMap;
        }

        public Task<OperationResult> Map(IReadOnlyList<TEntity> entities, IReadOnlyList<TModel> models, CancellationToken cancellationToken)
        {
            var propertyIds = entities
                .Select(ModelPropertyIdSelector)
                .ToArray();

            var propertyModels = EntriesSource(propertyIds);
            foreach (var entity in entities.WithIndex())
            {
                var propertyModel = propertyModels.First(entry => entry.Id == propertyIds[entity.index]);
                ModelPropertyMap(models[entity.index], propertyModel);
            }

            return Task.FromResult(new OperationResult(OperationResultCode.Successfully));
        }
    }

    private sealed class AsyncImpl : IOperationMappingProfile<TEntity, TModel>
    {
        private Action<TModel, TProperty> ModelPropertyMap { get; }
        private Func<TEntity, Guid> ModelPropertyIdSelector { get; }
        private Func<IReadOnlyList<Guid>, CancellationToken, Task<OperationResult<TProperty[]>>> EntriesSource { get; }

        public AsyncImpl
        (
            Func<TEntity, Guid> modelPropertyIdSelector,
            Action<TModel, TProperty> modelPropertyMap,
            Func<IReadOnlyList<Guid>, CancellationToken, Task<OperationResult<TProperty[]>>> entriesSource)
        {
            ModelPropertyIdSelector = modelPropertyIdSelector;
            EntriesSource = entriesSource;
            ModelPropertyMap = modelPropertyMap;
        }

        public async Task<OperationResult> Map(IReadOnlyList<TEntity> entities, IReadOnlyList<TModel> models, CancellationToken cancellationToken)
        {
            var propertyIds = entities
                .Select(ModelPropertyIdSelector)
                .ToArray();

            var propertyModels = await EntriesSource(propertyIds, cancellationToken);
            if (propertyModels.IsFailed)
                return propertyModels.Error;
                
            foreach (var entity in entities.WithIndex())
            {
                var propertyModel = propertyModels.Result!.First(entry => entry.Id == propertyIds[entity.index]);
                ModelPropertyMap(models[entity.index], propertyModel);
            }

            return OperationResultCode.Successfully;
        }
    }
}