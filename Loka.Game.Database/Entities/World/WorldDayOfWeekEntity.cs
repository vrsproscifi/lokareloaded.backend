﻿using System.ComponentModel.DataAnnotations;
using Loka.Database.Common.Entities;
using Loka.Game.Database.Enums.World;

namespace Loka.Game.Database.Entities.World;

[Index(nameof(Name))]
[Index(nameof(TypeId), IsUnique = true)]
public sealed class WorldDayOfWeekEntity : BaseEntity, INamedEntity, ITypedEntity<WorldDayOfWeekTypeId>
{
    [Required]
    [MaxLength(24)]
    public string Name { get; set; } = null!;
    public WorldDayOfWeekTypeId TypeId { get; set; }
        
    public WorldDayOfWeekEntity()
    {
    }

    public WorldDayOfWeekEntity(Guid id, WorldDayOfWeekTypeId typeId) : base(id)
    {
        Name = typeId.ToString();
        TypeId = typeId;
    }
}