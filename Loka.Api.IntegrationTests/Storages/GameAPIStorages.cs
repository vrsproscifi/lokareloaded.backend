﻿using Loka.Api.IntegrationTests.Services;

namespace Loka.Api.IntegrationTests.Storages;

public sealed class GameAPIStorages
{
    public GameStoreStorages GameStore { get; }

    public GameAPIStorages(GameAPIServices services)
    {
        GameStore = new GameStoreStorages(services.GameStore);
    }

    public async Task Initialize()
    {
        await TestContext.Progress.WriteLineAsync($"{nameof(GameAPIStorages)}.{nameof(Initialize)}");
        await GameStore.Initialize();
    }
}