﻿using Loka.Api.Features;
using Loka.Api.Features.Shared;
using Loka.Api.IntegrationTests.Services.Store;

namespace Loka.Api.IntegrationTests.Storages;

public sealed class GameStoreStorages
{
    private GetStoreItems.Types.Response Store { get; set; }
    private GameStoreService GameStoreService { get; }

    public IReadOnlyList<StoreItem.Types.Model> Products => Store.Products;

    public IReadOnlyList<GameBooster.Types.Model> Boosters => Store.Boosters;
    public IReadOnlyList<StoreCase.Types.Model> Cases => Store.Cases;

    public IReadOnlyList<StoreItem.Types.Model> Presets => Store.Products
        .Where(q => q.Category.Type == StoreItemCategory.Types.Type.Profile)
        .OrderBy(q => q.DisplayPosition)
        .ToArray();

    public IReadOnlyList<StoreItem.Types.Model> Items => Store.Products
        .Where(q => q.Category.Type != StoreItemCategory.Types.Type.Profile)
        .OrderBy(q => q.DisplayPosition)
        .ToArray();

    public GameStoreStorages(GameStoreService gameStoreService)
    {
        GameStoreService = gameStoreService;
    }

    public async Task Initialize()
    {
        await TestContext.Progress.WriteLineAsync($"{nameof(GameStoreStorages)}.{nameof(Initialize)}");
        Store = await GameStoreService.GetStoreItems();
    }

    public StoreItem.Types.Model GetProduct(UUID modelId)
    {
        return GetProduct(q => q.Id.Equals(modelId));
    }

    public StoreItem.Types.Model GetProduct(StoreItemCategory.Types.Type type)
    {
        return GetProduct(q => q.Category.Type == type);
    }

    public StoreItem.Types.Model GetProduct(Func<StoreItem.Types.Model, bool> predicate)
    {
        return Store.Products.First(predicate);
    }

    public StoreItem.Types.Model GetProductWithBlueprint()
    {
        return GetProduct(q => q.Blueprints.Any() && q.Blueprints.Any(b => b.Depends.Any()));
    }

    public StoreItem.Types.Model GetProductWithBlueprint(Func<StoreItem.Types.Model, bool> predicate)
    {
        return Store.Products
            .Where(q => q.Blueprints.Any() && q.Blueprints.Any(b => b.Depends.Any()))
            .First(predicate);
    }
}