﻿using System;

namespace Loka.Database.Common.Entities.Store;

public interface IStoreItemChildEntity
{
    Guid ModelId { get; }
}