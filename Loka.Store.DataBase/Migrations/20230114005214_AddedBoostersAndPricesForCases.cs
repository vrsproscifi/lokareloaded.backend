﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Loka.Store.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class AddedBoostersAndPricesForCases : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"));

            migrationBuilder.AddColumn<Guid>(
                name: "SellCurrencyId",
                schema: "Store",
                table: "CaseEntity",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SellPrice",
                schema: "Store",
                table: "CaseEntity",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "BoosterEntity",
                schema: "Store",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    TypeId = table.Column<int>(type: "integer", nullable: false),
                    BoostPercent = table.Column<int>(type: "integer", nullable: false),
                    SellCurrencyId = table.Column<Guid>(type: "uuid", nullable: true),
                    SellPrice = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BoosterEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_BoosterEntity_GameResourceEntity_SellCurrencyId",
                        column: x => x.SellCurrencyId,
                        principalSchema: "Store",
                        principalTable: "GameResourceEntity",
                        principalColumn: "EntityId");
                });

            migrationBuilder.InsertData(
                schema: "Store",
                table: "BoosterEntity",
                columns: new[] { "EntityId", "BoostPercent", "CreatedAt", "DeletedAt", "Name", "SellCurrencyId", "SellPrice", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"), 10, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "PremiumAccount", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 500, 1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"), 10, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "Experience", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 500, 2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"), 10, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "Reputation", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 500, 3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"), 10, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "Money", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 500, 4 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe505"), 10, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "Resources", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 500, 5 }
                });

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe510"),
                columns: new[] { "SellCurrencyId", "SellPrice" },
                values: new object[] { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 500 });

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe520"),
                columns: new[] { "SellCurrencyId", "SellPrice" },
                values: new object[] { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 600 });

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe530"),
                columns: new[] { "SellCurrencyId", "SellPrice" },
                values: new object[] { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 1500 });

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe540"),
                columns: new[] { "SellCurrencyId", "SellPrice" },
                values: new object[] { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 800 });

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe550"),
                columns: new[] { "SellCurrencyId", "SellPrice" },
                values: new object[] { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 900 });

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe560"),
                columns: new[] { "SellCurrencyId", "SellPrice" },
                values: new object[] { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 1700 });

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe570"),
                columns: new[] { "SellCurrencyId", "SellPrice" },
                values: new object[] { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 1800 });

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe580"),
                columns: new[] { "SellCurrencyId", "SellPrice" },
                values: new object[] { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 700 });

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "TypeId",
                value: 3);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "TypeId",
                value: 4);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "TypeId",
                value: 5);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "TypeId",
                value: 6);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "TypeId",
                value: 7);

            migrationBuilder.CreateIndex(
                name: "IX_CaseEntity_SellCurrencyId",
                schema: "Store",
                table: "CaseEntity",
                column: "SellCurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_BoosterEntity_SellCurrencyId",
                schema: "Store",
                table: "BoosterEntity",
                column: "SellCurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_CaseEntity_GameResourceEntity_SellCurrencyId",
                schema: "Store",
                table: "CaseEntity",
                column: "SellCurrencyId",
                principalSchema: "Store",
                principalTable: "GameResourceEntity",
                principalColumn: "EntityId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CaseEntity_GameResourceEntity_SellCurrencyId",
                schema: "Store",
                table: "CaseEntity");

            migrationBuilder.DropTable(
                name: "BoosterEntity",
                schema: "Store");

            migrationBuilder.DropIndex(
                name: "IX_CaseEntity_SellCurrencyId",
                schema: "Store",
                table: "CaseEntity");

            migrationBuilder.DropColumn(
                name: "SellCurrencyId",
                schema: "Store",
                table: "CaseEntity");

            migrationBuilder.DropColumn(
                name: "SellPrice",
                schema: "Store",
                table: "CaseEntity");

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "TypeId",
                value: 4);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "TypeId",
                value: 5);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "TypeId",
                value: 6);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "TypeId",
                value: 7);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "TypeId",
                value: 8);

            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "Service", 3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "Currency", 9 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "Resource", 10 }
                });
        }
    }
}
