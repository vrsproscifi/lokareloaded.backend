﻿using Loka.MatchMaking.Grains.Reminders.Processor;
using Loka.MatchMaking.Interfaces;

namespace Loka.Api.Orleans.Services;

public sealed class StartBackgroundOrleansServices : BackgroundService
{
    private IClusterClient ClusterClient { get; }

    public StartBackgroundOrleansServices(IClusterClient clusterClient)
    {
        ClusterClient = clusterClient;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var matchmaking = ClusterClient.GetGrain<IMatchMakingProcessorReminder>(Guid.Empty);
        await matchmaking.Start();
    }
}