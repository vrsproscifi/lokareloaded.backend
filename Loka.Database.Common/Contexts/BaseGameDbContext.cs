﻿using Microsoft.EntityFrameworkCore;

namespace Loka.Database.Common.Contexts;

public abstract class BaseGameDbContext : DbContext
{
    public const string MigrationsHistoryTable = "__EFMigrationsHistory";
        
    protected BaseGameDbContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.HasPostgresExtension("uuid-ossp");
        modelBuilder.HasPostgresExtension("pgcrypto");
    }

    internal static string GetDbContextName<TDbContext>() where TDbContext : DbContext
    {
        return typeof(TDbContext).Name;
    }

    internal static string GetDbContextScheme<TDbContext>() where TDbContext : DbContext
    {
        return typeof(TDbContext).Name!.Replace(nameof(DbContext), string.Empty);
    }
}

public abstract class BaseGameDbContext<TDbContext> : BaseGameDbContext where TDbContext : BaseGameDbContext
{
    protected static string SchemeName { get; } = GetDbContextScheme<TDbContext>();

    protected BaseGameDbContext(DbContextOptions<TDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
            
        modelBuilder.HasDefaultSchema(SchemeName);
    }
}