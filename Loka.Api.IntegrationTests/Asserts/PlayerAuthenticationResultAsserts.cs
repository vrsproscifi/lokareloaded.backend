﻿using Loka.Api.Features.Identity;
using MongoDB.Bson;

namespace Loka.Api.IntegrationTests.Asserts;

internal static class PlayerAuthenticationResultAsserts
{
    public static void AssertModel(this JwtTokenValueObject obj, string? errorMessage = default)
    {
        Assert.Multiple(() =>
        {
            Assert.That(obj.Token, Is.Not.Null, $"{errorMessage} | Invalid {nameof(obj.Token)}");
            Assert.That(obj.Token, Is.Not.Empty, $"{errorMessage} | Invalid {nameof(obj.Token)}");

            Assert.That(obj.ExpirationDate, Is.Not.Null, $"{errorMessage} | Invalid {nameof(obj.ExpirationDate)}");
            Assert.That(obj.NotBefore, Is.Not.Null, $"{errorMessage} | Invalid {nameof(obj.NotBefore)}");

            var numberOfJWTDots = obj.Token.Count(q => q == '.');
            Assert.That(numberOfJWTDots, Is.EqualTo(2), $"{errorMessage} | Invalid {nameof(obj.Token)}");
        });
    }

    public static void AssertModel(this PlayerAuthenticationResult signUpResult, string? errorMessage = default)
    {
        Assert.Multiple(() =>
        {
            Assert.That(Guid.TryParse(signUpResult.PlayerId, out var _), Is.True, $"{errorMessage} | Invalid {nameof(signUpResult.PlayerId)}, value : {signUpResult.PlayerId}");
            Assert.That(ObjectId.TryParse(signUpResult.SessionId, out var _), Is.True, $"{errorMessage} | Invalid {nameof(signUpResult.SessionId)}, value: {signUpResult.SessionId}");

            Assert.That(signUpResult.PlayerId, Is.Not.Empty, $"{errorMessage} | Invalid {nameof(signUpResult.PlayerId)}");
            Assert.That(signUpResult.SessionId, Is.Not.Empty, $"{errorMessage} | Invalid {nameof(signUpResult.SessionId)}");
            Assert.That(signUpResult.DisplayName, Is.Not.Empty, $"{errorMessage} | Invalid {nameof(signUpResult.DisplayName)}");
            Assert.That(signUpResult.Token, Is.Not.Null, $"{errorMessage} | Invalid {nameof(signUpResult.Token)}");

            signUpResult.Token.JwtToken.AssertModel($"{errorMessage} | Invalid {nameof(signUpResult.Token)}.{nameof(signUpResult.Token.JwtToken)}");
            signUpResult.Token.RefreshToken.AssertModel($"{errorMessage} | Invalid {nameof(signUpResult.Token)}.{nameof(signUpResult.Token.RefreshToken)}");
        });
    }
}