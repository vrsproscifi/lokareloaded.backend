﻿using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Entities;
using Loka.Inventory.DataBase.Entities.Items;

namespace Loka.Inventory.DataBase.Entities.Preset;

public sealed class PlayerInventoryPresetItemEntity : BaseEntity
{
    #region Preset

    [ForeignKey(nameof(PresetEntity))]
    public Guid PresetId { get; set; }

    public PlayerInventoryPresetEntity? PresetEntity { get; set; }

    #endregion

    #region Item

    [ForeignKey(nameof(ItemEntity))]
    public Guid ItemId { get; set; }

    public PlayerItemEntity? ItemEntity { get; set; }

    #endregion
}