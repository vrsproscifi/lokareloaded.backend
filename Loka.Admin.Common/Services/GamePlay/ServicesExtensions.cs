﻿using System;
using Loka.Admin.Common.Services.GamePlay.Fractions;
using Loka.Admin.Common.Services.GamePlay.GameMap;
using Loka.Admin.Common.Services.GamePlay.GameMode;
using Loka.Admin.Common.Services.GamePlay.GameVersion;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Admin.Common.Services.GamePlay;

public static class ServicesExtensions
{
    public static IServiceCollection AddAdminGamePlayServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<IFractionsService, FractionsService>();
        services.AddScoped<IGameMapService, GameMapService>();
        services.AddScoped<IGameModeService, GameModeService>();
        services.AddScoped<IGameVersionService, GameVersionService>();
            
        return services;
    }
}