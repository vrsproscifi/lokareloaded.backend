﻿using Global.Identity.Database;
using Global.Identity.Database.Entities;
using Loka.Identity.Database;
using Loka.Identity.Database.Entities.Player;
using Loka.Identity.Grains.Authentication;
using Loka.Identity.Grains.Storages.Sessions;
using Loka.Identity.Interfaces;
using Loka.Identity.Interfaces.Forms;
using Loka.Identity.Interfaces.Models;
using MongoDB.Bson;


namespace Loka.Identity.Grains.Grains;

public sealed class IdentityGrain : Grain, IIdentityGrain
{
    private IPlayerSessionStorage PlayerSessionStorage { get; }
    private GameIdentityDbContext GameIdentityDbContext { get; }
    private GlobalIdentityDbContext GlobalIdentityDbContext { get; }
    private IJwtBearerTokenGenerator TokenGenerator { get; }
    private GlobalIdentityMetricsDbContext GlobalIdentityMetricsDbContext { get; }

    private IPublishEndpoint PublishEndpoint { get; }

    public IdentityGrain
    (
        IPlayerSessionStorage playerSessionStorage,
        GameIdentityDbContext gameIdentityDbContext,
        IJwtBearerTokenGenerator tokenGenerator,
        GlobalIdentityDbContext globalIdentityDbContext,
        GlobalIdentityMetricsDbContext globalIdentityMetricsDbContext, IPublishEndpoint publishEndpoint
    )
    {
        PlayerSessionStorage = playerSessionStorage;
        GameIdentityDbContext = gameIdentityDbContext;
        GlobalIdentityDbContext = globalIdentityDbContext;
        GlobalIdentityMetricsDbContext = globalIdentityMetricsDbContext;
        PublishEndpoint = publishEndpoint;
        TokenGenerator = tokenGenerator;
    }

    public async Task<OperationResult<PlayerAuthenticationModel>> SignUp(SignUpForm form)
    {
        try
        {
            //======================================
            var userAccountEntity = await GlobalIdentityDbContext.UserAccountEntity.AddAsync(new UserAccountEntity()
            {
                EntityId = Guid.NewGuid(),
                Login = form.PlayerName.ToLowerInvariant(),
                Email = form.Email?.ToLowerInvariant(),
                PasswordHash = form.Password,
                CountryId = form.CountryId,
                GenderId = form.GenderId,
                Birthdate = form.Birthdate,
                RoleId = form.RoleId,
                LanguageId = form.LanguageId,
            });
            await GlobalIdentityDbContext.SaveChangesAsync();

            //======================================
            await GameIdentityDbContext.PlayerAccountEntity.AddAsync(new PlayerAccountEntity(form.PlayerName)
            {
                EntityId = userAccountEntity.Entity.EntityId,
                CreatedAt = userAccountEntity.Entity.CreatedAt,
            });

            await GameIdentityDbContext.SaveChangesAsync();

            //======================================
            var token = await IssueToken(userAccountEntity.Entity, form.IpAddress, form.TokenLifetime);
            if (token.IsFailed)
                return token.Error;

            await PublishEndpoint.Publish(new AfterSignUpEvent()
            {
                PlayerId = token.Result!.PlayerId,
                PlayerName = token.Result!.Login,
            });
            return token;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }

    }

    public async Task<OperationResult<PlayerAuthenticationModel>> SignIn(SignInForm form)
    {
        try
        {
            var userAccountEntity = await GlobalIdentityDbContext.UserAccountEntity
                .FirstOrDefaultAsync(entity => (entity.Login == form.UserNameOrEmail || entity.Email == form.UserNameOrEmail) && entity.PasswordHash == form.Password);

            if (userAccountEntity == null)
                return (OperationResultCode.WrongPassword);

            if (userAccountEntity.DeletedAt.HasValue)
                return (OperationResultCode.PlayerBanned);

            var token = await IssueToken(userAccountEntity, form.IpAddress, form.TokenLifetime);
            if (token.IsFailed)
                return token.Error;

            await PublishEndpoint.Publish(new AfterSignInEvent()
            {
                PlayerId = token.Result!.PlayerId,
                PlayerName = token.Result!.Login,
            });

            return token;
        }
        catch (Exception e)
        {
            return new OperationError(OperationResultCode.ExceptionError, e.ToString());
        }
    }

    private async Task<OperationResult<PlayerAuthenticationModel>> IssueToken(UserAccountEntity userAccountEntity, IPAddress ipAddress, TimeSpan tokenLifetime)
    {
        var existSession = await PlayerSessionStorage.HasActive(userAccountEntity.EntityId);
        if (existSession)
            return (OperationResultCode.PlayerHasActiveSession);

        var session = await PlayerSessionStorage.Create(userAccountEntity.EntityId);

        var sessionEntry = new UserAccountSessionEntry()
        {
            EntityId = ObjectId.GenerateNewId(),
            UserAccountId = userAccountEntity.EntityId,
            CreatedAt = session.CreatedAt,
            IpAddress = ipAddress
        };
        await GlobalIdentityMetricsDbContext.UserAccountSessionEntry.InsertOneAsync(sessionEntry);

        var jwtToken = TokenGenerator.GenerateToken(session, new[]
        {
            new Claim(ClaimTypes.Name, userAccountEntity.Login),
            new Claim(ClaimTypes.NameIdentifier, userAccountEntity.EntityId.ToString()),
            new Claim(ClaimTypes.Sid, sessionEntry.EntityId.ToString()),
            new Claim(ClaimTypes.Role, nameof(RefreshTokenModel.JwtToken)),
        });

        var refreshToken = TokenGenerator.GenerateRefreshToken(session, new[]
        {
            new Claim(ClaimTypes.Name, userAccountEntity.Login),
            new Claim(ClaimTypes.NameIdentifier, userAccountEntity.EntityId.ToString()),
            new Claim(ClaimTypes.Sid, sessionEntry.EntityId.ToString()),
            new Claim(ClaimTypes.Role, nameof(RefreshTokenModel.RefreshToken)),
        });

        return new PlayerAuthenticationModel()
        {
            Login = userAccountEntity.Login,
            PlayerId = userAccountEntity.EntityId,
            SessionId = sessionEntry.EntityId,
            Token = new RefreshTokenModel()
            {
                JwtToken = jwtToken,
                RefreshToken = refreshToken,
                TimeoutDate = session.TimeoutDate
            }
        };
    }

    public async Task<OperationResult<RefreshTokenModel>> RefreshToken(RefreshTokenForm form)
    {
        throw new NotImplementedException();
    }
}