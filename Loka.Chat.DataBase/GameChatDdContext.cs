﻿using Loka.Chat.DataBase.Entities;
using Loka.Common.Mongo;
using MongoDB.Driver;

namespace Loka.Chat.DataBase;

public sealed class GameChatDdContext : MongoDbContext
{
    public IMongoCollection<ChatChannelEntity> Channels { get; init; } = null!;
    public IMongoCollection<ChatChanelMessageEntity> Messages { get; init; } = null!;
    public IMongoCollection<ChatChanelMemberEntity> Members { get; init; } = null!;
    public IMongoCollection<ChatChanelEventEntity> Events { get; init; } = null!;

    public GameChatDdContext()
    {
    }
}