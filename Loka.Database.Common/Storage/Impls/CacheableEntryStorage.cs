﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Loka.Common.Cache.Collection;
using Loka.Common.Cache.Single;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Entities;
using Loka.Database.Common.Storage.Constructors;
using Microsoft.EntityFrameworkCore;

namespace Loka.Database.Common.Storage.Impls;

public abstract class CacheableEntryStorage<TModel, TEntity, TDbContext> : EntryStorage<TModel, TEntity, TDbContext>
    where TDbContext : DbContext
    where TEntity : BaseEntity
{
    private IDataCache<Guid, TModel> Cache { get; }
    private ICollectionCache<TModel> Models { get; }

    protected CacheableEntryStorage(CacheableEntryStorageConstructor<TDbContext, TModel> constructor) : base(constructor)
    {
        Cache = constructor.Cache;
        Models = constructor.Models;
    }

    public sealed override async Task<OperationResult<TModel>> GetById(Guid id, CancellationToken cancellationToken = default)
    {
        if (Cache.TryGet(id, out var cachedModel))
            return cachedModel;

        var getEntity = await base.GetById(id, cancellationToken);
        if (getEntity.IsSucceeded)
        {
            Cache.Put(getEntity.Result);
        }

        return getEntity;
    }

    public sealed override async Task<TModel[]?> GetList(CancellationToken cancellationToken = default)
    {
        if (Models.TryGet(out var cachedModels))
            return cachedModels;

        var models = await base.GetList(cancellationToken);
        return Models.Put(models);
    }
}