﻿using Loka.Inventory.Interfaces.Boosters;

namespace Loka.Inventory.Interfaces.Store.Requests;

public static class BuyStoreBooster
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<PlayerInventoryBoosterModel>
    {
        [Id(0)]
        public Guid ModelId { get; init; }
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public PlayerInventoryBoosterModel Booster { get; init; }
    }
}