﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Loka.Admin.Common.Models.GamePlay;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Extensions;
using Loka.Database.Common.Specifications;
using Loka.Game.Database;
using Loka.Game.Database.Entities.Store.Items;
using Microsoft.EntityFrameworkCore;

namespace Loka.Admin.Common.Services.GamePlay.Fractions;

internal sealed class FractionsService : IFractionsService
{
    private IMapper Mapper { get; }
    private SystemDbContext SystemDbContext { get; }

    public FractionsService(SystemDbContext systemDbContext, IMapper mapper)
    {
        SystemDbContext = systemDbContext;
        Mapper = mapper;
    }

    public async Task<OperationResult<FractionInformationModel>> GetById(Guid id, CancellationToken cancellationToken)
    {
        var entity = await SystemDbContext.FractionEntity.GetById(id, cancellationToken);
        if (entity == null)
            return OperationResultCode.PlayerNotFound;

        var model = Mapper.Map<FractionInformationModel>(entity);
        return model;
    }

    public async Task<OperationResult<FractionInformationModel[]>> GetByIds(IReadOnlyList<Guid> ids, CancellationToken cancellationToken = default)
    {
        var models = await SystemDbContext.GameItemCategoryEntity
            .Where(new EntityIdsRangeSpecification<GameItemCategoryEntity>(ids))
            .ProjectTo<FractionInformationModel>(Mapper.ConfigurationProvider)
            .ToArrayAsync(cancellationToken);

        return models;
    }
}