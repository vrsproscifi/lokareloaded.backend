﻿using Loka.Api.Features.Shared;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Errors;
using MongoDB.Bson;

namespace Loka.Api.IntegrationTests.Asserts;

public static class CommonAsserts
{
    public static void AssertResultCode(this RpcException? exception, OperationResultCode expectedResultCode, string? message = default)
    {
        Assert.That(exception, Is.Not.Null, $"Invalid {nameof(exception)} | Expected {nameof(RpcException)}  ");
        Assert.Multiple(() =>
        {
            Assert.That(exception!.Message, Is.Not.Empty, $"Invalid {nameof(exception)}.{nameof(exception.Message)}");

            var code = exception.Trailers.GetValue(nameof(OperationError.ResultCode));
            Assert.That(code, Is.Not.Empty, $"${message} | Invalid {nameof(OperationError.ResultCode)}");

            var parseResult = Enum.TryParse<OperationResultCode>(code, out var resultCode);
            Assert.IsTrue(parseResult, $"${message} | Invalid {nameof(OperationError.ResultCode)}");

            Assert.That(resultCode, Is.EqualTo(expectedResultCode), $"${message} | Invalid {nameof(OperationError.ResultCode)}");
        });
    }

    public static void AssertModel(this UUID id)
    {
        Assert.That(id, Is.Not.Null, $"Invalid {nameof(id)}");
        Assert.Multiple(() =>
        {
            Assert.That(id.Value, Is.Not.Null, $"Invalid {nameof(id)}.{nameof(id.Value)}");
            Assert.That(id.Value, Is.Not.Empty, $"Invalid {nameof(id)}.{nameof(id.Value)}");
            Assert.That(Guid.TryParse(id.Value, out var guid), Is.True, $"Invalid {nameof(id)}.{nameof(id.Value)} | Failed parse");
        });
    }

    public static void AssertModel(this ObjID id)
    {
        Assert.That(id, Is.Not.Null, $"Invalid {nameof(id)}");
        Assert.Multiple(() =>
        {
            Assert.That(id.Value, Is.Not.Null, $"Invalid {nameof(id)}.{nameof(id.Value)}");
            Assert.That(id.Value, Is.Not.Empty, $"Invalid {nameof(id)}.{nameof(id.Value)}");
            Assert.That(ObjectId.TryParse(id.Value, out var guid), Is.True, $"Invalid {nameof(id)}.{nameof(id.Value)} | Failed parse");
        });
    }
}