﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Loka.Common.EfCore.OrderBy;

public abstract class PropertySortMapping<TKey, TEntity, TOrderBy>
    where TKey : Enum
    where TEntity : class
    where TOrderBy : IOrderByMethod<TKey>
{
    protected delegate IQueryable<TEntity> OrderByDelegate(IQueryable<TEntity> query, TOrderBy orderBy);

    protected abstract IReadOnlyDictionary<TKey, OrderByDelegate> MappingDictionary { get; }

    internal IQueryable<TEntity> ApplySort(IQueryable<TEntity> query, TOrderBy orderBy)
    {
        var orderByQuery = MappingDictionary[orderBy.OrderBy];
        return orderByQuery(query, orderBy);
    }
}
    
public abstract class PropertySortMapping<TKey, TEntity> : PropertySortMapping<TKey, TEntity,  OrderByMethod<TKey>>
    where TKey : Enum
    where TEntity : class
{

}