﻿using Loka.Common.Authentication;
using Loka.Common.MediatR.Executors;
using Loka.Common.Orleans.Grains;
using Loka.Inventory.DataBase;
using Loka.Inventory.DataBase.Entities.Templates;
using Loka.Inventory.Interfaces.Presets;
using Loka.Inventory.Interfaces.Presets.Models;
using Loka.Inventory.Interfaces.Templates;

namespace Loka.Inventory.Grains.Presets;

public sealed class PlayerInventoryPresetGrain : Grain, IPlayerInventoryPresetGrain, IWithPlayerActor
{
    public PlayerActor Actor { get; set; } = null!;
    private IMapper Mapper { get; }
    private InventoryDbContext InventoryDbContext { get; }
    private IOperationExecutor OperationExecutor { get; }
    private IPlayerInventoryPresetRepository PlayerInventoryPresetRepository { get; }

    public PlayerInventoryPresetGrain(InventoryDbContext inventoryDbContext, IOperationExecutor operationExecutor, IPlayerInventoryPresetRepository playerInventoryPresetRepository, IMapper mapper)
    {
        InventoryDbContext = inventoryDbContext;
        OperationExecutor = operationExecutor;
        PlayerInventoryPresetRepository = playerInventoryPresetRepository;
        Mapper = mapper;
    }


    public Task<OperationResult<EquipInventoryItem.Result>> Equip(EquipInventoryItem.Request request)
    {
        return OperationExecutor.Execute(request);
    }

    public async Task<PlayerInventoryPresetModel.RPC> CreatePreset(Guid modelId)
    {
        try
        {
            var model = await PlayerInventoryPresetRepository.CreatePreset(Actor.PlayerId, modelId);
            return Mapper.Map<PlayerInventoryPresetModel.RPC>(model);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }

    }

    public Task<bool> HasPreset(Guid modelId)
    {
        return PlayerInventoryPresetRepository.HasPreset(Actor.PlayerId, modelId);
    }

    public async Task<OperationResult<GetPlayerInventoryPresets.Result>> GetPresets()
    {
        try
        {
            var entities = await PlayerInventoryPresetRepository.GetAll(Actor.PlayerId);
            return new GetPlayerInventoryPresets.Result
            {
                Presets = Mapper.Map<PlayerInventoryPresetModel.RPC[]>(entities)
            };
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }

    }

    public async Task<OperationResult<PlayerInventoryTemplateModel>> SavePresetAsTemplate(SavePresetAsTemplateForm form)
    {
        var preset = await InventoryDbContext.PlayerInventoryPresetEntity
            .Include(q => q.Items)
            .FirstOrDefaultAsync(q => q.EntityId == form.PresetId && q.PlayerId == Actor.PlayerId);

        if (preset == null)
            return OperationResultCode.UnknownError;

        var template = await InventoryDbContext.PlayerInventoryTemplateEntity.AddAsync(new PlayerInventoryTemplateEntity()
        {
            EntityId = Guid.NewGuid(),
            PlayerId = Actor.PlayerId,
            Name = form.Name,
            Items = preset.Items!.Select(item => new PlayerInventoryTemplateItemEntity
            {
                EntityId = Guid.NewGuid(),
                ItemId = item.ItemId,
                CreatedAt = DateTimeOffset.UtcNow,
            }).ToArray()
        });

        await InventoryDbContext.SaveChangesAsync();

        return new PlayerInventoryTemplateModel()
        {
            Id = template.Entity.EntityId,
            Name = template.Entity.Name,
            Entries = template.Entity.Items!.Select(item => new PlayerInventoryTemplateModel.ItemEntry()
            {
                Id = item.EntityId,
                ItemId = item.ItemId
            }).ToArray()
        };
    }
}