using Loka.Inventory.Interfaces.Presets.Models;
using Loka.Inventory.Interfaces.Templates;

namespace Loka.Inventory.Interfaces.Presets;

public interface IPlayerInventoryPresetGrain : IGrainWithGuidKey
{
    Task<PlayerInventoryPresetModel.RPC> CreatePreset(Guid modelId);
    Task<bool> HasPreset(Guid modelId);
    Task<OperationResult<GetPlayerInventoryPresets.Result>> GetPresets();

    Task<OperationResult<PlayerInventoryTemplateModel>> SavePresetAsTemplate(SavePresetAsTemplateForm form);
    Task<OperationResult<EquipInventoryItem.Result>> Equip(EquipInventoryItem.Request request);
}