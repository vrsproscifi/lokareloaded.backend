﻿namespace Loka.Common.Pagination;

public enum OrderByDirection 
{
    /// <summary>
    /// 1 -- 1000
    /// </summary>
    Ascending = 0,
        
    /// <summary>
    /// 1000 -- 1
    /// </summary>
    Descending = 1,
}