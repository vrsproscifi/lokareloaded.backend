﻿namespace Loka.Inventory.Interfaces.Presets.Models;

public static class GetPlayerInventoryPresets
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<Result>
    {
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public IReadOnlyList<PlayerInventoryPresetModel.RPC> Presets { get; init; } = ArraySegment<PlayerInventoryPresetModel.RPC>.Empty;
    }
}