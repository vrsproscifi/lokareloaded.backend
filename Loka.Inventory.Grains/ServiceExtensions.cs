﻿using Loka.Common;
using Loka.Common.MediatR.Extensions;
using Loka.Inventory.Grains.Boosters;
using Loka.Inventory.Grains.Cases;
using Loka.Inventory.Grains.Cases.Handlers;
using Loka.Inventory.Grains.Inventory;
using Loka.Inventory.Grains.Presets;
using Loka.Inventory.Grains.Presets.Handlers;
using Loka.Inventory.Grains.Resources;
using Loka.Inventory.Grains.Workbench.Handlers;
using Loka.Inventory.Interfaces.Boosters;
using Loka.Inventory.Interfaces.Cases;
using Loka.Inventory.Interfaces.Inventory;
using Loka.Inventory.Interfaces.Presets;
using Loka.Inventory.Interfaces.Presets.Models;
using Loka.Inventory.Interfaces.Resources;
using Loka.Inventory.Interfaces.Workbench;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Inventory.Grains;

public static class ServiceExtensions
{
    public static ServiceCollectionInitializer AddInventoryHandlers(this ServiceCollectionInitializer initializer)
    {
        initializer.Services.AddScoped<IPlayerInventoryPresetRepository, PlayerInventoryPresetRepository>();
        initializer.Services.AddScoped<IPlayerResourcesRepository, PlayerResourcesRepository>();
        initializer.Services.AddScoped<IPlayerInventoryItemsRepository, PlayerInventoryItemsRepository>();
        initializer.Services.AddScoped<IPlayerInventoryBoosterRepository, PlayerInventoryBoosterRepository>();
        initializer.Services.AddScoped<IPlayerInventoryCasesRepository, PlayerInventoryCasesRepository>();

        initializer.Services.AddRequestHandler<CraftWorkbenchItem.Request, CraftWorkbenchItem.Result, CraftWorkbenchItemRequestHandler>();
        initializer.Services.AddRequestHandler<EquipInventoryItem.Request, EquipInventoryItem.Result, EquipInventoryItemRequestHandler>();
        initializer.Services.AddRequestHandler<OpenPlayerCase.Request, OpenPlayerCase.Result, OpenCaseRequestHandler>();
        return initializer;
    }
}