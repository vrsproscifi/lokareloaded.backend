﻿using System.Security.Claims;

namespace Loka.Identity.Interfaces.Extensions;

public static class JwtBearerTokenExtensions
{
    public static Claim GetClaim(this ClaimsPrincipal principal, string name)
    {
        return TryGetClaim(principal, name, out var claim) ? claim : null;
    }

    public static bool TryGetClaim(this ClaimsPrincipal principal, string name, out Claim claim)
    {
        claim = principal.Claims.FirstOrDefault(q => q.Type == name);
        return claim != null;
    }

    public static Guid GetSessionId(this ClaimsPrincipal principal)
    {
        if (principal.TryGetClaim(ClaimTypes.Sid, out var claim) && !string.IsNullOrWhiteSpace(claim.Value))
            return Guid.Parse(claim.Value);
        return default;
    }

    public static Guid GetPlayerId(this ClaimsPrincipal principal)
    {
        if (principal.TryGetClaim(ClaimTypes.NameIdentifier, out var claim) && !string.IsNullOrWhiteSpace(claim.Value))
            return Guid.Parse(claim.Value);

        return default;
    }

    public static string GetPlayerName(this ClaimsPrincipal principal)
    {
        return principal.GetClaim(ClaimTypes.Name)?.Value ?? String.Empty;
    }
}