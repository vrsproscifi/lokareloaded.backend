﻿namespace Loka.Inventory.Interfaces.Discounts;

public interface IPlayerDiscountsGrain : IGrainWithGuidKey
{
    Task<OperationResult<GetPlayerDiscounts.Result>> GetDiscounts();
}