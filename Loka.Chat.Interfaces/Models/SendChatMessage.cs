﻿using MongoDB.Bson;

namespace Loka.Chat.Interfaces.Models;

public static class SendChatMessage
{
    [GenerateSerializer]
    public sealed class Request
    {
        [Id(0)]
        public ObjectId ChannelId { get; set; }
        
        [Id(1)]
        public string Message { get; set; } = null!;
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public ChatMessageEntry Message { get; init; } = null!;
    }
}