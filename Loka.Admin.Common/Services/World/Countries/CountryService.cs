﻿using Loka.Admin.Common.Models.Countries;
using Loka.Common.Operations.Codes;
using Loka.Database.Common.Storage.Constructors;
using Loka.Database.Common.Storage.Impls;
using Loka.Game.Database;
using Loka.Game.Database.Entities.World;
using Microsoft.EntityFrameworkCore;

namespace Loka.Admin.Common.Services.World.Countries;

internal sealed class CountryService : CacheableEntryStorage<CountryInformationModel, WorldCountryEntity, SystemDbContext>, ICountryService
{
    protected override OperationResultCode EntityNotFoundErrorCode => OperationResultCode.CountryNotFound;
    protected override DbSet<WorldCountryEntity> Table => DbContext.WorldCountryEntity;
        
    public CountryService(CacheableEntryStorageConstructor<SystemDbContext, CountryInformationModel> constructor) : base(constructor)
    {
    }
}