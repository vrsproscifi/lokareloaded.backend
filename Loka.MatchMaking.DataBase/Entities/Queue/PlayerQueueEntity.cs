﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using Loka.Database.Common.Entities;
using Newtonsoft.Json;

namespace Loka.MatchMaking.DataBase.Entities.Queue;

public enum PlayerQueueState : byte
{
    None,
    Ready,
    Searching,
    Playing,
    WaitingPlayers
}

public class PlayerQueueEntity : BaseEntity
{
    #region Squad

    [ForeignKey(nameof(QueueEntity))]
    public Guid? QueueId { get; set; }

    public PlayerQueueEntity? QueueEntity { get; set; }

    public List<PlayerQueueEntity>? Members { get; set; }

    #endregion

    public Guid? MatchId { get; set; }

    public PlayerQueueState State { get; set; }

    [Column(nameof(Options), TypeName = "jsonb")]
    public PlayerQueueOptions Options { get; set; } = null!;


    public PlayerQueueEntity()
    {
    }

    public PlayerQueueEntity(Guid playerId) : base(playerId)
    {
        State = PlayerQueueState.None;
    }

    public void StopSearch()
    {
        State = PlayerQueueState.None;
            
        foreach (var member in Members!)
        {
            member.State = PlayerQueueState.Searching;
        }
    }
        
    public void StartSearch(PlayerQueueOptions options)
    {
        State = PlayerQueueState.Searching;
        Options = options;
            
        foreach (var member in Members!)
        {
            member.State = PlayerQueueState.Searching;
            member.Options = options;
        }
    }
        
    public bool IsMembersReady()
    {
        return Members!.All(member => member.State == PlayerQueueState.Ready);
    }
        
    public bool IsSquadLeader()
    {
        return QueueId == null;
    }

    public bool IsEqualTargetServerVersion(Guid version)
    {
        return Members!.All(member => member.Options.TargetServerVersionId == version);
    }
}