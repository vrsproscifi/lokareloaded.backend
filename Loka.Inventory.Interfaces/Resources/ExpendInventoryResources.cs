﻿namespace Loka.Inventory.Interfaces.Resources;

public static class ExpendInventoryResources
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<Result>
    {
        [Id(0)]
        public IReadOnlyList<Entry> Resources { get; init; } = ArraySegment<Entry>.Empty;

        [GenerateSerializer]
        public sealed class Entry
        {
            [Id(0)]
            public Guid EntityId { get; init; }

            [Id(1)]
            public long Amount { get; init; }
        }
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public IReadOnlyList<PlayerResourceModel.Delta> Resources { get; init; } = ArraySegment<PlayerResourceModel.Delta>.Empty;
    }
}