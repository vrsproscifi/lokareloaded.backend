﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Entities;
using Loka.MatchMaking.DataBase.Enums.Server;
using Microsoft.EntityFrameworkCore;

namespace Loka.MatchMaking.DataBase.Entities.Server;

[Index(nameof(ServerVersionId))]
[Index(nameof(State))]
public class ClusterAvailableVersionEntity : BaseEntity
{
    [ForeignKey(nameof(ClusterEntity))]
    public Guid ClusterId { get; set; }
    public ClusterEntity? ClusterEntity { get; set; }
        
    public Guid ServerVersionId { get; set; }
    public ClusterServerVersionState State { get; set; }
}