﻿namespace Loka.Store.DataBase.Entities.Booster;

public sealed class BoosterEntity : BaseEntity, ITypedEntity<BoosterTypeId>
{
    [Required, MaxLength(128)]
    public string Name { get; init; } = null!;

    public BoosterTypeId TypeId { get; init; }

    public int BoostPercent { get; init; } = 10;


    #region Cost

    [ForeignKey(nameof(SellCurrencyEntity))]
    public Guid? SellCurrencyId { get; set; }

    public GameResourceEntity? SellCurrencyEntity { get; set; }
    public int SellPrice { get; set; }

    #endregion


    public BoosterEntity()
    {
    }

    public BoosterEntity(Guid id, BoosterTypeId typeId, Guid? sellCurrencyId, int sellPrice) : base(id)
    {
        Name = typeId.ToString();
        TypeId = typeId;
        SellCurrencyId = sellCurrencyId;
        SellPrice = sellPrice;
    }
}