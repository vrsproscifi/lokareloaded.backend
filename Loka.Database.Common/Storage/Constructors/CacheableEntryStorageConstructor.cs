using System;
using AutoMapper;
using Loka.Common.Cache.Collection;
using Loka.Common.Cache.Single;

namespace Loka.Database.Common.Storage.Constructors;

public sealed class CacheableEntryStorageConstructor<TDbContext, TModel> : EntryStorageConstructor<TDbContext>
{
    internal IDataCache<Guid, TModel> Cache { get; }
    internal ICollectionCache<TModel> Models { get; }

    public CacheableEntryStorageConstructor
    (
        IMapper mapper,
        TDbContext dbContext,
        IDataCache<Guid, TModel> cache,
        ICollectionCache<TModel> models
    ) : base(mapper, dbContext)
    {
        Cache = cache;
        Models = models;
    }
}