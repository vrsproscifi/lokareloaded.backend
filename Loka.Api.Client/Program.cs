﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Net.Client;

namespace Loka.Api.Client;

class Program
{
    static async Task Main(string[] args)
    {
        AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

        try
        {
            Console.WriteLine("Wait");

            await Task.Delay(5000);

            using var channel = GrpcChannel.ForAddress("http://localhost:5555", new GrpcChannelOptions()
            {
                Credentials = ChannelCredentials.Insecure,
                HttpHandler = new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = HttpClientHandler.DangerousAcceptAnyServerCertificateValidator
                }
            });


            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine($"Attempt connect {i}");
                if (await CheckConnect(channel))
                {
                    break;
                }
            }

            var response = await SignIn(channel, new GooglePlayAuthenticationQuery()
            {
                GooglePlayLogin = "sentike",
                GooglePlayTokenId = Guid.NewGuid().ToString()
            });

            Console.WriteLine($"PlayerId: {response.PlayerId}");
            Console.WriteLine($"SessionId: {response.SessionId}");
            Console.WriteLine($"DisplayName: {response.DisplayName}");
            Console.WriteLine($"JwtToken: {Environment.NewLine}{response.JwtToken}");


            var matchMaking = new MatchMakingQueue.MatchMakingQueueClient(channel);


            var headers = new Metadata();
            headers.Add("Authorization", $"Bearer {response.JwtToken}");

            var join = await matchMaking.JoinToMatchMakingQueueAsync(new JoinToMatchMakingQueueQuery()
            {
                GameVersionId = Guid.NewGuid().ToString(),
                TargetServerVersionId = Guid.NewGuid().ToString(),
            }, headers);

            Console.WriteLine($"Level: {join.Level}");
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
        }

        Console.WriteLine("End");
        Console.ReadLine();
    }

    private static async Task<bool> CheckConnect(GrpcChannel channel)
    {
        try
        {
            await SignIn(channel, new GooglePlayAuthenticationQuery()
            {
                GooglePlayLogin = Guid.NewGuid().ToString(),
                GooglePlayTokenId = Guid.NewGuid().ToString(),
            });

            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    private static async Task<GooglePlayAuthenticationModel> SignIn(GrpcChannel channel, GooglePlayAuthenticationQuery request)
    {
        var client = new PlayerIdentity.PlayerIdentityClient(channel);
        var response = await client.GooglePlayAuthenticationAsync(request);
        return response;
    }
}