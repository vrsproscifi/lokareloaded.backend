﻿namespace Loka.Inventory.Interfaces.Payments;

public interface IPlayerPaymentsGrain : IGrainWithGuidKey
{
    Task<OperationResult<PlayerPaymentTransactionModel>> PayFor(PayForItem.Request request);
    Task<OperationResult<PlayerPaymentTransactionModel>> PayFor(MultiplePayForItem.Request request);
    Task<OperationResult<RollBackPayment.Result>> RollBackPayment(RollBackPayment.Request request);
}