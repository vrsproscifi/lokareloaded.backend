﻿namespace Loka.Game.Database.Enums.Achievements;

public enum AchievementTypeId : short
{
    //----------------
    None,

    //----------------
    Kills,
    Deaths,
    Score,

    //----------------
    Money,
    Experience,

    //----------------
    FirstBlood,
    Assist,


    //----------------
    KillsByPistol,
    KillsByShotGun,
    KillsBySniperRifle,
    KillsByRifle,
    KillsByKnife,
    KillsByGrenade,

    //----------------
    SeriesKills,
    SeriesKillsByPistol,
    SeriesKillsByShotGun,
    SeriesKillsBySniperRifle,
    SeriesKillsByRifle,
    SeriesKillsByKnife,
    SeriesKillsByGrenade,

    //----------------
    MatchesTotal,

    MatchesWins,
    MatchesLoses,
    MatchesDefeats,

    //----------------
    HeadShot,

    //----------------
    //	TimeInGame in Seconds
    TimeInGame,

    //----------------
    BestScore,
    BestKills,

    //----------------
    FirstInMatch,
    SecondInMatch,
    ThirdInMatch,

    //----------------
    End
}