﻿namespace Loka.Inventory.Interfaces.Discounts;

public static class GetPlayerDiscounts
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<Result>
    {
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public IReadOnlyList<Entry> Entries { get; init; } = null!;
    }

    [GenerateSerializer]
    public sealed class Entry : IEntry
    {
        [Id(0)]
        public Guid Id { get; init; }

        [Id(1)]
        public Guid ModelId { get; init; }

        /// <summary>
        /// Информация о том, когда купон будет доступен снова. Если есть значение - то после этой даты скидка появится сама автоматически.
        /// Если значения нет, то скидка была одноразовая или значение устанавливается вручную
        /// </summary>
        [Id(2)]
        public DateTimeOffset? NextAvailableDate { get; set; }

        /// <summary>
        /// Информация о том, когда действие акции будет окончено.
        /// Если есть значение - то после этой даты скидка будет не действительна. Если значения нет, то скидка действует бессрочно
        /// </summary>
        [Id(3)]
        public DateTimeOffset? EndDiscountDate { get; set; }
    }
}