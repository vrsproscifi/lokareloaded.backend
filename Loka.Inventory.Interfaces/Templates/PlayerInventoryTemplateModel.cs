﻿namespace Loka.Inventory.Interfaces.Templates;

[GenerateSerializer]
public sealed class PlayerInventoryTemplateModel : IEntry, INamedEntry
{
    [Id(0)]
    public Guid Id { get; init; }

    [Id(1)]
    public string Name { get; init; } = null!;

    [Id(2)]
    public IReadOnlyList<ItemEntry> Entries { get; init; } = null!;


    [GenerateSerializer]
    public sealed class ItemEntry : IEntry
    {
        [Id(0)]
        public Guid Id { get; init; }

        [Id(1)]
        public Guid ItemId { get; init; }
    }
}