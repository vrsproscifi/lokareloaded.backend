﻿using MongoDB.Bson;

namespace Loka.Inventory.Interfaces.Payments;

[GenerateSerializer]
public sealed class PlayerPaymentTransactionModel
{
    [Id(0)]
    public ObjectId Id { get; init; }
    
    [Id(1)]
    public PlayerPaymentTransactionTypeId TypeId { get; init; }

    [Id(2)]
    public Guid ModelId { get; init; }
    
    [Id(3)]
    public long Count { get; init; }

    [Id(4)]
    public IReadOnlyList<ResourceEntry> Resources { get; init; } = ArraySegment<ResourceEntry>.Empty;

    [GenerateSerializer]
    public sealed class ResourceEntry
    {
        [Id(0)]
        public Guid ModelId { get; init; }

        [Id(1)]
        public long Amount { get; init; }
    }
}