﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Loka.Identity.Database.Migrations
{
    public partial class UpdatedPlayerAccountEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlayerProfileEntity",
                schema: "Identity");

            migrationBuilder.RenameColumn(
                name: "GooglePlayPlayerId",
                schema: "Identity",
                table: "PlayerAccountEntity",
                newName: "PlayerName");

            migrationBuilder.RenameColumn(
                name: "EmailConfirmed",
                schema: "Identity",
                table: "PlayerAccountEntity",
                newName: "IsNameConfirmed");

            migrationBuilder.RenameIndex(
                name: "IX_PlayerAccountEntity_GooglePlayPlayerId",
                schema: "Identity",
                table: "PlayerAccountEntity",
                newName: "IX_PlayerAccountEntity_PlayerName");

            migrationBuilder.AddColumn<long>(
                name: "Experience",
                schema: "Identity",
                table: "PlayerStatisticsEntity",
                type: "bigint",
                nullable: false,
                defaultValue: 0L);

            migrationBuilder.AddColumn<bool>(
                name: "IsLevelUpNotified",
                schema: "Identity",
                table: "PlayerStatisticsEntity",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "LastLevel",
                schema: "Identity",
                table: "PlayerStatisticsEntity",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Level",
                schema: "Identity",
                table: "PlayerStatisticsEntity",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "DeviceId",
                schema: "Identity",
                table: "PlayerAccountEntity",
                type: "character varying(128)",
                maxLength: 128,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<bool>(
                name: "IsEmailConfirmed",
                schema: "Identity",
                table: "PlayerAccountEntity",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastActivityDate",
                schema: "Identity",
                table: "PlayerAccountEntity",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.AddColumn<short>(
                name: "PrimeMatchesCount",
                schema: "Identity",
                table: "PlayerAccountEntity",
                type: "smallint",
                nullable: false,
                defaultValue: (short)0);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "PrimeMatchesNextDate",
                schema: "Identity",
                table: "PlayerAccountEntity",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.CreateIndex(
                name: "IX_PlayerAccountEntity_DeviceId",
                schema: "Identity",
                table: "PlayerAccountEntity",
                column: "DeviceId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PlayerAccountEntity_DeviceId",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropColumn(
                name: "Experience",
                schema: "Identity",
                table: "PlayerStatisticsEntity");

            migrationBuilder.DropColumn(
                name: "IsLevelUpNotified",
                schema: "Identity",
                table: "PlayerStatisticsEntity");

            migrationBuilder.DropColumn(
                name: "LastLevel",
                schema: "Identity",
                table: "PlayerStatisticsEntity");

            migrationBuilder.DropColumn(
                name: "Level",
                schema: "Identity",
                table: "PlayerStatisticsEntity");

            migrationBuilder.DropColumn(
                name: "DeviceId",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropColumn(
                name: "IsEmailConfirmed",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropColumn(
                name: "LastActivityDate",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropColumn(
                name: "PrimeMatchesCount",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropColumn(
                name: "PrimeMatchesNextDate",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.RenameColumn(
                name: "PlayerName",
                schema: "Identity",
                table: "PlayerAccountEntity",
                newName: "GooglePlayPlayerId");

            migrationBuilder.RenameColumn(
                name: "IsNameConfirmed",
                schema: "Identity",
                table: "PlayerAccountEntity",
                newName: "EmailConfirmed");

            migrationBuilder.RenameIndex(
                name: "IX_PlayerAccountEntity_PlayerName",
                schema: "Identity",
                table: "PlayerAccountEntity",
                newName: "IX_PlayerAccountEntity_GooglePlayPlayerId");

            migrationBuilder.CreateTable(
                name: "PlayerProfileEntity",
                schema: "Identity",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    DisplayName = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    Experience = table.Column<long>(type: "bigint", nullable: false),
                    IsLevelUpNotified = table.Column<bool>(type: "boolean", nullable: false),
                    IsNameConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    LastActivityDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    LastLevel = table.Column<int>(type: "integer", nullable: false),
                    Level = table.Column<int>(type: "integer", nullable: false),
                    PrimeMatchesCount = table.Column<short>(type: "smallint", nullable: false),
                    PrimeMatchesNextDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerProfileEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PlayerProfileEntity_PlayerAccountEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "Identity",
                        principalTable: "PlayerAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlayerProfileEntity_DisplayName",
                schema: "Identity",
                table: "PlayerProfileEntity",
                column: "DisplayName",
                unique: true);
        }
    }
}
