﻿using MongoDB.Bson;

namespace Loka.Chat.Interfaces.Models;

public static class GetChatMembers
{
    [GenerateSerializer]
    public sealed class Request
    {
        [Id(0)]
        public ObjectId ChannelId { get; init; }
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public IReadOnlyList<ChatMemberEntry> Entries { get; init; } = ArraySegment<ChatMemberEntry>.Empty;
    }
}