﻿using System;
using Loka.Common.Cache.Collection;
using Loka.Common.Cache.Single;
using Loka.Common.Entry;
using Loka.Database.Common.Storage.Constructors;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Database.Common.Storage.Extensions;

public static class EntryStorageExtensions
{
    public static IServiceCollection AddEntryStorage<TInterface, TImpl>(this IServiceCollection services)
        where TInterface : class, IIsEntryStorage
        where TImpl : class, TInterface
    {
        services.AddScoped(typeof(EntryStorageConstructor<>));
        services.AddScoped<TInterface, TImpl>();
        return services;
    }

    public static IServiceCollection AddCacheableEntryStorage<TInterface, TImpl, TModel>(this IServiceCollection services)
        where TInterface : class, IEntryStorage<TModel>
        where TImpl : class, TInterface
        where TModel : IEntry
    {
        services.AddScoped(typeof(CacheableEntryStorageConstructor<,>));

        services.AddScoped<IDataCache<Guid, TModel>, EntryDataCache<Guid, TModel>>();
        services.AddScoped<ICollectionCache<TModel>, CollectionCache<TModel>>();
        services.AddEntryStorage<TInterface, TImpl>();
        return services;
    }
}