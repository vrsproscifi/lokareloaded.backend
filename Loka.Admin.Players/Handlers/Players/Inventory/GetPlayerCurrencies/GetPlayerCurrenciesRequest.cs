﻿using System;
using Loka.Common.MediatR.Requests;
using Loka.Common.Ranges.Numerics;

namespace Loka.Admin.Players.Handlers.Players.Inventory.GetPlayerCurrencies;

public class GetPlayerCurrenciesRequest : IOperationRequest<GetPlayerCurrenciesResponse>
{
    public Guid PlayerId { get; set; }
    public string? Term { get; set; }
    public Guid[] Ids { get; set; }= null!;
    public NumericRange<long?, long?> Amount { get; set; }
}