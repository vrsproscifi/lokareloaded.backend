﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Common.Models.Store;
using Loka.Common.Operations.Results;

namespace Loka.Admin.Common.Services.Store.ItemCategories;

public interface IGameItemCategoryService
{
    Task<OperationResult<GameItemCategoryInformationModel>> GetById(Guid id, CancellationToken cancellationToken = default);
    Task<OperationResult<GameItemCategoryInformationModel[]>> GetByIds(IReadOnlyList<Guid> ids, CancellationToken cancellationToken = default);
}