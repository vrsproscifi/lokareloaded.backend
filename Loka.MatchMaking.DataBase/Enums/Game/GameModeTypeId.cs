﻿namespace Loka.MatchMaking.DataBase.Enums.Game;

public enum GameModeTypeId : byte
{
    None,

    TeamDeadMatch,

    LostDeadMatch,

    ResearchMatch,

    DuelMatch,
}