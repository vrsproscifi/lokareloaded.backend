﻿using Loka.Inventory.DataBase.Entities.Boosters;
using Loka.Inventory.Interfaces.Boosters;

namespace Loka.Inventory.Grains.Boosters;

internal sealed class PlayerInventoryBoostersMappingProfile : Profile
{
    public PlayerInventoryBoostersMappingProfile()
    {
        CreateMap<PlayerBoosterEntity, PlayerInventoryBoosterModel>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.BoosterId))
            .ForMember(q => q.Amount, q => q.MapFrom(w => w.Amount))
            .ForMember(q => q.IsActive, q => q.MapFrom(w => w.IsActive));
    }
}