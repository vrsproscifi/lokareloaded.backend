﻿using System.Threading.Tasks;
using Loka.MatchMaking.History.Entities;

namespace Loka.MatchMaking.History.Storage;

public sealed class MatchHistoryStorage
{
    private MatchMakingHistoryDdContext DdContext { get; }

    public MatchHistoryStorage(MatchMakingHistoryDdContext ddContext)
    {
        DdContext = ddContext;
    }

    public async Task AddMatchToHistory(MatchHistoryEntity entity)
    {
        await DdContext.Matches.InsertOneAsync(entity);
    }

}