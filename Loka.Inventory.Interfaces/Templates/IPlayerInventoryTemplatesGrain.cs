namespace Loka.Inventory.Interfaces.Templates;

public interface IPlayerInventoryTemplatesGrain : IGrainWithGuidKey
{
    Task<OperationResult<GetPlayerInventoryTemplates.Result>> GetInventoryTemplates();
    Task<OperationResult> UseTemplateAsPreset(UseTemplateAsPresetForm form);
}