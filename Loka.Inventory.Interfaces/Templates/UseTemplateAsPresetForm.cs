﻿namespace Loka.Inventory.Interfaces.Templates;

[GenerateSerializer]
public sealed class UseTemplateAsPresetForm
{
    [Id(0)]
    public Guid PresetId { get; init; }
    [Id(1)]
    public Guid TemplateId { get; init; }
}