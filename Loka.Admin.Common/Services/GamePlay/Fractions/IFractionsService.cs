﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Common.Models.GamePlay;
using Loka.Common.Operations.Results;

namespace Loka.Admin.Common.Services.GamePlay.Fractions;

public interface IFractionsService
{
    Task<OperationResult<FractionInformationModel>> GetById(Guid id, CancellationToken cancellationToken = default);
    Task<OperationResult<FractionInformationModel[]>> GetByIds(IReadOnlyList<Guid> ids, CancellationToken cancellationToken = default);
}