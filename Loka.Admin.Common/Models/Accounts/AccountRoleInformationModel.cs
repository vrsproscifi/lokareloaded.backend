﻿using Loka.Common.Entry;

namespace Loka.Admin.Common.Models.Accounts;

public sealed class AccountRoleInformationModel : INamedEntry
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;

    public override string ToString()
    {
        return $"Id: {Id} | Name: {Name}";
    }
}