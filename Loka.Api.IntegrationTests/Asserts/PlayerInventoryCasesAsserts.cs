﻿using Loka.Api.Features.Inventory.Cases;

namespace Loka.Api.IntegrationTests.Asserts;

public static class PlayerInventoryCasesAsserts
{
    public static void AssertResponse(this GetPlayerCases.Types.Response response)
    {
        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.That(response.Entries, Is.Not.Null, $"Invalid {nameof(response.Entries)} in {nameof(response)}");

        foreach (var casesEntry in response.Entries)
        {
            casesEntry.AssertModel();
        }
    }

    public static void AssertResponse(this OpenPlayerCase.Types.Response response)
    {
        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.Multiple(() =>
        {
            response.Case.AssertModel();

            Assert.That(response.Resources, Is.Not.Null, $"Invalid {nameof(response.Resources)}");
            Assert.That(response.Items, Is.Not.Null, $"Invalid {nameof(response.Items)}");

            foreach (var entry in response.Resources)
            {
                entry.AssertEntry();
            }

            foreach (var entry in response.Items)
            {
                entry.AssertEntry();
            }
        });
    }

    public static void AssertEntry(this OpenPlayerCase.Types.ItemEntry entry)
    {
        Assert.That(entry, Is.Not.Null, $"Invalid {nameof(entry)}");
        Assert.Multiple(() =>
        {
            Assert.That(entry.ModelId, Is.Not.Null, $"Invalid {nameof(entry)}.{nameof(entry.ModelId)}");
            Assert.That(entry.Amount, Is.GreaterThan(0), $"Invalid  {nameof(entry)}.{nameof(entry.Amount)}");
        });
    }

    public static void AssertEntry(this OpenPlayerCase.Types.ResourceEntry entry)
    {
        Assert.That(entry, Is.Not.Null, $"Invalid {nameof(entry)}");
        Assert.Multiple(() =>
        {
            Assert.That(entry.ModelId, Is.Not.Null, $"Invalid {nameof(entry)}.{nameof(entry.ModelId)}");
            Assert.That(entry.Amount, Is.GreaterThan(0), $"Invalid  {nameof(entry)}.{nameof(entry.Amount)}");
        });
    }

    public static void AssertModel(this PlayerInventoryCase entry)
    {
        Assert.That(entry, Is.Not.Null, $"Invalid {nameof(entry)}");
        Assert.Multiple(() =>
        {
            Assert.That(entry.Id, Is.Not.Null, $"Invalid {nameof(entry.Id)} in {nameof(entry)}");
            Assert.That(entry.ModelId, Is.Not.Null, $"Invalid {nameof(entry.ModelId)} in {nameof(entry)}");
            Assert.That(entry.Amount, Is.GreaterThanOrEqualTo(0), $"Invalid {nameof(entry.Amount)}");
        });
        
    }
}