﻿using System;

namespace Loka.MatchMaking.History.Entities;

public class MatchHistoryMatchInformation
{
    public Guid? WinnerTeamId { get; set; }
    public DateTimeOffset CreatedAt { get; set; }
    public DateTimeOffset StartedAt { get; set; }
    public DateTimeOffset FinishedAt { get; set; }
    public TimeSpan Duration { get; set; }
}