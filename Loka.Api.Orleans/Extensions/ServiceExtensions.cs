﻿using System;
using Loka.Api.Orleans.Services;
using Loka.Common.Random;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Orleans.Configuration;
using Orleans.Hosting;

namespace Loka.Api.Orleans.Extensions;

public static class ServiceExtensions
{
    public static IServiceCollection AddOrleansApiServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddSingleton<IRandomGenerator, RandomGenerator>();
        services.AddHostedService<SeedOrleansService>();
        services.AddHostedService<MatchMakingService>();
        return services;
    }
}