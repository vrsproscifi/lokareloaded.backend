﻿using AutoMapper;
using Loka.Common.Cache.DistributedCache;
using Loka.Database.Common.Repositories;
using Loka.Store.DataBase;
using Loka.Store.DataBase.Entities.Items;
using Loka.Store.Interfaces.Models.Items;
using Loka.Store.Interfaces.Repositories;

namespace Loka.Store.Grains.Items;

internal sealed class GameStoreItemCategoriesRepository : BaseRepository<StoreDbContext, GameItemCategoryEntity, StoreItemCategoryModel>, IGameStoreItemCategoriesRepository
{
    public GameStoreItemCategoriesRepository(ICacheProvider cacheProvider, StoreDbContext dbContext, IMapper mapper) : base(cacheProvider, dbContext, mapper)
    {
    }
}