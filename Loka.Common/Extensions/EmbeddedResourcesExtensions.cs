﻿using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Loka.Common.Extensions;

public static class EmbeddedResourcesExtensions
{
    /// <summary>
    /// Need use using var stream = GetResourceStream()
    /// </summary>
    public static Stream GetResourceStream(this Assembly assembly, string resourceName)
    {
        var resourceNames = assembly.GetManifestResourceNames();
        var resourcePath = resourceNames.FirstOrDefault(name => name.EndsWith(resourceName));
        if (string.IsNullOrWhiteSpace(resourcePath))
            throw new FileNotFoundException($"Resource with name {resourceName} not found in assembly {assembly.GetName().Name}", resourceName);

        var stream = assembly.GetManifestResourceStream(resourcePath);
        if (stream == null)
            throw new FileNotFoundException($"Resource with path {resourcePath} not found in assembly {assembly.GetName().Name}", resourceName);

        return stream;
    }

    public static string ReadResource(this Assembly assembly, string resourceName)
    {
        using var stream = assembly.GetResourceStream(resourceName);
        using var reader = new StreamReader(stream);
        var result = reader.ReadToEnd();
        return result;
    }

    public static Task<string> ReadResourceAsync(this Assembly assembly, string resourceName)
    {
        using var stream = assembly.GetResourceStream(resourceName);
        using var reader = new StreamReader(stream);
        return reader.ReadToEndAsync();
    }
}