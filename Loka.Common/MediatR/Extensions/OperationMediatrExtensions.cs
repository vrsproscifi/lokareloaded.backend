﻿using System;
using System.Collections.Generic;
using System.Text;
using Loka.Common.MediatR.Executors;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Loka.Common.Operations;

namespace Loka.Common.MediatR.Extensions;

public static partial class OperationMediatrExtensions
{
    public static IServiceCollection AddOperationExecutor(this IServiceCollection serviceCollection)
    {
        serviceCollection.AddMediatR(typeof(OperationMediatrExtensions));
            
        serviceCollection.RegisterOperationExceptionHandlers();
        serviceCollection.AddScoped<IOperationExecutor, OperationExecutor>();
        return serviceCollection;
    }
}