﻿using System.Linq.Expressions;
using Loka.Database.Common.Repositories;
using Loka.Inventory.DataBase;
using Loka.Inventory.DataBase.Entities.Boosters;
using Loka.Inventory.Interfaces.Boosters;

namespace Loka.Inventory.Grains.Boosters;

internal sealed class PlayerInventoryBoosterRepository : BaseChildRepository<InventoryDbContext, PlayerBoosterEntity, PlayerInventoryBoosterModel>.Raw, IPlayerInventoryBoosterRepository
{
    protected override Func<Guid, Expression<Func<PlayerBoosterEntity, bool>>> Predicate { get; } = playerId => entity => entity.PlayerId == playerId;
    public PlayerInventoryBoosterRepository(InventoryDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
    {
    }

    public async Task<PlayerInventoryBoosterModel> AddOrStack(Guid playerId, AddOrStackInventoryBooster.Request request)
    {
        var entity = await DbSet
            .Where(Predicate(playerId))
            .FirstOrDefaultAsync(q => q.BoosterId == request.ModelId);

        if (entity == null)
        {
            entity = new PlayerBoosterEntity()
            {
                EntityId = Guid.NewGuid(),
                PlayerId = playerId,
                BoosterId = request.ModelId,
                Amount = request.Amount,
                CreatedAt = DateTimeOffset.UtcNow,
                IsActive = false
            };
            await DbSet.AddAsync(entity);
            await SaveChanges();
        }
        else
        {
            entity.Amount += request.Amount;
            await SaveChanges();
        }
        
        return new PlayerInventoryBoosterModel()
        {
            Id = entity.EntityId,
            ModelId = entity.BoosterId,
            Amount = entity.Amount
        };
    }

    public async Task<OperationResult<PlayerInventoryBoosterModel>> Activate(Guid playerId, ActivatePlayerBooster.Request request)
    {
        var entity = await DbSet
            .Where(Predicate(playerId))
            .FirstOrDefaultAsync(q => q.EntityId == request.Id);

        if (entity == null)
            return (OperationResultCode.EntityNotFound, request.Id);

        entity.IsActive = true;
        await SaveChanges();

        return await GetById(playerId, request.Id);
    }

    public async Task<OperationResult<PlayerInventoryBoosterModel>> Deactivate(Guid playerId, DeactivatePlayerBooster.Request request)
    {
        var entity = await DbSet
            .Where(Predicate(playerId))
            .FirstOrDefaultAsync(q => q.EntityId == request.Id);

        if (entity == null)
            return (OperationResultCode.EntityNotFound, request.Id);

        entity.IsActive = false;
        await SaveChanges();

        return await GetById(playerId, request.Id);
    }

    public Task<int> CountActivated(Guid playerId)
    {
        return DbSet
            .Where(Predicate(playerId))
            .Where(q => q.IsActive)
            .CountAsync();
    }


}