﻿using System;
using MediatR.Pipeline;
using Loka.Common.MediatR.Requests;
using Loka.Common.Operations.Results;

namespace Loka.Common.MediatR.ExceptionHandlers;

public interface IOperationExceptionHandler<in TRequest, TResult, TException>
    : IRequestExceptionHandler<TRequest, OperationResult<TResult>, TException>
    where TRequest : IOperationRequest<TResult>
    where TException : Exception
{

}

public interface IOperationExceptionHandler<in TRequest, TException>
    : IRequestExceptionHandler<TRequest, OperationResult, TException>
    where TRequest : IOperationRequest
    where TException : Exception
{

}