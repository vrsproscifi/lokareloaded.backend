﻿namespace Loka.Inventory.Interfaces.Resources;

public static class GetPlayerResources
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<Result>
    {
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public IReadOnlyList<PlayerResourceModel.Lightweight> Entries { get; init; } = null!;
    }
}