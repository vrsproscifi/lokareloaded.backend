﻿using Loka.Chat.Interfaces.Models;
using Loka.Common.Operations.Results;
using MongoDB.Bson;

namespace Loka.Chat.Interfaces;

public interface IGameChatGrain : IGrainWithGuidKey
{
    Task<OperationResult<ChannelEntry>> GetChannel(ObjectId channelId);
    Task<OperationResult<SendChatMessage.Result>> SendChatMessage(SendChatMessage.Request request);
    Task<GetChatChannels.Result> GetChatChannels(GetChatChannels.Request request);
    Task<OperationResult<GetChatMessages.Result>> GetChatMessages(GetChatMessages.Request request);
    Task<CreatePrivateChannel.Result> CreatePrivateChannel(CreatePrivateChannel.Request request);
    Task<OperationResult<LeaveFromChannel.Result>> LeaveFromChannel(LeaveFromChannel.Request request);
    Task<OperationResult<KickFromChannel.Result>> KickFromChannel(KickFromChannel.Request request);
    Task<OperationResult<AssignRoleForChannelMember.Result>> AssignRole(AssignRoleForChannelMember.Request request);
    Task<OperationResult<GetChatMembers.Result>> GetChatMembers(GetChatMembers.Request request);
    Task<OperationResult<GetChatMember.Result>> GetChatMember(GetChatMember.Request request);
}