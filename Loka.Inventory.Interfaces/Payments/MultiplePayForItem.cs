﻿namespace Loka.Inventory.Interfaces.Payments;

public sealed class MultiplePayForItem
{
    [GenerateSerializer]
    public sealed class Request
    {
        [Id(0)]
        public Guid ModelId { get; init; }
        
        [Id(1)]
        public int Count { get; init; }

        [Id(2)]
        public IReadOnlyList<Entry> Resources { get; init; } = ArraySegment<Entry>.Empty;

        [GenerateSerializer]
        public sealed class Entry
        {
            [Id(0)]
            public Guid ModelId { get; init; }

            [Id(1)]
            public long Amount { get; init; }
        }
    }
}