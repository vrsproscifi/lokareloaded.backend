﻿using Loka.Common.Operations.Codes;

namespace Loka.Api.IntegrationTests.Tests.Inventory;

internal sealed class PlayerInventoryPresetsTests : BaseTest
{
    [Test]
    public async Task EquipItems()
    {
        var armour = Storages.GameStore.GetProduct(StoreItemCategory.Types.Type.Armour);
        var weapon = Storages.GameStore.GetProduct(StoreItemCategory.Types.Type.Weapon);
        var playerSession = await Services.PlayerIdentity.QuickSignUp();
        var item1 = await Services.PlayerInventoryItems.BuyItem(playerSession, weapon);
        var item2 = await Services.PlayerInventoryItems.BuyItem(playerSession, weapon);
        var item3 = await Services.PlayerInventoryItems.BuyItem(playerSession, armour);
        var boughtPreset = await Services.PlayerInventoryPresets.BuyInventoryPreset(playerSession, Storages.GameStore.Presets[1]);

        {
            await Services.PlayerInventoryPresets.EquipInventoryItem(playerSession, boughtPreset, item1);
            var presets = await Services.PlayerInventoryPresets.GetPlayerInventoryPresets(playerSession);
            var preset = presets.First(q => q.Id.Equals(boughtPreset.Id));
            Assert.That(preset.Items, Has.Count.EqualTo(1), $"Expected for only one equipped item");

            var presetItem = preset.Items.FirstOrDefault(i => i.ItemId.Equals(item1.Id));
            Assert.That(presetItem, Is.Not.Null, $"Equipped item ({item1.Id}) not found");
        }

        {
            await Services.PlayerInventoryPresets.EquipInventoryItem(playerSession, boughtPreset, item2);
            var presets = await Services.PlayerInventoryPresets.GetPlayerInventoryPresets(playerSession);
            var preset = presets.First(q => q.Id.Equals(boughtPreset.Id));
            Assert.That(preset.Items, Has.Count.EqualTo(1), $"Expected for only one equipped item");

            var presetItem = preset.Items.FirstOrDefault(i => i.ItemId.Equals(item2.Id));
            Assert.That(presetItem, Is.Not.Null, $"Equipped item ({item2.Id}) not found");
        }

        {
            await Services.PlayerInventoryPresets.EquipInventoryItem(playerSession, boughtPreset, item3);
            var presets = await Services.PlayerInventoryPresets.GetPlayerInventoryPresets(playerSession);
            var preset = presets.First(q => q.Id.Equals(boughtPreset.Id));
            Assert.That(preset.Items, Has.Count.EqualTo(2), $"Expected for only one equipped item");

            var presetItem = preset.Items.FirstOrDefault(i => i.ItemId.Equals(item3.Id));
            Assert.That(presetItem, Is.Not.Null, $"Equipped item ({item3.Id}) not found");
        }
    }

    [Test]
    public async Task BuyNextPresets()
    {
        var playerSession = await Services.PlayerIdentity.SignUp();
        var presetsBefore = await Services.PlayerInventoryPresets.GetPlayerInventoryPresets(playerSession);
        Assert.That(presetsBefore[0].ModelId, Is.EqualTo(Storages.GameStore.Presets[0].Id), $"Invalid first preset");

        await Services.PlayerInventoryPresets.BuyInventoryPreset(playerSession, Storages.GameStore.Presets[1]);
        await Services.PlayerInventoryPresets.BuyInventoryPreset(playerSession, Storages.GameStore.Presets[2]);

        var presetsAfter = await Services.PlayerInventoryPresets.GetPlayerInventoryPresets(playerSession);
        Assert.Multiple(() =>
        {
            Assert.That(presetsAfter[0].ModelId, Is.EqualTo(Storages.GameStore.Presets[0].Id), $"Invalid 0 preset");
            Assert.That(presetsAfter[1].ModelId, Is.EqualTo(Storages.GameStore.Presets[1].Id), $"Invalid 1 preset");
            Assert.That(presetsAfter[2].ModelId, Is.EqualTo(Storages.GameStore.Presets[2].Id), $"Invalid 2 preset");
        });
    }

    [Test]
    public async Task TwiceBuyFirstPreset()
    {
        var playerSession = await Services.PlayerIdentity.SignUp();

        var exception = Assert.ThrowsAsync<RpcException>(async () => { await Services.PlayerInventoryPresets.BuyInventoryPreset(playerSession, Storages.GameStore.Presets[0]); });
        exception.AssertResultCode(OperationResultCode.PresetAlreadyBought);
    }

    [Test]
    public async Task TwiceBuySecondPreset()
    {
        var playerSession = await Services.PlayerIdentity.SignUp();
        await Services.PlayerInventoryPresets.BuyInventoryPreset(playerSession, Storages.GameStore.Presets[1]);

        var exception = Assert.ThrowsAsync<RpcException>(async () => { await Services.PlayerInventoryPresets.BuyInventoryPreset(playerSession, Storages.GameStore.Presets[1]); });
        exception.AssertResultCode(OperationResultCode.PresetAlreadyBought);
    }

    [Test]
    public async Task TwiceBuyThirdPreset()
    {
        var playerSession = await Services.PlayerIdentity.SignUp();
        await Services.PlayerInventoryPresets.BuyInventoryPreset(playerSession, Storages.GameStore.Presets[1]);
        await Services.PlayerInventoryPresets.BuyInventoryPreset(playerSession, Storages.GameStore.Presets[2]);

        var exception = Assert.ThrowsAsync<RpcException>(async () => { await Services.PlayerInventoryPresets.BuyInventoryPreset(playerSession, Storages.GameStore.Presets[2]); });
        exception.AssertResultCode(OperationResultCode.PresetAlreadyBought);
    }
}