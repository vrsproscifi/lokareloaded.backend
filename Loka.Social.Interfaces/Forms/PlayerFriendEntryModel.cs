﻿using Loka.Game.Interfaces.Players.Dtos;

namespace Loka.Social.Interfaces.Forms;

[GenerateSerializer]
public sealed class PlayerFriendEntryModel
{
    [Id(0)]
    public Guid FriendId { get; set; }

    [Id(1)]
    public PlayerModel Player { get; set; } = null!;
}