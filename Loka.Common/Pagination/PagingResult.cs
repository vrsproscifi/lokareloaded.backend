﻿using System.Collections.Generic;
using System.Diagnostics;

namespace Loka.Common.Pagination;

[GenerateSerializer]
[DebuggerDisplay("Total: {Total}, Count: {Take}, Offset: {Offset}")]
public class PagingResult
{
    public PagingResult(int total, int take, int offset)
    {
        Total = total;
        Take = take;
        Offset = offset;
    }

    public PagingResult(long total, PagingQuery query) : this((int)total, query.Take, query.Offset)
    {
    }


    public PagingResult(int total, PagingQuery query) : this(total, query.Take, query.Offset)
    {
    }

    [Id(0)]
    public int Total { get; }

    [Id(1)]
    public int Take { get; }

    [Id(2)]
    public int Offset { get; }
}

[GenerateSerializer]
[DebuggerDisplay("Items: {Items.Count}, Total: {Total}, Count: {Take}, Offset: {Offset}")]
public class PagingResult<T> : PagingResult
{
    public PagingResult(IReadOnlyList<T> items, int total, int take, int offset) : base(total, take, offset)
    {
        Items = items;
    }

    public PagingResult(IReadOnlyList<T> items, int total, PagingQuery query) : base(total, query)
    {
        Items = items;
    }

    [Id(3)]
    public IReadOnlyList<T> Items { get; }
}