﻿using System;
using System.Collections;
using Loka.Common.ClickHouse.Attributes;

namespace Loka.Common.ClickHouse.Entities;

public abstract class ClickHouseEntity<TTable> : IClickHouseEntity, IEnumerable
    where TTable : ClickHouseEntity<TTable>
{
    public static ClickhouseTable Table { get; } = new ClickhouseTable(typeof(TTable));

    [ClickhouseTableColumn(nameof(Id), ClickhouseColumnType.UUID)]
    public Guid Id { get; set; }

    [ClickhouseTableColumn(nameof(CreatedAt), ClickhouseColumnType.DateTime)]
    public DateTime CreatedAt { get; set; }

    public IEnumerator GetEnumerator()
    {
        foreach (var column in Table.ColumnsForInsert)
        {
            yield return column.Property.GetValue(this);
        }
    }
}