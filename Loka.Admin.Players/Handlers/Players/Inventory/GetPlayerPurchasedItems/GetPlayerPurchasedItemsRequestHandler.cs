﻿using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Common.Models.Store;
using Loka.Admin.Common.Services.Store.ItemModels;
using Loka.Admin.Players.Specifications.Inventory;
using Loka.Common.MediatR.Handlers;
using Loka.Common.Operations.Mapping;
using Loka.Common.Operations.Results;
using Loka.Common.Pagination;
using Loka.Common.Ranges.Numerics;
using Loka.Database.Common.Specifications;
using Loka.Inventory.DataBase;
using Loka.Inventory.DataBase.Entities.Items;
using Microsoft.EntityFrameworkCore;

namespace Loka.Admin.Players.Handlers.Players.Inventory.GetPlayerPurchasedItems;

internal sealed class GetPlayerPurchasedItemsRequestHandler : IOperationHandler<GetPlayerPurchasedItemsRequest, PagingResult<GetPlayerPurchasedItemsResponse>>
{
    private InventoryDbContext InventoryDbContext { get; }
    private IGameItemModelService GameItemModelService { get; }
    private IOperationMappingProfile<PlayerItemEntity, GetPlayerPurchasedItemsResponse>[] MappingProfiles { get; }

    public GetPlayerPurchasedItemsRequestHandler(InventoryDbContext inventoryDbContext, IGameItemModelService gameItemModelService)
    {
        InventoryDbContext = inventoryDbContext;
        GameItemModelService = gameItemModelService;
        MappingProfiles = new[]
        {
            OperationMappingProfile<PlayerItemEntity, GetPlayerPurchasedItemsResponse, GameItemInformationModel>.Create
            (
                response => response.ModelId,
                (response, model) => response.Model = model,
                GameItemModelService.GetByIds
            )
        };
    }

    public async Task<OperationResult<PagingResult<GetPlayerPurchasedItemsResponse>>> Handle(GetPlayerPurchasedItemsRequest request, CancellationToken cancellationToken)
    {
        var query = InventoryDbContext
            .PlayerPurchasedItemEntity
            .Where(new PlayerChildSpecification<PlayerItemEntity>(request.PlayerId))
            .Where(new PlayerPurchasedItemAmountSpecification(request.Amount, NumericRangeBeginOptions.GreatOrEqual, NumericRangeEndOptions.LessOrEqual))
            .Where(new PlayerPurchasedItemUpgradeSpecification(request.Upgrade, NumericRangeBeginOptions.GreatOrEqual, NumericRangeEndOptions.LessOrEqual));

        var total = await query.WithPaging(request.Paging)
            .CountAsync(cancellationToken);

        var entities = await query
            .WithPaging(request.Paging)
            .ToArrayAsync(cancellationToken);

        var mapModelsResult = await entities.OperationMap(item => new GetPlayerPurchasedItemsResponse
        {
            Id = item.EntityId,
            Amount = item.Amount,
            Level = item.Level,
            PurchasedAt = item.CreatedAt,
            LastUseDate = item.LastUseDate
        }, MappingProfiles, cancellationToken);

        if (mapModelsResult.IsFailed)
            return mapModelsResult.Error;

        return new PagingResult<GetPlayerPurchasedItemsResponse>(mapModelsResult.Result, total, request.Paging);
    }
}