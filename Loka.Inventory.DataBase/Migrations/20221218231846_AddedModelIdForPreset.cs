﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Loka.Inventory.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class AddedModelIdForPreset : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsActive",
                schema: "Inventory",
                table: "PlayerInventoryPresetEntity",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "ModelId",
                schema: "Inventory",
                table: "PlayerInventoryPresetEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsActive",
                schema: "Inventory",
                table: "PlayerInventoryPresetEntity");

            migrationBuilder.DropColumn(
                name: "ModelId",
                schema: "Inventory",
                table: "PlayerInventoryPresetEntity");
        }
    }
}
