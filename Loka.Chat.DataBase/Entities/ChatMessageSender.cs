﻿using System.ComponentModel.DataAnnotations;

namespace Loka.Chat.DataBase.Entities;

public sealed class ChatMessageSender
{
    public Guid Id { get; init; }

    [MaxLength(128)]
    public string Name { get; init; } = null!;
}