﻿using Loka.Common.Entry;
using Loka.Store.Interfaces.Enums;

namespace Loka.Store.Interfaces.Models.Items;

[GenerateSerializer]
public sealed class StoreItemCategoryModel : IEntry, INamedEntry
{
    [Id(0)]
    public Guid Id { get; set; }
    
    [Id(1)]
    public string Name { get; set; } = null!;
    
    [Id(2)]
    public ItemCategoryTypeId TypeId { get; set; }
    
    
    public override string ToString()
    {
        return $"Id: {Id} | {TypeId} | {Name}";
    }
}