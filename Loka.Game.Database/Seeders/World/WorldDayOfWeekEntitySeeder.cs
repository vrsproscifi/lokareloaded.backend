﻿using Loka.Game.Database.Entities.World;
using Loka.Game.Database.Enums.World;

namespace Loka.Game.Database.Seeders.World;

public static class WorldDayOfWeekEntitySeeder
{
    public static Guid Everyday { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe101");
    public static Guid Monday { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe102");
    public static Guid Tuesday { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe103");
    public static Guid Wednesday { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe104");
    public static Guid Thursday { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe105");
    public static Guid Friday { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe106");
    public static Guid Saturday { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe107");
    public static Guid Sunday { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe108");

    public static WorldDayOfWeekEntity[] Entities { get; } = new[]
    {
        new WorldDayOfWeekEntity(Everyday, WorldDayOfWeekTypeId.Everyday),
        new WorldDayOfWeekEntity(Monday, WorldDayOfWeekTypeId.Monday),
        new WorldDayOfWeekEntity(Tuesday, WorldDayOfWeekTypeId.Tuesday),
        new WorldDayOfWeekEntity(Wednesday, WorldDayOfWeekTypeId.Wednesday),
        new WorldDayOfWeekEntity(Thursday, WorldDayOfWeekTypeId.Thursday),
        new WorldDayOfWeekEntity(Friday, WorldDayOfWeekTypeId.Friday),
        new WorldDayOfWeekEntity(Saturday, WorldDayOfWeekTypeId.Saturday),
        new WorldDayOfWeekEntity(Sunday, WorldDayOfWeekTypeId.Sunday),
    };
}