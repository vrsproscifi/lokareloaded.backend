﻿using System.Collections.Generic;

namespace Loka.Common.Cache.Single;

public interface IDataCache<TKey, TData>
{
    bool Exists(TKey id);
    bool Exists(IReadOnlyList<TKey> ids);
    bool Exists(IReadOnlyList<TKey> ids, out List<TKey> notFoundIds);
        
    bool TryGet(TKey id, out TData model);
    bool TryGet(IReadOnlyList<TKey> ids, out List<TData> models, out List<TKey> notFoundIds);

    TData Put(TData model);
    void Put(IReadOnlyList<TData> models);
}