﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Loka.Common.Reflection;

public static class GenericReflectionExtensions
{
    private static string[] ExceptionList { get; } = new[]
    {
        "Microsoft.VisualStudio",
    };

    public static Assembly[] GetAssemblies()
    {
        return AppDomain.CurrentDomain.GetAssemblies()
            .Where(q => q.IsDynamic == false && !string.IsNullOrWhiteSpace(q.FullName) && ExceptionList.All(except => q.FullName!.StartsWith(except) == false))
            .ToArray();
    }
        
    public static Type[] GetLoadedTypes()
    {
        return GetAssemblies()
            .SelectMany(q => q.GetTypes())
            .ToArray();
    }

    public static bool IsImplementedInterface<TInterface>(this Type type)
    {
        return type.IsImplementedInterface(typeof(TInterface));
    }     
        
    public static bool IsImplementedInterface(this Type type, Type implemented)
    {
        return type.GetInterfaces().Any(x => x == implemented);
    }

    public static bool IsImplementedClass<TClass>(this Type type)
    {
        return type.IsSubclassOf(typeof(TClass));
    }

    public static bool IsImplementedGenericInterface(this Type type, Type interfaceType)
    {
        return type.GetInterfaces().Any(x =>
            x.IsGenericType &&
            x.GetGenericTypeDefinition() == interfaceType);
    }

    public static Type GetImplementedInterface(this Type type, Type interfaceType)
    {
        return type.GetInterfaces().First(x =>
            x.IsGenericType &&
            x.GetGenericTypeDefinition() == interfaceType);
    }

    public static Type[] GetGenericArguments(this Type type, Type interfaceType)
    {
        return type.GetImplementedInterface(interfaceType)?.GetGenericArguments() ?? Array.Empty<Type>();
    }

    /// <summary>
    /// Extract the "real" type T from Nullable if required
    /// </summary>
    public static Type UnwrapNullableType(this Type type)
    {
        if (type is null)
        {
            return null;
        }

        return Nullable.GetUnderlyingType(type) ?? type;
    }
}