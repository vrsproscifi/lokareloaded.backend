﻿using MongoDB.Bson;

namespace Loka.Chat.Interfaces.Models;

public static class GetChatChannels
{
    [GenerateSerializer]
    public sealed class Request
    {

    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public IReadOnlyList<ChannelEntry> Entries { get; init; } = ArraySegment<ChannelEntry>.Empty;
    }
}