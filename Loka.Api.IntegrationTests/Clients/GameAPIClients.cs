﻿using Loka.Api.Features;
using Loka.Api.Features.Identity;
using Loka.Api.Features.Inventory.Boosters;
using Loka.Api.Features.Inventory.Cases;
using Loka.Api.Features.Inventory.Items;
using Loka.Api.Features.Inventory.Presets;
using Loka.Api.Features.Inventory.Resources;
using Loka.Api.Features.Inventory.Templates;

namespace Loka.Api.IntegrationTests.Clients;

public sealed class GameAPIClients
{
    public GrpcChannel GrpcChannel { get; }
    public PlayerIdentity.PlayerIdentityClient PlayerIdentityClient { get; }
    public PlayerInventoryItems.PlayerInventoryItemsClient PlayerInventoryItemsClient { get; }
    public PlayerInventoryResources.PlayerInventoryResourcesClient PlayerInventoryResourcesClient { get; }
    public PlayerInventoryTemplates.PlayerInventoryTemplatesClient PlayerInventoryTemplatesClient { get; }
    public PlayerInventoryPresets.PlayerInventoryPresetsClient PlayerInventoryPresetsClient { get; }
    public PlayerInventoryCases.PlayerInventoryCasesClient PlayerInventoryCasesClient { get; }
    public PlayerInventoryBoosters.PlayerInventoryBoostersClient PlayerInventoryBoostersClient { get; }
    public PlayerWorkbench.PlayerWorkbenchClient PlayerWorkbenchClient { get; }
    public MatchMakingQueue.MatchMakingQueueClient PlayerMatchMakingClient { get; }
    public GameStore.GameStoreClient GameStoreClient { get; }
    public GameChat.GameChatClient GameChatClient { get; }

    public GameAPIClients(GrpcChannel grpcChannel)
    {
        GrpcChannel = grpcChannel;

        PlayerIdentityClient = new PlayerIdentity.PlayerIdentityClient(GrpcChannel);
        PlayerInventoryItemsClient = new PlayerInventoryItems.PlayerInventoryItemsClient(GrpcChannel);
        PlayerWorkbenchClient = new PlayerWorkbench.PlayerWorkbenchClient(GrpcChannel);
        PlayerMatchMakingClient = new MatchMakingQueue.MatchMakingQueueClient(GrpcChannel);
        PlayerInventoryCasesClient = new PlayerInventoryCases.PlayerInventoryCasesClient(GrpcChannel);
        PlayerInventoryResourcesClient = new PlayerInventoryResources.PlayerInventoryResourcesClient(GrpcChannel);
        PlayerInventoryTemplatesClient = new PlayerInventoryTemplates.PlayerInventoryTemplatesClient(GrpcChannel);
        PlayerInventoryPresetsClient = new PlayerInventoryPresets.PlayerInventoryPresetsClient(GrpcChannel);
        PlayerInventoryBoostersClient = new PlayerInventoryBoosters.PlayerInventoryBoostersClient(GrpcChannel);
        GameChatClient = new GameChat.GameChatClient(GrpcChannel);

        GameStoreClient = new GameStore.GameStoreClient(GrpcChannel);
    }
}