﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ClickHouse.Ado;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Loka.Common.ClickHouse.Entities;
using Loka.Common.Extensions;

namespace Loka.Common.ClickHouse.Repositories;

public sealed class ClickHouseRepositoryConstructor<TEntity>
    where TEntity : ClickHouseEntity<TEntity>, new()
{
    internal ILogger< ClickHouseRepository<TEntity>> Logger { get; }
    internal IServiceProvider ServiceProvider { get; }
        
    public ClickHouseRepositoryConstructor(ILogger< ClickHouseRepository<TEntity>> logger, IServiceProvider serviceProvider)
    {
        Logger = logger;
        ServiceProvider = serviceProvider;
    }
}
    
public abstract class ClickHouseRepository<TEntity> : IClickHouseRepository
    where TEntity : ClickHouseEntity<TEntity>, new()
{
    private static TimeSpan Timeout { get; } = TimeSpan.FromSeconds(5);
        
    protected ClickhouseTable Table { get; } = ClickHouseEntity<TEntity>.Table;
    protected ILogger<ClickHouseRepository<TEntity>> Logger { get; }

    private IServiceProvider ServiceProvider { get; }
        
    public ClickHouseRepository(ClickHouseRepositoryConstructor<TEntity> constructor)
    {
        Logger = constructor.Logger;
        ServiceProvider = constructor.ServiceProvider;
    }

    public bool Insert(TEntity entity)
    {
        try
        {
            using var provider = ServiceProvider.CreateScope();
            var connection = provider.ServiceProvider.GetRequiredService<ClickHouseConnection>();
            connection.Open();
                
            using var command = connection.CreateCommand();
            command.CommandText = Table.InsertQuery;
            command.Parameters.Add(new ClickHouseParameter
            {
                ParameterName = "bulk",
                Value = new object[] {entity}
            });

            command.ExecuteNonQuery();
            return true;
        }
        catch (Exception e)
        {
            Logger.LogError($"Failed {nameof(Insert)}({entity.Id}) to {Table.Engine.Name}, Exception: {e}");
            return false;
        }
    }

    public async Task<bool> InsertAsync(TEntity entity)
    {
        using var cancellationTokenSource = new CancellationTokenSource(Timeout);
        return await Task.Run(() => Insert(entity), cancellationTokenSource.Token);
    }

    public IReadOnlyList<TEntity> Select(string? where = default, string? orderBy = default, int? skip = default, int? limit = default)
    {
        return Select(where, orderBy, skip, limit, Table.ColumnsNames);
    }
        
    public async Task<IReadOnlyList<TEntity>> SelectAsync(string? where = default, string? orderBy = default, int? skip = default, int? limit = default)
    {
        using var cancellationTokenSource = new CancellationTokenSource(Timeout);
        return await Task.Run(() => Select(where, orderBy, skip, limit, Table.ColumnsNames), cancellationTokenSource.Token);
    }

    public IReadOnlyList<TEntity> Select(int? skip = default, int? limit = default, params string[] propertyNames)
    {
        return Select(string.Empty, string.Empty, skip, limit, propertyNames);
    }

    public IReadOnlyList<TEntity> Select(string? where, string? orderBy = default, int? skip = default, int? limit = default, params string[] propertyNames)
    {
        using var provider = ServiceProvider.CreateScope();
        var connection = provider.ServiceProvider.GetRequiredService<ClickHouseConnection>();
        connection.Open();

        using var command = connection.CreateCommand();
        command.CommandText = $"SELECT {propertyNames.CommaEnumeration()} FROM {Table.Engine.Name}";
        if (!string.IsNullOrWhiteSpace(where))
            command.CommandText += $" WHERE {where}";

        if (!string.IsNullOrWhiteSpace(orderBy))
            command.CommandText += $" Order By {orderBy}";

        if (limit.HasValue)
        {
            if (skip.HasValue)
                command.CommandText += $" LIMIT {skip},{limit}";
            else
                command.CommandText += $" LIMIT {limit}";
        }

        using var reader = command.ExecuteReader();

        var values = new List<TEntity>(16);
        var properties = Table.Columns
            .Where(q => propertyNames.Contains(q.Name))
            .ToDictionary(q => q.Name, q => q);

        reader.ReadAll(r =>
        {
            var entity = Activator.CreateInstance<TEntity>();
            foreach (var propertyName in propertyNames.WithIndex())
            {
                var value = r.GetValue(propertyName.index);
                properties[propertyName.item].Property.SetValue(entity, value);
            }

            values.Add(entity);
        });

        return values;
    }

    public IReadOnlyList<TTuple> Select<TTuple>(Func<IDataReader, TTuple> selector, string? where = default, params string[] propertyNames)
    {
        using var provider = ServiceProvider.CreateScope();
        var connection = provider.ServiceProvider.GetRequiredService<ClickHouseConnection>();
        connection.Open();

        using var command = connection.CreateCommand();            
        command.CommandText = $"SELECT {propertyNames.CommaEnumeration()} FROM {Table.Engine.Name}";
        if (!string.IsNullOrWhiteSpace(where))
            command.CommandText += $" WHERE {where}";

        using var reader = command.ExecuteReader();

        var values = new List<TTuple>(16);
        reader.ReadAll(r => values.Add(selector(r)));

        return values;
    }

    public bool DropTable()
    {
        Logger.LogInformation($"> Begin {nameof(DropTable)} {Table.Engine.Name}");

        try
        {
            using var provider = ServiceProvider.CreateScope();
            var connection = provider.ServiceProvider.GetRequiredService<ClickHouseConnection>();
            connection.Open();

            using var command = connection.CreateCommand();
            command.CommandText = Table.DropQuery;
            command.ExecuteNonQuery();
            return true;
        }
        catch (Exception e)
        {
            Logger.LogError($"Failed {nameof(DropTable)} to {Table.Engine.Name}, Exception: {e}");
            return false;
        }
    }

    public bool CreateTable()
    {
        Logger.LogInformation($"> Begin check exist table {Table.Engine.Name}");

        try
        {
            using var provider = ServiceProvider.CreateScope();
            var connection = provider.ServiceProvider.GetRequiredService<ClickHouseConnection>();
            connection.Open();

            using var command = connection.CreateCommand();
            command.CommandText = Table.CreateQuery;
/*
                var names = new[]
                {
                    "TopTemperature",
                    "TopHumidity",
                    "TopHeatIndex",
                    
                    "MiddleTemperature",
                    "MiddleHumidity",
                    "MiddleHeatIndex",
                    
                    "BottomTemperature",
                    "BottomHumidity",
                    "BottomHeatIndex",
                    
                    
                    //"GasCO",
                    //"GasCO2",
                    
                    "CreatedAt",
                };
                
                foreach (var name in names)
                {
                    command.CommandText = $"ALTER TABLE MushroomFarm.FarmRoomStatistics MODIFY COLUMN {name} DateTime CODEC(T64, LZ4 )";
                    command.ExecuteNonQuery();
                }*/

            Logger.LogInformation($"> Successfully create table {Table.Engine.Name}");
            return true;
        }
        catch (Exception e)
        {
            Logger.LogError($"Failed {nameof(CreateTable)} to {Table.Engine.Name}, Exception: {e}");
            return false;
        }
    }
}