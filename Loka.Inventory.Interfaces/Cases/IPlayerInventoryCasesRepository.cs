﻿using Loka.Database.Common.Repositories;

namespace Loka.Inventory.Interfaces.Cases;

public interface IPlayerInventoryCasesRepository : IBaseChildRepository<PlayerInventoryCaseModel>
{
    Task<PlayerInventoryCaseModel> AddOrStack(Guid playerId, AddOrStackInventoryCase.Request request);
}