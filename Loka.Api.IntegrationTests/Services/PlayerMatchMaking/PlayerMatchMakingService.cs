﻿using Loka.Api.Features;
using Loka.Api.IntegrationTests.Clients;

namespace Loka.Api.IntegrationTests.Services.PlayerMatchMaking;

public sealed class PlayerMatchMakingService
{
    private MatchMakingQueue.MatchMakingQueueClient MakingQueueClient { get; }

    public PlayerMatchMakingService(GameAPIClients clients)
    {
        MakingQueueClient = clients.PlayerMatchMakingClient;
    }

    public async Task<JoinToMatchMakingQueueModel> JoinToMatchMakingQueue()
    {
        var result = await MakingQueueClient.JoinToMatchMakingQueueAsync(new JoinToMatchMakingQueueQuery()
        {

        });

        Assert.NotNull(result, $"Failed {nameof(JoinToMatchMakingQueue)}");
        return result;
    }
}