﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Global.Identity.Database.Migrations
{
    /// <inheritdoc />
    public partial class UserAccountEntityChanges : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastActivityDate",
                schema: "GlobalIdentity",
                table: "UserAccountEntity");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastActivityDate",
                schema: "GlobalIdentity",
                table: "UserAccountEntity",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));
        }
    }
}
