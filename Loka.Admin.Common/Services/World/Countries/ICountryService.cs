﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Common.Models.Countries;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Storage;

namespace Loka.Admin.Common.Services.World.Countries;

public interface ICountryService : IEntryStorage<CountryInformationModel>
{

}