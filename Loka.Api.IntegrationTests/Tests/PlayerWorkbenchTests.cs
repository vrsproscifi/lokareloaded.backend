﻿namespace Loka.Api.IntegrationTests.Tests;

internal sealed class PlayerWorkbenchTests : BaseTest
{
    [Test]
    [TestCase(StoreItemCategory.Types.Type.Weapon)]
    public async Task CraftNotStackable(StoreItemCategory.Types.Type type)
    {
        var playerSession = await Services.PlayerIdentity.SignUp();
        await Task.Delay(2000);

        var product = Storages.GameStore.GetProductWithBlueprint(q => q.Category.Type == type);
        var productBlueprint = product!.Blueprints.First(q => q.Depends.Any());

        var resourcesBefore = await Services.PlayerInventoryResources.GetPlayerResources(playerSession);
        var craftedItemFromWorkbench1 = await Services.PlayerInventoryItems.CraftWorkbenchItem(playerSession, product.Id);
        Assert.That(craftedItemFromWorkbench1.Amount, Is.EqualTo(1), $"Invalid {nameof(craftedItemFromWorkbench1)}.{nameof(craftedItemFromWorkbench1.Amount)}");
        var resourcesAfter1 = await Services.PlayerInventoryResources.GetPlayerResources(playerSession);

        var craftedItemFromWorkbench2 = await Services.PlayerInventoryItems.CraftWorkbenchItem(playerSession, product.Id);
        Assert.That(craftedItemFromWorkbench2.Amount, Is.EqualTo(1), $"Invalid {nameof(craftedItemFromWorkbench2)}.{nameof(craftedItemFromWorkbench2.Amount)}");

        var resourcesAfter2 = await Services.PlayerInventoryResources.GetPlayerResources(playerSession);

        foreach (var blueprintDepend in productBlueprint.Depends)
        {
            var resourceBefore = resourcesBefore.First(q => q.ModelId.Equals(blueprintDepend.Resource.Id));
            var resourceAfter = resourcesAfter1.First(q => q.ModelId.Equals(blueprintDepend.Resource.Id));

            Assert.Less
            (
                resourceAfter.Amount, resourceBefore.Amount,
                $"Invalid resourceAfter {resourceAfter.Id} ({blueprintDepend.Resource.Type}) | before: {resourceBefore.Amount} | after: {resourceAfter.Amount} | expected: {resourceBefore.Amount - blueprintDepend.Amount}"
            );
        }

        Assert.That(craftedItemFromWorkbench2!.Id, Is.Not.EqualTo(craftedItemFromWorkbench1!.Id), $"It was expected that the same item would not be merged");

        var purchasedItems1 = await Services.PlayerInventoryItems.GetPlayerPurchasedItems(playerSession);
        var craftedItemFromInventory1 = purchasedItems1.FirstOrDefault(q => q.Id.Equals(craftedItemFromWorkbench1.Id));
        craftedItemFromInventory1!.AssertEqual(craftedItemFromWorkbench1);
    }

    [Test]
    [TestCase(StoreItemCategory.Types.Type.Grenade)]
    [TestCase(StoreItemCategory.Types.Type.Kits)]
    public async Task CraftStackable(StoreItemCategory.Types.Type type)
    {
        var playerSession = await Services.PlayerIdentity.SignUp();
        var product = Storages.GameStore.GetProductWithBlueprint(q => q.Category.Type == type);
        var productBlueprint = product!.Blueprints.First(q => q.Depends.Any());

        var resourcesBefore = await Services.PlayerInventoryResources.GetPlayerResources(playerSession);
        var craftedItemFromWorkbench1 = await Services.PlayerInventoryItems.CraftWorkbenchItem(playerSession, product.Id);
        Assert.That(craftedItemFromWorkbench1.Amount, Is.EqualTo(1), $"Invalid {nameof(craftedItemFromWorkbench1)}.{nameof(craftedItemFromWorkbench1.Amount)}");

        var resourcesAfter1 = await Services.PlayerInventoryResources.GetPlayerResources(playerSession);

        var craftedItemFromWorkbench2 = await Services.PlayerInventoryItems.CraftWorkbenchItem(playerSession, product.Id);
        Assert.That(craftedItemFromWorkbench2.Amount, Is.EqualTo(2), $"Invalid {nameof(craftedItemFromWorkbench2)}.{nameof(craftedItemFromWorkbench2.Amount)}");

        var resourcesAfter2 = await Services.PlayerInventoryResources.GetPlayerResources(playerSession);

        foreach (var blueprintDepend in productBlueprint.Depends)
        {
            var resourceBefore = resourcesBefore.First(q => q.ModelId.Equals(blueprintDepend.Resource.Id));
            var resourceAfter = resourcesAfter1.First(q => q.ModelId.Equals(blueprintDepend.Resource.Id));

            Assert.Less
            (
                resourceAfter.Amount, resourceBefore.Amount,
                $"Invalid resourceAfter {resourceAfter.Id} ({blueprintDepend.Resource.Type}) | before: {resourceBefore.Amount} | after: {resourceAfter.Amount} | expected: {resourceBefore.Amount - blueprintDepend.Amount}"
            );
        }

        Assert.That(craftedItemFromWorkbench2.Id, Is.EqualTo(craftedItemFromWorkbench1.Id), $"The same item was expected to be merged");

        var purchasedItems1 = await Services.PlayerInventoryItems.GetPlayerPurchasedItems(playerSession);
        var craftedItemFromInventory1 = purchasedItems1.FirstOrDefault(q => q.Id.Equals(craftedItemFromWorkbench1.Id));
        craftedItemFromInventory1!.AssertEqual(craftedItemFromWorkbench1);
        Assert.That(craftedItemFromInventory1.Amount, Is.EqualTo(2), $"Invalid {nameof(craftedItemFromInventory1)}.{nameof(craftedItemFromInventory1.Amount)}");
    }
}