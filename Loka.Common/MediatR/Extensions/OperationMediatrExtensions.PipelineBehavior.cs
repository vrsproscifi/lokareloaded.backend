﻿using System;
using System.Collections.Generic;
using System.Text;
using Loka.Common.MediatR.Pipelines;
using Loka.Common.MediatR.Requests;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Loka.Common.Operations;
using Loka.Common.Operations.Results;

namespace Loka.Common.MediatR.Extensions;

public static partial class OperationMediatrExtensions
{
    public static IServiceCollection AddPipelineBehavior<TRequest, TPipelineBehavior>(this IServiceCollection serviceCollection)
        where TRequest : IOperationRequest
        where TPipelineBehavior : class, IOperationPipelineBehavior<TRequest>
    {
        serviceCollection.AddScoped<IPipelineBehavior<TRequest, OperationResult>, TPipelineBehavior>();
        return serviceCollection;
    }

    public static IServiceCollection AddPipelineBehavior<TRequest, TResult, TPipelineBehavior>(this IServiceCollection serviceCollection)
        where TRequest : IOperationRequest<TResult>
        where TPipelineBehavior : class, IOperationPipelineBehavior<TRequest, TResult>
    {
        serviceCollection.AddScoped<IPipelineBehavior<TRequest, OperationResult<TResult>>, TPipelineBehavior>();
        return serviceCollection;
    }
}