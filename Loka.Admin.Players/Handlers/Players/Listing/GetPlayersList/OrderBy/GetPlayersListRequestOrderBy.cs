﻿namespace Loka.Admin.Players.Handlers.Players.Listing.GetPlayersList.OrderBy;

public enum GetPlayersListRequestOrderBy
{
    Name = 0,
    Email = 1,
    Level = 2,
    Country = 3,
    Language = 4,
}