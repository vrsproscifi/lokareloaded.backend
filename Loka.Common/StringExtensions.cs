using System;
using System.Collections.Generic;
using System.Linq;

namespace Loka.Common;

public static class StringExtensions
{
    public static string SafeSubstring(this string str, int lenght)
    {
        return str.Substring(0, Math.Min(str.Length, lenght));
    }

    public static string CommaEnumeration<T>(this IEnumerable<T> source)
    {
        return source.SeparatorEnumeration(", ");
    }
        
    public static string SeparatorEnumeration<T>(this IEnumerable<T> source, string sepearator)
    {
        return string.Join(sepearator, source);
    }        
        
    /// <summary>
    /// <para>Short extension for reverse string</para>
    /// <para>Returns the reversed string instead of a byte array</para>
    /// </summary>
    public static string ReverseString(this string str)
    {
        return new string(str.Reverse().ToArray());
    }
}