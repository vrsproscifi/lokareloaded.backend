﻿using System.Threading.Tasks;
using Grpc.Core;
using Loka.Admin.Players.Handlers.Matches.GetPlayerMatchesHistory;
using Loka.Common.AspNet.Extensions.OperationResults;
using Loka.Common.AspNet.Features;
using Loka.Common.Pagination;

namespace Loka.Admin.Api.Features.Players;

public sealed class PlayerMatchesController : PlayerMatches.PlayerMatchesBase
{
    private ControllerServices<PlayerMatchesController> Services { get; }

    public PlayerMatchesController(ControllerServices<PlayerMatchesController> services)
    {
        Services = services;
    }
        
    public override Task<GetPlayerMatchesHistoryResult> GetPlayerMatchesHistory(GetPlayerMatchesHistoryQuery query, ServerCallContext context)
    {
        var request = Services.Mapper.Map<GetPlayerMatchesHistoryRequest>(query);
        return Services.Executor.Execute(request).ReturnResult<PagingResult<GetPlayerMatchesHistoryResponse>, GetPlayerMatchesHistoryResult>(Services.Mapper);
    }
}