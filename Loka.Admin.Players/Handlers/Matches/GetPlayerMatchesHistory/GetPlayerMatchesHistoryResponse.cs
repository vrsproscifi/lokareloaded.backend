﻿using System;
using Loka.Admin.Common.Models.GamePlay;
using Loka.Common.Entry;

namespace Loka.Admin.Players.Handlers.Matches.GetPlayerMatchesHistory;

public class GetPlayerMatchesHistoryResponse : IEntry
{
    public Guid Id { get; set; }
    public bool HasSquad { get; set; }

    public GameModeInformationModel GameMode { get; set; } = null!;
    public GameMapInformationModel GameMap { get; set; } = null!;
    public GameVersionInformationModel GameVersion { get; set; } = null!;

    public DateTimeOffset CreatedAt { get; set; }
    public DateTimeOffset StartedAt { get; set; }
    public DateTimeOffset FinishedAt { get; set; }
    public TimeSpan Duration { get; set; }


    public short Difficulty { get; set; }
    public short NumberOfBots { get; set; }
    public short NumberOfPlayers { get; set; }
}