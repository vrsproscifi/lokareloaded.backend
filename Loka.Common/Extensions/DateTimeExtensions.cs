﻿using System.Runtime.CompilerServices;

namespace Loka.Common.Extensions;

public static class DateTimeExtensions
{
    public static DateTime TruncateMicroseconds(this DateTime dateTime)
    {
        return dateTime.Truncate(TimeSpan.FromMicroseconds(1));
    }

    public static DateTime TruncateMinutes(this DateTime dateTime)
    {
        return dateTime.Truncate(TimeSpan.FromMinutes(1));
    }

    public static DateTime TruncateHours(this DateTime dateTime)
    {
        return dateTime.Truncate(TimeSpan.FromHours(1));
    }

    public static DateTime TruncateMilliseconds(this DateTime dateTime)
    {
        return dateTime.Truncate(TimeSpan.FromMilliseconds(1));
    }

    public static DateTime TruncateSeconds(this DateTime dateTime)
    {
        return dateTime.Truncate(TimeSpan.FromSeconds(1));
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining | MethodImplOptions.AggressiveOptimization)]
    public static DateTime Truncate(this DateTime dateTime, TimeSpan timeSpan)
    {
        if (timeSpan == TimeSpan.Zero) return dateTime; // Or could throw an ArgumentException
        if (dateTime == DateTime.MinValue || dateTime == DateTime.MaxValue) return dateTime; // do not modify "guard" values
        return dateTime.AddTicks(-(dateTime.Ticks % timeSpan.Ticks));
    }
}