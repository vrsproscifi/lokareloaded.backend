﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Entities;
using Loka.Database.Common.Interfaces;
using Loka.Inventory.DataBase.Entities.Items;

namespace Loka.Inventory.DataBase.Entities.Templates;

public sealed class PlayerInventoryTemplateItemEntity : BaseEntity
{
    #region Preset

    [ForeignKey(nameof(TemplateEntity))]
    public Guid TemplateId { get; set; }

    public PlayerInventoryTemplateEntity? TemplateEntity { get; set; }

    #endregion

    #region Item

    [ForeignKey(nameof(ItemEntity))]
    public Guid ItemId { get; set; }

    public PlayerItemEntity? ItemEntity { get; set; }

    #endregion
}