﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Loka.Inventory.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class FirstInit2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlayerInventoryItemEntity",
                schema: "Inventory");

            migrationBuilder.RenameColumn(
                name: "CurrencyId",
                schema: "Inventory",
                table: "PlayerCurrencyEntity",
                newName: "ModelId");

            migrationBuilder.RenameColumn(
                name: "CaseId",
                schema: "Inventory",
                table: "PlayerCaseEntity",
                newName: "ModelId");

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastUseDate",
                schema: "Inventory",
                table: "PlayerCaseEntity",
                type: "timestamp with time zone",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "NextNotifyDate",
                schema: "Inventory",
                table: "PlayerCaseEntity",
                type: "timestamp with time zone",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "PlayerInventoryPresetEntity",
                schema: "Inventory",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlayerId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerInventoryPresetEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "PlayerInventoryTemplateEntity",
                schema: "Inventory",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    PlayerId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerInventoryTemplateEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "PlayerInventoryPresetItemEntity",
                schema: "Inventory",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    PresetId = table.Column<Guid>(type: "uuid", nullable: false),
                    ItemId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerInventoryPresetItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PlayerInventoryPresetItemEntity_PlayerInventoryPresetEntity~",
                        column: x => x.PresetId,
                        principalSchema: "Inventory",
                        principalTable: "PlayerInventoryPresetEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlayerInventoryPresetItemEntity_PlayerPurchasedItemEntity_I~",
                        column: x => x.ItemId,
                        principalSchema: "Inventory",
                        principalTable: "PlayerPurchasedItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlayerInventoryTemplateItemEntity",
                schema: "Inventory",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    TemplateId = table.Column<Guid>(type: "uuid", nullable: false),
                    ItemId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerInventoryTemplateItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PlayerInventoryTemplateItemEntity_PlayerInventoryTemplateEn~",
                        column: x => x.TemplateId,
                        principalSchema: "Inventory",
                        principalTable: "PlayerInventoryTemplateEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_PlayerInventoryTemplateItemEntity_PlayerPurchasedItemEntity~",
                        column: x => x.ItemId,
                        principalSchema: "Inventory",
                        principalTable: "PlayerPurchasedItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlayerInventoryPresetItemEntity_ItemId",
                schema: "Inventory",
                table: "PlayerInventoryPresetItemEntity",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerInventoryPresetItemEntity_PresetId",
                schema: "Inventory",
                table: "PlayerInventoryPresetItemEntity",
                column: "PresetId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerInventoryTemplateItemEntity_ItemId",
                schema: "Inventory",
                table: "PlayerInventoryTemplateItemEntity",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerInventoryTemplateItemEntity_TemplateId",
                schema: "Inventory",
                table: "PlayerInventoryTemplateItemEntity",
                column: "TemplateId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlayerInventoryPresetItemEntity",
                schema: "Inventory");

            migrationBuilder.DropTable(
                name: "PlayerInventoryTemplateItemEntity",
                schema: "Inventory");

            migrationBuilder.DropTable(
                name: "PlayerInventoryPresetEntity",
                schema: "Inventory");

            migrationBuilder.DropTable(
                name: "PlayerInventoryTemplateEntity",
                schema: "Inventory");

            migrationBuilder.DropColumn(
                name: "LastUseDate",
                schema: "Inventory",
                table: "PlayerCaseEntity");

            migrationBuilder.DropColumn(
                name: "NextNotifyDate",
                schema: "Inventory",
                table: "PlayerCaseEntity");

            migrationBuilder.RenameColumn(
                name: "ModelId",
                schema: "Inventory",
                table: "PlayerCurrencyEntity",
                newName: "CurrencyId");

            migrationBuilder.RenameColumn(
                name: "ModelId",
                schema: "Inventory",
                table: "PlayerCaseEntity",
                newName: "CaseId");

            migrationBuilder.CreateTable(
                name: "PlayerInventoryItemEntity",
                schema: "Inventory",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    ItemId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    PlayerId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerInventoryItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PlayerInventoryItemEntity_PlayerPurchasedItemEntity_ItemId",
                        column: x => x.ItemId,
                        principalSchema: "Inventory",
                        principalTable: "PlayerPurchasedItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlayerInventoryItemEntity_ItemId",
                schema: "Inventory",
                table: "PlayerInventoryItemEntity",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerInventoryItemEntity_PlayerId",
                schema: "Inventory",
                table: "PlayerInventoryItemEntity",
                column: "PlayerId");
        }
    }
}
