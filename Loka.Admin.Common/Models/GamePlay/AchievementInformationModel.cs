﻿using System;
using Loka.Game.Database.Enums.Achievements;

namespace Loka.Admin.Common.Models.GamePlay;

public sealed class AchievementInformationModel
{
    public Guid Id { get; set; }
    public string Name { get; set; }= null!;
    public AchievementTypeId TypeId { get; set; }

    public override string ToString()
    {
        return $"Id: {Id} | Name: {Name} | TypeId: {TypeId}";
    }
}