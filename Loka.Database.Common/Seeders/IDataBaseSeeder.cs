﻿using System.Threading;
using System.Threading.Tasks;

namespace Loka.Database.Common.Seeders;

public interface IDataBaseSeeder
{
    Task Seed(CancellationToken cancellationToken);
}