﻿namespace Loka.Database.Common.Interfaces;

public interface INamedEntity
{
    string Name { get; }
}

public interface ITypedEntity<TType> where TType : Enum
{
    TType TypeId { get; }
}

public interface IAdminCommentedEntity
{
    string? AdminComment { get; }
}