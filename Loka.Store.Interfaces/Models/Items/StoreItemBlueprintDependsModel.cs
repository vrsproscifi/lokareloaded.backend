﻿using Loka.Store.Interfaces.Models.Resources;

namespace Loka.Store.Interfaces.Models.Items;

[GenerateSerializer]
public sealed class StoreItemBlueprintDependsModel
{
    [Id(0)]
    public Guid Id { get; set; }

    [Id(1)]
    public GameResourceModel Resource { get; set; } = null!;

    [Id(2)]
    public int Amount { get; set; }
}
