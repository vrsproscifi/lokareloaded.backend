using Microsoft.Extensions.Logging;

namespace Loka.Common.MassTransit.MessageBrokers;

public sealed class ClusterConsumerServices<TConsumer>
{
    public IClusterClient ClusterClient { get; }
    public ILogger<TConsumer> Logger { get; }

    public IServiceProvider ServiceProvider { get; }
    
    public ClusterConsumerServices(IClusterClient clusterClient, ILogger<TConsumer> logger, IServiceProvider serviceProvider)
    {
        ClusterClient = clusterClient;
        Logger = logger;
        ServiceProvider = serviceProvider;
    }
}