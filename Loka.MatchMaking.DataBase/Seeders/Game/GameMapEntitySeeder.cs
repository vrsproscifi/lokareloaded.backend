﻿using System;
using Loka.MatchMaking.DataBase.Entities.Game;
using Loka.MatchMaking.DataBase.Enums.Game;

namespace Loka.MatchMaking.DataBase.Seeders.Game;

public static class GameMapEntitySeeder
{
    public static Guid Hangar { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe601");
        
    public static GameMapEntity[] Entities { get; } = new[]
    {
        new GameMapEntity(Hangar, GameMapTypeId.Hangar),
    };
}