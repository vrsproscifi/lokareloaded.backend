﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Entities;
using Loka.Database.Common.Extensions;
using Loka.Database.Common.Storage.Constructors;
using Microsoft.EntityFrameworkCore;

namespace Loka.Database.Common.Storage.Impls;

public abstract class EntryStorage<TModel, TEntity, TDbContext> : IEntryStorage<TModel>
    where TDbContext : DbContext
    where TEntity : BaseEntity
{
    private IMapper Mapper { get; }
    protected TDbContext DbContext { get; }
    protected abstract OperationResultCode EntityNotFoundErrorCode { get; }
    protected abstract DbSet<TEntity> Table { get; }

    protected EntryStorage(EntryStorageConstructor<TDbContext> constructor)
    {
        DbContext = constructor.DbContext;
        Mapper =  constructor.Mapper;
    }

    public virtual async Task<OperationResult<TModel>> GetById(Guid id, CancellationToken cancellationToken = default)
    {
        var entity = await Table.GetById(id, cancellationToken);
        if (entity == null)
            return EntityNotFoundErrorCode;

        var model = Mapper.Map<TModel>(entity);
        return model;
    }

    public virtual Task<TModel[]> GetList(CancellationToken cancellationToken = default)
    {
        return Table
            .OrderBy(q => q.EntityId)
            .ProjectTo<TModel>(Mapper.ConfigurationProvider)
            .ToArrayAsync(cancellationToken);
    }
}