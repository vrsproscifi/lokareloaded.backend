﻿using AutoMapper.Extensions.EnumMapping;
using Loka.Api.Features.Inventory.Items;
using Loka.Store.Interfaces.Enums;
using Loka.Store.Interfaces.Models.Boosters;
using Loka.Store.Interfaces.Models.Cases;
using Loka.Store.Interfaces.Models.Fractions;
using Loka.Store.Interfaces.Models.Items;
using Loka.Store.Interfaces.Models.Resources;

namespace Loka.Api.Features.Store;

public sealed class GameStoreMappingProfiles : Profile
{
    public GameStoreMappingProfiles()
    {
        CreateMap<Loka.Inventory.Interfaces.Store.Requests.BuyStoreItem.Result, BuyGameStoreItem.Types.Response>()
            .ForMember(q => q.Item, q => q.MapFrom(w => w.Item));

        CreateMap<ItemCategoryTypeId, StoreItemCategory.Types.Type>()
            .ConvertUsingEnumMapping(q => q
                .MapValue(ItemCategoryTypeId.None, StoreItemCategory.Types.Type.None)
                .MapValue(ItemCategoryTypeId.Weapon, StoreItemCategory.Types.Type.Weapon)
                .MapValue(ItemCategoryTypeId.Character, StoreItemCategory.Types.Type.Character)
                .MapValue(ItemCategoryTypeId.Profile, StoreItemCategory.Types.Type.Profile)
                .MapValue(ItemCategoryTypeId.Material, StoreItemCategory.Types.Type.Material)
                .MapValue(ItemCategoryTypeId.Grenade, StoreItemCategory.Types.Type.Grenade)
                .MapValue(ItemCategoryTypeId.Kits, StoreItemCategory.Types.Type.Kits)
                .MapValue(ItemCategoryTypeId.Mines, StoreItemCategory.Types.Type.Mines)
            )
            .ReverseMap();

        CreateMap<FractionTypeId, GameFraction.Types.Type>()
            .ConvertUsingEnumMapping(q => q
                .MapValue(FractionTypeId.None, GameFraction.Types.Type.None)
                .MapValue(FractionTypeId.Keepers, GameFraction.Types.Type.Keepers)
                .MapValue(FractionTypeId.Keepers_Friend, GameFraction.Types.Type.KeepersFriend)
                .MapValue(FractionTypeId.RIFT, GameFraction.Types.Type.Rift)
                .MapValue(FractionTypeId.RIFT_Friend, GameFraction.Types.Type.RiftFriend)
            )
            .ReverseMap();
        
        CreateMap<BoosterTypeId, GameBooster.Types.Type>()
            .ConvertUsingEnumMapping(q => q
                .MapValue(BoosterTypeId.None, GameBooster.Types.Type.None)
                .MapValue(BoosterTypeId.PremiumAccount, GameBooster.Types.Type.PremiumAccount)
                .MapValue(BoosterTypeId.Experience, GameBooster.Types.Type.Experience)
                .MapValue(BoosterTypeId.Reputation, GameBooster.Types.Type.Reputation)
                .MapValue(BoosterTypeId.Money, GameBooster.Types.Type.Money)
                .MapValue(BoosterTypeId.Resources, GameBooster.Types.Type.Resources)
            )
            .ReverseMap();

        CreateMap<ResourceTypeId, GameResource.Types.Type>()
            .ConvertUsingEnumMapping(q => q
                .MapValue(ResourceTypeId.None, GameResource.Types.Type.None)
                .MapValue(ResourceTypeId.Coin, GameResource.Types.Type.Coin)
                .MapValue(ResourceTypeId.Money, GameResource.Types.Type.Money)
                .MapValue(ResourceTypeId.Donate, GameResource.Types.Type.Donate)
                .MapValue(ResourceTypeId.Iron, GameResource.Types.Type.Iron)
                .MapValue(ResourceTypeId.Copper, GameResource.Types.Type.Copper)
                .MapValue(ResourceTypeId.Steel, GameResource.Types.Type.Steel)
                .MapValue(ResourceTypeId.Rags, GameResource.Types.Type.Rags)
            )
            .ReverseMap();

        CreateMap<StoreItemCategoryModel, StoreItemCategory.Types.Model>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.Name, q => q.MapFrom(w => w.Name))
            .ForMember(q => q.Type, q => q.MapFrom(w => w.TypeId));

        CreateMap<GameResourceModel, GameResource.Types.Model>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.Name, q => q.MapFrom(w => w.Name))
            .ForMember(q => q.DefaultValue, q => q.MapFrom(w => w.DefaultValue))
            .ForMember(q => q.Type, q => q.MapFrom(w => w.TypeId));

        CreateMap<StoreFractionModel, GameFraction.Types.Model>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.Name, q => q.MapFrom(w => w.Name))
            .ForMember(q => q.Type, q => q.MapFrom(w => w.TypeId));


        CreateMap<StoreItemBlueprintModel, StoreItem.Types.Blueprint>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.Depends, q => q.MapFrom(w => w.Depends));

        CreateMap<StoreItemBlueprintDependsModel, StoreItem.Types.Blueprint.Types.Depend>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.Resource, q => q.MapFrom(w => w.Resource))
            .ForMember(q => q.Amount, q => q.MapFrom(w => w.Amount));


        CreateMap<StoreItemModel, StoreItem.Types.Model>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.Name, q => q.MapFrom(w => w.Name))
            .ForMember(q => q.Level, q => q.MapFrom(w => w.Level))
            .ForMember(q => q.DisplayPosition, q => q.MapFrom(w => w.DisplayPosition))

            //===========================
            .ForMember(q => q.Category, q => q.MapFrom(w => w.Category))
            .ForMember(q => q.Fraction, q => q.MapFrom(w => w.Fraction))

            //===========================
            .ForMember(q => q.SellCurrency, q => q.MapFrom(w => w.SellCurrency))
            .ForMember(q => q.SellPrice, q => q.MapFrom(w => w.SellPrice))
            //===========================
            .ForMember(q => q.SubscribeCurrency, q => q.MapFrom(w => w.SubscribeCurrency))
            .ForMember(q => q.SubscribePrice, q => q.MapFrom(w => w.SubscribePrice))
            .ForMember(q => q.Blueprints, q => q.MapFrom(w => w.Blueprints));

        CreateMap<GameBoosterModel, GameBooster.Types.Model>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.Name, q => q.MapFrom(w => w.Name))
            .ForMember(q => q.Type, q => q.MapFrom(w => w.TypeId))
            .ForMember(q => q.SellPrice, q => q.MapFrom(w => w.SellPrice ?? 0))
            .ForMember(q => q.SellCurrency, q => q.MapFrom(w => w.SellCurrency))
            .ForMember(q => q.BoostPercent, q => q.MapFrom(w => w.BoostPercent));

        CreateMap<StoreCaseModel, StoreCase.Types.Model>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.Name, q => q.MapFrom(w => w.Name))
            .ForMember(q => q.SellPrice, q => q.MapFrom(w => w.SellPrice ?? 0))
            .ForMember(q => q.SellCurrency, q => q.MapFrom(w => w.SellCurrency))
            .ForMember(q => q.Resources, q => q.MapFrom(w => w.Resources))
            .ForMember(q => q.Items, q => q.MapFrom(w => w.Items));

        CreateMap<StoreCaseModel.Item, StoreCase.Types.ItemEntry>()
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.Model.Id))
            .ForMember(q => q.MinimumCount, q => q.MapFrom(w => w.MinimumCount))
            .ForMember(q => q.MaximumCount, q => q.MapFrom(w => w.MaximumCount))
            .ForMember(q => q.ChanceOfDrop, q => q.MapFrom(w => w.ChanceOfDrop));

        CreateMap<StoreCaseModel.Resource, StoreCase.Types.ResourceEntry>()
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.Model.Id))
            .ForMember(q => q.MinimumCount, q => q.MapFrom(w => w.MinimumCount))
            .ForMember(q => q.MaximumCount, q => q.MapFrom(w => w.MaximumCount))
            .ForMember(q => q.ChanceOfDrop, q => q.MapFrom(w => w.ChanceOfDrop));
        
    }
}