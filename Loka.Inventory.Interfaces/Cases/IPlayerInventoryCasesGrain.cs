namespace Loka.Inventory.Interfaces.Cases;

public interface IPlayerInventoryCasesGrain : IGrainWithGuidKey
{
    Task<PlayerInventoryCaseModel> AddOrStackInventoryCase(AddOrStackInventoryCase.Request request);
    Task<OperationResult<OpenPlayerCase.Result>> OpenCase(OpenPlayerCase.Request request);
    Task<OperationResult<GetPlayerCases.Result>> GetCases();
}