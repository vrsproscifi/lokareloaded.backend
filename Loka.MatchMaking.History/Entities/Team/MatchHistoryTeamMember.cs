using System;

namespace Loka.MatchMaking.History.Entities.Team;

public class MatchHistoryTeamMember
{
    public Guid MemberId { get; set; }
        
    public string PlayerName { get; set; }
    public Guid PlayerId { get; set; }
        
    public MatchHistoryTeamStatus Status { get; set; }
        
    public byte Squad { get; set; }
    public byte Level { get; set; }
    public int Score { get; set; }
    public int Elo { get; set; }
}