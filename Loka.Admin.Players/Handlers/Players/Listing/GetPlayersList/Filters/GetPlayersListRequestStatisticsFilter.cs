﻿using Loka.Common.Ranges.Numerics;

namespace Loka.Admin.Players.Handlers.Players.Listing.GetPlayersList.Filters;

public sealed class GetPlayersListRequestStatisticsFilter
{
    public NumericRange<int?, int?> Level { get; set; }
    public NumericRange<int?, int?> Elo { get; set; }
    public NumericRange<int?, int?> Kills { get; set; }
    public NumericRange<int?, int?> Deaths { get; set; }
    public NumericRange<int?, int?> Wins { get; set; }
    public NumericRange<int?, int?> Loses { get; set; }
}