﻿using System;
using Loka.Database.Common.Contexts;
using Loka.Social.DataBase.Entities.Friends;
using Microsoft.EntityFrameworkCore;

namespace Loka.Social.DataBase;

public sealed class SocialDbContext : BaseGameDbContext<SocialDbContext>
{
    public SocialDbContext(DbContextOptions<SocialDbContext> options) : base(options)
    {
    }
        
    public DbSet<PlayerFriendEntity> PlayerFriendEntity { get; set; }
}