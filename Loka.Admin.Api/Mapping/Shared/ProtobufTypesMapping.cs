﻿using System;
using AutoMapper;
using Google.Protobuf.WellKnownTypes;

namespace Loka.Admin.Api.Mapping.Shared;

public sealed class ProtobufTypesMapping : Profile
{
    public ProtobufTypesMapping()
    {
        CreateMap<DateTimeOffset, Timestamp>()
            .ConstructUsing(dateTime => Timestamp.FromDateTimeOffset(dateTime));

        CreateMap<DateTime, Timestamp>()
            .ConstructUsing(dateTime => Timestamp.FromDateTime(dateTime));
            
        /* CreateMap<int?, Int32Value>()
             .ConstructUsing(dateTime => Timestamp.FromDateTime(dateTime));*/
    }
}