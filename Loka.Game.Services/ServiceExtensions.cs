﻿using Loka.Game.Interfaces.Players;
using Loka.Game.Services.Players;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Game.Services;

public static class ServiceExtensions
{
    public static IServiceCollection AddGameServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<IPlayersService, PlayersService>();

        return services;
    }

}