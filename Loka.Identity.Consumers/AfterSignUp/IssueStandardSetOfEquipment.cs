﻿using Loka.Common.MassTransit.MessageBrokers;
using Loka.Inventory.Interfaces.Inventory;
using Loka.Inventory.Interfaces.Inventory.Requests;
using Loka.Inventory.Interfaces.Presets;
using Loka.Inventory.Interfaces.Presets.Models;
using Loka.Inventory.Interfaces.Resources;
using Loka.Inventory.Interfaces.Store;
using Loka.Store.Interfaces;
using Loka.Store.Interfaces.Enums;
using Microsoft.Extensions.Logging;

namespace Loka.Identity.Consumers.AfterSignUp;

public sealed class IssueStandardSetOfEquipmentConsumer : ClusterConsumer<AfterSignUpEvent>
{
    public IssueStandardSetOfEquipmentConsumer(ClusterConsumerServices<ClusterConsumer<AfterSignUpEvent>> services) : base(services)
    {
    }

    public override async Task Consume(ConsumeContext<AfterSignUpEvent> context)
    {
        await base.Consume(context);

        await IssueStandardSetOfResources(context);

        var items = await IssueStandardSetOfEquipment(context);

        var issuePresetResult = await IssueInventoryPresets(context);
        if (issuePresetResult.GetValueIfSucceeded(out var preset))
        {
            var inventory = Services.ClusterClient.GetGrain<IPlayerInventoryPresetGrain>(context.Message.PlayerId);
            foreach (var item in items)
            {
                var equipResult = await inventory.Equip(new EquipInventoryItem.Request()
                {
                    PresetId = preset.Preset.Id,
                    ItemId = item.Id
                });

                if (equipResult.IsFailed)
                {
                    Services.Logger.LogError($"Failed equip {item.Id} to preset {preset.Preset.Id} | for player {context.Message.PlayerId} | Error: {equipResult.Error}");
                }
            }
        }
        // Logger.LogInformation($"Begin {nameof(IssueStandardSetOfEquipmentConsumer)} for player {context.Message.PlayerId}");
    }

    private async Task IssueStandardSetOfResources(ConsumeContext<AfterSignUpEvent> context)
    {
        var store = Services.ClusterClient.GetGameStoreGrain();
        var resources = await store.GetGameResources();
        foreach (var resource in resources)
        {
            var inventory = Services.ClusterClient.GetGrain<IPlayerInventoryResourcesGrain>(context.Message.PlayerId);
            await inventory.GetResourceByType(resource.Id);
        }
    }

    private async Task<List<PlayerInventoryItemModel.RPC>> IssueStandardSetOfEquipment(ConsumeContext<AfterSignUpEvent> context)
    {
        var inventory = Services.ClusterClient.GetGrain<IPlayerInventoryItemsGrain>(context.Message.PlayerId);
        var store = Services.ClusterClient.GetGameStoreGrain();

        var standardSetOfEquipment = await store.GetStandardSetOfEquipment();

        var items = new List<PlayerInventoryItemModel.RPC>(standardSetOfEquipment.Count);
        foreach (var model in standardSetOfEquipment)
        {
            var addItemToInventoryResult = await inventory.AddOrStackInventoryItem(new AddOrStackInventoryItem.Request()
            {
                ModelId = model.Id,
                Amount = model.AmountDefault
            });

            if (addItemToInventoryResult.GetValueIfSucceeded(out var item))
            {
                //Services.Logger.LogInformation($"Successfully issued {item.ModelId} [Amount: {item.Amount}] | {item.Id} | for player {context.Message.PlayerId}");
                items.Add(item.Item);
            }
            else
            {
                Services.Logger.LogError($"Failed issue {model.Id} [Amount: {model.AmountDefault}] | for player {context.Message.PlayerId} | Error: {addItemToInventoryResult.Error}");
            }
        }

        return items;
    }

    private async Task<OperationResult<BuyPlayerInventoryPresets.Result>> IssueInventoryPresets(ConsumeContext<AfterSignUpEvent> context)
    {
        var gameStore = Services.ClusterClient.GetGameStoreGrain();
        var playerStore = Services.ClusterClient.GetGrain<IPlayerStoreGrain>(context.Message.PlayerId);

        var presets = await gameStore.GetInventoryPresets();
        var preset = presets.FirstOrDefault(q => q.Flags.HasFlag(ItemEntityFlags.IssueAfterRegistration));
        if (preset == null)
            return OperationResultCode.UnknownError;

        return await playerStore.BuyPreset(new BuyPlayerInventoryPresets.Request()
        {
            ModelId = preset.Id
        });
    }


    public sealed class ConsumerDefinition : ConsumerDefinition<IssueStandardSetOfEquipmentConsumer>
    {
        public ConsumerDefinition()
        {
            EndpointName = nameof(IssueStandardSetOfEquipmentConsumer);
        }
    }
}