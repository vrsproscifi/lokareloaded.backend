﻿using System.Threading.Tasks;
using Loka.Common.MediatR.Requests;
using Loka.Common.Operations.Results;

namespace Loka.Common.MediatR.Executors;

public interface IOperationExecutor
{
    Task<OperationResult> Execute(IOperationRequest request);
    Task<OperationResult<TResult>> Execute<TResult>(IOperationRequest<TResult> request);
}