﻿using Microsoft.Extensions.Caching.Memory;

namespace Loka.Common.Cache.Collection;

public sealed class CollectionCache<TData> : ICollectionCache<TData>
{
    private IMemoryCache MemoryCache { get; }

    public CollectionCache(IMemoryCache memoryCache)
    {
        MemoryCache = memoryCache;
    }

    public bool TryGet(out TData[]? entries)
    {
        return MemoryCache.TryGetValue(typeof(TData).Name, out entries);
    }

    public TData[] Put(TData[] entries)
    {
        return MemoryCache.Set(typeof(TData).Name, entries);
    }
}