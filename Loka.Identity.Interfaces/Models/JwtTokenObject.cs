﻿namespace Loka.Identity.Interfaces.Models;

[GenerateSerializer]
public sealed class JwtTokenObject
{
    [Id(0)]
    public string Token { get; init; } = null!;

    [Id(1)]
    public TokenType Type { get; init; }

    [Id(2)]
    public DateTime NotBefore { get; init; }

    [Id(3)]
    public DateTime ExpirationDate { get; init; }
    
    public enum TokenType
    {
        JwtToken,
        RefreshToken
    }
}