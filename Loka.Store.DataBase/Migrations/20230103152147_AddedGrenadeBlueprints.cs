﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Loka.Store.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class AddedGrenadeBlueprints : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameItemBlueprintEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "ItemModelId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe110"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910") },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe116"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909") }
                });

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "AmountInStack",
                value: -1L);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "AmountInStack",
                value: -1L);

            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                columns: new[] { "EntityId", "Amount", "BlueprintId", "CreatedAt", "DeletedAt", "ResourceId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe913"), 10, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe116"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe202") },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe914"), 25, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe110"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe202") }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe913"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe914"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemBlueprintEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe110"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemBlueprintEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe116"));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "AmountInStack",
                value: 1L);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "AmountInStack",
                value: 1L);
        }
    }
}
