﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Loka.Common.MediatR.Handlers;
using Loka.Common.Operations.Results;

namespace Loka.Admin.Players.Handlers.Players.Inventory.GetPlayerCurrencies;

public class GetPlayerCurrenciesRequestHandler : IOperationHandler<GetPlayerCurrenciesRequest, GetPlayerCurrenciesResponse>
{
    public Task<OperationResult<GetPlayerCurrenciesResponse>> Handle(GetPlayerCurrenciesRequest request, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }
}