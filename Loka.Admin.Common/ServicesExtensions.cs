﻿using System;
using Loka.Admin.Common.Services.GamePlay;
using Loka.Admin.Common.Services.GamePlay.Fractions;
using Loka.Admin.Common.Services.Roles;
using Loka.Admin.Common.Services.World;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Admin.Common;

public static class ServicesExtensions
{
    public static IServiceCollection AddAdminCommonServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddAdminGamePlayServices(configuration);
        services.AddAdminWorldServices(configuration);
        services.AddScoped<IAccountRoleService, AccountRoleService>();

        return services;
    }
}