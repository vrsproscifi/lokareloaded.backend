﻿using Loka.Common.Authentication;
using Loka.Common.Orleans.Grains;
using Loka.MatchMaking.DataBase;
using Loka.MatchMaking.Interfaces;
using Orleans;

namespace Loka.MatchMaking.Grains.Grains.Squad;

internal class MatchMakingSquadGrain : Grain, IMatchMakingSquadGrain, IWithPlayerActor
{
    public PlayerActor Actor { get; set; } = null!;
    private MatchMakingDbContext MatchMakingDbContext { get; }

    public MatchMakingSquadGrain(MatchMakingDbContext matchMakingDbContext)
    {
        MatchMakingDbContext = matchMakingDbContext;
    }

    /*
    public async Task<OperationResult<SendSquadInviteRequest>> OnSendSquadInvite(SendSquadInviteRequest request)
    {
        throw new System.NotImplementedException();
    }

    public async Task<OperationResult<SendSquadInviteRequest>> OnAcceptSquadInvite(AcceptSquadInviteRequest request)
    {
        throw new System.NotImplementedException();
    }

    public async Task<OperationResult<SquadInformation>> OnGetSquadInformation(RevokeSquadInviteRequest request)
    {
        var queue = MatchMakingDbContext.PlayerQueueEntity
            .Include(entity => entity.Members)
            .FirstAsync(entity => entity.EntityId == Actor.PlayerId);

        var response = new SquadInformation();
        response.Members.AddRange(new []
        {
            new SquadMember()
            {
                Player = new PlayerModel()
                {
                    
                }
            }, 
        });
        return response;
    }

    public async Task<OperationResult<SendSquadInviteRequest>> OnRevokeSquadInvite(RevokeSquadInviteRequest request)
    {
        throw new System.NotImplementedException();
    }*/
}