﻿using AutoMapper;
using Loka.Admin.Common.Models.Countries;
using Loka.Game.Database.Entities.World;

namespace Loka.Admin.Common.Mapping.World;

internal sealed class LanguageInformationModelMapping : Profile
{
    public LanguageInformationModelMapping()
    {
        CreateMap<WorldLanguageEntity, LanguageInformationModel>()
            .ForMember(q => q.Id, q => q.MapFrom(p => p.EntityId))
            .ForMember(q => q.Name, q => q.MapFrom(p => p.Name));
    }
}