﻿using Global.Identity.Database;
using Loka.Admin.Common.Services.Roles;
using Loka.Admin.Common.Services.World.Countries;
using Loka.Admin.Common.Services.World.Languages;
using Loka.Admin.Players.Models;
using Loka.Admin.Players.Services;
using Loka.Common.MediatR.Handlers;
using Loka.Common.Operations;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Extensions;
using Loka.Identity.Database;

namespace Loka.Admin.Players.Handlers.Players.Listing.GetPlayerInformation;

public sealed class GetPlayerInformationRequestHandler : IOperationHandler<GetPlayerInformationRequest, GetPlayerInformationResponse>
{
    private GameIdentityDbContext IdentityDbContext { get; }
    private GlobalIdentityDbContext GlobalIdentityDbContext { get; }
    private ICountryService CountryService { get; }
    private ILanguageService LanguageService { get; }
    private IAccountRoleService AccountRoleService { get; }
    private IPlayerStatisticsService PlayerStatisticsService { get; }

    public GetPlayerInformationRequestHandler
    (
        GameIdentityDbContext identityDbContext,
        ICountryService countryService,
        ILanguageService languageService,
        IAccountRoleService accountRoleService,
        IPlayerStatisticsService playerStatisticsService
    )
    {
        IdentityDbContext = identityDbContext;
        CountryService = countryService;
        LanguageService = languageService;
        AccountRoleService = accountRoleService;
        PlayerStatisticsService = playerStatisticsService;
    }

    public async Task<OperationResult<GetPlayerInformationResponse>> Handle(GetPlayerInformationRequest request, CancellationToken cancellationToken)
    {
        var player = await GlobalIdentityDbContext.UserAccountEntity.GetById
        (
            request.PlayerId,
            cancellationToken
        );

        var model = new GetPlayerInformationResponse
        {
            EntityId = player.EntityId,

            Email = new PlayerEmailObject(player.Email, player.IsEmailConfirmed),
            Login = new PlayerNameObject(player.Login, player.IsNameConfirmed),

            RegisteredAt = player.CreatedAt,
            PremiumEndDate = player.PremiumEndDate,

            LastActivityAt = player.LastActivityDate,
        };

        return await model
            .ExecuteSequence(player.CountryId, CountryService.GetById, country => model.Country = country, cancellationToken)
            .ExecuteSequence(player.LanguageId, LanguageService.GetById, language => model.Language = language, cancellationToken)
            .ExecuteSequence(player.RoleId, AccountRoleService.GetById, role => model.Role = role, cancellationToken)
            .ExecuteSequence(player.EntityId, PlayerStatisticsService.GetDetailedPlayerStatistics, statistics => model.Statistics = statistics, cancellationToken);
    }
}