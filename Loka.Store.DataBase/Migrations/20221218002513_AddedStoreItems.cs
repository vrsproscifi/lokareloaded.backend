﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Loka.Store.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class AddedStoreItems : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SellCurrencyId",
                schema: "Store",
                table: "GameItemEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SubscribeCurrencyId",
                schema: "Store",
                table: "GameItemEntity");

            migrationBuilder.AlterColumn<Guid>(
                name: "SubscribeCurrencyId",
                schema: "Store",
                table: "GameItemEntity",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.AlterColumn<Guid>(
                name: "SellCurrencyId",
                schema: "Store",
                table: "GameItemEntity",
                type: "uuid",
                nullable: true,
                oldClrType: typeof(Guid),
                oldType: "uuid");

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 16, DateTimeKind.Unspecified).AddTicks(3158), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 16, DateTimeKind.Unspecified).AddTicks(3815), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 16, DateTimeKind.Unspecified).AddTicks(4020), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 16, DateTimeKind.Unspecified).AddTicks(4208), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 16, DateTimeKind.Unspecified).AddTicks(6479), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 16, DateTimeKind.Unspecified).AddTicks(7258), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 16, DateTimeKind.Unspecified).AddTicks(7264), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 15, DateTimeKind.Unspecified).AddTicks(8569), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 15, DateTimeKind.Unspecified).AddTicks(9457), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 15, DateTimeKind.Unspecified).AddTicks(9669), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 15, DateTimeKind.Unspecified).AddTicks(9865), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 16, DateTimeKind.Unspecified).AddTicks(55), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 16, DateTimeKind.Unspecified).AddTicks(244), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 16, DateTimeKind.Unspecified).AddTicks(432), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 16, DateTimeKind.Unspecified).AddTicks(618), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 16, DateTimeKind.Unspecified).AddTicks(803), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 0, 25, 13, 16, DateTimeKind.Unspecified).AddTicks(987), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameItemEntity",
                columns: new[] { "EntityId", "AdminComment", "AmountDefault", "AmountInStack", "AmountMaximum", "AmountMinimum", "CategoryId", "CreatedAt", "DeletedAt", "DisplayPosition", "Duration", "Flags", "FractionId", "Level", "Name", "SellCurrencyId", "SellPrice", "SubscribeCurrencyId", "SubscribePrice" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"), null, 1L, 1L, null, 1L, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 0, null, (byte)0, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"), 0, "AK-47", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 1000, null, 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"), null, 1L, 1L, null, 1L, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 0, null, (byte)0, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"), 0, "M16", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 1500, null, 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"), null, 1L, 1L, null, 1L, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 0, null, (byte)0, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"), 0, "Shield", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 1000, null, 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"), null, 1L, 1L, null, 1L, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 0, null, (byte)0, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"), 0, "Grenade 1", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 500, null, 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"), null, 1L, 1L, null, 1L, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 0, null, (byte)0, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"), 0, "Grenade 2", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 750, null, 0 }
                });

            migrationBuilder.AddForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SellCurrencyId",
                schema: "Store",
                table: "GameItemEntity",
                column: "SellCurrencyId",
                principalSchema: "Store",
                principalTable: "GameCurrencyEntity",
                principalColumn: "EntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SubscribeCurrencyId",
                schema: "Store",
                table: "GameItemEntity",
                column: "SubscribeCurrencyId",
                principalSchema: "Store",
                principalTable: "GameCurrencyEntity",
                principalColumn: "EntityId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SellCurrencyId",
                schema: "Store",
                table: "GameItemEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SubscribeCurrencyId",
                schema: "Store",
                table: "GameItemEntity");

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"));

            migrationBuilder.AlterColumn<Guid>(
                name: "SubscribeCurrencyId",
                schema: "Store",
                table: "GameItemEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.AlterColumn<Guid>(
                name: "SellCurrencyId",
                schema: "Store",
                table: "GameItemEntity",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"),
                oldClrType: typeof(Guid),
                oldType: "uuid",
                oldNullable: true);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 555, DateTimeKind.Unspecified).AddTicks(5708), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 555, DateTimeKind.Unspecified).AddTicks(6708), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 555, DateTimeKind.Unspecified).AddTicks(6919), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 555, DateTimeKind.Unspecified).AddTicks(7114), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 556, DateTimeKind.Unspecified).AddTicks(3829), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 556, DateTimeKind.Unspecified).AddTicks(4650), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 556, DateTimeKind.Unspecified).AddTicks(4863), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 556, DateTimeKind.Unspecified).AddTicks(9014), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 556, DateTimeKind.Unspecified).AddTicks(9667), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 556, DateTimeKind.Unspecified).AddTicks(9874), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 557, DateTimeKind.Unspecified).AddTicks(66), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 557, DateTimeKind.Unspecified).AddTicks(252), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 557, DateTimeKind.Unspecified).AddTicks(438), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 557, DateTimeKind.Unspecified).AddTicks(619), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 557, DateTimeKind.Unspecified).AddTicks(800), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 557, DateTimeKind.Unspecified).AddTicks(981), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 557, DateTimeKind.Unspecified).AddTicks(1162), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.AddForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SellCurrencyId",
                schema: "Store",
                table: "GameItemEntity",
                column: "SellCurrencyId",
                principalSchema: "Store",
                principalTable: "GameCurrencyEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SubscribeCurrencyId",
                schema: "Store",
                table: "GameItemEntity",
                column: "SubscribeCurrencyId",
                principalSchema: "Store",
                principalTable: "GameCurrencyEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
