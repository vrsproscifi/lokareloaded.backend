﻿namespace Loka.Inventory.Interfaces.Boosters;

public static class ActivatePlayerBooster
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<PlayerInventoryBoosterModel>
    {
        [Id(0)]
        public Guid Id { get; init; }
    }
}