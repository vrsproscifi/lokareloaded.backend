﻿using System.ComponentModel.DataAnnotations;
using Loka.Chat.Interfaces.Enums;
using Loka.Database.Common.Interfaces;
using MongoDB.Bson;

namespace Loka.Chat.DataBase.Entities;

public sealed class ChatChannelEntity : ITypedEntity<ChatChanelType>
{
    public ObjectId Id { get; set; }

    [MaxLength(128)]
    public string? Name { get; set; }

    public DateTimeOffset CreatedAt { get; set; }
    public ChatChanelType TypeId { get; set; }

    public Guid? RegionId { get; set; }
    public Guid? TeamId { get; set; }
    public Guid? MatchId { get; set; }
    public Guid? ClanId { get; set; }
}