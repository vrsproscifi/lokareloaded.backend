﻿using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Loka.Identity.Grains.Storages.Sessions;
using Loka.Identity.Interfaces.Models;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Loka.Identity.Grains.Authentication;

internal sealed class JwtBearerTokenGenerator : IJwtBearerTokenGenerator
{
    private IOptionsMonitor<JwtBearerAuthenticationSettings> SettingsMonitor { get; }
    private JwtBearerAuthenticationSettings Settings => SettingsMonitor.CurrentValue;

    private SigningCredentials SigningCredentials { get; set; }
    private JwtSecurityTokenHandler JwtSecurityTokenHandler { get; }

    public JwtBearerTokenGenerator(IOptionsMonitor<JwtBearerAuthenticationSettings> settingsMonitor)
    {
        SettingsMonitor = settingsMonitor;
        SettingsMonitor.OnChange(OnSettingsChanged);
        SigningCredentials = CreateSigningCredentials(Settings.SigningKey!);
        JwtSecurityTokenHandler = new JwtSecurityTokenHandler();
    }

    private SigningCredentials CreateSigningCredentials(string key)
    {
        var bytes = Encoding.UTF8.GetBytes(key);
        var symmetricSecurityKey = new SymmetricSecurityKey(bytes);
        return new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256Signature);
    }

    private void OnSettingsChanged(JwtBearerAuthenticationSettings settings)
    {
        SigningCredentials = CreateSigningCredentials(Settings.SigningKey!);
    }

    public JwtTokenObject GenerateToken(PlayerSession session, Claim[] claims)
    {
        var jwt = new JwtSecurityToken
        (
            issuer: Settings.Issuer,
            audience: Settings.Audience,
            notBefore: session.CreatedAt.UtcDateTime,
            expires: session.ExpirationDate.UtcDateTime,
            signingCredentials: SigningCredentials,
            claims: claims
        );

        return new JwtTokenObject()
        {
            Token = JwtSecurityTokenHandler.WriteToken(jwt),
            Type = JwtTokenObject.TokenType.JwtToken,
            NotBefore = session.CreatedAt.UtcDateTime,
            ExpirationDate = session.ExpirationDate.UtcDateTime
        };
    }

    public JwtTokenObject GenerateRefreshToken(PlayerSession session, Claim[] claims)
    {
        var notBefore = session.ExpirationDate.AddMinutes(-1).UtcDateTime;
        var jwt = new JwtSecurityToken
        (
            issuer: Settings.Issuer,
            audience: Settings.Audience,
            notBefore: notBefore,
            expires: session.ExpirationDate.UtcDateTime,
            signingCredentials: SigningCredentials,
            claims: claims
        );

        return new JwtTokenObject()
        {
            Token = JwtSecurityTokenHandler.WriteToken(jwt),
            Type = JwtTokenObject.TokenType.RefreshToken,
            NotBefore = notBefore,
            ExpirationDate = session.ExpirationDate.UtcDateTime
        };
    }
}