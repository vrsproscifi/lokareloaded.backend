﻿using System;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Loka.Common.Reflection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Loka.Common.Tarantool;

public class TarantoolClientService : IHostedService
{
    private TarantoolClient TarantoolClient { get; }
    private ILogger<TarantoolClientService> Logger { get; }

    public TarantoolClientService(TarantoolClient tarantoolClient, ILogger<TarantoolClientService> logger)
    {
        TarantoolClient = tarantoolClient;
        Logger = logger;
    }

    public async Task StartAsync(CancellationToken cancellationToken)
    {
        return;
        Logger.LogInformation($"Begin discovery converters");

        var assemblies = GenericReflectionExtensions.GetAssemblies();

        Logger.LogInformation($"Begin discovery converters, assemblies: {assemblies.Length}");
        foreach (var assembly in assemblies)
        {
            TarantoolClient.MsgPackContext.DiscoverConverters(assembly);
        }

        TarantoolClient.MsgPackContext.RegisterConverter(new MessagePackGuidConverter());

        Logger.LogInformation($"Begin {nameof(Tarantool)} connect to {TarantoolClient.ConnectionString}");
        await TarantoolClient.Client.Connect();
        Logger.LogInformation($"Complete {nameof(Tarantool)} connect to {TarantoolClient.ConnectionString} | IsConnected: {TarantoolClient.IsConnected}");
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        Logger.LogInformation($"Begin {nameof(Tarantool)} disconnect to {TarantoolClient.ConnectionString}");
        TarantoolClient.Client?.Dispose();
        Logger.LogInformation($"Complete {nameof(Tarantool)} disconnect to {TarantoolClient.Options.ConnectionOptions}");
        return Task.CompletedTask;
    }
}