﻿namespace Loka.Api.Features.Workbench;

internal sealed class PlayerWorkbenchMappingProfiles : Profile
{
    public PlayerWorkbenchMappingProfiles()
    {
        //==========================================================
        CreateMap<Loka.Inventory.Interfaces.Workbench.CraftWorkbenchItem.Result, CraftWorkbenchItem.Types.Response>()
            .ForMember(q => q.Item, q => q.MapFrom(w => w.Item));
    }
}