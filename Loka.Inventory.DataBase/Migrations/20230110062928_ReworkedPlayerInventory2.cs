﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Loka.Inventory.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class ReworkedPlayerInventory2 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ModelId",
                schema: "Inventory",
                table: "PlayerCaseEntity",
                newName: "CaseId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CaseId",
                schema: "Inventory",
                table: "PlayerCaseEntity",
                newName: "ModelId");
        }
    }
}
