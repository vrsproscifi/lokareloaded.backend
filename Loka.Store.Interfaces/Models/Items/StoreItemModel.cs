﻿using Loka.Common.Entry;
using Loka.Store.Interfaces.Enums;
using Loka.Store.Interfaces.Models.Fractions;
using Loka.Store.Interfaces.Models.Resources;

namespace Loka.Store.Interfaces.Models.Items;

[GenerateSerializer]
public sealed class StoreItemModel : IEntry, INamedEntry
{
    [Id(0)]
    public Guid Id { get; set; }

    [Id(1)]
    public string Name { get; set; } = null!;

    [Id(2)]
    public int Level { get; set; }

    [Id(3)]
    public StoreItemCategoryModel Category { get; set; } = null!;

    [Id(4)]
    public StoreFractionModel? Fraction { get; set; }

    [Id(5)]
    public GameResourceModel? SellCurrency { get; set; }

    [Id(6)]
    public int? SellPrice { get; set; }

    [Id(7)]
    public GameResourceModel? SubscribeCurrency { get; set; }

    [Id(8)]
    public int? SubscribePrice { get; set; }

    //------------------------------------
    //            Etc
    //------------------------------------
    [Id(9)]
    public ItemEntityFlags Flags { get; set; }

    [Id(10)]
    public TimeSpan? Duration { get; set; }

    [Id(11)]
    public int DisplayPosition { get; set; }

    //------------------------------------
    [Id(12)]
    public long AmountDefault { get; set; } = 1;

    [Id(13)]
    public long AmountInStack { get; set; } = 1;

    [Id(14)]
    public long AmountMinimum { get; set; } = 1;

    [Id(15)]
    public long? AmountMaximum { get; set; }

    //------------------------------------
    [Id(16)]
    public string? AdminComment { get; set; }

    //------------------------------------
    [Id(17)]
    public IReadOnlyList<StoreItemBlueprintModel> Blueprints { get; set; } = ArraySegment<StoreItemBlueprintModel>.Empty;
}