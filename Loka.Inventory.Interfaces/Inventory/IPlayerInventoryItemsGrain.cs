﻿using Loka.Inventory.Interfaces.Inventory.Requests;

namespace Loka.Inventory.Interfaces.Inventory;

public interface IPlayerInventoryItemsGrain : IGrainWithGuidKey
{
    Task<OperationResult<AddOrStackInventoryItem.Result>> AddItemToInventory(AddOrStackInventoryItem.Request request);
    Task<OperationResult<AddOrStackInventoryItem.Result>> AddOrStackInventoryItem(AddOrStackInventoryItem.Request request);

    Task<OperationResult<GetPlayerPurchasedItems.Result>> GetPurchasedItems();
}