﻿using Loka.Api.Features.Inventory.Items;
using Loka.Api.Features.Inventory.Presets;

namespace Loka.Api.IntegrationTests.Asserts;

public static class PlayerInventoryPresetsAsserts
{
    public static void AssertModel(this PlayerInventoryPreset? model)
    {
        Assert.That(model, Is.Not.Null, $"Invalid {model}");
        Assert.Multiple(() =>
        {
            Assert.That(model.Id, Is.Not.Null, $"Invalid {nameof(model.Id)} in {nameof(model)}");
            Assert.That(model.ModelId, Is.Not.Null, $"Invalid {nameof(model.ModelId)} in {nameof(model)}");
            Assert.That(model.Items, Is.Not.Null, $"Invalid {nameof(model.Items)} in {nameof(model)}");

            foreach (var item in model.Items)
            {
                Assert.That(item.Id, Is.Not.Null, $"Invalid {nameof(item.Id)} in {nameof(model)}({model.Id})");
                Assert.That(item.ItemId, Is.Not.Null, $"Invalid {nameof(item.ItemId)} in {nameof(model)}({model.Id})");
            }
        });
    }

    public static void AssertResponse(this EquipInventoryItem.Types.Response? response)
    {
        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.Multiple(() =>
        {
            Assert.That(response.Id, Is.Not.Null, $"Invalid {nameof(response)}.{nameof(response.Id)}");
            Assert.That(response.PresetId, Is.Not.Null, $"Invalid {nameof(response)}.{nameof(response.PresetId)}");
            
            response.Id.AssertModel();
            response.PresetId.AssertModel();
        });
    }

    public static void AssertResponse(this GetPlayerInventoryPresets.Types.Response? response)
    {
        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.That(response.Entities, Is.Not.Null, $"Invalid {nameof(response)}.{nameof(response.Entities)}");

        foreach (var model in response.Entities)
        {
            model.AssertModel();
        }
    }
}