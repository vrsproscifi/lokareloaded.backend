﻿using Loka.Game.Database.Entities.Achievements;
using Loka.Game.Database.Enums.Achievements;

namespace Loka.Game.Database.Seeders.Achievements;

public static class AchievementEntitySeeder
{
    public static Guid Kills { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe100");
    public static Guid Deaths { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe101");
    public static Guid Score { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe102");
    public static Guid Money { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe103");
    public static Guid Experience { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe104");
    public static Guid FirstBlood { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe105");
    public static Guid Assist { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe106");
    public static Guid KillsByPistol { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe107");
    public static Guid KillsByShotGun { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe108");
    public static Guid KillsBySniperRifle { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe109");
    public static Guid KillsByRifle { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe110");
    public static Guid KillsByKnife { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe111");
    public static Guid KillsByGrenade { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe112");
    public static Guid SeriesKills { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe113");
    public static Guid SeriesKillsByPistol { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe114");
    public static Guid SeriesKillsByShotGun { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe115");
    public static Guid SeriesKillsBySniperRifle { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe116");
    public static Guid SeriesKillsByRifle { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe117");
    public static Guid SeriesKillsByKnife { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe118");
    public static Guid SeriesKillsByGrenade { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe119");
    public static Guid MatchesTotal { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe120");
    public static Guid MatchesWins { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe121");
    public static Guid MatchesLoses { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe122");
    public static Guid MatchesDefeats { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe123");
    public static Guid HeadShot { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe124");
    public static Guid TimeInGame { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe125");
    public static Guid BestScore { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe126");
    public static Guid BestKills { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe127");
    public static Guid FirstInMatch { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe128");
    public static Guid SecondInMatch { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe129");
    public static Guid ThirdInMatch { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-900c8dffe130");

    public static AchievementEntity[] Entities { get; } = new[]
    {
        new AchievementEntity(Kills, AchievementTypeId.Kills, AchievementMethodId.Incrementable),
        new AchievementEntity(Deaths, AchievementTypeId.Deaths, AchievementMethodId.Incrementable),
        new AchievementEntity(Score, AchievementTypeId.Score, AchievementMethodId.Incrementable),
        new AchievementEntity(Money, AchievementTypeId.Money, AchievementMethodId.Incrementable),
        new AchievementEntity(Experience, AchievementTypeId.Experience, AchievementMethodId.Incrementable),
            
        new AchievementEntity(FirstBlood, AchievementTypeId.FirstBlood, AchievementMethodId.Incrementable),
        new AchievementEntity(Assist, AchievementTypeId.Assist, AchievementMethodId.Incrementable),
            
        new AchievementEntity(KillsByPistol, AchievementTypeId.KillsByPistol, AchievementMethodId.Incrementable),
        new AchievementEntity(KillsByShotGun, AchievementTypeId.KillsByShotGun, AchievementMethodId.Incrementable),
        new AchievementEntity(KillsBySniperRifle, AchievementTypeId.KillsBySniperRifle, AchievementMethodId.Incrementable),
        new AchievementEntity(KillsByRifle, AchievementTypeId.KillsByRifle, AchievementMethodId.Incrementable),
        new AchievementEntity(KillsByKnife, AchievementTypeId.KillsByKnife, AchievementMethodId.Incrementable),
        new AchievementEntity(KillsByGrenade, AchievementTypeId.KillsByGrenade, AchievementMethodId.Incrementable),
            
        new AchievementEntity(SeriesKills, AchievementTypeId.SeriesKills, AchievementMethodId.Incrementable),
        new AchievementEntity(SeriesKillsByPistol, AchievementTypeId.SeriesKillsByPistol, AchievementMethodId.Incrementable),
        new AchievementEntity(SeriesKillsByShotGun, AchievementTypeId.SeriesKillsByShotGun, AchievementMethodId.Incrementable),
        new AchievementEntity(SeriesKillsBySniperRifle, AchievementTypeId.SeriesKillsBySniperRifle, AchievementMethodId.Incrementable),
        new AchievementEntity(SeriesKillsByRifle, AchievementTypeId.SeriesKillsByRifle, AchievementMethodId.Incrementable),
        new AchievementEntity(SeriesKillsByKnife, AchievementTypeId.SeriesKillsByKnife, AchievementMethodId.Incrementable),
        new AchievementEntity(SeriesKillsByGrenade, AchievementTypeId.SeriesKillsByGrenade, AchievementMethodId.Incrementable),
            
        new AchievementEntity(MatchesTotal, AchievementTypeId.MatchesTotal, AchievementMethodId.Incrementable),
        new AchievementEntity(MatchesWins, AchievementTypeId.MatchesWins, AchievementMethodId.Incrementable),
        new AchievementEntity(MatchesLoses, AchievementTypeId.MatchesLoses, AchievementMethodId.Incrementable),
        new AchievementEntity(MatchesDefeats, AchievementTypeId.MatchesDefeats, AchievementMethodId.Incrementable),
        new AchievementEntity(HeadShot, AchievementTypeId.HeadShot, AchievementMethodId.Incrementable),
        new AchievementEntity(TimeInGame, AchievementTypeId.TimeInGame, AchievementMethodId.Incrementable),
            
        new AchievementEntity(BestScore, AchievementTypeId.BestScore, AchievementMethodId.Settable),
        new AchievementEntity(BestKills, AchievementTypeId.BestKills, AchievementMethodId.Settable),
            
        new AchievementEntity(FirstInMatch, AchievementTypeId.FirstInMatch, AchievementMethodId.Incrementable),
        new AchievementEntity(SecondInMatch, AchievementTypeId.SecondInMatch, AchievementMethodId.Incrementable),
        new AchievementEntity(ThirdInMatch, AchievementTypeId.ThirdInMatch, AchievementMethodId.Incrementable),
    };
}