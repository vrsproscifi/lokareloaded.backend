﻿namespace Loka.Inventory.Interfaces.Payments;

public enum PlayerPaymentTransactionTypeId
{
    None,
    Item,
    Booster,
    Case,
    Preset,
    Template,
    Craft
}