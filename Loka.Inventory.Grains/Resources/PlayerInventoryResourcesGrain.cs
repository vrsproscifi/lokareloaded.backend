﻿using Loka.Common.Authentication;
using Loka.Common.MediatR.Executors;
using Loka.Common.Orleans.Grains;
using Loka.Inventory.Interfaces.Resources;

namespace Loka.Inventory.Grains.Resources;

public sealed class PlayerInventoryResourcesGrain : Grain, IPlayerInventoryResourcesGrain, IWithPlayerActor
{
    public PlayerActor Actor { get; set; } = null!;
    private IOperationExecutor OperationExecutor { get; }
    private IPlayerResourcesRepository PlayerResourcesRepository { get; }

    public PlayerInventoryResourcesGrain
    (
        IOperationExecutor operationExecutor,
        IPlayerResourcesRepository playerResourcesRepository
    )
    {
        OperationExecutor = operationExecutor;
        PlayerResourcesRepository = playerResourcesRepository;
    }

    public async Task<OperationResult<GetPlayerResources.Result>> GetResources()
    {
        var resources = await PlayerResourcesRepository.GetAll(Actor.PlayerId);
        return new GetPlayerResources.Result()
        {
            Entries = resources
        };
    }

    public async Task<OperationResult<GetPlayerResources.Result>> GetResources(IReadOnlyList<Guid> ids)
    {
        var resources = await PlayerResourcesRepository.GetByIds(Actor.PlayerId, ids);
        return new GetPlayerResources.Result()
        {
            Entries = resources
        };
    }

    public Task<PlayerResourceModel.Lightweight> GetResourceByType(Guid id)
    {
        return PlayerResourcesRepository.GetOrCreateByType(Actor.PlayerId, id);
    }

    public async Task<OperationResult<PlayerResourceModel.Delta>> GiveInventoryResource(GiveInventoryResource.Request request)
    {
        var getResourceResult = await PlayerResourcesRepository.GetById(Actor.PlayerId, request.EntityId);
        if (getResourceResult.GetValueIfSucceeded(out var resource))
        {
            if (request.Amount < 0 && resource.Amount < request.Amount)
                return OperationResultCode.ResourceNotEnough;

            var result = await PlayerResourcesRepository.Update(Actor.PlayerId, new UpdatePlayerResourceRequest()
            {
                ResourceId = resource.Id,
                Value = resource.Amount + request.Amount
            });
            return result;
        }

        return getResourceResult.Error!;
    }

    public async Task<OperationResult<ExpendInventoryResources.Result>> ExpendInventoryResources(ExpendInventoryResources.Request request)
    {
        var ids = request.Resources
            .Select(q => q.EntityId)
            .ToArray();

        var resources = await PlayerResourcesRepository.GetByIds(Actor.PlayerId, ids);
        foreach (var resource in resources)
        {
            var requirement = request.Resources.First(q => q.EntityId == resource.Id);
            if (resource.Amount < requirement.Amount)
                return OperationResultCode.ResourceNotEnough;
        }
        
        var updateResult = await PlayerResourcesRepository.Update(Actor.PlayerId, resources.Select(resource =>
        {
            var requirement = request.Resources.First(q => q.EntityId == resource.Id);
            return new UpdatePlayerResourceRequest
            {
                ResourceId = resource.Id,
                Value = resource.Amount - requirement.Amount
            };
        }).ToArray());

        return new ExpendInventoryResources.Result()
        {
            Resources = updateResult
        };
    }
}