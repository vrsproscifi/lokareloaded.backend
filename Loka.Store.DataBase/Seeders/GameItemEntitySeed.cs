﻿using Loka.Store.DataBase.Entities.Items;

namespace Loka.Store.DataBase.Seeders;

public static class GameItemEntitySeed
{
    public static Guid AK47 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe906");
    public static Guid AK47_BP1 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe916");
    public static Guid AK47_BP2 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe926");
    public static Guid M16 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe907");
    public static Guid Shield { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe908");
    public static Guid Grenade1 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe909");
    public static Guid Grenade1_BP1 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe116");
    public static Guid Grenade2 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe910");
    public static Guid Grenade2_BP1 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe110");

    public static Guid Aid1 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe911");
    public static Guid Aid1_BP1 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe911");
    public static Guid Aid2 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe912");
    public static Guid Aid2_BP1 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe912");

    public static Guid Profile_01 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe001");
    public static Guid Profile_02 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe002");
    public static Guid Profile_03 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe003");

    public static GameItemEntity[] Entities { get; } = new[]
    {
        new GameItemEntity(AK47, GameItemCategoryEntitySeed.Weapon, "AK-47", FractionEntitySeed.Keepers, 1000, GameResourceEntitySeed.Money, ItemEntityFlags.IssueAfterRegistration),
        new GameItemEntity(M16, GameItemCategoryEntitySeed.Weapon, "M16", FractionEntitySeed.Keepers, 1500, GameResourceEntitySeed.Money),
        new GameItemEntity(Shield, GameItemCategoryEntitySeed.Armour, "Shield", FractionEntitySeed.Keepers, 1000, GameResourceEntitySeed.Money),
        new GameItemEntity(Grenade1, GameItemCategoryEntitySeed.Grenade, "Grenade 1", FractionEntitySeed.Keepers, 500, GameResourceEntitySeed.Money)
        {
            AmountInStack = -1
        },
        new GameItemEntity(Grenade2, GameItemCategoryEntitySeed.Grenade, "Grenade 2", FractionEntitySeed.RIFT, 750, GameResourceEntitySeed.Money)
        {
            AmountInStack = -1
        },     
        
        new GameItemEntity(Aid1, GameItemCategoryEntitySeed.Kits, "AID 1", FractionEntitySeed.Keepers, 500, GameResourceEntitySeed.Money)
        {
            AmountInStack = -1
        },
        new GameItemEntity(Aid2, GameItemCategoryEntitySeed.Kits, "AID 2", FractionEntitySeed.RIFT, 750, GameResourceEntitySeed.Money)
        {
            AmountInStack = -1
        },
        new GameItemEntity(Profile_01, GameItemCategoryEntitySeed.Profile, "Profile 01", FractionEntitySeed.Keepers, 0, GameResourceEntitySeed.Money, ItemEntityFlags.IssueAfterRegistration),
        new GameItemEntity(Profile_02, GameItemCategoryEntitySeed.Profile, "Profile 02", FractionEntitySeed.Keepers, 10000, GameResourceEntitySeed.Money),
        new GameItemEntity(Profile_03, GameItemCategoryEntitySeed.Profile, "Profile 03", FractionEntitySeed.Keepers, 100, GameResourceEntitySeed.Donate),
    };

    public static GameItemBlueprintEntity[] GameItemBlueprintEntities { get; } = new[]
    {
        new GameItemBlueprintEntity()
        {
            EntityId = AK47_BP1,
            ItemModelId = AK47
        },
        new GameItemBlueprintEntity()
        {
            EntityId = AK47_BP2,
            ItemModelId = AK47
        },
        new GameItemBlueprintEntity()
        {
            EntityId = Grenade1_BP1,
            ItemModelId = Grenade1
        },
        new GameItemBlueprintEntity()
        {
            EntityId = Grenade2_BP1,
            ItemModelId = Grenade2
        },
        new GameItemBlueprintEntity()
        {
            EntityId = Aid1_BP1,
            ItemModelId = Aid1
        },
        new GameItemBlueprintEntity()
        {
            EntityId = Aid2_BP1,
            ItemModelId = Aid2
        },
    };

    public static GameItemBlueprintDependEntity[] GameItemBlueprintDependEntities { get; } = new[]
    {
        #region AK47

        new GameItemBlueprintDependEntity()
        {
            EntityId = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe910"),
            BlueprintId = AK47_BP2,
            ResourceId = GameResourceEntitySeed.Copper,
            Amount = 1000
        },
        new GameItemBlueprintDependEntity()
        {
            EntityId = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe911"),
            BlueprintId = AK47_BP2,
            ResourceId = GameResourceEntitySeed.Iron,
            Amount = 50
        },
        new GameItemBlueprintDependEntity()
        {
            EntityId = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe912"),
            BlueprintId = AK47_BP2,
            ResourceId = GameResourceEntitySeed.Steel,
            Amount = 50
        },

        #endregion

        #region Grenades
        
        new GameItemBlueprintDependEntity()
        {
            EntityId = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe913"),
            BlueprintId = Grenade1_BP1,
            ResourceId = GameResourceEntitySeed.Steel,
            Amount = 10
        },
        new GameItemBlueprintDependEntity()
        {
            EntityId = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe914"),
            BlueprintId = Grenade2_BP1,
            ResourceId = GameResourceEntitySeed.Steel,
            Amount = 25
        },

        #endregion
        
        #region Aids
        
        new GameItemBlueprintDependEntity()
        {
            EntityId = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe915"),
            BlueprintId = Aid1_BP1,
            ResourceId = GameResourceEntitySeed.Rags,
            Amount = 10
        },
        new GameItemBlueprintDependEntity()
        {
            EntityId = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe916"),
            BlueprintId = Aid2_BP1,
            ResourceId = GameResourceEntitySeed.Rags,
            Amount = 25
        },

        #endregion

    };
}