﻿using System;
using System.Linq;
using Loka.Database.Common.Entities;

namespace Loka.Database.Common.Specifications;

public sealed class PlayerChildSpecification<TEntity> : Specification.IQueryableSpecification<TEntity>
    where TEntity : BaseEntity, IPlayerChildEntity
{
    private Guid PlayerId { get; }

    public PlayerChildSpecification(Guid playerId)
    {
        PlayerId = playerId;
    }

    public IQueryable<TEntity> Apply(IQueryable<TEntity> query)
    {
        return query.Where(q => q.PlayerId == PlayerId);
    }
}