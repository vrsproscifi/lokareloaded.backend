﻿namespace Loka.Social.Interfaces.Forms;

public static class GetPlayerFriendsList
{
    [GenerateSerializer]
    public class Request
    {
        [Id(0)]
        public string? Term { get; init; }

        [Id(1)]
        public bool IncludeBanned { get; init; }
    }

    [GenerateSerializer]
    public class Result
    {
        [Id(0)]
        public IReadOnlyList<PlayerFriendEntryModel> Entries { get; set; } = ArraySegment<PlayerFriendEntryModel>.Empty;
    }
}