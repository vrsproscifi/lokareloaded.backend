﻿using MongoDB.Bson;

namespace Loka.Chat.Interfaces.Models;

[GenerateSerializer]
public sealed class ChatMessageEntry
{
    [Id(0)]
    public ObjectId Id { get; set; }

    [Id(1)]
    public ChatMemberEntry Sender { get; set; } = null!;

    [Id(2)]
    public string Message { get; set; } = null!;

    [Id(3)]
    public DateTime CreatedAt { get; set; }
}