﻿namespace Loka.Game.Database.Enums.Achievements;

public enum AchievementMethodId : byte
{
    /// <summary>
    /// Used for repeated actions. For example: the number of murders, the number of fights, wins
    /// </summary>
    Incrementable,

    /// <summary>
    /// Used for actions that can be performed once, but have a lot of steps. For example a series of murders
    /// </summary>
    Settable,

    End
}