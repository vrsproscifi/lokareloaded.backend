﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Entities;

namespace Loka.MatchMaking.DataBase.Entities.Match;

public sealed class MatchTeamEntity : BaseEntity
{
    [ForeignKey(nameof(MatchEntity))]
    public Guid MatchId { get; set; }
    public MatchEntity? MatchEntity { get; set; }
        
    public List<MatchMemberEntity>? Members { get; set; }
}