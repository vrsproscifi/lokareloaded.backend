﻿using Loka.Deployment.DataBase.Entities;

namespace Loka.Deployment.DataBase;

public sealed class DeploymentDbContext : BaseGameDbContext<DeploymentDbContext>
{
    public DeploymentDbContext(DbContextOptions<DeploymentDbContext> options) : base(options)
    {
    }

    public DbSet<GameVersionEntity> GameVersionEntity { get; set; } = null!;
    public DbSet<ServerVersionEntity> ServerVersionEntity { get; set; } = null!;
}