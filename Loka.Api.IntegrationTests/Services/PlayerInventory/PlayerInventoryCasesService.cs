﻿using Loka.Api.Features.Inventory.Cases;
using Loka.Api.Features.Shared;
using Loka.Api.IntegrationTests.Clients;
using Loka.Api.IntegrationTests.Services.PlayerIdentity;

namespace Loka.Api.IntegrationTests.Services.PlayerInventory;

public sealed class PlayerInventoryCasesService
{
    private Loka.Api.Features.Inventory.Cases.PlayerInventoryCases.PlayerInventoryCasesClient PlayerInventoryCasesClient { get; }

    public PlayerInventoryCasesService(GameAPIClients client)
    {
        PlayerInventoryCasesClient = client.PlayerInventoryCasesClient;
    }

    public Task<PlayerInventoryCase> BuyGameStoreCase(PlayerSession playerSession, StoreCase.Types.Model model)
    {
        return BuyGameStoreCase(playerSession, model.Id);
    }

    public async Task<PlayerInventoryCase> BuyGameStoreCase(PlayerSession playerSession, UUID modelId)
    {
        var response = await PlayerInventoryCasesClient.BuyGameStoreCaseAsync(new BuyGameStoreCase.Types.Request()
        {
            ModelId = modelId
        }, playerSession.UseJWT());
       
        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.That(response.Case, Is.Not.Null, $"Invalid {nameof(response)}.{nameof(response.Case)}");
        Assert.That(response.Case.ModelId, Is.EqualTo(modelId), $"Invalid {nameof(response)}.{nameof(response.Case)}.{nameof(response.Case.ModelId)}");
        response.Case.Id.AssertModel();
        response.Case.ModelId.AssertModel();

        return response.Case;
    }

    public async Task<IReadOnlyList<PlayerInventoryCase>> GetPlayerCases(PlayerSession playerSession)
    {
        var response = await PlayerInventoryCasesClient.GetPlayerCasesAsync(new GetPlayerCases.Types.Request(), playerSession.UseJWT());
        response.AssertResponse();
        return response!.Entries;
    }

    public Task<OpenPlayerCase.Types.Response> OpenPlayerCase(PlayerSession playerSession, PlayerInventoryCase entry)
    {
        return OpenPlayerCase(playerSession, entry.Id);
    }

    public async Task<OpenPlayerCase.Types.Response> OpenPlayerCase(PlayerSession playerSession, UUID caseId)
    {
        var response = await PlayerInventoryCasesClient.OpenPlayerCaseAsync(new OpenPlayerCase.Types.Request()
        {
            Id = caseId
        }, playerSession.UseJWT());
        response.AssertResponse();
        await TestContext.Progress.WriteLineAsync($"OpenPlayerCase: {caseId} | PlayerId: {playerSession.PlayerId}");

        return response!;
    }
}