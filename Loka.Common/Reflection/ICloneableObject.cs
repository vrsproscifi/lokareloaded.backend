﻿namespace Loka.Common.Reflection;

public interface ICloneableObject<out T>
{
    T CloneObject();
}