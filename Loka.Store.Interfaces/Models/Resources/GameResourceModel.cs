﻿using Loka.Common.Entry;
using Loka.Store.Interfaces.Enums;

namespace Loka.Store.Interfaces.Models.Resources;

[GenerateSerializer]
public sealed class GameResourceModel : IEntry, INamedEntry
{
    [Id(0)]
    public Guid Id { get; init; }

    [Id(1)]
    public string Name { get; init; } = null!;

    [Id(2)]
    public ResourceTypeId TypeId { get; init; }

    [Id(3)]
    public int DefaultValue { get; init; }

    public override string ToString()
    {
        return $"Id: {Id} | {TypeId} | {Name}";
    }
}