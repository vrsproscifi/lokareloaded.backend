﻿using System;

namespace Loka.Admin.Players.Handlers.Players.Listing.GetPlayersList.Filters;

public sealed class GetPlayersListRequestFilter
{
    public string Term { get; set; }
    public Guid[] CountriesIds { get; set; } = Array.Empty<Guid>();
    public Guid[] LanguagesIds { get; set; } = Array.Empty<Guid>();
        
        
    public bool? EmailConfirmed { get; set; }
    public bool? NameConfirmed { get; set; }
    public bool? HasActivePremium { get; set; }
        
    public GetPlayersListRequestStatisticsFilter Statistics { get; set; }
}