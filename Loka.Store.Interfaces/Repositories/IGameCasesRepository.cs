﻿using Loka.Database.Common.Repositories;
using Loka.Store.Interfaces.Models.Cases;

namespace Loka.Store.Interfaces.Repositories;

public interface IGameCasesRepository : IBaseRepository<StoreCaseModel>
{

}