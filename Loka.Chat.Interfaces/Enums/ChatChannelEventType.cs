﻿namespace Loka.Chat.Interfaces.Enums;

public enum ChatChannelEventType
{
    Create,
    Close,
    ChangeName,
    
    Invite,
    Kick,
    Left,
    AssignRole,
}