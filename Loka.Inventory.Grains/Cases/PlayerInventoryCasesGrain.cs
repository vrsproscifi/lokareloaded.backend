﻿using Loka.Common.Authentication;
using Loka.Common.MediatR.Executors;
using Loka.Common.Orleans.Grains;
using Loka.Inventory.Interfaces.Cases;

namespace Loka.Inventory.Grains.Cases;

public sealed class PlayerInventoryCasesGrain : Grain, IPlayerInventoryCasesGrain, IWithPlayerActor
{
    public PlayerActor Actor { get; set; } = null!;
    private IOperationExecutor OperationExecutor { get; }
    private IPlayerInventoryCasesRepository PlayerInventoryCasesRepository { get; }

    public PlayerInventoryCasesGrain(IOperationExecutor operationExecutor, IPlayerInventoryCasesRepository playerInventoryCasesRepository)
    {
        OperationExecutor = operationExecutor;
        PlayerInventoryCasesRepository = playerInventoryCasesRepository;
    }

    public Task<PlayerInventoryCaseModel> AddOrStackInventoryCase(AddOrStackInventoryCase.Request request)
    {
        return PlayerInventoryCasesRepository.AddOrStack(Actor.PlayerId, request);
    }

    public Task<OperationResult<OpenPlayerCase.Result>> OpenCase(OpenPlayerCase.Request request)
    {
        return OperationExecutor.Execute(request);
    }

    public async Task<OperationResult<GetPlayerCases.Result>> GetCases()
    {
        var cases = await PlayerInventoryCasesRepository.GetAll(Actor.PlayerId);
        return new GetPlayerCases.Result()
        {
            Entries = cases
        };
    }
}