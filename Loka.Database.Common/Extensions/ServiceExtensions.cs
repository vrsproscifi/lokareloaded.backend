﻿using System;
using System.Linq;
using Loka.Common;
using Loka.Common.EfCore.DbQueries;
using Loka.Common.Reflection;
using Loka.Database.Common.Contexts;
using Loka.Database.Common.Seeders;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Database.Common.Extensions;

public delegate void ConfigureDbContextDelegate(DbContextOptionsBuilder optionsBuilder, string connectionString, string schemeName, string migrationsAssemblyName);

public static class ServiceExtensions
{
    public static IServiceCollection AddGameDbContextSeeders(this IServiceCollection services, IConfiguration configuration)
    {
        var seeders = GenericReflectionExtensions.GetLoadedTypes()
            .Where(q => q.IsImplementedInterface<IDataBaseSeeder>())
            .Where(q => !q.IsAbstract)
            .ToArray();

        foreach (var seeder in seeders)
        {
            services.AddScoped(typeof(IDataBaseSeeder), seeder);
        }

        return services;
    }

    public static IServiceCollection AddGameDbQueries(this IServiceCollection services, IConfiguration configuration)
    {
        var dbQueries = GenericReflectionExtensions.GetLoadedTypes()
            .Where(q => q.IsImplementedInterface<IBaseDbQueries>())
            .Where(q => !q.IsAbstract)
            .ToArray();

        foreach (var dbQuery in dbQueries)
        {
            var queryInterface = dbQuery.GetInterfaces();
            services.AddScoped(queryInterface[1], dbQuery);
        }

        return services;
    }

    public static ServiceCollectionInitializer AddGameDbContext<TDbContext>(this ServiceCollectionInitializer initializer, ConfigureDbContextDelegate? configureDbContext = default)
        where TDbContext : BaseGameDbContext<TDbContext>
    {
        //var dbContextName = BaseGameDbContext.GetDbContextName<TDbContext>();
        var dbContextName = "SystemDbContext";
        var connectionString = initializer.Configuration
            .GetSection("Application.DataBase")
            .GetValue<string>(dbContextName);

        var schemeName = BaseGameDbContext.GetDbContextScheme<TDbContext>();
        var migrationsAssemblyName = typeof(TDbContext).Assembly.GetName().Name;

        // Console.WriteLine($"AddGameDbContext[dbContextName: {dbContextName}][schemeName: {schemeName}] | connectionString: {connectionString}");

        initializer.Services.AddDbContext<TDbContext>(builder =>
        {
            builder.EnableDetailedErrors();
            if (configureDbContext == null)
            {
                builder.UseNpgsql(connectionString, options =>
                {
                    options.EnableRetryOnFailure();
                    options.UseRelationalNulls(false);
                    options.MigrationsHistoryTable(BaseGameDbContext.MigrationsHistoryTable, schemeName);
                    options.MigrationsAssembly(typeof(TDbContext).Assembly.GetName().Name);
                });
            }
            else
            {
                configureDbContext(builder, connectionString, schemeName, migrationsAssemblyName);
            }
        });

        return initializer;
    }
}