﻿using Loka.Api.Features.Identity;
using Loka.Api.Features.Shared;
using Loka.Api.IntegrationTests.Services.PlayerIdentity;
using Microsoft.Net.Http.Headers;

namespace Loka.Api.IntegrationTests.Extensions;

public static class RequestMetadataExtensions
{
    public static UUID ToUUID(this Guid guid)
    {
        return new UUID()
        {
            Value = guid.ToString("D")
        };
    }

    public static Metadata UseJWT(this PlayerAuthenticationResult result)
    {
        result.AssertModel();

        return new Metadata()
        {
            {
                HeaderNames.Authorization, $"Bearer {result.Token.JwtToken.Token}"
            }
        };
    }

    public static Metadata UseJWT(this PlayerSession result)
    {
        return result.Result.UseJWT();
    }
}