﻿using Loka.Inventory.Interfaces.Inventory;

namespace Loka.Inventory.Interfaces.Store.Requests;

public static class BuyStoreItem
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<Result>
    {
        [Id(0)]
        public Guid ModelId { get; init; }
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public PlayerInventoryItemModel.RPC Item { get; init; } = null!;
    }
}