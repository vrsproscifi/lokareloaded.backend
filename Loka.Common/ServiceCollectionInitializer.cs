﻿using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Loka.Common.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Loka.Common;

public sealed class ServiceCollectionInitializer
{
    public IServiceCollection Services { get; }
    public IConfiguration Configuration { get; }
    public IHostEnvironment Environment { get; }
    public string ApplicationName { get; }
    public Type[] Types { get; }
    public Type[] LoadedTypes { get; }
    public Assembly[] Assemblies { get; }

    private void LoadReferencedAssemblies(Assembly entry)
    {
        var assembiles = entry
            .GetReferencedAssemblies()
            .Where(q => q.Name!.Contains(ApplicationName))
            .ToArray();

        foreach (var assembly in assembiles)
        {
            LoadReferencedAssemblies(Assembly.Load(assembly));
        }
    }

    public ServiceCollectionInitializer(string applicationName, IServiceCollection services, IConfiguration configurations, IHostEnvironment environments)
    {
        Services = services;
        Configuration = configurations;
        Environment = environments;
        ApplicationName = applicationName;

        LoadReferencedAssemblies(Assembly.GetEntryAssembly()!);
        LoadedTypes = GenericReflectionExtensions.GetLoadedTypes();
        Types = LoadedTypes
            .Where(q => q.Assembly.IsDynamic == false && q.Assembly.GetName().Name!.Contains(ApplicationName))
            .ToArray();

        Assemblies = Types.Select(q => q.Assembly).Distinct().ToArray();
    }

    public ServiceCollectionInitializer AddScoped<[DynamicallyAccessedMembers(DynamicallyAccessedMemberTypes.PublicConstructors)] TService>()
        where TService : class
    {
        Services.AddScoped(typeof(TService));
        return this;

    }
    
    public ServiceCollectionInitializer AddScoped<TService, [DynamicallyAccessedMembers(DynamicallyAccessedMemberTypes.PublicConstructors)] TImplementation>()
        where TService : class
        where TImplementation : class, TService
    {
        Services.AddScoped<TService, TImplementation>();
        return this;
    }
}