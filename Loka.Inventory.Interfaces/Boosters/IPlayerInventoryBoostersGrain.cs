﻿namespace Loka.Inventory.Interfaces.Boosters;

public interface IPlayerInventoryBoostersGrain : IGrainWithGuidKey
{
    Task<OperationResult<PlayerInventoryBoosterModel>> ActivatePlayerBooster(ActivatePlayerBooster.Request request);
    Task<OperationResult<PlayerInventoryBoosterModel>> DeactivatePlayerBooster(DeactivatePlayerBooster.Request request);
    Task<PlayerInventoryBoosterModel> AddOrStackInventoryBooster(AddOrStackInventoryBooster.Request request);
    Task<OperationResult<GetPlayerBoosters.Result>> GetPlayerBoosters();
}