﻿using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Errors;

namespace Loka.Common.Operations.Results;

[GenerateSerializer]
[DebuggerDisplay("IsSucceeded: {IsSucceeded}, Error: {Error?.Code} | {Error?.ErrorMessage}")]
public class OperationResult
{
    [Id(0)]
    public OperationError? Error { get; }

    public virtual bool IsSucceeded => Error == null || Error.ResultCode == OperationResultCode.Successfully;

    [MemberNotNullWhen(true, nameof(Error))]
    public bool IsFailed => !IsSucceeded;

    public OperationResult()
    {
        Error = null;
    }

    public OperationResult(OperationError error)
    {
        Error = error;
    }

    public OperationResult(OperationResultCode code, params object[] args)
    {
        if (code != OperationResultCode.Successfully)
        {
            Error = new OperationError(code, args);
        }
    }

    public OperationResult(OperationResultCode code)
    {
        if (code != OperationResultCode.Successfully)
        {
            Error = new OperationError(code);
        }
    }

    public static implicit operator OperationResult(OperationResultCode code) => new OperationResult(code);
    public static implicit operator OperationResult(OperationError error) => new OperationResult(error);

    public static implicit operator OperationResult((OperationResultCode code, object[] args) response) => new OperationResult(response.code, response.args);
    public static implicit operator OperationResult((OperationResultCode code, object args) response) => new OperationResult(response.code, response.args);
}

[GenerateSerializer]
[DebuggerDisplay("IsSucceeded: {IsSucceeded}, Result: {Result}, Error: {Error?.ErrorMessage}")]
public class OperationResult<TResult> : OperationResult
{
    [Id(1)]
    public TResult? Result { get; }

    [MemberNotNullWhen(true, nameof(Result))]
    public override bool IsSucceeded => base.IsSucceeded;

    public OperationResult()
    {
        Result = default;
    }

    public OperationResult(TResult result)
    {
        Result = result;
    }

    public OperationResult(OperationError error) : base(error)
    {
    }

    public OperationResult(OperationResultCode code) : base(code)
    {
    }

    public OperationResult(OperationResultCode code, params object[] args) : base(code, args)
    {
    }


    public bool GetValueIfSucceeded([NotNullWhen(true)] out TResult? result)
    {
        result = Result;

        return IsSucceeded;
    }

    public static implicit operator OperationResult<TResult>(TResult result) => new OperationResult<TResult>(result);
    public static implicit operator OperationResult<TResult>(OperationResultCode code) => new OperationResult<TResult>(code);
    public static implicit operator OperationResult<TResult>(OperationError error) => new OperationResult<TResult>(error);


    public static implicit operator OperationResult<TResult>((OperationResultCode code, object[] args) response) => new OperationResult<TResult>(response.code, response.args);
    public static implicit operator OperationResult<TResult>((OperationResultCode code, object args) response) => new OperationResult<TResult>(response.code, response.args);
}