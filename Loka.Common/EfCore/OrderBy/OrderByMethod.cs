﻿using System;

namespace Loka.Common.EfCore.OrderBy;

public readonly struct OrderByMethod<TOrder> : IOrderByMethod<TOrder>
    where TOrder : Enum
{
    public bool OrderByAsc { get; }
    public TOrder OrderBy { get; }
        
    public OrderByMethod(TOrder orderBy, bool orderByAsc = true)
    {
        OrderBy = orderBy;
        OrderByAsc = orderByAsc;
    }

    public override string ToString()
    {
        return $"OrderBy: {OrderBy}, OrderByAsc: {OrderByAsc}";
    }
}