﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Loka.Store.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class AddedBlueprintSeeders6 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "BlueprintId",
                value: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe926"));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe911"),
                column: "BlueprintId",
                value: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe926"));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe912"),
                column: "BlueprintId",
                value: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe926"));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "BlueprintId",
                value: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe916"));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe911"),
                column: "BlueprintId",
                value: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe916"));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe912"),
                column: "BlueprintId",
                value: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe916"));
        }
    }
}
