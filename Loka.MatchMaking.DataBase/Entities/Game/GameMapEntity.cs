﻿using System;
using System.ComponentModel.DataAnnotations;
using Loka.Database.Common.Entities;
using Loka.Database.Common.Interfaces;
using Loka.MatchMaking.DataBase.Enums.Game;
using Microsoft.EntityFrameworkCore;

namespace Loka.MatchMaking.DataBase.Entities.Game;

[Index(nameof(Name))]
[Index(nameof(TypeId), IsUnique = true)]
public class GameMapEntity : BaseEntity, INamedEntity
{
    [Required]
    [MaxLength(64)]
    public string Name { get; set; } = null!;

    public GameMapTypeId TypeId { get; set; }

    public int MinimumMembers { get; set; }
    public int MaximumMembers { get; set; }


    public GameMapEntity()
    {
    }

    internal GameMapEntity(Guid id, GameMapTypeId typeId) : base(id)
    {
        Name = typeId.ToString();
        TypeId = typeId;
    }
}