﻿using Loka.Common.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Orleans.Runtime;

namespace Loka.Common.MassTransit.MessageBrokers;

public abstract class ClusterConsumer<TEvent> : IConsumer<TEvent> where TEvent : class, IClusterEvent
{
    protected ClusterConsumerServices<ClusterConsumer<TEvent>> Services { get; }

    protected ClusterConsumer(ClusterConsumerServices<ClusterConsumer<TEvent>> services)
    {
        Services = services;
    }

    public virtual Task Consume(ConsumeContext<TEvent> context)
    {
        RequestContext.Set(nameof(PlayerActor.PlayerId), context.Message.PlayerId);
        RequestContext.Set(nameof(PlayerActor.PlayerName), context.Message.PlayerName);


        var actorStorage = Services.ServiceProvider.GetRequiredService<PlayerActorStorage>();
        actorStorage.Actor = new PlayerActor(context.Message.PlayerId, context.Message.PlayerName);

        return Task.CompletedTask;
    }
}