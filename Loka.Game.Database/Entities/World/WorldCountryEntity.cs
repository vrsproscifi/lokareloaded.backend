﻿using System.ComponentModel.DataAnnotations;
using Loka.Database.Common.Entities;
using Loka.Game.Database.Enums.World;

namespace Loka.Game.Database.Entities.World;

[Index(nameof(Name))]
[Index(nameof(Code))]
[Index(nameof(TypeId), IsUnique = true)]
public sealed class WorldCountryEntity : BaseEntity, INamedEntity, ITypedEntity<WorldCountryTypeId>
{
    [Required]
    [MaxLength(64)]
    public string Name { get; set; } = null!;
        
    [Required]
    [MaxLength(3)]
    public string Code { get; set; } = null!;
        
    public WorldCountryTypeId TypeId { get; set; }
        
    public WorldCountryEntity()
    {
    }

    public WorldCountryEntity(Guid id, WorldCountryTypeId typeId, string name) : base(id)
    {
        Name = name;
        Code = typeId.ToString();
        TypeId = typeId;
    }
}