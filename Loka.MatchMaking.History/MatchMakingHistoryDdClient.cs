﻿using System;
using Loka.Common.Mongo;
using Loka.MatchMaking.History.Entities;
using MongoDB.Driver;

namespace Loka.MatchMaking.History;

public sealed class MatchMakingHistoryDdContext : MongoDbContext
{
    public IMongoCollection<MatchHistoryEntity> Matches { get; set; } = null!;

    public MatchMakingHistoryDdContext()
    {
    }
}