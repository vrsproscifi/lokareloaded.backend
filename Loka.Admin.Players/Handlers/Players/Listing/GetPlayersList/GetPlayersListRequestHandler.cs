﻿using Global.Identity.Database;
using Loka.Admin.Common.Services.Roles;
using Loka.Admin.Common.Services.World.Countries;
using Loka.Admin.Common.Services.World.Languages;
using Loka.Admin.Players.Models;
using Loka.Admin.Players.Services;
using Loka.Admin.Players.Specifications;
using Loka.Common.MediatR.Handlers;
using Loka.Common.Operations.Results;
using Loka.Common.Pagination;
using Loka.Database.Common.Specifications;
using Loka.Identity.Database;
using Microsoft.EntityFrameworkCore;

namespace Loka.Admin.Players.Handlers.Players.Listing.GetPlayersList;

public sealed class GetPlayersListRequestHandler : IOperationHandler<GetPlayersListRequest, PagingResult<GetPlayersListResponse>>
{
    private GameIdentityDbContext IdentityDbContext { get; }
    private GlobalIdentityDbContext GlobalIdentityDbContext { get; }
    private ICountryService CountryService { get; }
    private ILanguageService LanguageService { get; }
    private IAccountRoleService AccountRoleService { get; }
    private IPlayerStatisticsService PlayerStatisticsService { get; }

    public GetPlayersListRequestHandler
    (
        GameIdentityDbContext identityDbContext,
        ICountryService countryService,
        ILanguageService languageService,
        IAccountRoleService accountRoleService,
        IPlayerStatisticsService playerStatisticsService, GlobalIdentityDbContext globalIdentityDbContext)
    {
        IdentityDbContext = identityDbContext;
        CountryService = countryService;
        LanguageService = languageService;
        AccountRoleService = accountRoleService;
        PlayerStatisticsService = playerStatisticsService;
        GlobalIdentityDbContext = globalIdentityDbContext;
    }

    public async Task<OperationResult<PagingResult<GetPlayersListResponse>>> Handle(GetPlayersListRequest request, CancellationToken cancellationToken)
    {
        var query = GlobalIdentityDbContext.UserAccountEntity
            .OrderBy(q => q.EntityId)
            .Where(new PlayerNameSpecification(request.Filter.Term, request.Filter.NameConfirmed));
        //.Where(new PlayerEloSpecification(request.Filter.Statistics.Elo))
        //.Where(new PlayerLevelSpecification(request.Filter.Statistics.Level))
        //.Where(new PlayerWinsSpecification(request.Filter.Statistics.Wins));

        var count = await query.CountAsync(cancellationToken);
        var players = await query
            .WithPaging(request.Paging)
            .ToArrayAsync(cancellationToken);

        var models = players.Select(player => new GetPlayersListResponse
        {
            Id = player.EntityId,

            Email = new PlayerEmailObject(player.Email, player.IsEmailConfirmed),
            Name = new PlayerNameObject(player.Login, player.IsNameConfirmed),

            RegisteredAt = player.CreatedAt,
            PremiumEndDate = player.PremiumEndDate,

            LastActivityAt = player.LastActivityDate,
        }).ToArray();

        return new PagingResult<GetPlayersListResponse>(models, count, request.Paging);
    }
}