﻿using System;
using Loka.Common.Entry;
using Loka.Game.Database.Enums.Store;

namespace Loka.Admin.Common.Models.GamePlay;

public sealed class CurrencyInformationModel : IEntry
{
    public Guid Id { get; set; }
    public string Name { get; set; }= null!;
    public CurrencyTypeId TypeId { get; set; }

    public override string ToString()
    {
        return $"Id: {Id} | Name: {Name} | TypeId: {TypeId}";
    }
}