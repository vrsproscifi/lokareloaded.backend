﻿using System;
using Loka.Common.MediatR.Requests;
using Loka.Common.Pagination;
using Loka.Common.Ranges.Numerics;

namespace Loka.Admin.Players.Handlers.Players.Inventory.GetPlayerPurchasedItems;

public class GetPlayerPurchasedItemsRequest : IOperationRequest<PagingResult<GetPlayerPurchasedItemsResponse>>
{
    public Guid PlayerId { get; set; }
    public string Term { get; set; }
    public PagingQuery Paging { get; set; }
    public NumericRange<int?, int?> Amount { get; set; }
    public NumericRange<short?, short?> Level { get; set; }
    public NumericRange<byte?, byte?> Upgrade { get; set; }
}