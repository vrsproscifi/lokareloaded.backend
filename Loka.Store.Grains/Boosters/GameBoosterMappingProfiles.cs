﻿using AutoMapper;
using Loka.Store.DataBase.Entities.Booster;
using Loka.Store.Interfaces.Models.Boosters;

namespace Loka.Store.Grains.Boosters;

internal sealed class GameBoosterMappingProfiles : Profile
{
    public GameBoosterMappingProfiles()
    {
        CreateMap<BoosterEntity, GameBoosterModel>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.Name, q => q.MapFrom(w => w.Name))
            .ForMember(q => q.SellPrice, q => q.MapFrom(w => w.SellPrice))
            .ForMember(q => q.TypeId, q => q.MapFrom(w => w.TypeId))
            .ForMember(q => q.BoostPercent, q => q.MapFrom(w => w.BoostPercent))
            .ForMember(q => q.SellCurrency, q => q.Ignore());

    }
}