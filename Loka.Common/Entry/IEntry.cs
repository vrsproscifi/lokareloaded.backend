﻿using System;

namespace Loka.Common.Entry;

public interface IEntry
{
    Guid Id { get; }
}