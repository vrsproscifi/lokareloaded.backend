﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Loka.Inventory.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class ReworkedPlayerInventory : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlayerCurrencyEntity",
                schema: "Inventory");

            migrationBuilder.CreateTable(
                name: "PlayerResourceEntity",
                schema: "Inventory",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlayerId = table.Column<Guid>(type: "uuid", nullable: false),
                    ModelId = table.Column<Guid>(type: "uuid", nullable: false),
                    Amount = table.Column<long>(type: "bigint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerResourceEntity", x => x.EntityId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlayerResourceEntity_PlayerId",
                schema: "Inventory",
                table: "PlayerResourceEntity",
                column: "PlayerId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlayerResourceEntity",
                schema: "Inventory");

            migrationBuilder.CreateTable(
                name: "PlayerCurrencyEntity",
                schema: "Inventory",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Amount = table.Column<long>(type: "bigint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    ModelId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlayerId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerCurrencyEntity", x => x.EntityId);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlayerCurrencyEntity_PlayerId",
                schema: "Inventory",
                table: "PlayerCurrencyEntity",
                column: "PlayerId");
        }
    }
}
