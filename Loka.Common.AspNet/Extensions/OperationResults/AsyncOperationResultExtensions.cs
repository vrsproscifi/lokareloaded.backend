﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Loka.Common.Operations.Results;

namespace Loka.Common.AspNet.Extensions.OperationResults;

public static class AsyncOperationResultExtensions
{
    public static async Task<TModel> ReturnResult<TResult, TModel>(this Task<OperationResult<TResult>> operationResultTask, IMapper mapper, Action<TModel> customize)
    {
        var result = await operationResultTask;
        return result.ReturnResult(mapper, customize);
    }

    public static async Task<TModel> ReturnResult<TResult, TModel>(this Task<OperationResult<TResult>> operationResultTask, Func<TResult, TModel> map)
    {
        var result = await operationResultTask;
        return result.ReturnResult(map);
    }
        
    public static async Task<TModel> ReturnResult<TResult, TModel>(this Task<OperationResult<TResult>> operationResultTask, IMapper mapper)
    {
        var result = await operationResultTask;
        return result.ReturnResult<TResult, TModel>(mapper);
    }

    public static async Task<TResult> ReturnResult<TResult>(this Task<OperationResult<TResult>> operationResultTask)
    {
        var result = await operationResultTask;
        return result.ReturnResult();
    }
        
    public static async Task<Empty> ReturnResult(this Task<OperationResult> operationResultTask)
    {
        var result = await operationResultTask;
        return result.ReturnResult();
    }
}