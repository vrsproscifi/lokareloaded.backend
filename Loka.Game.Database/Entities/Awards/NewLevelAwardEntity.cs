﻿namespace Loka.Game.Database.Entities.Awards;

public sealed class NewLevelAwardEntity : BaseEntity, IAdminCommentedEntity
{
    public int Level { get; set; }
    public Guid CaseId { get; set; }

    [MaxLength(256)]
    public string? AdminComment { get; set; }
}