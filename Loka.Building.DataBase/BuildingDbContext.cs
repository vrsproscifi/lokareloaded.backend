﻿using Loka.Building.DataBase.Entities;
using Loka.Database.Common.Contexts;
using Microsoft.EntityFrameworkCore;

namespace Loka.Building.DataBase;

public sealed class BuildingDbContext : BaseGameDbContext<BuildingDbContext>
{
    public BuildingDbContext(DbContextOptions<BuildingDbContext> options) : base(options)
    {
    }

    public DbSet<IslandEntity> IslandEntity { get; init; } = null!;
    public DbSet<IslandBuildingEntity> IslandBuildingEntity { get; init; } = null!;
}