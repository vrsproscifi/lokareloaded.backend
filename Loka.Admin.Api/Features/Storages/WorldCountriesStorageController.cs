﻿using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Loka.Admin.Common.Services.World.Countries;
using Loka.Common.AspNet.Features;

namespace Loka.Admin.Api.Features.Storages;

public sealed class WorldCountriesStorageController : WorldCountriesStorage.WorldCountriesStorageBase
{
    private ControllerServices<WorldCountriesStorageController> Services { get; }
    private ICountryService CountryService { get; }

    public WorldCountriesStorageController(ICountryService countryService, ControllerServices<WorldCountriesStorageController> services)
    {
        CountryService = countryService;
        Services = services;
    }

    public override async Task<GetWorldCountriesResult> GetWorldCountries(Empty request, ServerCallContext context)
    {
        var languages = await CountryService.GetList(context.CancellationToken);
        return Services.Mapper.Map<GetWorldCountriesResult>(languages);
    }
}