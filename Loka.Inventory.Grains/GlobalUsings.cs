﻿global using MassTransit;
global using Microsoft.EntityFrameworkCore;
global using Orleans;

global using Loka.Common.Operations.Codes;
global using Loka.Common.Operations.Errors;
global using Loka.Common.Operations.Results;
global using System.Net;
global using System.Security.Claims;
global using AutoMapper;
global using AutoMapper.QueryableExtensions;