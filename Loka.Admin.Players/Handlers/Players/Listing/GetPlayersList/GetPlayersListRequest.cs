﻿using Loka.Admin.Players.Handlers.Players.Listing.GetPlayersList.Filters;
using Loka.Admin.Players.Handlers.Players.Listing.GetPlayersList.OrderBy;
using Loka.Common.MediatR.Requests;
using Loka.Common.Pagination;

namespace Loka.Admin.Players.Handlers.Players.Listing.GetPlayersList;

public sealed class GetPlayersListRequest : IOperationRequest<PagingResult<GetPlayersListResponse>>
{
    public GetPlayersListRequestOrderBy OrderBy { get; set; }
    public OrderByDirection OrderByDirection { get; set; }

    public GetPlayersListRequestFilter Filter { get; set; }
    public PagingQuery Paging { get; set; }
}