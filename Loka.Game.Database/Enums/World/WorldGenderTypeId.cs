﻿namespace Loka.Game.Database.Enums.World;

public enum WorldGenderTypeId : byte
{
    None = 0,

    /// <summary>
    /// Мужской
    /// </summary>
    Male,

    /// <summary>
    /// Женский
    /// </summary>
    Female,
}