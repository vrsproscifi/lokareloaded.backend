﻿using System;
using Loka.MatchMaking.History.Entities.Team;
using MongoDB.Bson;

namespace Loka.MatchMaking.History.Entities;

public class MatchHistoryEntity
{
    public ObjectId Id { get; set; }

    public Guid MatchId { get; set; }
    public Guid GameModeId { get; set; }
    public Guid GameMapId { get; set; }
    public Guid GameVersionId { get; set; }
        
    public long GameVersion { get; set; }
    public byte Level { get; set; }

    public MatchHistoryProcessInformation Process { get; set; }
    public MatchHistoryMatchInformation Match { get; set; }
    public MatchHistoryTeamInformation[] Teams { get; set; }
}