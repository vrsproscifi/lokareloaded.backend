﻿namespace Loka.Store.DataBase.Entities.Items;

[Index(nameof(Level))]
[Index(nameof(DisplayPosition))]
public sealed class GameItemEntity : BaseEntity, INamedEntity, IAdminCommentedEntity
{
    [Required]
    [MaxLength(64)]
    public string Name { get; set; } = null!;


    //------------------------------------
    //            Fraction
    //------------------------------------

    #region Fraction

    [ForeignKey(nameof(FractionEntity))]
    public Guid FractionId { get; set; }

    public FractionEntity? FractionEntity { get; set; }

    public int Level { get; set; }

    #endregion

    //------------------------------------
    //            Category
    //------------------------------------

    #region Category

    [ForeignKey(nameof(CategoryEntity))]
    public Guid CategoryId { get; set; }

    public GameItemCategoryEntity? CategoryEntity { get; set; }

    #endregion

    //------------------------------------
    //            Cost
    //------------------------------------

    #region Cost

    [ForeignKey(nameof(SellCurrencyEntity))]
    public Guid? SellCurrencyId { get; set; }

    public GameResourceEntity? SellCurrencyEntity { get; set; }
    public int SellPrice { get; set; }

    #endregion

    #region Subscribe

    [ForeignKey(nameof(SubscribeCurrencyEntity))]
    public Guid? SubscribeCurrencyId { get; set; }

    public GameResourceEntity? SubscribeCurrencyEntity { get; set; }
    public int SubscribePrice { get; set; }

    #endregion

    //------------------------------------
    //            Etc
    //------------------------------------
    public ItemEntityFlags Flags { get; set; }
    public TimeSpan? Duration { get; set; }
    public int DisplayPosition { get; set; }

    public long AmountDefault { get; set; } = 1;
    public long AmountInStack { get; set; } = 1;
    public long AmountMinimum { get; set; } = 1;
    public long? AmountMaximum { get; set; }

    [MaxLength(1024)]
    public string? AdminComment { get; set; }

    public IReadOnlyList<GameItemBlueprintEntity>? Blueprints { get; set; }

    public GameItemEntity()
    {
    }

    internal GameItemEntity(Guid id, Guid categoryId, string name, Guid fractionId, int sellPrice, Guid sellCurrencyId, ItemEntityFlags flags = ItemEntityFlags.None)
    {
        EntityId = id;
        Name = name;
        CategoryId = categoryId;
        FractionId = fractionId;
        SellCurrencyId = sellCurrencyId;
        SellPrice = sellPrice;
        Flags = flags;
    }

    public override string ToString()
    {
        return $"{EntityId:D} | {Name} | Flags: {Flags}";
    }
}