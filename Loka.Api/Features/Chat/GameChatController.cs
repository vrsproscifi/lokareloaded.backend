﻿using Loka.Chat.Interfaces;
using MongoDB.Bson;

namespace Loka.Api.Features.Chat;

internal sealed class GameChatController : GameChat.GameChatBase
{
    private ControllerServices<GameChatController> Services { get; }
    private PlayerActor PlayerActor { get; }

    public GameChatController(ControllerServices<GameChatController> services, PlayerActor playerActor)
    {
        Services = services;
        PlayerActor = playerActor;
    }

    public override Task<AssignRoleForChannelMember.Types.Response> AssignRole(AssignRoleForChannelMember.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IGameChatGrain>(PlayerActor.PlayerId);
        var req = Services.Mapper.Map<Loka.Chat.Interfaces.Models.AssignRoleForChannelMember.Request>(request);
        var result = grain.AssignRole(req);
        return result.ReturnResult<Loka.Chat.Interfaces.Models.AssignRoleForChannelMember.Result, AssignRoleForChannelMember.Types.Response>(Services.Mapper);
    }

    public override async Task<GetChatChannels.Types.Response> GetChatChannels(GetChatChannels.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IGameChatGrain>(PlayerActor.PlayerId);
        var result = await grain.GetChatChannels(new Loka.Chat.Interfaces.Models.GetChatChannels.Request()
        {
        });

        return Services.Mapper.Map<GetChatChannels.Types.Response>(result);
    }

    public override async Task<GetChatMessages.Types.Response> GetChatMessages(GetChatMessages.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IGameChatGrain>(PlayerActor.PlayerId);
        var result = await grain.GetChatMessages(new Loka.Chat.Interfaces.Models.GetChatMessages.Request()
        {
            ChannelId = new ObjectId(request.ChannelId.Value),
            EarlyThen = request.EarlyThen.ToDateTime(),
            LaterThen = request.LaterThen.ToDateTime()
        });

        return result.ReturnResult<Loka.Chat.Interfaces.Models.GetChatMessages.Result, GetChatMessages.Types.Response>(Services.Mapper);
    }

    public override async Task<SendChatMessage.Types.Response> SendChatMessage(SendChatMessage.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IGameChatGrain>(PlayerActor.PlayerId);
        var result = await grain.SendChatMessage(new Loka.Chat.Interfaces.Models.SendChatMessage.Request()
        {
            ChannelId = new ObjectId(request.ChannelId.Value),
            Message = request.Message,
        });

        return result.ReturnResult<Loka.Chat.Interfaces.Models.SendChatMessage.Result, SendChatMessage.Types.Response>(Services.Mapper);
    }

    public override Task<GetChatMember.Types.Response> GetChatMember(GetChatMember.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IGameChatGrain>(PlayerActor.PlayerId);
        var result = grain.GetChatMember(new Loka.Chat.Interfaces.Models.GetChatMember.Request()
        {
            ChannelId = ObjectId.Parse(request.ChannelId.Value),
            PlayerId = Guid.Parse(request.PlayerId.Value)
        });
        return result.ReturnResult<Loka.Chat.Interfaces.Models.GetChatMember.Result, GetChatMember.Types.Response>(Services.Mapper);
    }

    public override Task<GetChatMembers.Types.Response> GetChatMembers(GetChatMembers.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IGameChatGrain>(PlayerActor.PlayerId);
        var result = grain.GetChatMembers(new Loka.Chat.Interfaces.Models.GetChatMembers.Request()
        {
            ChannelId = ObjectId.Parse(request.ChannelId.Value)
        });
        return result.ReturnResult<Loka.Chat.Interfaces.Models.GetChatMembers.Result, GetChatMembers.Types.Response>(Services.Mapper);
    }

    public override Task<LeaveFromChannel.Types.Response> LeaveFromChannel(LeaveFromChannel.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IGameChatGrain>(PlayerActor.PlayerId);
        var result = grain.LeaveFromChannel(new Loka.Chat.Interfaces.Models.LeaveFromChannel.Request()
        {
            ChannelId = ObjectId.Parse(request.ChannelId.Value),
        });
        return result.ReturnResult<Loka.Chat.Interfaces.Models.LeaveFromChannel.Result, LeaveFromChannel.Types.Response>(Services.Mapper);
    }

    public override async Task<CreatePrivateChannel.Types.Response> CreatePrivateChannel(CreatePrivateChannel.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IGameChatGrain>(PlayerActor.PlayerId);
        var result = await grain.CreatePrivateChannel(new Loka.Chat.Interfaces.Models.CreatePrivateChannel.Request()
        {
            Name = request.Name,
        });

        var model = Services.Mapper.Map<CreatePrivateChannel.Types.Response>(result);
        return model;
    }
}