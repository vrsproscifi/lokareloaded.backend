﻿namespace Loka.Chat.Interfaces.Enums;

[Flags]
public enum ChatMemberPermissions
{
    ReadMessages = 0,
    SendMessages = 1,
    InviteNewMembers = 2,
    KickMembers = 4,
    AssignMemberRole = 8,
    Owner = 16,
}