﻿namespace Loka.Inventory.Interfaces.Boosters;

public static class GetPlayerBoosters
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<Result>
    {
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public IReadOnlyList<PlayerInventoryBoosterModel> Entries { get; init; } = null!;
    }
}