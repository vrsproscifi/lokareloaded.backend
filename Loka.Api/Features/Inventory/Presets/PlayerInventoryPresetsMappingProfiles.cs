﻿using Loka.Inventory.Interfaces.Presets.Models;

namespace Loka.Api.Features.Inventory.Presets;

internal sealed class PlayerInventoryPresetsMappingProfiles : Profile
{
    public PlayerInventoryPresetsMappingProfiles()
    {
        //==========================================================
        CreateMap<Loka.Inventory.Interfaces.Presets.Models.GetPlayerInventoryPresets.Result, GetPlayerInventoryPresets.Types.Response>()
            .ForMember(q => q.Entities, q => q.MapFrom(w => w.Presets));

        CreateMap<Loka.Inventory.Interfaces.Store.BuyPlayerInventoryPresets.Result, Loka.Api.Features.Inventory.Presets.BuyInventoryPreset.Types.Response>()
            .ForMember(q => q.Preset, q => q.MapFrom(w => w.Preset));

        CreateMap<PlayerInventoryPresetModel.RPC, PlayerInventoryPreset>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.ModelId))
            .ForMember(q => q.IsActive, q => q.MapFrom(w => w.IsActive))
            .ForMember(q => q.Items, q => q.MapFrom(w => w.Items));

        CreateMap<PlayerInventoryPresetModel.RPC.ItemEntry, PlayerInventoryPreset.Types.ItemEntry>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.ItemId, q => q.MapFrom(w => w.ItemId));
        
        CreateMap<Loka.Inventory.Interfaces.Presets.Models.EquipInventoryItem.Result,  Loka.Api.Features.Inventory.Items.EquipInventoryItem.Types.Response>()
            .ForMember(q => q.PresetId, q => q.MapFrom(w => w.PresetId))
            .ForMember(q => q.Id, q => q.MapFrom(w => w.ItemId));
        
    }
}