﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Common.Models.GamePlay;
using Loka.Common.Operations.Results;

namespace Loka.Admin.Common.Services.GamePlay.GameMap;

public interface IGameMapService
{
    Task<OperationResult<GameMapInformationModel>> GetById(Guid id, CancellationToken cancellationToken = default);
}