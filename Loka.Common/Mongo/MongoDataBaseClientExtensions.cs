﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace Loka.Common.Mongo;

public static class MongoDataBaseClientExtensions
{
    public static ServiceCollectionInitializer AddMongoDbContext<TDbClient>
    (
        this ServiceCollectionInitializer initializer
    )
        where TDbClient : MongoDbContext, new()
    {
        var settingsString = initializer.Configuration.GetConnectionString(typeof(TDbClient).Name);
        var settings = MongoUrl.Create(settingsString);
        initializer.Services.AddSingleton(provider =>
        {
            var logger = provider.GetRequiredService<ILogger<TDbClient>>();
            return MongoDbContext.Create<TDbClient>(settings, logger);
        });
        return initializer;
    }
}