﻿using AutoMapper;
using Loka.Store.DataBase.Entities;
using Loka.Store.DataBase.Entities.Items;
using Loka.Store.Interfaces.Models;
using Loka.Store.Interfaces.Models.Fractions;
using Loka.Store.Interfaces.Models.Items;
using Loka.Store.Interfaces.Models.Resources;

namespace Loka.Store.Grains.MappingProfiles;

internal sealed class StoreModels : Profile
{
    public StoreModels()
    {
        CreateMap<GameItemEntity, StoreItemModel>()
            //===
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.Name, q => q.MapFrom(w => w.Name))
            .ForMember(q => q.AdminComment, q => q.MapFrom(w => w.AdminComment))

            //===
            .ForMember(q => q.SellPrice, q => q.MapFrom(w => w.SellPrice))
            .ForMember(q => q.SubscribePrice, q => q.MapFrom(w => w.SubscribePrice))

            //===
            .ForMember(q => q.Flags, q => q.MapFrom(w => w.Flags))
            .ForMember(q => q.Level, q => q.MapFrom(w => w.Level))
            .ForMember(q => q.Duration, q => q.MapFrom(w => w.Duration))
            .ForMember(q => q.DisplayPosition, q => q.MapFrom(w => w.DisplayPosition))
            //===
            .ForMember(q => q.AmountDefault, q => q.MapFrom(w => w.AmountDefault))
            .ForMember(q => q.AmountInStack, q => q.MapFrom(w => w.AmountInStack))
            .ForMember(q => q.AmountMinimum, q => q.MapFrom(w => w.AmountMinimum))
            .ForMember(q => q.AmountMaximum, q => q.MapFrom(w => w.AmountMaximum))
            .ForMember(q => q.SellCurrency, q => q.Ignore())
            .ForMember(q => q.SubscribeCurrency, q => q.Ignore())
            .ForMember(q => q.Fraction, q => q.Ignore())
            .ForMember(q => q.Category, q => q.Ignore());



        CreateMap<GameItemCategoryEntity, StoreItemCategoryModel>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.Name, q => q.MapFrom(w => w.Name))
            .ForMember(q => q.TypeId, q => q.MapFrom(w => w.TypeId));


        CreateMap<FractionEntity, StoreFractionModel>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.Name, q => q.MapFrom(w => w.Name))
            .ForMember(q => q.TypeId, q => q.MapFrom(w => w.TypeId));

        CreateMap<GameResourceEntity, GameResourceModel>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.Name, q => q.MapFrom(w => w.Name))
            .ForMember(q => q.TypeId, q => q.MapFrom(w => w.TypeId))
            .ForMember(q => q.DefaultValue, q => q.MapFrom(w => w.DefaultValue));

        CreateMap<GameItemBlueprintEntity, StoreItemBlueprintModel>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.Depends, q => q.Ignore());


        CreateMap<GameItemBlueprintDependEntity, StoreItemBlueprintDependsModel>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.Amount, q => q.MapFrom(w => w.Amount))
            .ForMember(q => q.Resource, q => q.Ignore());


    }
}