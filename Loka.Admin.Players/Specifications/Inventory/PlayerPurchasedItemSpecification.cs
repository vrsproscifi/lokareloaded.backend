﻿using Loka.Common.Ranges.Numerics;
using Loka.Database.Common.Specifications;
using Loka.Inventory.DataBase.Entities.Items;

namespace Loka.Admin.Players.Specifications.Inventory;

internal abstract class PlayerPurchasedItemSpecification<TRange> : RangeSpecification<PlayerItemEntity, TRange>
    where TRange : struct
{
    protected PlayerPurchasedItemSpecification(NumericRange<TRange?, TRange?> range, NumericRangeBeginOptions beginOptions, NumericRangeEndOptions endOptions) : base(range, beginOptions, endOptions)
    {
    }

    protected PlayerPurchasedItemSpecification(NumericRange<TRange?, TRange?> range, NumericRangeOptions options) : base(range, options)
    {
    }
}