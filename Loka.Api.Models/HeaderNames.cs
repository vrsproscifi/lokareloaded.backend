﻿using Grpc.Core;

namespace Loka.Api.Models;

public static class HeaderExtensions
{
    public static TimeSpan DefaultTokenLifetime { get; } = TimeSpan.FromMinutes(15);
    public const string TokenLifetime = nameof(TokenLifetime);

    public static TimeSpan GetTimeSpan(this Metadata metadata, string key, TimeSpan defaultValue)
    {
        var value = metadata.GetValue(key);
        if (string.IsNullOrWhiteSpace(value))
            return defaultValue;

        return TimeSpan.TryParse(value, out var result) ? result : defaultValue;
    }
}