﻿using AutoMapper;
using Loka.Store.DataBase.Entities.Cases;
using Loka.Store.Interfaces.Models.Cases;

namespace Loka.Store.Grains.Cases;

internal sealed class GameCasesMappingProfiles : Profile
{
    public GameCasesMappingProfiles()
    {
        CreateMap<CaseEntity, StoreCaseModel>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.Name, q => q.MapFrom(w => w.Name))
            .ForMember(q => q.SellPrice, q => q.MapFrom(w => w.SellPrice))
            .ForMember(q => q.SellCurrency, q => q.Ignore())
            .ForMember(q => q.RandomMaxDropItems, q => q.MapFrom(w => w.RandomMaxDropItems))
            .ForMember(q => q.RandomMaxDropResources, q => q.MapFrom(w => w.RandomMaxDropResources));
        
        CreateMap<CaseItemEntity, StoreCaseModel.Item>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.MaximumCount, q => q.MapFrom(w => w.MaximumCount))
            .ForMember(q => q.MinimumCount, q => q.MapFrom(w => w.MinimumCount))
            .ForMember(q => q.ChanceOfDrop, q => q.MapFrom(w => w.ChanceOfDrop));
        
        CreateMap<CaseResourceEntity, StoreCaseModel.Resource>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.MaximumCount, q => q.MapFrom(w => w.MaximumCount))
            .ForMember(q => q.MinimumCount, q => q.MapFrom(w => w.MinimumCount))
            .ForMember(q => q.ChanceOfDrop, q => q.MapFrom(w => w.ChanceOfDrop));
        
    }
}