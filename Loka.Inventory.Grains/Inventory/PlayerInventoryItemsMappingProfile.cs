﻿using Loka.Inventory.DataBase.Entities.Items;
using Loka.Inventory.Interfaces.Inventory;

namespace Loka.Inventory.Grains.Inventory;

internal sealed class PlayerInventoryItemsMappingProfile : Profile
{
    public PlayerInventoryItemsMappingProfile()
    {
        CreateMap<PlayerItemEntity, PlayerInventoryItemModel.Repository>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            //.ForMember(q => q.ModelId, q => q.MapFrom(w => w.ModelId))
            .ForMember(q => q.Level, q => q.MapFrom(w => w.Level))
            .ForMember(q => q.LastUseDate, q => q.MapFrom(w => w.LastUseDate))
            .ForMember(q => q.Amount, q => q.MapFrom(w => w.Amount))
            .ForMember(q => q.Modules, q => q.Ignore());
        
        CreateMap<PlayerInventoryItemModel.Repository, PlayerInventoryItemModel.RPC>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.Model.Id))
            .ForMember(q => q.Level, q => q.MapFrom(w => w.Level))
            .ForMember(q => q.LastUseDate, q => q.MapFrom(w => w.LastUseDate))
            .ForMember(q => q.Amount, q => q.MapFrom(w => w.Amount))
            .ForMember(q => q.Modules, q => q.MapFrom(w => w.Modules));

        CreateMap<PlayerInventoryItemModel.Repository.Module, PlayerInventoryItemModel.RPC.Module>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.Model.Id))
            .ForMember(q => q.Level, q => q.MapFrom(w => w.Level));

    }
}