﻿using AutoMapper;
using Loka.Common.Cache.DistributedCache;
using Loka.Database.Common.Repositories;
using Loka.Game.Database;
using Loka.Store.DataBase;
using Loka.Store.DataBase.Entities;
using Loka.Store.Interfaces.Models;
using Loka.Store.Interfaces.Models.Fractions;
using Loka.Store.Interfaces.Repositories;

namespace Loka.Store.Grains.Repositories;

internal sealed class GameStoreFractionsRepository : BaseRepository<StoreDbContext, FractionEntity, StoreFractionModel>, IGameStoreFractionsRepository
{
    public GameStoreFractionsRepository(ICacheProvider cacheProvider, StoreDbContext dbContext, IMapper mapper) : base(cacheProvider, dbContext, mapper)
    {
    }
}