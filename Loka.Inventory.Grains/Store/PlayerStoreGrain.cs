﻿using Loka.Common.Authentication;
using Loka.Common.Orleans.Grains;
using Loka.Inventory.Interfaces.Boosters;
using Loka.Inventory.Interfaces.Cases;
using Loka.Inventory.Interfaces.Inventory;
using Loka.Inventory.Interfaces.Inventory.Requests;
using Loka.Inventory.Interfaces.Payments;
using Loka.Inventory.Interfaces.Presets;
using Loka.Inventory.Interfaces.Presets.Models;
using Loka.Inventory.Interfaces.Store;
using Loka.Inventory.Interfaces.Store.Requests;
using Loka.Store.Interfaces;

namespace Loka.Inventory.Grains.Store;

internal sealed class PlayerStoreGrain : Grain, IPlayerStoreGrain, IWithPlayerActor
{
    public PlayerActor Actor { get; set; } = null!;

    public async Task<OperationResult<BuyPlayerInventoryPresets.Result>> BuyPreset(BuyPlayerInventoryPresets.Request request)
    {
        var store = GrainFactory.GetGameStoreGrain();
        var presets = await store.GetInventoryPresets();
        var presetModel = presets.FirstOrDefault(preset => preset.Id == request.ModelId);
        if (presetModel == null)
            return OperationResultCode.EntityNotFound;

        var inventory = GrainFactory.GetGrain<IPlayerInventoryPresetGrain>(Actor.PlayerId);
        var hasPreset = await inventory.HasPreset(request.ModelId);
        if (hasPreset)
            return OperationResultCode.PresetAlreadyBought;

        var paymentsGrain = GrainFactory.GetGrain<IPlayerPaymentsGrain>(Actor.PlayerId);
        var paymentResult = await paymentsGrain.PayFor(new PayForItem.Request()
        {
            ModelId = presetModel.Id,
            ResourceId = presetModel.SellCurrency!.Id,
            Price = presetModel.SellPrice!.Value,
            TypeId = PlayerPaymentTransactionTypeId.Preset
        });

        if (paymentResult.IsFailed)
            return paymentResult.Error;

        var preset = await inventory.CreatePreset(presetModel.Id);
        return new BuyPlayerInventoryPresets.Result()
        {
            Preset = preset
        };
    }

    public async Task<OperationResult<BuyStoreItem.Result>> BuyStoreItem(BuyStoreItem.Request request)
    {
        var store = GrainFactory.GetGameStoreGrain();
        var getModelResult = await store.GetStoreItem(request.ModelId);
        if (getModelResult.GetValueIfSucceeded(out var model))
        {
            var paymentsGrain = GrainFactory.GetGrain<IPlayerPaymentsGrain>(Actor.PlayerId);
            var paymentResult = await paymentsGrain.PayFor(new PayForItem.Request()
            {
                ModelId = model.Id,
                ResourceId = model.SellCurrency!.Id,
                Price = model.SellPrice!.Value,
                TypeId = PlayerPaymentTransactionTypeId.Item
            });

            if (paymentResult.IsFailed)
                return paymentResult.Error;

            var inventory = GrainFactory.GetGrain<IPlayerInventoryItemsGrain>(Actor.PlayerId);
            var addItemToInventoryResult = await inventory.AddOrStackInventoryItem(new AddOrStackInventoryItem.Request()
            {
                ModelId = model.Id,
                Amount = model.AmountDefault
            });

            if (addItemToInventoryResult.GetValueIfSucceeded(out var inventoryItem))
            {
                return new BuyStoreItem.Result()
                {
                    Item = inventoryItem.Item
                };
            }
            else
            {
                await paymentsGrain.RollBackPayment(new RollBackPayment.Request()
                {
                    TransactionId = paymentResult.Result!.Id
                });

                return addItemToInventoryResult.Error!;
            }
        }

        return getModelResult.Error!;
    }

    public async Task<OperationResult<BuyStoreBooster.Result>> BuyStoreBooster(BuyStoreBooster.Request request)
    {
        var store = GrainFactory.GetGameStoreGrain();
        var getModelResult = await store.GetGameStoreBooster(request.ModelId);
        if (getModelResult.GetValueIfSucceeded(out var model))
        {
            var paymentsGrain = GrainFactory.GetGrain<IPlayerPaymentsGrain>(Actor.PlayerId);
            var paymentResult = await paymentsGrain.PayFor(new PayForItem.Request()
            {
                ModelId = model.Id,
                ResourceId = model.SellCurrency!.Id,
                Price = model.SellPrice!.Value,
                TypeId = PlayerPaymentTransactionTypeId.Booster
            });

            if (paymentResult.IsFailed)
                return paymentResult.Error;

            var inventory = GrainFactory.GetGrain<IPlayerInventoryBoostersGrain>(Actor.PlayerId);
            var booster = await inventory.AddOrStackInventoryBooster(new AddOrStackInventoryBooster.Request()
            {
                ModelId = model.Id,
                Amount = 1
            });

            return new BuyStoreBooster.Result()
            {
                Booster = booster
            };
        }

        return getModelResult.Error!;
    }

    public async Task<OperationResult<BuyStoreCase.Result>> BuyStoreCase(BuyStoreCase.Request request)
    {
        var store = GrainFactory.GetGameStoreGrain();
        var getModelResult = await store.GetGameStoreCase(request.ModelId);
        if (getModelResult.GetValueIfSucceeded(out var model))
        {
            var paymentsGrain = GrainFactory.GetGrain<IPlayerPaymentsGrain>(Actor.PlayerId);
            var paymentResult = await paymentsGrain.PayFor(new PayForItem.Request()
            {
                ModelId = model.Id,
                ResourceId = model.SellCurrency!.Id,
                Price = model.SellPrice!.Value,
                TypeId = PlayerPaymentTransactionTypeId.Case
            });

            if (paymentResult.IsFailed)
                return paymentResult.Error;

            var inventory = GrainFactory.GetGrain<IPlayerInventoryCasesGrain>(Actor.PlayerId);
            var playerCase = await inventory.AddOrStackInventoryCase(new AddOrStackInventoryCase.Request()
            {
                ModelId = model.Id,
                Amount = 1
            });

            return new BuyStoreCase.Result()
            {
                Case = playerCase
            };
        }

        return getModelResult.Error!;
    }
}