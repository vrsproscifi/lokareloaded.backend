﻿using Loka.Inventory.Interfaces.Templates;

namespace Loka.Api.Features.Inventory.Templates;

internal sealed class PlayerInventoryTemplatesController : PlayerInventoryTemplates.PlayerInventoryTemplatesBase
{
    private ControllerServices<PlayerInventoryTemplatesController> Services { get; }
    private PlayerActor PlayerActor { get; }

    public PlayerInventoryTemplatesController(ControllerServices<PlayerInventoryTemplatesController> services, PlayerActor playerActor)
    {
        Services = services;
        PlayerActor = playerActor;
    }

    public override Task<GetPlayerInventoryTemplates.Types.Response> GetPlayerInventoryTemplates(GetPlayerInventoryTemplates.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerInventoryTemplatesGrain>(PlayerActor.PlayerId);
        return grain.GetInventoryTemplates()
            .ReturnResult<Loka.Inventory.Interfaces.Templates.GetPlayerInventoryTemplates.Result, GetPlayerInventoryTemplates.Types.Response>(Services.Mapper);
    }
}