﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Loka.Store.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class AddedProfiles : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(5316), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(6051), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(6257), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(6445), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(8528), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(9289), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(9294), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 447, DateTimeKind.Unspecified).AddTicks(9872), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(788), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(1020), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(1211), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(1398), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(1578), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(1909), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(2268), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(2576), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(2835), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameItemEntity",
                columns: new[] { "EntityId", "AdminComment", "AmountDefault", "AmountInStack", "AmountMaximum", "AmountMinimum", "CategoryId", "CreatedAt", "DeletedAt", "DisplayPosition", "Duration", "Flags", "FractionId", "Level", "Name", "SellCurrencyId", "SellPrice", "SubscribeCurrencyId", "SubscribePrice" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe001"), null, 1L, 1L, null, 1L, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 0, null, (byte)4, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"), 0, "Profile 01", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 0, null, 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe002"), null, 1L, 1L, null, 1L, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 0, null, (byte)0, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"), 0, "Profile 02", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 10000, null, 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe003"), null, 1L, 1L, null, 1L, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 0, null, (byte)0, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"), 0, "Profile 03", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"), 100, null, 0 }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe001"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe002"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe003"));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 918, DateTimeKind.Unspecified).AddTicks(1403), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 918, DateTimeKind.Unspecified).AddTicks(2068), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 918, DateTimeKind.Unspecified).AddTicks(2285), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 918, DateTimeKind.Unspecified).AddTicks(2485), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 918, DateTimeKind.Unspecified).AddTicks(4669), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 918, DateTimeKind.Unspecified).AddTicks(5480), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameCurrencyEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 918, DateTimeKind.Unspecified).AddTicks(5485), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 917, DateTimeKind.Unspecified).AddTicks(6273), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 917, DateTimeKind.Unspecified).AddTicks(7446), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 917, DateTimeKind.Unspecified).AddTicks(7668), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 917, DateTimeKind.Unspecified).AddTicks(7865), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 917, DateTimeKind.Unspecified).AddTicks(8063), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 917, DateTimeKind.Unspecified).AddTicks(8256), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 917, DateTimeKind.Unspecified).AddTicks(8551), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 917, DateTimeKind.Unspecified).AddTicks(8759), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 917, DateTimeKind.Unspecified).AddTicks(8947), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 20, 6, 9, 917, DateTimeKind.Unspecified).AddTicks(9141), new TimeSpan(0, 0, 0, 0, 0)));
        }
    }
}
