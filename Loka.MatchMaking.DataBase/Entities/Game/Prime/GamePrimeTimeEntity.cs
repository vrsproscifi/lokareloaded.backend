﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Entities;
using Loka.Database.Common.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Loka.MatchMaking.DataBase.Entities.Game.Prime;

[Index(nameof(Name))]
public sealed class GamePrimeTimeEntity : BaseEntity, INamedEntity
{
    [Required]
    [MaxLength(64)]
    public string Name { get; set; } = null!;

    #region GameMode

    [ForeignKey(nameof(GameModeEntity))]
    public Guid? GameModeId { get; set; }
    public GameModeEntity? GameModeEntity { get; set; }

    #endregion

    public TimeSpan From { get; set; }
    public TimeSpan End { get; set; }

    public int? Level { get; set; }
    public int BonusExperience { get; set; }
    public int BonusMoney { get; set; }
}