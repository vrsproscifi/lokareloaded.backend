﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Common.Models.GamePlay;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Extensions;
using Loka.MatchMaking.DataBase;

namespace Loka.Admin.Common.Services.GamePlay.GameMap;

internal sealed class GameMapService : IGameMapService
{
    private MatchMakingDbContext MatchMakingDbContext { get; }

    public GameMapService(MatchMakingDbContext matchMakingDbContext)
    {
        MatchMakingDbContext = matchMakingDbContext;
    }
        
    public async Task<OperationResult<GameMapInformationModel>> GetById(Guid id, CancellationToken cancellationToken)
    {
        var entity = await MatchMakingDbContext.GameMapEntity.GetById(id, cancellationToken);
        if (entity == null)
            return OperationResultCode.PlayerNotFound;

        var model = new GameMapInformationModel
        {
            Id = entity.EntityId,
            Name = entity.Name
        };

        return model;
    }
}