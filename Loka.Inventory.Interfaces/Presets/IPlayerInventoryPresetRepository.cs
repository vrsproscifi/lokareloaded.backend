﻿using Loka.Database.Common.Repositories;
using Loka.Inventory.Interfaces.Presets.Models;

namespace Loka.Inventory.Interfaces.Presets;

public interface IPlayerInventoryPresetRepository : IBaseChildRepository<PlayerInventoryPresetModel.Repository>
{
    Task<PlayerInventoryPresetModel.Repository> CreatePreset(Guid playerId, Guid modelId);
    Task<bool> HasPreset(Guid playerId, Guid modelId);
    Task<Guid> AddSlotItem( Guid presetId, Guid itemId);
    Task<Guid> SetSlotItem(Guid presetId, Guid slotId, Guid itemId);
}