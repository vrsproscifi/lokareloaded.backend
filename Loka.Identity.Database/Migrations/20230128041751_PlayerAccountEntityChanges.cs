﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Loka.Identity.Database.Migrations
{
    /// <inheritdoc />
    public partial class PlayerAccountEntityChanges : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastActivityDate",
                schema: "GameIdentity",
                table: "PlayerAccountEntity",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastActivityDate",
                schema: "GameIdentity",
                table: "PlayerAccountEntity");
        }
    }
}
