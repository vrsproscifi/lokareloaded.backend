﻿using System;
using Loka.Database.Common.Contexts;
using Loka.MatchMaking.DataBase.Entities.Game;
using Loka.MatchMaking.DataBase.Entities.Game.Prime;
using Loka.MatchMaking.DataBase.Entities.Match;
using Loka.MatchMaking.DataBase.Entities.Queue;
using Loka.MatchMaking.DataBase.Entities.Server;
using Loka.MatchMaking.DataBase.Seeders.Game;
using Loka.MatchMaking.DataBase.Seeders.Server;
using Microsoft.EntityFrameworkCore;

namespace Loka.MatchMaking.DataBase;

public sealed class MatchMakingDbContext : BaseGameDbContext<MatchMakingDbContext>
{
    public MatchMakingDbContext(DbContextOptions<MatchMakingDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<ServerRegionEntity>().HasData(ServerRegionEntitySeeder.Entities);
        modelBuilder.Entity<GameMapEntity>().HasData(GameMapEntitySeeder.Entities);
        modelBuilder.Entity<GameModeEntity>().HasData(GameModeEntitySeeder.Entities);
        modelBuilder.Entity<GameModeMapEntity>().HasData(GameModeMapEntitySeeder.Entities);

        modelBuilder.Entity<MatchEntity>()
            .HasOne(q => q.Process!)
            .WithOne(q => q.MatchEntity!)
            .HasForeignKey<MatchProcessEntity>(q => q.EntityId);
    }

    public DbSet<PlayerQueueEntity> PlayerQueueEntity { get; set; } = null!;

    #region Cluster

    public DbSet<ClusterEntity> ClusterEntity { get; set; } = null!;
    public DbSet<ServerRegionEntity> ServerRegionEntity { get; set; } = null!;

    #endregion

    #region Game

    public DbSet<GameMapEntity> GameMapEntity { get; set; } = null!;
    public DbSet<GameModeEntity> GameModeEntity { get; set; } = null!;
    public DbSet<GamePrimeTimeEntity> GamePrimeTimeEntity { get; set; } = null!;

    #endregion

    #region Match

    public DbSet<MatchEntity> MatchEntity { get; set; } = null!;
    public DbSet<MatchProcessEntity> MatchProcessEntity { get; set; } = null!;
    public DbSet<MatchMemberEntity> MatchMemberEntity { get; set; } = null!;
    public DbSet<MatchTeamEntity> MatchTeamEntity { get; set; } = null!;

    #endregion
}