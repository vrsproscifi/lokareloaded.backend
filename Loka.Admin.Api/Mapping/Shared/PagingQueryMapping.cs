﻿using AutoMapper;
using Loka.Common.Pagination;

namespace Loka.Admin.Api.Mapping.Shared;

public sealed class PagingQueryMapping : Profile
{
    public PagingQueryMapping()
    {
        CreateMap<PagingQueryModel, PagingQuery>()
            .ForMember(q => q.Take, q => q.MapFrom(p => p.Count))
            .ForMember(q => q.Offset, q => q.MapFrom(p => p.Offset))
            .ReverseMap();

        CreateMap<PagingResult, PagingResultModel>()
            .ForMember(q => q.Take, q => q.MapFrom(p => p.Take))
            .ForMember(q => q.Offset, q => q.MapFrom(p => p.Offset))
            .ForMember(q => q.Total, q => q.MapFrom(p => p.Total));
    }
}