﻿using System;
using System.Linq;
using AutoMapper;

namespace Loka.Admin.Api.Mapping.Shared;

public sealed class UUIDMapping : Profile
{
    public UUIDMapping()
    {


        CreateMap<UUID, Guid>().ConstructUsing(q => Guid.Parse(q.Value));
        CreateMap<UUID, Guid?>().ConstructUsing(q => Guid.Parse(q.Value));

        CreateMap<Guid, UUID>().ConstructUsing(q => new UUID()
        {
            Value = q.ToString()
        });

        CreateMap<Guid?, UUID>().ConstructUsing(q => q.HasValue
            ? new UUID()
            {
                Value = q.ToString()
            }
            : null);
    }
}