﻿using Loka.Inventory.Interfaces.Cases;
using Loka.Inventory.Interfaces.Store.Requests;

namespace Loka.Api.Features.Inventory.Cases;

public sealed class PlayerInventoryCasesMappingProfiles : Profile
{
    public PlayerInventoryCasesMappingProfiles()
    {
        //==========================================================
        CreateMap<BuyStoreCase.Result, BuyGameStoreCase.Types.Response>()
            .ForMember(q => q.Case, q => q.MapFrom(w => w.Case));

        //==========================================================
        CreateMap<Loka.Inventory.Interfaces.Cases.GetPlayerCases.Result, GetPlayerCases.Types.Response>()
            .ForMember(q => q.Entries, q => q.MapFrom(w => w.Entries));

        CreateMap<PlayerInventoryCaseModel, PlayerInventoryCase>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.ModelId))
            .ForMember(q => q.Amount, q => q.MapFrom(w => w.Amount));

        //==========================================================
        CreateMap<OpenPlayerCase.Types.Request, Loka.Inventory.Interfaces.Cases.OpenPlayerCase.Request>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id));

        CreateMap<Loka.Inventory.Interfaces.Cases.OpenPlayerCase.Result, OpenPlayerCase.Types.Response>()
            .ForMember(q => q.Case, q => q.MapFrom(w => w.Case))
            .ForMember(q => q.Resources, q => q.MapFrom(w => w.Resources))
            .ForMember(q => q.Items, q => q.MapFrom(w => w.Items));

        CreateMap<Loka.Inventory.Interfaces.Cases.OpenPlayerCase.ItemsEntry, OpenPlayerCase.Types.ItemEntry>()
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.ModelId))
            .ForMember(q => q.Amount, q => q.MapFrom(w => w.Amount));

        CreateMap<Loka.Inventory.Interfaces.Cases.OpenPlayerCase.ResourceEntry, OpenPlayerCase.Types.ResourceEntry>()
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.ModelId))
            .ForMember(q => q.Amount, q => q.MapFrom(w => w.Amount));
    }
}