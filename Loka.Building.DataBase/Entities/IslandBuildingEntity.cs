﻿using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Entities;

namespace Loka.Building.DataBase.Entities;

public sealed class IslandBuildingEntity : BaseEntity
{
    public Guid ModelId { get; init; }

    [ForeignKey(nameof(IslandEntity))]
    public Guid IslandId { get; init; }

    public IslandEntity? IslandEntity { get; init; }
}