﻿using System;
using Loka.Admin.Common.Models.GamePlay;
using Loka.Common.Entry;

namespace Loka.Admin.Players.Handlers.Players.Inventory.Models;

public sealed class PlayerCurrencyEntry : IEntry
{
    public Guid Id { get; set; }
    public CurrencyInformationModel Currency { get; set; }
    public long Amount { get; set; }
}