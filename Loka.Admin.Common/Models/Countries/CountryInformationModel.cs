﻿using System;
using Loka.Common.Entry;

namespace Loka.Admin.Common.Models.Countries;

public sealed class CountryInformationModel : IEntry, INamedEntry
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;

    public override string ToString()
    {
        return $"Id: {Id} | Name: {Name}";
    }
}