﻿using AutoMapper;
using Loka.Admin.Api.Features.Storages;

namespace Loka.Admin.Api.Mapping.Shared;

public sealed class RoleInformationMapping : Profile
{
    public RoleInformationMapping()
    {
        CreateMap<Common.Models.Accounts.AccountRoleInformationModel, RoleInformationModel>()
            .ForMember(q => q.Id, q => q.MapFrom(p => p.Id))
            .ForMember(q => q.Name, q => q.MapFrom(p => p.Name));

        CreateMap<Common.Models.Accounts.AccountRoleInformationModel[], GetAccountRolesResult>()
            .ForMember(q => q.Entries, q => q.MapFrom(p => p));
    }
}