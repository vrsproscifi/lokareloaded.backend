﻿
using Loka.Store.DataBase.Entities.Items;

namespace Loka.Store.DataBase.Entities;

[Index(nameof(Name))]
[Index(nameof(TypeId), IsUnique = true)]
public sealed class FractionEntity : BaseEntity, INamedEntity
{
    [Required]
    [MaxLength(64)]
    public string Name { get; set; } = null!;

    public FractionTypeId TypeId { get; set; }

    public IReadOnlyList<GameItemEntity>? ItemEntities { get; set; }

    public FractionEntity()
    {
    }

    internal FractionEntity(Guid id, FractionTypeId typeId) : base(id)
    {
        EntityId = id;
        Name = typeId.ToString();
        TypeId = typeId;
        CreatedAt = DefaultCreatedAtDate;
    }
}