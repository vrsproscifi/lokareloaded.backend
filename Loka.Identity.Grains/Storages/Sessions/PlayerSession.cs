﻿namespace Loka.Identity.Grains.Storages.Sessions;

public sealed class PlayerSession
{
    public Guid PlayerId { get; set; }
    public DateTimeOffset CreatedAt { get; set; }
    public DateTimeOffset TimeoutDate { get; set; }
    public DateTimeOffset ExpirationDate { get; set; }

    public override string ToString()
    {
        return $"PlayerId: {PlayerId}, CreatedAt: {CreatedAt:s}, TimeOutDate: {TimeoutDate:s} | ExpirationDate: {ExpirationDate:s}";
    }
}