﻿using System.ComponentModel.DataAnnotations;
using Loka.Database.Common.Entities;
using Loka.Game.Database.Enums.World;

namespace Loka.Game.Database.Entities.World;

[Index(nameof(Name))]
[Index(nameof(Code))]
[Index(nameof(TypeId), IsUnique = true)]
public class WorldLanguageEntity : BaseEntity, INamedEntity, ITypedEntity<WorldLanguageTypeId>
{
    [Required]
    [MaxLength(64)]
    public string Name { get; set; } = null!;

    [Required]
    [MaxLength(3)]
    public string Code { get; set; } = null!;

    public WorldLanguageTypeId TypeId { get; set; }

    public WorldLanguageEntity()
    {
    }

    public WorldLanguageEntity(Guid id, WorldLanguageTypeId typeId, string code) : base(id)
    {
        Name = typeId.ToString();
        TypeId = typeId;
        Code = code;
    }
}