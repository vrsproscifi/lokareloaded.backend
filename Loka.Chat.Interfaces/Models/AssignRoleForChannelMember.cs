﻿using Loka.Chat.Interfaces.Enums;
using MongoDB.Bson;

namespace Loka.Chat.Interfaces.Models;

public static class AssignRoleForChannelMember
{
    [GenerateSerializer]
    public sealed class Request
    {
        [Id(0)]
        public ObjectId ChannelId { get; init; }

        [Id(1)]
        public Guid PlayerId { get; init; }

        [Id(2)]
        public ChatMemberPermissions Permissions { get; init; }
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public ChannelEntry Channel { get; init; } = null!;

        [Id(1)]
        public ChatMemberEntry Member { get; set; } = null!;

        [Id(2)]
        public ChatMemberPermissions Permissions { get; set; }
    }
}