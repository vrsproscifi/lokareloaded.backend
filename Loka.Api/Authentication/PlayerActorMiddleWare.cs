﻿using Loka.Identity.Interfaces.Extensions;
using Orleans.Runtime;

namespace Loka.Api.Authentication;

internal class PlayerActorMiddleWare : IMiddleware
{
    private PlayerActorStorage ActorStorage { get; }

    public PlayerActorMiddleWare(PlayerActorStorage actorStorage)
    {
        ActorStorage = actorStorage;
    }

    public Task InvokeAsync(HttpContext context, RequestDelegate next)
    {
        if (context.User.Identity?.IsAuthenticated ?? false)
        {
            var user = context.User;

            ActorStorage.Actor = new PlayerActor
            (
                user.GetPlayerId(),
                user.GetPlayerName()
            );

            RequestContext.Set(nameof(PlayerActor.PlayerId), ActorStorage.Actor.PlayerId);
            RequestContext.Set(nameof(PlayerActor.PlayerName), ActorStorage.Actor.PlayerName);
        }
        else
        {
            ActorStorage.Actor = PlayerActor.NotAuthorized;
        }

        return next(context);
    }
}