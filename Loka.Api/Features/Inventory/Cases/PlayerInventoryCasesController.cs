﻿using Loka.Inventory.Interfaces.Cases;
using Loka.Inventory.Interfaces.Store;
using Loka.Inventory.Interfaces.Store.Requests;

namespace Loka.Api.Features.Inventory.Cases;

public class PlayerInventoryCasesController : PlayerInventoryCases.PlayerInventoryCasesBase
{
    private ControllerServices<PlayerInventoryCasesController> Services { get; }
    private PlayerActor PlayerActor { get; }

    public PlayerInventoryCasesController(ControllerServices<PlayerInventoryCasesController> services, PlayerActor playerActor)
    {
        Services = services;
        PlayerActor = playerActor;
    }

    public override Task<BuyGameStoreCase.Types.Response> BuyGameStoreCase(BuyGameStoreCase.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerStoreGrain>(PlayerActor.PlayerId);
        return grain.BuyStoreCase(new BuyStoreCase.Request()
        {
            ModelId =request.ModelId.ToGuid()
        }).ReturnResult<BuyStoreCase.Result, BuyGameStoreCase.Types.Response>(Services.Mapper);
    }

    public override Task<GetPlayerCases.Types.Response> GetPlayerCases(GetPlayerCases.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerInventoryCasesGrain>(PlayerActor.PlayerId);
        return grain.GetCases().ReturnResult<Loka.Inventory.Interfaces.Cases.GetPlayerCases.Result, GetPlayerCases.Types.Response>(Services.Mapper);
    }

    public override Task<OpenPlayerCase.Types.Response> OpenPlayerCase(OpenPlayerCase.Types.Request request, ServerCallContext context)
    {
        var req = Services.Mapper.Map<OpenPlayerCase.Types.Request, Loka.Inventory.Interfaces.Cases.OpenPlayerCase.Request>(request);
        var grain = Services.ClusterClient.GetGrain<IPlayerInventoryCasesGrain>(PlayerActor.PlayerId);
        return grain.OpenCase(req).ReturnResult<Loka.Inventory.Interfaces.Cases.OpenPlayerCase.Result, OpenPlayerCase.Types.Response>(Services.Mapper);
    }
}