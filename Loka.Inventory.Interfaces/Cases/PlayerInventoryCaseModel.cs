﻿namespace Loka.Inventory.Interfaces.Cases;

[GenerateSerializer]
public sealed class PlayerInventoryCaseModel : IEntry
{
    [Id(0)]
    public Guid Id { get; init; }

    [Id(1)]
    public Guid ModelId { get; init; }

    [Id(2)]
    public long Amount { get; init; }
}