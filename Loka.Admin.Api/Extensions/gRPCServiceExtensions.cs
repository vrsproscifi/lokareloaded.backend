﻿using System.Diagnostics.CodeAnalysis;
using Loka.Admin.Api.Features.Players;
using Loka.Admin.Api.Features.Storages;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Hosting;

namespace Loka.Admin.Api.Extensions;

[SuppressMessage("ReSharper", "InconsistentNaming")]
public static class GRPCServiceExtensions
{
    public static IEndpointRouteBuilder AddGRPCControllers(this IEndpointRouteBuilder builder, IHostEnvironment environment)
    {
        builder.MapGrpcService<PlayersController>().EnableGrpcWeb();
        builder.MapGrpcService<PlayerMatchesController>().EnableGrpcWeb();
            
        builder.MapGrpcService<AccountRolesController>().EnableGrpcWeb();
        builder.MapGrpcService<WorldCountriesStorageController>().EnableGrpcWeb();
        builder.MapGrpcService<WorldLanguagesStorageController>().EnableGrpcWeb();

        return builder;
    }
}