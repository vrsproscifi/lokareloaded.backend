﻿using System;
using Loka.Admin.Common;
using Loka.Admin.Players.Handlers.Players.Listing.GetPlayerInformation;
using Loka.Admin.Players.Handlers.Players.Listing.GetPlayersList;
using Loka.Admin.Players.Services;
using Loka.Common.MediatR.Extensions;
using Loka.Common.Pagination;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Admin.Players;

public static class ServiceExtensions
{
    public static IServiceCollection AddAdminPlayerServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddAdminCommonServices(configuration);

        services.AddScoped<IPlayerStatisticsService, PlayerStatisticsService>();
            
        services.AddRequestHandler<GetPlayerInformationRequest, GetPlayerInformationResponse, GetPlayerInformationRequestHandler>();
        services.AddRequestHandler<GetPlayersListRequest, PagingResult<GetPlayersListResponse>, GetPlayersListRequestHandler>();

        return services;
    }
}