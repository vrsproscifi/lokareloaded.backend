﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Loka.Database.Common.Entities;

public abstract class BaseEntity
{
    [Key]
    public Guid EntityId { get; set; }

    public DateTimeOffset CreatedAt { get; set; }
    public DateTimeOffset? DeletedAt { get; set; } = null;

    public BaseEntity()
    {
    }

    protected BaseEntity(Guid entityId)
    {
        EntityId = entityId;
        CreatedAt = DefaultCreatedAtDate;
    }

    protected static DateTimeOffset DefaultCreatedAtDate { get; } = new DateTimeOffset(2022, 01, 01, 0, 0, 0, 0, 0, TimeSpan.Zero);
}