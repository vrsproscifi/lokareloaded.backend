﻿using Loka.Common.Operations.Results;
using Loka.Store.Interfaces.Models;
using Loka.Store.Interfaces.Models.Boosters;
using Loka.Store.Interfaces.Models.Cases;
using Loka.Store.Interfaces.Models.Fractions;
using Loka.Store.Interfaces.Models.Items;
using Loka.Store.Interfaces.Models.Resources;

namespace Loka.Store.Interfaces;

public interface IGameStoreGrain : IGrainWithGuidKey
{
    #region Store items

    Task<IReadOnlyList<StoreItemModel>> GetStoreItems();
    Task<OperationResult<StoreItemModel>> GetStoreItem(Guid id);
    Task<IReadOnlyList<StoreItemModel>> GetStoreItems(IReadOnlyList<Guid> ids);
    Task<IReadOnlyList<StoreItemModel>> GetStandardSetOfEquipment();
    
    Task<IReadOnlyList<StoreItemModel>> GetInventoryPresets();
    Task<IReadOnlyList<StoreItemModel>> GetInventoryTemplates();
    
    #endregion
 
    Task<IReadOnlyList<GameResourceModel>> GetGameResources();

    Task<IReadOnlyList<StoreItemCategoryModel>> GetStoreCategories();
    Task<IReadOnlyList<StoreFractionModel>> GetGameFractions();
    
    Task<IReadOnlyList<StoreCaseModel>> GetGameStoreCases();
    Task<OperationResult<StoreCaseModel>> GetGameStoreCase(Guid id);
    
    Task<OperationResult<GameBoosterModel>> GetGameStoreBooster(Guid id);
    Task<IReadOnlyList<GameBoosterModel>> GetGameStoreBoosters();
}