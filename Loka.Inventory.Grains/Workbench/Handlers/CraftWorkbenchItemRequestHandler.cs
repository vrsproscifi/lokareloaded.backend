﻿using Loka.Common.Authentication;
using Loka.Common.MediatR.Handlers;
using Loka.Inventory.Interfaces.Inventory;
using Loka.Inventory.Interfaces.Inventory.Requests;
using Loka.Inventory.Interfaces.Payments;
using Loka.Inventory.Interfaces.Workbench;
using Loka.Store.Interfaces.Repositories;

namespace Loka.Inventory.Grains.Workbench.Handlers;

internal sealed class CraftWorkbenchItemRequestHandler : IOperationHandler<CraftWorkbenchItem.Request, CraftWorkbenchItem.Result>
{
    private IGameStoreItemsRepository GameStoreItemsRepository { get; }
    private IClusterClient ClusterClient { get; }
    private PlayerActor Actor { get; }

    public CraftWorkbenchItemRequestHandler
    (
        IGameStoreItemsRepository gameStoreItemsRepository,
        IClusterClient clusterClient,
        PlayerActor actor
    )
    {
        GameStoreItemsRepository = gameStoreItemsRepository;
        ClusterClient = clusterClient;
        Actor = actor;
    }

    public async Task<OperationResult<CraftWorkbenchItem.Result>> Handle(CraftWorkbenchItem.Request request, CancellationToken cancellationToken)
    {
        var inventory = ClusterClient.GetGrain<IPlayerInventoryItemsGrain>(Actor.PlayerId);
        var paymentsGrain = ClusterClient.GetGrain<IPlayerPaymentsGrain>(Actor.PlayerId);

        var getItemResult = await GameStoreItemsRepository.GetById(request.ModelId);
        if (getItemResult.GetValueIfSucceeded(out var model))
        {
            var blueprints = model.Blueprints;
            if (!blueprints.Any())
                return OperationResultCode.ItemDoesntHaveAnyBlueprints;

            foreach (var blueprint in blueprints)
            {
                if (!blueprint.Depends.Any())
                    continue;

                var payResult = await paymentsGrain.PayFor(new MultiplePayForItem.Request()
                {
                    ModelId = model.Id,
                    Count = request.Count,
                    Resources = blueprint.Depends.Select(q => new MultiplePayForItem.Request.Entry()
                    {
                        ModelId = q.Resource.Id,
                        Amount = q.Amount
                    }).ToArray()
                });

                if (payResult.GetValueIfSucceeded(out var payment))
                {
                    var addItemToInventoryResult = await inventory.AddOrStackInventoryItem(new AddOrStackInventoryItem.Request()
                    {
                        ModelId = model.Id,
                        Amount = model.AmountDefault * request.Count,
                    });

                    if (addItemToInventoryResult.GetValueIfSucceeded(out var inventoryItem))
                    {
                        return new CraftWorkbenchItem.Result
                        {
                            Item = inventoryItem.Item
                        };
                    }
                    else
                    {
                        await paymentsGrain.RollBackPayment(new RollBackPayment.Request()
                        {
                            TransactionId = payment.Id
                        });
                        return addItemToInventoryResult.Error!;
                    }
                }
                else if (payResult.Error!.ResultCode != OperationResultCode.ResourceNotEnough)
                {
                    return payResult.Error;
                }
            }
        }

        return getItemResult.Error!;
    }
}