﻿using AutoMapper;
using Loka.Admin.Players.Models;

namespace Loka.Admin.Api.Mapping.Shared;

public sealed class PlayerObjectsMapping : Profile
{
    public PlayerObjectsMapping()
    {
        CreateMap<PlayerName, PlayerNameObject>()
            .ConstructUsing(q => new PlayerNameObject(q.Name, q.Confirmed));

        CreateMap<PlayerEmail, PlayerEmailObject>()
            .ConstructUsing(q => new PlayerEmailObject(q.Email, q.Confirmed));

        CreateMap<PlayerNameObject, PlayerName>().ConstructUsing(q => new PlayerName
        {
            Name = q.Name,
            Confirmed = q.IsConfirmed
        });

        CreateMap<PlayerEmailObject, PlayerEmail>().ConstructUsing(q => new PlayerEmail
        {
            Email = q.Email,
            Confirmed = q.IsConfirmed
        });
    }
}