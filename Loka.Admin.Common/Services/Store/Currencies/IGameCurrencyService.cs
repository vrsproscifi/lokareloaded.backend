﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Common.Models.GamePlay;
using Loka.Admin.Common.Models.Store;
using Loka.Common.Operations.Results;

namespace Loka.Admin.Common.Services.Store.Currencies;

public interface IGameCurrencyService
{
    Task<OperationResult<CurrencyInformationModel>> GetById(Guid id, CancellationToken cancellationToken = default);
    Task<OperationResult<CurrencyInformationModel[]>> GetByIds(IReadOnlyList<Guid> ids, CancellationToken cancellationToken = default);
}