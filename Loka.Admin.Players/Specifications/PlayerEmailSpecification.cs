﻿using System.Linq;
using Global.Identity.Database.Entities;
using Loka.Database.Common.Specifications;
using Loka.Identity.Database.Entities.Player;

namespace Loka.Admin.Players.Specifications;

internal sealed class PlayerEmailSpecification : Specification.IQueryableSpecification<UserAccountEntity>
{
    public string PlayerEmail { get; }
    public bool? PlayerEmailConfirmed { get; }

    public PlayerEmailSpecification(string playerName, bool? playerNameConfirmed)
    {
        PlayerEmail = playerName;
        PlayerEmailConfirmed = playerNameConfirmed;
    }
        
    public IQueryable<UserAccountEntity> Apply(IQueryable<UserAccountEntity> query)
    {
        var resultQuery = query;

        if (PlayerEmailConfirmed.HasValue)
            resultQuery = query.Where(player => player.IsEmailConfirmed == PlayerEmailConfirmed);
            
        if (!string.IsNullOrWhiteSpace(PlayerEmail))
            resultQuery = query.Where(player => player.Email.Contains(PlayerEmail));

        return resultQuery;
    }
}