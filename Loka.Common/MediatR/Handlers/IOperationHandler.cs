﻿using MediatR;
using Loka.Common.Operations.Results;
using Loka.Common.MediatR.Requests;


namespace Loka.Common.MediatR.Handlers;

public interface IOperationHandler<in TRequest> : IRequestHandler<TRequest, OperationResult>
    where TRequest : IOperationRequest
{

}

public interface IOperationHandler<in TRequest, TResult> : IRequestHandler<TRequest, OperationResult<TResult>> 
    where TRequest : IOperationRequest<TResult>
{

}