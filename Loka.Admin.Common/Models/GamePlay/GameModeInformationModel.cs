﻿using System;
using Loka.Common.Entry;
using Loka.MatchMaking.DataBase.Enums.Game;

namespace Loka.Admin.Common.Models.GamePlay;

public sealed class GameModeInformationModel: IEntry
{
    public Guid Id { get; set; }
    public string Name { get; set; } = null!;
    public GameModeTypeId TypeId { get; set; }

    public override string ToString()
    {
        return $"Id: {Id} | Name: {Name} | TypeId: {TypeId}";
    }
}