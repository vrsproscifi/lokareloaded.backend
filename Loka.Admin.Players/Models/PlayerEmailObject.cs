﻿namespace Loka.Admin.Players.Models;

public readonly struct PlayerEmailObject
{
    public string? Email { get; }
    public bool IsConfirmed { get; }
        
    public PlayerEmailObject(string? email, bool isConfirmed)
    {
        Email = email;
        IsConfirmed = isConfirmed;
    }
}