﻿namespace Loka.Store.Interfaces.Enums;

public enum BonusCaseTypeId : byte
{
    None,
        
    EveryDay,
    AfterMatch,
    AfterWatchAd,
}