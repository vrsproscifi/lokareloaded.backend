﻿using Loka.Chat.Interfaces.Enums;
using MongoDB.Bson;

namespace Loka.Chat.DataBase.Entities;

public sealed class ChatChanelEventEntity
{
    public ObjectId Id { get; set; }
    public ObjectId ChannelId { get; set; }

    public ChatChannelEventType TypeId { get; set; }

    public DateTime CreatedAt { get; set; }
    
    public ChatMessageSender Sender { get; set; } = null!;
    
    public string? Value { get; set; }
}