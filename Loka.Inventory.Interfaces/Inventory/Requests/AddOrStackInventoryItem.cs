﻿namespace Loka.Inventory.Interfaces.Inventory.Requests;

public static class AddOrStackInventoryItem
{
    [GenerateSerializer]
    public sealed class Request
    {
        [Id(0)]
        public Guid ModelId { get; init; }
        
        [Id(1)]
        public long Amount { get; init; }
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public PlayerInventoryItemModel.RPC Item { get; init; } = null!;
    }
}