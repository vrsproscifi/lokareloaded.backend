﻿using System.Threading.Tasks;
using Grpc.Core;
using Loka.Admin.Players.Handlers.Players.Listing.GetPlayerInformation;
using Loka.Admin.Players.Handlers.Players.Listing.GetPlayersList;
using Loka.Common.AspNet.Extensions.OperationResults;
using Loka.Common.AspNet.Features;
using Loka.Common.Pagination;

namespace Loka.Admin.Api.Features.Players;

public sealed class PlayersController : GamePlayers.GamePlayersBase
{
    private ControllerServices<PlayersController> Services { get; }

    public PlayersController(ControllerServices<PlayersController> services)
    {
        Services = services;
    }

    public override Task<GetPlayersListResult> GetPlayersList(GetPlayersListQuery query, ServerCallContext context)
    {
        var request = Services.Mapper.Map<GetPlayersListRequest>(query);
        return Services.Executor.Execute(request).ReturnResult<PagingResult<GetPlayersListResponse>, GetPlayersListResult>(Services.Mapper);
    }

    public override Task<GetPlayerInformationResult> GetPlayerInformation(GetPlayerInformationQuery query, ServerCallContext context)
    {
        var request = Services.Mapper.Map<GetPlayerInformationRequest>(query);
        return Services.Executor.Execute(request).ReturnResult<GetPlayerInformationResponse, GetPlayerInformationResult>(Services.Mapper);
    }
}