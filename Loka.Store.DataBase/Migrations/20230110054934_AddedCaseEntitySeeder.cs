﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Loka.Store.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class AddedCaseEntitySeeder : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CaseItemEntity_CaseEntity_ModelId",
                schema: "Store",
                table: "CaseItemEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_CaseItemEntity_GameItemEntity_ItemId",
                schema: "Store",
                table: "CaseItemEntity");

            migrationBuilder.RenameColumn(
                name: "ItemId",
                schema: "Store",
                table: "CaseItemEntity",
                newName: "CaseId");

            migrationBuilder.RenameIndex(
                name: "IX_CaseItemEntity_ItemId",
                schema: "Store",
                table: "CaseItemEntity",
                newName: "IX_CaseItemEntity_CaseId");

            migrationBuilder.AlterColumn<double>(
                name: "ChanceOfDrop",
                schema: "Store",
                table: "CaseItemEntity",
                type: "double precision",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "integer");

            migrationBuilder.AddColumn<int>(
                name: "OrderBy",
                schema: "Store",
                table: "CaseItemEntity",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "RandomMaxDropResources",
                schema: "Store",
                table: "CaseEntity",
                type: "integer",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "CaseResourceEntity",
                schema: "Store",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    CaseId = table.Column<Guid>(type: "uuid", nullable: false),
                    ModelId = table.Column<Guid>(type: "uuid", nullable: false),
                    MinimumCount = table.Column<int>(type: "integer", nullable: false),
                    MaximumCount = table.Column<int>(type: "integer", nullable: false),
                    ChanceOfDrop = table.Column<double>(type: "double precision", nullable: false),
                    OrderBy = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseResourceEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_CaseResourceEntity_CaseEntity_CaseId",
                        column: x => x.CaseId,
                        principalSchema: "Store",
                        principalTable: "CaseEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CaseResourceEntity_GameResourceEntity_ModelId",
                        column: x => x.ModelId,
                        principalSchema: "Store",
                        principalTable: "GameResourceEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "Store",
                table: "CaseEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "RandomMaxDropItems", "RandomMaxDropResources" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe510"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "Grenades", 5, 5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe520"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "GrenadesAndAids", 5, 5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe530"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "GrenadesAndAK47", 5, 5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe540"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "IronAndCooper", 5, 5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe550"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "GrenadesAndIron", 5, 5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe560"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "AK47AndIron", 5, 5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe570"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "Mixed", 5, 5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe580"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, "Iron", 5, 5 }
                });

            migrationBuilder.InsertData(
                schema: "Store",
                table: "CaseItemEntity",
                columns: new[] { "EntityId", "CaseId", "ChanceOfDrop", "CreatedAt", "DeletedAt", "MaximumCount", "MinimumCount", "ModelId", "OrderBy" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe510"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe510"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe521"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe520"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe522"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe520"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe911"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe531"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe530"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe532"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe530"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe550"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe550"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe560"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe560"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe571"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe570"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe572"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe570"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe573"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe570"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe911"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe574"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe570"), 1.0, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe912"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe575"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe570"), 1.0, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe576"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe570"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"), 0 }
                });

            migrationBuilder.InsertData(
                schema: "Store",
                table: "CaseResourceEntity",
                columns: new[] { "EntityId", "CaseId", "ChanceOfDrop", "CreatedAt", "DeletedAt", "MaximumCount", "MinimumCount", "ModelId", "OrderBy" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe550"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe550"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe560"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe560"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe561"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe540"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe562"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe540"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe203"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe571"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe570"), 1.0, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe202"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe572"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe570"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe203"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe573"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe570"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe574"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe570"), 1.0, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe204"), 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe580"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe580"), 0.5, new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 10, 1, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"), 0 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_CaseResourceEntity_CaseId",
                schema: "Store",
                table: "CaseResourceEntity",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseResourceEntity_ModelId",
                schema: "Store",
                table: "CaseResourceEntity",
                column: "ModelId");

            migrationBuilder.AddForeignKey(
                name: "FK_CaseItemEntity_CaseEntity_CaseId",
                schema: "Store",
                table: "CaseItemEntity",
                column: "CaseId",
                principalSchema: "Store",
                principalTable: "CaseEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CaseItemEntity_GameItemEntity_ModelId",
                schema: "Store",
                table: "CaseItemEntity",
                column: "ModelId",
                principalSchema: "Store",
                principalTable: "GameItemEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_CaseItemEntity_CaseEntity_CaseId",
                schema: "Store",
                table: "CaseItemEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_CaseItemEntity_GameItemEntity_ModelId",
                schema: "Store",
                table: "CaseItemEntity");

            migrationBuilder.DropTable(
                name: "CaseResourceEntity",
                schema: "Store");

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe540"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe580"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe510"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe521"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe522"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe531"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe532"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe550"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe560"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe571"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe572"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe573"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe574"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe575"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe576"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe510"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe520"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe530"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe550"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe560"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "CaseEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe570"));

            migrationBuilder.DropColumn(
                name: "OrderBy",
                schema: "Store",
                table: "CaseItemEntity");

            migrationBuilder.DropColumn(
                name: "RandomMaxDropResources",
                schema: "Store",
                table: "CaseEntity");

            migrationBuilder.RenameColumn(
                name: "CaseId",
                schema: "Store",
                table: "CaseItemEntity",
                newName: "ItemId");

            migrationBuilder.RenameIndex(
                name: "IX_CaseItemEntity_CaseId",
                schema: "Store",
                table: "CaseItemEntity",
                newName: "IX_CaseItemEntity_ItemId");

            migrationBuilder.AlterColumn<int>(
                name: "ChanceOfDrop",
                schema: "Store",
                table: "CaseItemEntity",
                type: "integer",
                nullable: false,
                oldClrType: typeof(double),
                oldType: "double precision");

            migrationBuilder.AddForeignKey(
                name: "FK_CaseItemEntity_CaseEntity_ModelId",
                schema: "Store",
                table: "CaseItemEntity",
                column: "ModelId",
                principalSchema: "Store",
                principalTable: "CaseEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_CaseItemEntity_GameItemEntity_ItemId",
                schema: "Store",
                table: "CaseItemEntity",
                column: "ItemId",
                principalSchema: "Store",
                principalTable: "GameItemEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
