using System;

namespace Loka.Common.EfCore.OrderBy;

public interface IOrderByMethod<out TOrder>
    where TOrder : Enum
{
    bool OrderByAsc { get; }
    TOrder OrderBy { get; }
}