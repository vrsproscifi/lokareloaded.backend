﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Common.Models.Store;
using Loka.Common.Operations.Results;

namespace Loka.Admin.Common.Services.Store.ItemModels;

public interface IGameItemModelService
{
    Task<OperationResult<GameItemInformationModel>> GetById(Guid id, CancellationToken cancellationToken = default);
    Task<OperationResult<GameItemInformationModel[]>> GetByIds(IReadOnlyList<Guid> ids, CancellationToken cancellationToken = default);
}