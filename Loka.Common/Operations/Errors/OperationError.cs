﻿using System.Diagnostics;
using System.Text.Json.Serialization;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;

namespace Loka.Common.Operations.Errors;

[GenerateSerializer]
[DebuggerDisplay("Code: {ResultCode}, ErrorMessage: {ErrorMessage}")]
public class OperationError
{
    [Id(0)]
    public string? ErrorMessage { get; }
    [Id(1)]
    public OperationResultCode ResultCode { get; set; }

    [JsonConstructor]
    public OperationError(OperationResultCode code, string? errorMessage)
    {
        ErrorMessage = errorMessage;
        ResultCode = code;
    }

    public OperationError(OperationResultCode code, params object[] args)
    {
        //ErrorMessage = code.GetResponseMessage(args);
        ResultCode = code;
    }

    public OperationError(OperationResultCode code)
    {
        //ErrorMessage = code.GetResponseMessage();
        ResultCode = code;
    }

    public OperationError()
    {
            
    }
        

    public override string ToString()
    {
        return $"Code: {ResultCode}, ErrorMessage: {ErrorMessage}";
    }
        
}