﻿using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Loka.Admin.Common.Services.World.Languages;
using Loka.Common.AspNet.Features;

namespace Loka.Admin.Api.Features.Storages;

public sealed class WorldLanguagesStorageController : WorldLanguagesStorage.WorldLanguagesStorageBase
{
    private ControllerServices<WorldLanguagesStorageController> Services { get; }
    private ILanguageService CountryService { get; }

    public WorldLanguagesStorageController(ControllerServices<WorldLanguagesStorageController> services, ILanguageService countryService)
    {
        Services = services;
        CountryService = countryService;
    }

    public override async Task<GetWorldLanguagesResult> GetWorldLanguages(Empty request, ServerCallContext context)
    {
        var countries = await CountryService.GetList(context.CancellationToken);
        return Services.Mapper.Map<GetWorldLanguagesResult>(countries);
    }
}