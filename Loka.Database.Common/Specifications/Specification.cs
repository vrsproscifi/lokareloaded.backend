﻿using System.Linq;

namespace Loka.Database.Common.Specifications;

public static class Specification
{
    public interface IQueryableSpecification<T>
        where T : class
    {
        IQueryable<T> Apply(IQueryable<T> query);
    }

    public static IQueryable<T> Where<T>(this IQueryable<T> source,
        IQueryableSpecification<T> spec)
        where T : class
        => spec.Apply(source);
}