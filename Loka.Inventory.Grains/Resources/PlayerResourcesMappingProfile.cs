﻿using Loka.Inventory.DataBase.Entities.Currencies;
using Loka.Inventory.Interfaces.Resources;

namespace Loka.Inventory.Grains.Resources;

internal sealed class PlayerResourcesMappingProfile : Profile
{
    public PlayerResourcesMappingProfile()
    {
        CreateMap<PlayerResourceEntity, PlayerResourceModel.Lightweight>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.Amount, q => q.MapFrom(w => w.Amount))
            .ForMember(q => q.Resource, q => q.Ignore());

        CreateMap<PlayerResourceEntity, PlayerResourceModel.Delta>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.PreviousBalance, q => q.MapFrom(w => w.Amount))
            .ForMember(q => q.Resource, q => q.Ignore());
    }
}