﻿using Loka.Database.Common.Repositories;
using Loka.Store.Interfaces.Models;
using Loka.Store.Interfaces.Models.Resources;

namespace Loka.Store.Interfaces.Repositories;

public interface IGameResourcesRepository : IBaseRepository<GameResourceModel>
{

}