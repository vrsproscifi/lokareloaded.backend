﻿using System;
using Loka.MatchMaking.DataBase.Entities.Server;
using Loka.MatchMaking.DataBase.Enums.Server;

namespace Loka.MatchMaking.DataBase.Seeders.Server;

public static class ServerRegionEntitySeeder
{
    public static Guid RuEast { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe601");
    public static Guid RuCentral { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe602");
    public static Guid RuSouth { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe603");
    public static Guid UsNorth { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe604");
    public static Guid UsSouth { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe605");
    public static Guid China { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe606");
    public static Guid Japan { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe607");
    public static Guid Australian { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe608");

    public static ServerRegionEntity[] Entities { get; } = new[]
    {
        new ServerRegionEntity(RuEast, ServerRegionTypeId.RuEast),
        new ServerRegionEntity(RuCentral, ServerRegionTypeId.RuCentral),
        new ServerRegionEntity(RuSouth, ServerRegionTypeId.RuSouth),
            
        new ServerRegionEntity(UsNorth, ServerRegionTypeId.UsNorth),
        new ServerRegionEntity(UsSouth, ServerRegionTypeId.UsSouth),
            
        new ServerRegionEntity(China, ServerRegionTypeId.China),
        new ServerRegionEntity(Japan, ServerRegionTypeId.Japan),
            
        new ServerRegionEntity(Australian, ServerRegionTypeId.Australian),
    };
}