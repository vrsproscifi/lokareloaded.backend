﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Common.Models.Accounts;
using Loka.Common.Operations.Results;

namespace Loka.Admin.Common.Services.Roles;

public interface IAccountRoleService
{
    Task<OperationResult<AccountRoleInformationModel>> GetById(Guid id, CancellationToken cancellationToken = default);
    Task<AccountRoleInformationModel[]> GetList(CancellationToken cancellationToken = default);
}