﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Loka.Database.Common.Entities;
using Loka.Database.Common.Interfaces;
using Loka.MatchMaking.DataBase.Enums.Game;
using Microsoft.EntityFrameworkCore;

namespace Loka.MatchMaking.DataBase.Entities.Game;

[Index(nameof(Name))]
[Index(nameof(TypeId), IsUnique = true)]
public class GameModeEntity : BaseEntity, INamedEntity
{
    [Required]
    [MaxLength(64)]
    public string Name { get; set; }

    public GameModeTypeId TypeId { get; set; }

    public TimeSpan DurationTime { get; set; }

    public int ScoreLimit { get; set; }
        
    public List<GameModeMapEntity>? Maps { get; set; }
        
    public GameModeEntity()
    {
    }

    internal GameModeEntity(Guid id, GameModeTypeId typeId) : base(id)
    {
        Name = typeId.ToString();
        TypeId = typeId;
        ScoreLimit = 100;
        DurationTime = TimeSpan.FromMinutes(7);
    }
}