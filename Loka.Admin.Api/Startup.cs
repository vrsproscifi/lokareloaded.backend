using System;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Api.Extensions;
using Loka.Admin.Players;
using Loka.Common;
using Loka.Common.AspNet.Extensions;
using Loka.Common.AspNet.Features;
using Loka.Common.MediatR.Extensions;
using Loka.Common.Ranges.MinMaxAvg;
using Loka.MatchMaking.History;
using Loka.MatchMaking.History.Entities;
using Loka.MatchMaking.History.Entities.Team;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;
using MongoDB.Bson;

namespace Loka.Admin.Api;

public class TestService : BackgroundService
{
    public TestService(IServiceProvider serviceProvider)
    {
        ServiceProvider = serviceProvider;
    }

    private IServiceProvider ServiceProvider { get; }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        var scope = ServiceProvider.CreateScope();
        var dbContext = scope.ServiceProvider.GetRequiredService<MatchMakingHistoryDdContext>();
        await dbContext.Matches.InsertOneAsync(new MatchHistoryEntity()
        {
            Id = ObjectId.GenerateNewId(),
            Level = 10,
            Match = new MatchHistoryMatchInformation()
            {
                Duration = TimeSpan.FromDays(8),
                CreatedAt = DateTimeOffset.Now,
                FinishedAt = DateTimeOffset.Now,
                StartedAt = DateTimeOffset.Now
            },
            Process = new MatchHistoryProcessInformation()
            {
                Memory = new MinMaxAvg<short>()
                {
                    Avg = 10,
                    Max = 30,
                    Min = 5
                },
                Processor = new MinMaxAvg<byte>()
                {
                    Avg = 10,
                    Max = 30,
                    Min = 5
                },
                ShutdownAt = DateTimeOffset.Now,
                ShutdownCode = 10,
                StartedAt = DateTimeOffset.Now,
            },
            GameVersion = 10,
            MatchId = Guid.NewGuid(),
            Teams = new[]
            {
                new MatchHistoryTeamInformation()
                {
                    Score = 10,
                },
            }
        }, cancellationToken: stoppingToken);
    }
}

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        IdentityModelEventSource.ShowPII = true; //Add this line

        services.AddControllers();
        services.AddLokaDbContexts(Configuration);

        services.AddOperationExecutor();
        services.AddAuthenticationServices(Configuration);
        services.AddAdminOrleansServices(Configuration);
        services.AddGRPCServices(Configuration);
        services.AddHostedService<TestService>();
        services.AddAdminPlayerServices(Configuration);
            
        services.AddAutoMapperWithProfiles();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }

        app.UseCors(builder =>
        {
            builder.WithOrigins(new[]
                {
                    "http://localhost:4200",
                    "https://localhost:4201",
                    "http://localhost:3000"
                })
                .AllowAnyHeader()
                .AllowAnyMethod();
        });


        //app.UseHttpsRedirection();

        app.UseRouting();
        app.UseGrpcWeb(); // Must be added between UseRouting and UseEndpoints

        app.UseAuthentication();
        app.UseAuthorization();
        //  app.UseMiddleware<PlayerActorMiddleWare>();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
            endpoints.AddGRPCControllers(env);
        });
    }
}