﻿using System.Linq;
using Loka.Common.Ranges.Numerics;
using Loka.Database.Common.Specifications;
using Loka.Identity.Database.Entities.Player;

namespace Loka.Admin.Players.Specifications.Statistics;

internal abstract class PlayerStatisticsSpecification<TRange> : RangeSpecification<PlayerAccountEntity, TRange>
    where TRange : struct
{
    protected PlayerStatisticsSpecification(NumericRange<TRange?, TRange?> range, NumericRangeBeginOptions beginOptions, NumericRangeEndOptions endOptions) : base(range, beginOptions, endOptions)
    {
    }

    protected PlayerStatisticsSpecification(NumericRange<TRange?, TRange?> range, NumericRangeOptions options) : base(range, options)
    {
    }
}