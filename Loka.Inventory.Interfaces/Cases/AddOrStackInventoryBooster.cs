﻿namespace Loka.Inventory.Interfaces.Cases;

public static class AddOrStackInventoryCase
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<PlayerInventoryCaseModel>
    {
        [Id(0)]
        public Guid ModelId { get; init; }

        [Id(1)]
        public long Amount { get; init; }
    }
}