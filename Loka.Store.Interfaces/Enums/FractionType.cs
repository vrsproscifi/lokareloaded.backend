﻿namespace Loka.Store.Interfaces.Enums;

public enum FractionTypeId
{
    None = 0,

    /// <summary>
    /// Keepers of artificial intelligence Alysium
    /// Uses electronic advanced technologies
    /// </summary>
    Keepers = 1,

    /// <summary>
    /// Allies keepers of artificial intelligence Elysium
    /// Uses advanced classical weapons
    /// </summary>
    Keepers_Friend = 2,

    //======================
    /// <summary>
    /// Radicals against the artificial intelligence
    /// Uses classical weapons
    /// </summary>
    RIFT = 3,

    /// <summary>
    /// Allies radicals against the artificial intelligence
    /// Uses advanced electronic/classical weapons
    /// </summary>
    RIFT_Friend = 4,
}