﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Players.Models;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Extensions;
using Loka.Identity.Database;

namespace Loka.Admin.Players.Services;

internal sealed class PlayerStatisticsService : IPlayerStatisticsService
{
    private GameIdentityDbContext IdentityDbContext { get; }

    public PlayerStatisticsService(GameIdentityDbContext identityDbContext)
    {
        IdentityDbContext = identityDbContext;
    }

    public async Task<OperationResult<DetailedPlayerStatisticsModel>> GetDetailedPlayerStatistics(Guid playerId, CancellationToken cancellationToken)
    {
        var statistics = await IdentityDbContext.PlayerStatisticsEntity.GetById(playerId, cancellationToken);
        if (statistics == null)
            return OperationResultCode.PlayerNotFound;

        return new DetailedPlayerStatisticsModel
        {
            Level = (uint) statistics.Score,
            Elo = (uint) statistics.Elo,

            Kills = (uint) statistics.Kills,
            Deaths = (uint) statistics.Deaths,
            Supports = (uint) statistics.Supports,

            Wins = (uint) statistics.Wins,
            Loses = (uint) statistics.Loses,
            Draws = (uint) statistics.Draws,
        };
    }
}