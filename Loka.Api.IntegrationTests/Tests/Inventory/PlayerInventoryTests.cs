﻿using Loka.Api.Features.Inventory.Items;

namespace Loka.Api.IntegrationTests.Tests.Inventory;

internal sealed class PlayerInventoryTests : BaseTest
{
    [Test]
    public async Task GetPlayerInventoryTemplates()
    {
        var playerSession = await Services.PlayerIdentity.SignUp();
        var templates = await Services.PlayerInventoryTemplates.GetPlayerInventoryTemplates(playerSession);
        Assert.That(templates, Is.Empty, $"Failed execute {nameof(Services.PlayerInventoryTemplates.GetPlayerInventoryTemplates)} for {playerSession.PlayerId}");
    }

    [Test]
    public async Task GetPlayerPurchasedItems()
    {
        var playerSession = await Services.PlayerIdentity.SignUp();
        var getTemplatesResult = await Clients.PlayerInventoryItemsClient.GetPlayerPurchasedItemsAsync(new GetPlayerPurchasedItems.Types.Query(), playerSession.UseJWT());
        Assert.That(getTemplatesResult, Is.Not.Null, $"Failed execute {nameof(Clients.PlayerInventoryItemsClient.GetPlayerPurchasedItems)} for {playerSession.PlayerId}");
        Assert.That(getTemplatesResult.Entries, Is.Not.Null, $"Failed execute {nameof(Clients.PlayerInventoryItemsClient.GetPlayerPurchasedItems)} for {playerSession.PlayerId}");
        Assert.That(getTemplatesResult.Entries, Is.Not.Empty, $"Failed execute {nameof(Clients.PlayerInventoryItemsClient.GetPlayerPurchasedItems)} for {playerSession.PlayerId}");
    }

    [Test]
    public async Task GetPlayerDiscounts()
    {
        var playerSession = await Services.PlayerIdentity.SignUp();
        var getTemplatesResult = await Clients.PlayerInventoryItemsClient.GetPlayerDiscountsAsync(new GetPlayerDiscounts.Types.Request(), playerSession.UseJWT());
        Assert.That(getTemplatesResult, Is.Not.Null, $"Failed execute {nameof(Clients.PlayerInventoryItemsClient.GetPlayerDiscounts)} for {playerSession.PlayerId}");
        Assert.That(getTemplatesResult.Entries, Is.Not.Null, $"Failed execute {nameof(Clients.PlayerInventoryItemsClient.GetPlayerDiscounts)} for {playerSession.PlayerId}");
        Assert.That(getTemplatesResult.Entries, Is.Empty, $"Failed execute {nameof(Clients.PlayerInventoryItemsClient.GetPlayerDiscounts)} for {playerSession.PlayerId}");
    }

    [Test]
    public async Task GetPlayerCurrencies()
    {
        var playerSession = await Services.PlayerIdentity.SignUp();
        var resources = await Services.PlayerInventoryResources.GetPlayerResources(playerSession);
        Assert.That(resources, Is.Not.Empty, $"Failed execute {nameof(Services.PlayerInventoryResources.GetPlayerResources)} for {playerSession.PlayerId}");
    }
}