﻿using System;
using System.Linq;

namespace Loka.Common.Pagination;

public static class PaginationExtensions
{
    private const int MaxCount = 40;
    public static IQueryable<T> WithPaging<T>(this IQueryable<T> queryable, PagingQuery pagingQuery)
    {
        var count = Math.Min(MaxCount, pagingQuery.Take);
        return queryable.Skip(pagingQuery.Offset * count).Take(count);
    }
}