﻿using System;

namespace Loka.MatchMaking.DataBase.Entities.Queue;

public class PlayerQueueOptions
{
    public Guid[] GameModesIds { get; set; } = null!;
    public Guid[] GameMapsIds { get; set; } = null!;
    public Guid[] RegionsIds { get; set; } = null!;

    public Guid TargetServerVersionId { get; set; }

    public TimeSpan RoundTime { get; set; }

    public short Difficulty { get; set; } = 4;
    public short NumberOfBots { get; set; }
}