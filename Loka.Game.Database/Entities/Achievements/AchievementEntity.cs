﻿using Loka.Database.Common.Interfaces;
using Loka.Game.Database.Enums.Achievements;

namespace Loka.Game.Database.Entities.Achievements;

[Index(nameof(Name))]
[Index(nameof(TypeId), IsUnique = true)]
[Index(nameof(MethodId))]
public class AchievementEntity : BaseEntity, INamedEntity, IAdminCommentedEntity
{
    [Required]
    [MaxLength(128)]
    public string Name { get; set; } = null!;

    public AchievementTypeId TypeId { get; set; }
    public AchievementMethodId MethodId { get; set; }
    
    [MaxLength(8192)]
    public string? AdminComment { get; set; }

    public AchievementEntity()
    {
    }

    internal AchievementEntity(Guid id, AchievementTypeId typeId, AchievementMethodId methodId) : base(id)
    {
        Name = typeId.ToString();
        TypeId = typeId;
        MethodId = methodId;
    }

}