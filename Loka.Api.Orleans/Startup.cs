using Global.Identity.Database;
using Loka.Api.Orleans.Extensions;
using Loka.Api.Orleans.Services;
using Loka.Chat.DataBase;
using Loka.Common;
using Loka.Common.Authentication;
using Loka.Common.Cache.DistributedCache;
using Loka.Common.MassTransit.MessageBrokers;
using Loka.Common.MediatR.Extensions;
using Loka.Common.Mongo;
using Loka.Common.Tarantool;
using Loka.Database.Common.Extensions;
using Loka.Game.Database;
using Loka.Identity.Database;
using Loka.Game.Services;
using Loka.Identity.Grains;
using Loka.Identity.Interfaces.Extensions;
using Loka.Identity.Interfaces.Models;
using Loka.Inventory.DataBase;
using Loka.Inventory.Grains;
using Loka.MatchMaking.DataBase;
using Loka.MatchMaking.History;
using Loka.Social.DataBase;
using Loka.Social.Grains;
using Loka.Store.DataBase;
using Loka.Store.Grains;
using Microsoft.IdentityModel.Logging;

namespace Loka.Api.Orleans;

public class Startup
{
    public Startup(IConfiguration configuration, IWebHostEnvironment hostEnvironment)
    {
        Configuration = configuration;
        HostEnvironment = hostEnvironment;
    }

    public IWebHostEnvironment HostEnvironment { get; }
    public IConfiguration Configuration { get; }
    public ServiceCollectionInitializer Initializer { get; private set; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
        IdentityModelEventSource.ShowPII = true; //Add this line
        // services.AddOpenTelemetry()
        //     .WithMetrics(q => q.AddMeter("Microsoft.Orleans"))
        //     .WithTracing(builder =>
        //     {
        //         builder
        //             .AddConsoleExporter()
        //             .AddSource(new[]
        //             {
        //                 HostEnvironment.ApplicationName,
        //                 "Microsoft.Orleans.Runtime",
        //                 "Microsoft.Orleans.Application",
        //             })
        //             .SetResourceBuilder(ResourceBuilder.CreateDefault().AddService(serviceName: HostEnvironment.ApplicationName, serviceVersion: "1.0"))
        //             .AddHttpClientInstrumentation()
        //             .AddAspNetCoreInstrumentation();
        //     });

        Initializer = new ServiceCollectionInitializer(nameof(Loka), services, Configuration, HostEnvironment);
        Initializer.AddRedisCache();
        Initializer.AddStoreServices();

        Initializer.AddGameDbContext<GlobalIdentityDbContext>();
        Initializer.AddMongoDbContext<GlobalIdentityMetricsDbContext>();
        Initializer.AddMongoDbContext<GameChatDdContext>();
        Initializer.AddMongoDbContext<InventoryPaymentsDbContext>();
        Initializer.AddMongoDbContext<MatchMakingHistoryDdContext>();

        Initializer.AddGameDbContext<GameIdentityDbContext>();
        Initializer.AddGameDbContext<SystemDbContext>();
        Initializer.AddGameDbContext<StoreDbContext>();
        Initializer.AddGameDbContext<MatchMakingDbContext>();
        Initializer.AddGameDbContext<SocialDbContext>();
        Initializer.AddGameDbContext<InventoryDbContext>();
        services.AddOperationExecutor();

        Initializer.AddIdentityApiServices();
        Initializer.AddInventoryHandlers();

        services.AddHostedService<StartBackgroundOrleansServices>();
        services.AddControllers();
        services.AddOrleansApiServices(Configuration);
        services.AddIdentityOrleansServices(Configuration);
        services.AddTarantoolClientServices(Configuration);
        Initializer.AddGameDbContext<GameIdentityDbContext>();
        services.AddPlayerSocialsOrleansServices(Configuration);
        services.AddGameServices(Configuration);
        services.AddAutoMapperWithProfiles();
        Initializer.AddMassTransitRabbitMQ((context, cfg) =>
        {
            //  cfg.Publish<AfterSignUpEvent>(x =>
            //  {
            //      x.Durable = false; // default: true
            //      x.AutoDelete = true; // default: false
            //      x.ExchangeType = "fanout"; // default, allows any valid exchange type
            //  });
        });

        var settingsSection = Configuration
            .GetSection(nameof(JwtBearerAuthenticationSettings));

        services.Configure<JwtBearerAuthenticationSettings>(settingsSection);
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseDeveloperExceptionPage();
        app.UseHttpsRedirection();

        app.UseRouting();

        app.UseAuthorization();

        app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
    }
}