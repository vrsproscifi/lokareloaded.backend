﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Loka.Store.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class AddedBlueprintSeeders3 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 452, DateTimeKind.Unspecified).AddTicks(9161), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 453, DateTimeKind.Unspecified).AddTicks(121), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 453, DateTimeKind.Unspecified).AddTicks(386), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 453, DateTimeKind.Unspecified).AddTicks(626), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 452, DateTimeKind.Unspecified).AddTicks(3812), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 452, DateTimeKind.Unspecified).AddTicks(4937), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 452, DateTimeKind.Unspecified).AddTicks(5211), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 452, DateTimeKind.Unspecified).AddTicks(5459), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 452, DateTimeKind.Unspecified).AddTicks(5699), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 452, DateTimeKind.Unspecified).AddTicks(5931), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 452, DateTimeKind.Unspecified).AddTicks(6238), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 452, DateTimeKind.Unspecified).AddTicks(6470), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 452, DateTimeKind.Unspecified).AddTicks(6694), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 452, DateTimeKind.Unspecified).AddTicks(6912), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 453, DateTimeKind.Unspecified).AddTicks(3797), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 453, DateTimeKind.Unspecified).AddTicks(4780), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 453, DateTimeKind.Unspecified).AddTicks(4787), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 453, DateTimeKind.Unspecified).AddTicks(5043), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe202"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 453, DateTimeKind.Unspecified).AddTicks(5280), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe203"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 35, 36, 453, DateTimeKind.Unspecified).AddTicks(5509), new TimeSpan(0, 0, 0, 0, 0)));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(5335), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(6177), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(6446), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(6695), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 844, DateTimeKind.Unspecified).AddTicks(9322), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(472), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(754), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(1011), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(1309), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(1581), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(1835), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(2074), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(2477), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(2893), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 845, DateTimeKind.Unspecified).AddTicks(9611), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 846, DateTimeKind.Unspecified).AddTicks(767), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 846, DateTimeKind.Unspecified).AddTicks(775), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 846, DateTimeKind.Unspecified).AddTicks(1112), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe202"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 846, DateTimeKind.Unspecified).AddTicks(1403), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe203"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 34, 42, 846, DateTimeKind.Unspecified).AddTicks(1646), new TimeSpan(0, 0, 0, 0, 0)));
        }
    }
}
