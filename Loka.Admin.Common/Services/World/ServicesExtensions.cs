﻿using Loka.Admin.Common.Models.Countries;
using Loka.Admin.Common.Services.World.Countries;
using Loka.Admin.Common.Services.World.Languages;
using Loka.Database.Common.Storage;
using Loka.Database.Common.Storage.Extensions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Admin.Common.Services.World;

public static class ServicesExtensions
{
    public static IServiceCollection AddAdminWorldServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddCacheableEntryStorage<ICountryService, CountryService, CountryInformationModel>();
        services.AddCacheableEntryStorage<ILanguageService, LanguageService, LanguageInformationModel>();
        return services;
    }
}