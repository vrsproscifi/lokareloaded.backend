﻿using Loka.MatchMaking.Interfaces;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Orleans.Runtime;

namespace Loka.MatchMaking.Grains.Reminders.Processor;

public sealed class MatchMakingProcessorReminder : Grain, IMatchMakingProcessorReminder
{
    private IGrainReminder? GrainReminder { get; set; }
    private ILogger<MatchMakingProcessorReminder> Logger { get; }
    private IOptions<ReminderOptions> ReminderOptions { get; }

    public MatchMakingProcessorReminder(ILogger<MatchMakingProcessorReminder> logger, IOptions<ReminderOptions> reminderOptions)
    {
        Logger = logger;
        ReminderOptions = reminderOptions;
    }

    public async Task ReceiveReminder(string reminderName, TickStatus status)
    {
        Logger.LogInformation("Thanks for reminding me-- I almost forgot!");
        var processorGrain = GrainFactory.GetGrain<IMatchMakingProcessorGrain>(Guid.Empty);
        await processorGrain.Process();
    }

    public override async Task OnActivateAsync(CancellationToken cancellationToken)
    {
        await Start();
        await base.OnActivateAsync(cancellationToken);
    }

    public override async Task OnDeactivateAsync(DeactivationReason reason, CancellationToken cancellationToken)
    {
        await Stop();
        await base.OnDeactivateAsync(reason, cancellationToken);
    }

    public async Task Start()
    {
        if (GrainReminder == null)
        {
            Logger.LogInformation($"Begin {nameof(GrainReminderExtensions.RegisterOrUpdateReminder)} with {ReminderOptions.Value.MinimumReminderPeriod}");
            GrainReminder = await this.RegisterOrUpdateReminder
            (
                nameof(MatchMakingProcessorReminder),
                ReminderOptions.Value.MinimumReminderPeriod,
                ReminderOptions.Value.MinimumReminderPeriod
            );
        }
    }

    public async Task Stop()
    {
        Logger.LogInformation($"Begin {nameof(GrainReminderExtensions.UnregisterReminder)}");
        if (GrainReminder != null)
        {
            await this.UnregisterReminder(GrainReminder);
            GrainReminder = null;
        }
    }
}