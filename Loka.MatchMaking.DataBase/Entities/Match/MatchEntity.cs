﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Entities;
using Loka.MatchMaking.DataBase.Entities.Game;
using Loka.MatchMaking.DataBase.Entities.Server;

namespace Loka.MatchMaking.DataBase.Entities.Match;

public sealed class MatchEntity : BaseEntity
{
    public int GameVersion { get; set; }

    public TimeSpan Duration { get; set; }
        
    public int ScoreLimit { get; set; }
        
    public int Difficulty { get; set; } = 4;
        
    public bool IsAllowJoinNewPlayers { get; set; }

    public Guid? WinnerTeamId { get; set; }

    public MatchProcessEntity? Process { get; set; }
        
    #region Game mode

    [ForeignKey(nameof(GameModeEntity))]
    public Guid GameModeId { get; set; }
    public GameModeEntity? GameModeEntity { get; set; }

    #endregion
        
    #region Game map

    [ForeignKey(nameof(GameMapEntity))]
    public Guid GameMapId { get; set; }
    public GameMapEntity? GameMapEntity { get; set; }        

    #endregion
        
        
    #region Cluster

    [ForeignKey(nameof(ClusterEntity))]
    public Guid ClusterId { get; set; }

    public ClusterEntity? ClusterEntity { get; set; }
    public short ClusterServerPort { get; set; }

    #endregion
}