﻿namespace Loka.Game.Database.Entities.Skills;

public sealed class SkillEntity : BaseEntity, INamedEntity
{
    [Required, MaxLength(128)]
    public string Name { get; set; } = null!;

    public int DefaultLevel { get; set; }
    public int MaxLevel { get; set; }
}