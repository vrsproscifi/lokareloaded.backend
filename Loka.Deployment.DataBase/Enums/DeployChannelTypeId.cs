﻿namespace Loka.Deployment.DataBase.Enums;

public enum DeployChannelTypeId : byte
{
    None,

    Internal,
    Alpha,
    Beta,
    Production,
}