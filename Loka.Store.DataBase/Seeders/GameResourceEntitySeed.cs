﻿using Loka.Store.DataBase.Entities;

namespace Loka.Store.DataBase.Seeders;

public static class GameResourceEntitySeed
{
    public static Guid Coin { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe101");
    public static Guid Donate { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe102");
    public static Guid Money { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe103");
    
    public static Guid Iron { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe201");
    public static Guid Steel { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe202");
    public static Guid Copper { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe203");
    public static Guid Rags { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe204");

    public static GameResourceEntity[] Entities { get; } = new[]
    {
        new GameResourceEntity(Coin, ResourceTypeId.Coin, 1000),
        new GameResourceEntity(Donate, ResourceTypeId.Donate, 100),
        new GameResourceEntity(Money, ResourceTypeId.Money, 0),
        
        new GameResourceEntity(Iron, ResourceTypeId.Iron, 10000),
        new GameResourceEntity(Steel, ResourceTypeId.Steel, 5000),
        new GameResourceEntity(Copper, ResourceTypeId.Copper, 10000),
        new GameResourceEntity(Rags, ResourceTypeId.Rags, 1000),
    };
}