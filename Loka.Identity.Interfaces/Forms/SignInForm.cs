﻿using System.Net;

namespace Loka.Identity.Interfaces.Forms;

[GenerateSerializer]
public sealed class SignInForm
{
    [Id(0)]
    public string UserNameOrEmail { get; init; } = null!;

    [Id(1)]
    public string Password { get; init; } = null!;

    [Id(2)]
    public IPAddress IpAddress { get; init; } = null!;

    [Id(3)]
    public TimeSpan TokenLifetime { get; init; }
}