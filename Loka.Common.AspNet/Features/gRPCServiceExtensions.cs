﻿using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Core.Interceptors;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Common.AspNet.Features;

[SuppressMessage("ReSharper", "InconsistentNaming")]
public static class GRPCServiceExtensions
{
    public class GlobalServerLoggerInterceptor : Interceptor
    {
        public override async Task<TModel> UnaryServerHandler<TRequest, TModel>(TRequest request, ServerCallContext context, UnaryServerMethod<TRequest, TModel> continuation)
        {
            var Model = await base.UnaryServerHandler(request, context, continuation);
            return Model;
        }
    }
        
    public static IServiceCollection AddGRPCServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddGrpc(options =>
        {
            options.EnableDetailedErrors = true;
            options.Interceptors.Add(typeof(GlobalServerLoggerInterceptor));
        });

        services.AddScoped(typeof(ControllerServices<>));
        services.AddScoped(typeof(ControllerServices<,>));

        return services;
    }
}