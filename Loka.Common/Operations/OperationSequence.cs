﻿using System;
using System.Threading.Tasks;
using Loka.Common.Operations.Results;

namespace Loka.Common.Operations;

public static class WhenOperationSequence
{
    public static async Task<OperationResult> When<TResult>(this Task<OperationResult<TResult>> getResultTask, Action<TResult> action)
    {
        var result = await getResultTask;
        if (result.IsFailed)
            return result.Error;
            
        action.Invoke(result.Result!);
        return result;
    }
}

public static class OperationSequence
{
    public static async Task<OperationResult<TResult>> ExecuteSequence<TResult>(this TResult result, Func<Task<OperationResult>> action)
    {
        var actionResult = await action.Invoke();
        if (actionResult.IsFailed)
            return actionResult.Error;

        return result;
    }

    public static async Task<OperationResult<TResult>> ExecuteSequence<TResult>(this Task<OperationResult<TResult>> getResultTask, Func<Task<OperationResult>> action)
    {
        var result = await getResultTask;
        if (result.IsSucceeded)
        {
            var actionResult = await action.Invoke();
            if (actionResult.IsFailed)
                return actionResult.Error;
        }

        return result;
    }   
        
    public static async Task<OperationResult<TResult>> ExecuteSequence<TResult>(this Task<OperationResult<TResult>> getResultTask, Task<OperationResult> action)
    {
        var result = await getResultTask;
        if (result.IsSucceeded)
        {
            var actionResult = await action;
            if (actionResult.IsFailed)
                return actionResult.Error;
        }

        return result;
    }
}