﻿using Loka.Api.Features.Inventory.Boosters;

namespace Loka.Api.IntegrationTests.Asserts;

public static class PlayerInventoryBoostersAsserts
{
    public static void AssertModel(this PlayerInventoryBooster model)
    {
        model.Id.AssertModel();
        model.ModelId.AssertModel();
    }

    public static void AssertResponse(this ActivatePlayerInventoryBooster.Types.Response response)
    {
        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.That(response.Booster, Is.Not.Null, $"Invalid {nameof(response)}.{nameof(response.Booster)}");
        response.Booster.AssertModel();
    }

    public static void AssertResponse(this DeactivatePlayerInventoryBooster.Types.Response response)
    {
        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.That(response.Booster, Is.Not.Null, $"Invalid {nameof(response)}.{nameof(response.Booster)}");
        response.Booster.AssertModel();
    }

    public static void AssertResponse(this GetPlayerInventoryBoosters.Types.Response response)
    {
        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.That(response.Entries, Is.Not.Null, $"Invalid {nameof(response)}.{nameof(response.Entries)}");

        foreach (var resource in response.Entries)
        {
            Assert.That(resource, Is.Not.Null, $"Invalid {resource} in {nameof(response)}");
            Assert.Multiple(() =>
            {
                Assert.That(resource.Id, Is.Not.Null, $"Invalid {nameof(resource.Id)} in {nameof(resource)}");
                Assert.That(resource.ModelId, Is.Not.Null, $"Invalid {nameof(resource.ModelId)} in {nameof(resource)}");
                Assert.That(resource.Amount, Is.GreaterThan(0), $"Invalid {nameof(resource.Amount)} in {nameof(resource)}");
            });
        }
    }
}