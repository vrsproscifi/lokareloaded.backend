﻿using Loka.Common.Orleans.Extensions;
using Loka.Social.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Social.Grains;

public static class ServiceExtensions
{
    public static IServiceCollection AddPlayerSocialsApiServices(this IServiceCollection services, IConfiguration configuration)
    {
        return services;
    }

    public static IServiceCollection AddPlayerSocialsOrleansServices(this IServiceCollection services, IConfiguration configuration)
    {
        return services;
    }
}