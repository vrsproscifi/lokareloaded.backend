﻿using Loka.Api.Features.Inventory.Boosters;
using Loka.Api.Features.Shared;
using Loka.Api.IntegrationTests.Clients;
using Loka.Api.IntegrationTests.Services.PlayerIdentity;

namespace Loka.Api.IntegrationTests.Services.PlayerInventory;

public sealed class PlayerInventoryBoostersService
{
    private PlayerInventoryBoosters.PlayerInventoryBoostersClient PlayerInventoryBoostersClient { get; }

    public PlayerInventoryBoostersService(GameAPIClients client)
    {
        PlayerInventoryBoostersClient = client.PlayerInventoryBoostersClient;
    }

    public async Task<PlayerInventoryBooster> BuyBooster(PlayerSession playerSession, GameBooster.Types.Model model)
    {
        await TestContext.Progress.WriteLineAsync($"BuyGameStoreBooster: {model.Id} | PlayerId: {playerSession.PlayerId}");
        var response = await PlayerInventoryBoostersClient.BuyBoosterAsync(new BuyGameStoreBooster.Types.Request()
        {
            ModelId = model.Id
        }, playerSession.UseJWT());

        Assert.That(response, Is.Not.Null, $"Invalid {nameof(response)}");
        Assert.That(response.Booster, Is.Not.Null, $"Invalid {nameof(response)}.{nameof(response.Booster)}");
        Assert.That(response.Booster.ModelId, Is.EqualTo(model.Id), $"Invalid {nameof(response)}.{nameof(response.Booster)}.{nameof(response.Booster.ModelId)}");
        response.Booster.Id.AssertModel();
        response.Booster.ModelId.AssertModel();

        return response.Booster;
    }

    public Task<PlayerInventoryBooster> ActivateBooster(PlayerSession playerSession, PlayerInventoryBooster booster)
    {
        return ActivateBooster(playerSession, booster.Id);
    }
    
    public async Task<PlayerInventoryBooster> ActivateBooster(PlayerSession playerSession, UUID id)
    {
        var response = await PlayerInventoryBoostersClient.ActivateBoosterAsync(new ActivatePlayerInventoryBooster.Types.Request()
        {
            Id = id
        }, playerSession.UseJWT());

        Assert.Multiple(() =>
        {
            response.AssertResponse();
            Assert.That(response.Booster.Id, Is.EqualTo(id), $"Invalid {nameof(response)}.{nameof(response.Booster)}.{nameof(response.Booster.Id)}");
            Assert.That(response.Booster.IsActivated, Is.True, $"Invalid {nameof(response)}.{nameof(response.Booster)}.{nameof(response.Booster.IsActivated)}");
        });

        await TestContext.Progress.WriteLineAsync($"{nameof(ActivateBooster)}: {id} | PlayerId: {playerSession.PlayerId}");
        return response.Booster;
    }
    
    public Task<PlayerInventoryBooster> DeactivateBooster(PlayerSession playerSession, PlayerInventoryBooster booster)
    {
        return DeactivateBooster(playerSession, booster.Id);
    }
    
    public async Task<PlayerInventoryBooster> DeactivateBooster(PlayerSession playerSession, UUID id)
    {
        var response = await PlayerInventoryBoostersClient.DeactivateBoosterAsync(new DeactivatePlayerInventoryBooster.Types.Request()
        {
            Id = id
        }, playerSession.UseJWT());

        Assert.Multiple(() =>
        {
            response.AssertResponse();
            Assert.That(response.Booster.Id, Is.EqualTo(id), $"Invalid {nameof(response)}.{nameof(response.Booster)}.{nameof(response.Booster.Id)}");
            Assert.That(response.Booster.IsActivated, Is.False, $"Invalid {nameof(response)}.{nameof(response.Booster)}.{nameof(response.Booster.IsActivated)}");
        });

        await TestContext.Progress.WriteLineAsync($"{nameof(DeactivateBooster)}: {id} | PlayerId: {playerSession.PlayerId}");
        return response.Booster;
    }

    public async Task<IReadOnlyList<PlayerInventoryBooster>> GetBoosters(PlayerSession playerSession)
    {
        var response = await PlayerInventoryBoostersClient.GetBoostersAsync(new GetPlayerInventoryBoosters.Types.Request(), playerSession.UseJWT());
        response.AssertResponse();
        return response.Entries;
    }
}