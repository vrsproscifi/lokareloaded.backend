﻿using Loka.Common.Entry;

namespace Loka.Store.Interfaces.Models.Items;

[GenerateSerializer]
public sealed class StoreItemBlueprintModel : IEntry
{
    [Id(0)]
    public Guid Id { get; set; }

    [Id(1)]
    public IReadOnlyList<StoreItemBlueprintDependsModel> Depends { get; set; } = ArraySegment<StoreItemBlueprintDependsModel>.Empty;
}