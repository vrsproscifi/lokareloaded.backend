﻿global using System;
global using System.Text;
global using System.Collections.Generic;
global using System.Linq;
global using System.Threading.Tasks;
global using Loka.Database.Common.Contexts;
global using Microsoft.EntityFrameworkCore;
global using Loka.Common.Mongo;
global using System.ComponentModel.DataAnnotations;
global using Loka.Database.Common.Entities;
global using Loka.Database.Common.Interfaces;

global using System.ComponentModel.DataAnnotations.Schema;
global using Loka.Store.Interfaces.Enums;
 