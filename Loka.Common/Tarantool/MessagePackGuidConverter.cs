﻿using System;
using System.IO;
using ProGaudi.MsgPack.Light;

namespace Loka.Common.Tarantool;

internal class MessagePackGuidConverter : IMsgPackConverter<Guid>, IMsgPackConverter
{
    private const byte FixedExt16 = 0xd8;
    private const byte ExtGuid = 2;

    public void Initialize(MsgPackContext context)
    {
    }

    public void Write(Guid value, IMsgPackWriter writer)
    {
        writer.Write(FixedExt16);
        writer.Write(ExtGuid);
        writer.Write(value.ToByteArray());
    }

    public Guid Read(IMsgPackReader reader)
    {
        // We need read string header
        var type = reader.ReadByte();
        if (type != FixedExt16)
            throw new InvalidDataException($"Invalid data type, type: {(int) type} / 0x{type:X} / {(DataTypes) type} | Excepted: {FixedExt16} / 0x{FixedExt16:X} / FixedExt16");

        var ext = reader.ReadByte();
        if (ext != ExtGuid)
            throw new InvalidDataException($"Invalid extension type, type: {(int) type} / 0x{type:X} / {(DataTypes) type} | ext: {(int) ext} | Excepted: 0x{FixedExt16:X} / FixedExt16, Ext: {ExtGuid}");

        var arraySegment = reader.ReadBytes(16);
        var guid = new Guid(arraySegment);
        return guid;
    }
}