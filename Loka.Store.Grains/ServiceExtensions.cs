﻿using Loka.Common;
using Loka.Store.Grains.Boosters;
using Loka.Store.Grains.Cases;
using Loka.Store.Grains.Items;
using Loka.Store.Grains.Repositories;
using Loka.Store.Interfaces.Repositories;

namespace Loka.Store.Grains;

public static class ServiceExtensions
{
    public static ServiceCollectionInitializer AddStoreServices(this ServiceCollectionInitializer initializer)
    {
        initializer.AddCaseStoreServices();
        initializer.AddGameStoreItemsServices();

        initializer.AddScoped<IGameResourcesRepository, GameResourcesRepository>();
        initializer.AddScoped<IGameStoreFractionsRepository, GameStoreFractionsRepository>();
        initializer.AddScoped<IGameBoostersRepository, GameBoostersRepository>();

        return initializer;
    }

    private static ServiceCollectionInitializer AddCaseStoreServices(this ServiceCollectionInitializer initializer)
    {
        initializer.AddScoped<IGameCasesRepository, GameCasesRepository>();
        initializer.AddScoped<GameCaseResourcesRepository>();
        initializer.AddScoped<GameCaseItemsRepository>();
        return initializer;
    }

    private static ServiceCollectionInitializer AddGameStoreItemsServices(this ServiceCollectionInitializer initializer)
    {
        initializer.AddScoped<IGameStoreItemsRepository, GameStoreItemsRepository>();
        initializer.AddScoped<IGameStoreItemBlueprintsRepository, GameStoreItemBlueprintsRepository>();
        initializer.AddScoped<IGameStoreItemCategoriesRepository, GameStoreItemCategoriesRepository>();
        return initializer;
    }
}