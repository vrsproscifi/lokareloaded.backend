﻿using System;
using AutoMapper;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Loka.Common.Operations.Results;

namespace Loka.Common.AspNet.Extensions.OperationResults;

public static class SyncOperationResultExtensions
{
    private static Empty Empty { get; } = new Empty();

    public static TResult ReturnResult<TResult>(this OperationResult<TResult> result)
    {
        if (result.IsFailed)
        {
            var metaData = new Metadata
            {
                { nameof(result.Error.ResultCode), result.Error.ResultCode.ToString() }
            };
            throw new RpcException(new Status(StatusCode.Internal, result.Error.ToString()), metaData);
        }

        return result.Result!;
    }

    public static Empty ReturnResult(this OperationResult result)
    {
        if (result.IsFailed)
        {
            var metaData = new Metadata
            {
                { nameof(result.Error.ResultCode), result.Error.ResultCode.ToString() }
            };
            throw new RpcException(new Status(StatusCode.Internal, result.Error.ToString()), metaData);
        }

        return Empty;
    }

    public static TModel ReturnResult<TResult, TModel>(this OperationResult<TResult> result, Func<TResult, TModel> map)
    {
        if (result.IsFailed)
        {
            var metaData = new Metadata
            {
                { nameof(result.Error.ResultCode), result.Error.ResultCode.ToString() }
            };

            throw new RpcException(new Status(StatusCode.Internal, result.Error.ToString()), metaData);
        }

        return map(result.Result!);
    }

    public static TModel ReturnResult<TResult, TModel>(this OperationResult<TResult> result, IMapper mapper)
    {
        if (result.IsFailed)
        {
            var metaData = new Metadata
            {
                { nameof(result.Error.ResultCode), result.Error.ResultCode.ToString() }
            };
            throw new RpcException(new Status(StatusCode.Internal, result.Error.ToString()), metaData);
        }

        var model = mapper.Map<TModel>(result.Result!);
        return model;
    }

    public static TModel ReturnResult<TResult, TModel>(this OperationResult<TResult> result, IMapper mapper, Action<TModel>? customize)
    {
        if (result.IsFailed)
        {
            var metaData = new Metadata
            {
                { nameof(result.Error.ResultCode), result.Error.ResultCode.ToString() }
            };
            throw new RpcException(new Status(StatusCode.Internal, result.Error.ToString()), metaData);
        }

        var model = mapper.Map<TModel>(result.Result!);
        customize?.Invoke(model);

        return model;
    }
}