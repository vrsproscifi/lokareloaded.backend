﻿using System;
using Loka.Database.Common.Entities;
using Loka.Database.Common.Entities.Store;
using Microsoft.EntityFrameworkCore;

namespace Loka.Inventory.DataBase.Entities.Cases;

[Index(nameof(PlayerId))]
public sealed class PlayerCaseEntity : BaseEntity, IPlayerChildEntity, IStoreCaseChildEntity
{
    public Guid PlayerId { get; set; }
    public Guid CaseId { get; set; }
    public long Amount { get; set; }
    
    public DateTimeOffset? NextNotifyDate { get; set; }
    public DateTimeOffset? LastUseDate { get; set; }
}