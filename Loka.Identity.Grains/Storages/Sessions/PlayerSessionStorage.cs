﻿using Loka.Common.Cache.DistributedCache;
using Loka.Identity.Interfaces.Models;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Options;

namespace Loka.Identity.Grains.Storages.Sessions;

internal sealed class PlayerSessionRedisStorage : IPlayerSessionStorage
{
    private const string Prefix = "sessions_";
    private ICacheProvider CacheProvider { get; }

    private JwtBearerAuthenticationSettings Settings { get; }

    public PlayerSessionRedisStorage(ICacheProvider cacheProvider, IOptions<JwtBearerAuthenticationSettings> settings)
    {
        CacheProvider = cacheProvider;
        Settings = settings.Value;
    }

    public Task<PlayerSession> Create(Guid playerId)
    {
        return CacheProvider.Set($"{Prefix}{playerId:N}", new PlayerSession
        {
            PlayerId = playerId,
            CreatedAt = DateTimeOffset.UtcNow,
            TimeoutDate = DateTimeOffset.UtcNow.Add(Settings.SessionTimeout),
            ExpirationDate = DateTimeOffset.UtcNow.Add(Settings.TokenLifeTime)
        }, new DistributedCacheEntryOptions()
        {
            AbsoluteExpirationRelativeToNow = Settings.TokenLifeTime
        });
    }

    public Task Remove(Guid playerId)
    {
        return CacheProvider.Remove($"{Prefix}{playerId:N}");
    }

    public Task<PlayerSession> Refresh(Guid playerId)
    {
        return Create(playerId);
    }

    public Task<PlayerSession?> Get(Guid playerId)
    {
        return CacheProvider.Get<PlayerSession>($"{Prefix}{playerId:N}");
    }

    public async Task<bool> HasActive(Guid playerId)
    {
        var session = await GetActive(playerId);
        return session != null;
    }

    public async Task<PlayerSession?> GetActive(Guid playerId)
    {
        var session = await Get(playerId);
        if (session != null &&
            session.ExpirationDate > DateTimeOffset.UtcNow &&
            session.TimeoutDate > DateTimeOffset.UtcNow)
        {
            return session;
        }

        return null;
    }
}