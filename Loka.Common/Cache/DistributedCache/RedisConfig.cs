﻿using Microsoft.Extensions.Configuration;

namespace Loka.Common.Cache.DistributedCache;

public sealed class RedisConfig
{
    public string Host { get; set; } = null!;
    public int Port { get; set; }

    public string? UserName { get; set; }
    public string? Password { get; set; }

    public string? InstanceName { get; set; }

    public bool? UseSSL { get; set; } = false;
}