﻿using Loka.Api.Features.Inventory.Templates;
using Loka.Api.IntegrationTests.Clients;
using Loka.Api.IntegrationTests.Services.PlayerIdentity;

namespace Loka.Api.IntegrationTests.Services.PlayerInventory;

public sealed class PlayerInventoryTemplatesService
{
    private PlayerInventoryTemplates.PlayerInventoryTemplatesClient PlayerInventoryTemplatesClient { get; }

    public PlayerInventoryTemplatesService(GameAPIClients client)
    {
        PlayerInventoryTemplatesClient = client.PlayerInventoryTemplatesClient;
    }

    public async Task<IReadOnlyList<PlayerInventoryTemplate>> GetPlayerInventoryTemplates(PlayerSession playerSession)
    {
        var response = await PlayerInventoryTemplatesClient.GetPlayerInventoryTemplatesAsync(new GetPlayerInventoryTemplates.Types.Request(), playerSession.UseJWT());
        Assert.That(response, Is.Not.Null, $"Invalid {response}");
        Assert.That(response.Entities, Is.Not.Null, $"Invalid {response}");

        foreach (var resource in response.Entities)
        {
            Assert.That(resource, Is.Not.Null, $"Invalid {resource} in {nameof(response)}");
            Assert.That(resource.Id, Is.Not.Null, $"Invalid {nameof(resource.Id)} in {nameof(resource)}");
            Assert.That(resource.Items, Is.Not.Null, $"Invalid {nameof(resource.Items)} in {nameof(resource)}");
        }

        return response.Entities;
    }
}