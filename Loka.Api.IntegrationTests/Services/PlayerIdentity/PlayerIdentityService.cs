﻿using Loka.Api.IntegrationTests.Clients;
using Loka.Api.IntegrationTests.Generators;

namespace Loka.Api.IntegrationTests.Services.PlayerIdentity;

public class PlayerIdentityService
{
    private GameAPIClients Client { get; }
    private PlayerIdentityGenerators PlayerIdentityGenerators { get; }

    public PlayerIdentityService(GameAPIClients gameAPIClients)
    {
        Client = gameAPIClients;
        PlayerIdentityGenerators = new PlayerIdentityGenerators();
    }

    public async Task<PlayerSession> QuickSignUp()
    {
        await TestContext.Progress.WriteLineAsync($"Prepare for execute {nameof(Client.PlayerIdentityClient.SignUpAsync)}");
        var signUpQuery = PlayerIdentityGenerators.GenerateSignUp();

        await TestContext.Progress.WriteLineAsync($"Execute {nameof(Client.PlayerIdentityClient.SignUpAsync)} | Query: {signUpQuery}");
        var signUpResult = await Client.PlayerIdentityClient.SignUpAsync(signUpQuery);
        signUpResult.AssertModel();
        //Assert.AreEqual(signUpResult.DisplayName, signUpQuery.PlayerName, $"Invalid {signUpResult.PlayerId} | Must be equal");

        return new PlayerSession(signUpQuery, signUpResult);
    }

    public async Task<PlayerSession> SignUp()
    {
        var result = await QuickSignUp();
        await Task.Delay(10000);
        return result;
    }
}