﻿using System.Text.Json;
using Microsoft.Extensions.Caching.Distributed;

namespace Loka.Common.Cache.DistributedCache;

public sealed class CacheProvider : ICacheProvider
{
    private static DistributedCacheEntryOptions DefaultCacheEntryOptions { get; } = new DistributedCacheEntryOptions()
    {
        AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(1)
    };
    
    private readonly IDistributedCache _cache;

    public CacheProvider(IDistributedCache cache)
    {
        _cache = cache;
    }

    public async Task<T> GetOrAdd<T>(string key, Func<string, Task<T>> fetcher, DistributedCacheEntryOptions? options = null) where T : class
    {
        var entry = await Get<T>(key);
        if (entry == null)
        {
            entry = await fetcher(key);
            await Set<T>(key, entry, options);
        }

        return entry;
    }

    public async Task<T?> Get<T>(string key) where T : class
    {
        var cachedEntity = await _cache.GetStringAsync(key);
        return cachedEntity == null ? null : JsonSerializer.Deserialize<T>(cachedEntity);
    }


    public async Task<T> Set<T>(string key, T value, DistributedCacheEntryOptions? options = default) where T : class
    {
        var json = JsonSerializer.Serialize(value);
        await _cache.SetStringAsync(key, json, options ?? DefaultCacheEntryOptions);
        return value;
    }

    public async Task Remove(string key)
    {
        await _cache.RemoveAsync(key);
    }
}