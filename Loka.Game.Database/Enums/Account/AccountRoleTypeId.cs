﻿namespace Loka.Game.Database.Enums.Account;

public enum AccountRoleTypeId : byte
{
    None = 0,
    Partner = 1,
    Moderator = 2,
    Administrator = 3,
    Developer = 4,
    BetaTester = 5,
}