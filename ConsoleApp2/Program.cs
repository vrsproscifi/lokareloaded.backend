﻿using System;
using System.IO;
using System.Text;
using Loka.Common;
using Loka.Common.Extensions;
using Newtonsoft.Json;

namespace ConsoleApp2;

class Program
{
    static void Main(string[] args)
    {
        var achievements = EnumerableExtension.GetEnumValues<AchievementTypeId>();
        var sb = new StringBuilder(achievements.Length * 256);

        int index = 100;
        foreach (var achievement in achievements)
        {
            sb.AppendLine($"public static Guid {achievement} {{ get; }} = Guid.Parse(\"a607b39d-6e02-45ce-9b77-900c8dffe{index}\");");
            index++;
        }

        sb.AppendLine("public static AchievementEntity[] Entities { get; } = new[]");
        sb.AppendLine("{");
        foreach (var achievement in achievements)
        {
            sb.AppendLine($"new AchievementEntity({achievement}, AchievementTypeId.{achievement}, AchievementMethodId.Incrementable),");
        }
        sb.AppendLine("};");

        var cs = sb.ToString();
        Console.WriteLine(cs);
        Console.ReadKey();
    }

    private string GenerateCountries()
    {
        var json = File.ReadAllText("C:\\workspace\\contries.json");
        var countries = JsonConvert.DeserializeObject<Country[]>(json);
            
        var sb = new StringBuilder(countries.Length * 256);

        int index = 100;
        foreach (var country in countries)
        {
            var name = country.name
                .Replace(" ", string.Empty)
                .Replace("-", string.Empty);
            sb.AppendLine($"public static Guid {name} {{ get; }} = Guid.Parse(\"a607b39d-6e02-45ce-9b77-200c8dffe{index}\");");
            index++;
        }

        sb.AppendLine("public static WorldLanguageEntity[] Entities { get; } = new[]");
        sb.AppendLine("{");
        foreach (var country in countries)
        {
            var name = country.name
                .Replace(" ", string.Empty)
                .Replace("-", string.Empty);
            sb.AppendLine($"new WorldCountryEntity({name}, WorldCountryTypeId.{country.code.ToUpperInvariant()}, \"{country.name}\"),");
        }
        sb.AppendLine("};");

        var cs = sb.ToString();
        return cs;
    }
}

    
public enum AchievementTypeId : short
{

    //----------------
    Kills,
    Deaths,
    Score,

    //----------------
    Money,
    Experience,

    //----------------
    FirstBlood,
    Assist,


    //----------------
    KillsByPistol,
    KillsByShotGun,
    KillsBySniperRifle,
    KillsByRifle,
    KillsByKnife,
    KillsByGrenade,

    //----------------
    SeriesKills,
    SeriesKillsByPistol,
    SeriesKillsByShotGun,
    SeriesKillsBySniperRifle,
    SeriesKillsByRifle,
    SeriesKillsByKnife,
    SeriesKillsByGrenade,

    //----------------
    MatchesTotal,

    MatchesWins,
    MatchesLoses,
    MatchesDefeats,

    //----------------
    HeadShot,

    //----------------
    //	TimeInGame in Seconds
    TimeInGame,

    //----------------
    BestScore,
    BestKills,

    //----------------
    FirstInMatch,
    SecondInMatch,
    ThirdInMatch,
}
    
public class Country
{
    public string name { get; set; }
    public string code { get; set; }
}