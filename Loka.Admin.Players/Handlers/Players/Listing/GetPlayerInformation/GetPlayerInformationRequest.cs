﻿using System;
using Loka.Common.MediatR.Requests;

namespace Loka.Admin.Players.Handlers.Players.Listing.GetPlayerInformation;

public sealed class GetPlayerInformationRequest : IOperationRequest<GetPlayerInformationResponse>
{
    public Guid PlayerId { get; set; }
}