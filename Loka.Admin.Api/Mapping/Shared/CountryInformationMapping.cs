﻿using AutoMapper;
using Loka.Admin.Api.Features.Storages;

namespace Loka.Admin.Api.Mapping.Shared;

public sealed class CountryInformationMapping : Profile
{
    public CountryInformationMapping()
    {
        CreateMap<Common.Models.Countries.CountryInformationModel, CountryInformationModel>()
            .ForMember(q => q.Id, q => q.MapFrom(p => p.Id))
            .ForMember(q => q.Name, q => q.MapFrom(p => p.Name));
            
        CreateMap<Common.Models.Countries.CountryInformationModel[], GetWorldCountriesResult>()
            .ForMember(q => q.Entries, q => q.MapFrom(p => p));
    }
}