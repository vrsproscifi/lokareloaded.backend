﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Loka.Inventory.DataBase.Migrations
{
    public partial class FirstInit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Inventory");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:pgcrypto", ",,")
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", ",,");

            migrationBuilder.CreateTable(
                name: "PlayerCaseEntity",
                schema: "Inventory",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlayerId = table.Column<Guid>(type: "uuid", nullable: false),
                    CaseId = table.Column<Guid>(type: "uuid", nullable: false),
                    Amount = table.Column<long>(type: "bigint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerCaseEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "PlayerCurrencyEntity",
                schema: "Inventory",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlayerId = table.Column<Guid>(type: "uuid", nullable: false),
                    CurrencyId = table.Column<Guid>(type: "uuid", nullable: false),
                    Amount = table.Column<long>(type: "bigint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerCurrencyEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "PlayerDiscountEntity",
                schema: "Inventory",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlayerId = table.Column<Guid>(type: "uuid", nullable: false),
                    ModelId = table.Column<Guid>(type: "uuid", nullable: false),
                    LastUsingDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    NextAvailableDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    EndDiscountDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerDiscountEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "PlayerPurchasedItemEntity",
                schema: "Inventory",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlayerId = table.Column<Guid>(type: "uuid", nullable: false),
                    ModelId = table.Column<Guid>(type: "uuid", nullable: false),
                    Level = table.Column<int>(type: "integer", nullable: false),
                    Amount = table.Column<long>(type: "bigint", nullable: false),
                    LastUseDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerPurchasedItemEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "PlayerInventoryItemEntity",
                schema: "Inventory",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    PlayerId = table.Column<Guid>(type: "uuid", nullable: false),
                    ItemId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerInventoryItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PlayerInventoryItemEntity_PlayerPurchasedItemEntity_ItemId",
                        column: x => x.ItemId,
                        principalSchema: "Inventory",
                        principalTable: "PlayerPurchasedItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlayerCaseEntity_PlayerId",
                schema: "Inventory",
                table: "PlayerCaseEntity",
                column: "PlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerCurrencyEntity_PlayerId",
                schema: "Inventory",
                table: "PlayerCurrencyEntity",
                column: "PlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerDiscountEntity_PlayerId",
                schema: "Inventory",
                table: "PlayerDiscountEntity",
                column: "PlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerInventoryItemEntity_ItemId",
                schema: "Inventory",
                table: "PlayerInventoryItemEntity",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerInventoryItemEntity_PlayerId",
                schema: "Inventory",
                table: "PlayerInventoryItemEntity",
                column: "PlayerId");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerPurchasedItemEntity_PlayerId",
                schema: "Inventory",
                table: "PlayerPurchasedItemEntity",
                column: "PlayerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlayerCaseEntity",
                schema: "Inventory");

            migrationBuilder.DropTable(
                name: "PlayerCurrencyEntity",
                schema: "Inventory");

            migrationBuilder.DropTable(
                name: "PlayerDiscountEntity",
                schema: "Inventory");

            migrationBuilder.DropTable(
                name: "PlayerInventoryItemEntity",
                schema: "Inventory");

            migrationBuilder.DropTable(
                name: "PlayerPurchasedItemEntity",
                schema: "Inventory");
        }
    }
}
