﻿namespace Loka.Store.DataBase.Entities.Items;

public sealed class GameItemBlueprintEntity : BaseEntity
{
    [ForeignKey(nameof(ItemModelEntity))]
    public Guid ItemModelId { get; set; }
    public GameItemEntity? ItemModelEntity { get; set; }
}