﻿using Loka.Chat.DataBase;
using Loka.Chat.DataBase.Entities;
using Loka.Chat.Interfaces;
using Loka.Chat.Interfaces.Enums;
using Loka.Chat.Interfaces.Models;
using Loka.Common.Authentication;
using Loka.Common.Extensions;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;
using Loka.Common.Orleans.Grains;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Loka.Chat.Grains;

internal sealed class GameChatGrain : Grain, IGameChatGrain, IWithPlayerActor
{
    public PlayerActor Actor { get; set; } = null!;
    private GameChatDdContext GameChatDdContext { get; }

    public GameChatGrain(GameChatDdContext gameChatDdContext)
    {
        GameChatDdContext = gameChatDdContext;
    }

    public async Task<OperationResult<SendChatMessage.Result>> SendChatMessage(SendChatMessage.Request request)
    {
        var checkPermissionsResult = await HasPermissions(request.ChannelId, ChatMemberPermissions.SendMessages);
        if (checkPermissionsResult.GetValueIfSucceeded(out var hasPermissions) && hasPermissions)
        {
            var message = new ChatChanelMessageEntity()
            {
                Id = ObjectId.GenerateNewId(),
                ChannelId = request.ChannelId,
                CreatedAt = DateTime.UtcNow.TruncateMilliseconds(),
                Sender = new ChatMessageSender()
                {
                    Id = Actor.PlayerId,
                    Name = Actor.PlayerName
                },
                Message = request.Message,
            };
            await GameChatDdContext.Messages.InsertOneAsync(message);

            return new SendChatMessage.Result()
            {
                Message = new ChatMessageEntry()
                {
                    Id = message.Id,
                    Message = message.Message,
                    Sender = new ChatMemberEntry()
                    {
                        PlayerName = message.Sender.Name,
                        PlayerId = message.Sender.Id,
                    },
                    CreatedAt = message.CreatedAt
                }
            };
        }

        return checkPermissionsResult.Error!;
    }

    public async Task<GetChatChannels.Result> GetChatChannels(GetChatChannels.Request request)
    {
        var channelIds = await GameChatDdContext.Members
            .Find(q => q.PlayerId == Actor.PlayerId)
            .Project(q => q.ChannelId)
            .ToListAsync();

        var channels = await GameChatDdContext.Channels
            .Find(q => channelIds.Contains(q.Id) || q.TypeId == ChatChanelType.Global)
            .ToListAsync();

        return new GetChatChannels.Result()
        {
            Entries = channels.Select(channel => new ChannelEntry()
            {
                Id = channel.Id,
                Name = channel.Name
            }).ToArray()
        };
    }

    public async Task<OperationResult<GetChatMessages.Result>> GetChatMessages(GetChatMessages.Request request)
    {
        var checkPermissionsResult = await HasPermissions(request.ChannelId, ChatMemberPermissions.ReadMessages);
        if (checkPermissionsResult.GetValueIfSucceeded(out var hasPermissions) && hasPermissions)
        {
            var messages = await GetChatMessages_Internal(request);
            var events = await GetEvents_Internal(request);

            return new GetChatMessages.Result()
            {
                LaterThen = request.LaterThen,
                EarlyThen = request.EarlyThen,

                Messages = messages.messages,
                HasMessagesBefore = messages.hasMessagesBefore,

                Events = events.events,
                HasEventsBefore = events.hasEventsBefore
            };
        }

        return checkPermissionsResult.Error!;
    }

    private async Task<(ChatMessageEntry[] messages, bool hasMessagesBefore)> GetChatMessages_Internal(GetChatMessages.Request request)
    {
        var entities = await GameChatDdContext.Messages
            .Find(q => q.ChannelId == request.ChannelId && q.CreatedAt >= request.LaterThen && q.CreatedAt < request.EarlyThen)
            .ToListAsync();

        var hasMessagesBefore = await GameChatDdContext.Messages
            .Find(q => q.ChannelId == request.ChannelId && q.CreatedAt < request.LaterThen && q.CreatedAt < request.EarlyThen)
            .AnyAsync();

        var messages = entities.Select(message => new ChatMessageEntry()
        {
            Id = message.Id,
            Sender = new ChatMemberEntry()
            {
                PlayerId = message.Sender.Id,
                PlayerName = message.Sender.Name,
            },
            Message = message.Message,
            CreatedAt = message.CreatedAt,
        }).ToArray();

        return (messages, hasMessagesBefore);
    }

    private async Task<(ChatEventEntry[] events, bool hasEventsBefore)> GetEvents_Internal(GetChatMessages.Request request)
    {
        var entities = await GameChatDdContext.Events
            .Find(q => q.ChannelId == request.ChannelId && q.CreatedAt >= request.LaterThen && q.CreatedAt < request.EarlyThen)
            .ToListAsync();

        var events = entities.Select(q => new ChatEventEntry
        {
            Id = q.Id,
            CreatedAt = q.CreatedAt,
            TypeId = q.TypeId,
            Sender = new ChatMemberEntry()
            {
                PlayerId = q.Sender.Id,
                PlayerName = q.Sender.Name
            },
            Value = q.Value
        }).ToArray();

        var hasEventsBefore = await GameChatDdContext.Events
            .Find(q => q.ChannelId == request.ChannelId && q.CreatedAt < request.LaterThen && q.CreatedAt < request.EarlyThen)
            .AnyAsync();

        return (events, hasEventsBefore);
    }

    public async Task<CreateChannel.Result> CreateChannel(CreateChannel.Request request)
    {
        var channel = new ChatChannelEntity()
        {
            Id = ObjectId.GenerateNewId(),
            Name = request.Name,
            CreatedAt = DateTimeOffset.UtcNow,
            TypeId = request.TypeId,
            ClanId = request.ClanId,
            MatchId = request.MatchId,
            RegionId = request.RegionId,
            TeamId = request.TeamId
        };

        await GameChatDdContext.Channels.InsertOneAsync(channel);

        return new CreateChannel.Result()
        {
            Channel = new ChannelEntry()
            {
                Id = channel.Id,
                Name = channel.Name,
                TypeId = channel.TypeId
            },
        };
    }

    public async Task<CreatePrivateChannel.Result> CreatePrivateChannel(CreatePrivateChannel.Request request)
    {
        var channel = await CreateChannel(new CreateChannel.Request()
        {
            Name = request.Name,
            TypeId = ChatChanelType.Private
        });

        await AssignRole_Internal(new AssignRoleForChannelMember.Request()
        {
            ChannelId = channel.Channel.Id,
            Permissions = ChatMemberPermissions.SendMessages | ChatMemberPermissions.InviteNewMembers | ChatMemberPermissions.KickMembers | ChatMemberPermissions.AssignMemberRole | ChatMemberPermissions.Owner,
            PlayerId = Actor.PlayerId
        });

        await AddChatEvent(channel.Channel.Id, entity =>
        {
            entity.TypeId = ChatChannelEventType.Create;
            entity.Value = channel.Channel.Name;
        });

        return new CreatePrivateChannel.Result()
        {
            Channel = channel.Channel,
        };
    }

    private Task AddChatEvent(ObjectId channelId, Action<ChatChanelEventEntity> customize)
    {
        var entity = new ChatChanelEventEntity()
        {
            Id = ObjectId.GenerateNewId(),
            ChannelId = channelId,
            CreatedAt = DateTime.UtcNow,
            Sender = new ChatMessageSender()
            {
                Id = Actor.PlayerId,
                Name = Actor.PlayerName
            },
        };

        customize(entity);
        return GameChatDdContext.Events.InsertOneAsync(entity);
    }

    public async Task<OperationResult<LeaveFromChannel.Result>> LeaveFromChannel(LeaveFromChannel.Request request)
    {
        var getChannelResult = await GetChannel(request.ChannelId);
        if (getChannelResult.GetValueIfSucceeded(out var channel))
        {
            await GameChatDdContext.Members
                .FindOneAndDeleteAsync(q => q.ChannelId == request.ChannelId && q.PlayerId == Actor.PlayerId);

            await AddChatEvent(channel.Id, entity =>
            {
                entity.TypeId = ChatChannelEventType.Left;
                entity.Value = Actor.PlayerId.ToString();
            });
        }

        return getChannelResult.Error!;
    }

    public async Task<OperationResult<KickFromChannel.Result>> KickFromChannel(KickFromChannel.Request request)
    {
        var checkPermissionsResult = await HasPermissions(request.ChannelId, ChatMemberPermissions.KickMembers);
        if (checkPermissionsResult.GetValueIfSucceeded(out var hasPermissions) && hasPermissions)
        {
            await GameChatDdContext.Members
                .FindOneAndDeleteAsync(q => q.ChannelId == request.ChannelId && q.PlayerId == request.PlayerId);

            await AddChatEvent(request.ChannelId, entity =>
            {
                entity.TypeId = ChatChannelEventType.Kick;
                entity.Value = request.PlayerId.ToString();
            });
        }

        return checkPermissionsResult.Error!;
    }

    public async Task<OperationResult<AssignRoleForChannelMember.Result>> AssignRole(AssignRoleForChannelMember.Request request)
    {
        var checkPermissionsResult = await HasPermissions(request.ChannelId, ChatMemberPermissions.AssignMemberRole);
        if (checkPermissionsResult.GetValueIfSucceeded(out var hasPermissions) && hasPermissions)
        {
            if (request.PlayerId == Actor.PlayerId)
                return OperationResultCode.BadRequest;

            var member = await GameChatDdContext.Members
                .Find(q => q.ChannelId == request.ChannelId && q.PlayerId == request.PlayerId)
                .FirstOrDefaultAsync();

            if (member == null)
            {
                await GameChatDdContext.Members.InsertOneAsync(new ChatChanelMemberEntity()
                {
                    Id = ObjectId.GenerateNewId(),
                    ChannelId = request.ChannelId,
                    PlayerId = request.PlayerId,
                    Permissions = request.Permissions,
                    AssignedBy = Actor.PlayerId
                });
            }
            else
            {
                await GameChatDdContext.Members.ReplaceOneAsync(q => q.ChannelId == request.ChannelId && q.PlayerId == request.PlayerId, new ChatChanelMemberEntity()
                {
                    Id = member.Id,
                    ChannelId = member.ChannelId,
                    PlayerId = member.PlayerId,
                    Permissions = request.Permissions,
                    AssignedBy = Actor.PlayerId
                });
            }

            await AddChatEvent(request.ChannelId, entity =>
            {
                entity.TypeId = ChatChannelEventType.AssignRole;
                entity.Value = request.PlayerId.ToString();
            });

            var getChannelResult = await GetChannel(request.ChannelId);
            return new AssignRoleForChannelMember.Result()
            {
                Channel = getChannelResult.Result!,
                Member = new ChatMemberEntry()
                {
                    PlayerId = request.PlayerId,
                    PlayerName = string.Empty
                },
                Permissions = request.Permissions
            };
        }

        return checkPermissionsResult.Error!;
    }

    public async Task<OperationResult<GetChatMembers.Result>> GetChatMembers(GetChatMembers.Request request)
    {
        var checkPermissionsResult = await HasPermissions(request.ChannelId, ChatMemberPermissions.ReadMessages);
        if (checkPermissionsResult.GetValueIfSucceeded(out var hasPermissions) && hasPermissions)
        {
            var entities = await GameChatDdContext.Members
                .Find(m => m.ChannelId == request.ChannelId)
                .ToListAsync();

            var members = entities.Select(member => new ChatMemberEntry
            {
                PlayerId = member.PlayerId,
                PlayerName = string.Empty
            }).ToArray();

            return new GetChatMembers.Result()
            {
                Entries = members
            };
        }

        return checkPermissionsResult.Error!;
    }

    public async Task<OperationResult<GetChatMember.Result>> GetChatMember(GetChatMember.Request request)
    {
        var checkPermissionsResult = await HasPermissions(request.ChannelId, ChatMemberPermissions.ReadMessages);
        if (checkPermissionsResult.GetValueIfSucceeded(out var hasPermissions) && hasPermissions)
        {
            var getChannelResult = await GetChannel(request.ChannelId);
            if (getChannelResult.GetValueIfSucceeded(out var channel))
            {
                var member = await GameChatDdContext.Members
                    .Find(m => m.ChannelId == request.ChannelId && m.PlayerId == request.PlayerId)
                    .FirstOrDefaultAsync();

                if (member == null)
                    return OperationResultCode.EntityNotFound;

                return new GetChatMember.Result()
                {
                    Member = new ChatMemberEntry()
                    {
                        PlayerId = member.PlayerId,
                        PlayerName = string.Empty,
                    },
                    Permissions = member.Permissions
                };
            }
        }

        return checkPermissionsResult.Error!;
    }

    private async Task<OperationResult<bool>> HasPermissions(ObjectId channelId, ChatMemberPermissions permissions)
    {
        var getChannelResult = await GetChannel(channelId);
        if (getChannelResult.GetValueIfSucceeded(out var channel))
        {
            if (channel.TypeId == ChatChanelType.Global && permissions is ChatMemberPermissions.ReadMessages or ChatMemberPermissions.SendMessages)
                return true;

            var member = await GameChatDdContext.Members
                .Find(m => m.ChannelId == channelId && m.PlayerId == Actor.PlayerId)
                .FirstOrDefaultAsync();

            if (member != null && member.Permissions.HasFlag(permissions))
                return true;

            return OperationResultCode.PlayerDoesntHavePermissions;
        }

        return getChannelResult.Error!;
    }

    public async Task<OperationResult<ChannelEntry>> GetChannel(ObjectId channelId)
    {
        var entity = await GameChatDdContext.Channels.Find(c => c.Id == channelId).FirstAsync();
        if (entity == null)
            return OperationResultCode.EntityNotFound;

        return new ChannelEntry
        {
            Id = entity.Id,
            Name = entity.Name,
            TypeId = entity.TypeId
        };
    }

    private Task AssignRole_Internal(AssignRoleForChannelMember.Request request)
    {
        return GameChatDdContext.Members.InsertOneAsync(new ChatChanelMemberEntity()
        {
            Id = ObjectId.GenerateNewId(),
            ChannelId = request.ChannelId,
            PlayerId = request.PlayerId,
            AssignedBy = Actor.PlayerId,
            Permissions = request.Permissions
        });
    }
}