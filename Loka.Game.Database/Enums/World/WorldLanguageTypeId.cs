﻿namespace Loka.Game.Database.Enums.World;

public enum WorldLanguageTypeId : byte
{
    None = 0,

    Russian,
    English,
    German,
}