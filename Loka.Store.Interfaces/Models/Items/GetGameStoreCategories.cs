﻿namespace Loka.Store.Interfaces.Models.Items;

public sealed class GetGameStoreCategories
{
    [GenerateSerializer]
    public sealed class Request
    {
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public IReadOnlyList<StoreItemCategoryModel> Entries { get; set; } = ArraySegment<StoreItemCategoryModel>.Empty;
    }
}