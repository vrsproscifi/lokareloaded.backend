﻿using System.ComponentModel.DataAnnotations;
using Loka.Database.Common.Entities;
using Loka.Game.Database.Enums.World;

namespace Loka.Game.Database.Entities.World;

[Index(nameof(Name))]
[Index(nameof(TypeId), IsUnique = true)]
public sealed class WorldGenderEntity : BaseEntity, INamedEntity, ITypedEntity<WorldGenderTypeId>
{
    [Required]
    [MaxLength(8)]
    public string Name { get; set; } = null!;
    public WorldGenderTypeId TypeId { get; set; }
        
    public WorldGenderEntity()
    {
    }

    public WorldGenderEntity(Guid id, WorldGenderTypeId typeId) : base(id)
    {
        Name = typeId.ToString();
        TypeId = typeId;
    }
}