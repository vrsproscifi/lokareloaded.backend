﻿using System.Threading;
using System.Threading.Tasks;
using Loka.Common.MediatR.Handlers;
using Loka.Common.Operations.Results;
using Loka.Common.Pagination;

namespace Loka.Admin.Players.Handlers.Matches.GetPlayerMatchesHistory;

public class GetPlayerMatchesHistoryRequestHandler : IOperationHandler<GetPlayerMatchesHistoryRequest, PagingResult<GetPlayerMatchesHistoryResponse>>
{
    public Task<OperationResult<PagingResult<GetPlayerMatchesHistoryResponse>>> Handle(GetPlayerMatchesHistoryRequest request, CancellationToken cancellationToken)
    {
        throw new System.NotImplementedException();
    }
}