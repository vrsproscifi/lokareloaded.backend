﻿using System;
using System.ComponentModel.DataAnnotations;
using Loka.Database.Common.Entities;
using Loka.Database.Common.Interfaces;
using Loka.MatchMaking.DataBase.Enums.Server;
using Microsoft.EntityFrameworkCore;

namespace Loka.MatchMaking.DataBase.Entities.Server;

[Index(nameof(Name))]
[Index(nameof(TypeId), IsUnique = true)]
public sealed class ServerRegionEntity : BaseEntity, INamedEntity
{
    [Required]
    [MaxLength(64)]
    public string Name { get; set; } = null!;
    public ServerRegionTypeId TypeId { get; set; }
        
    public ServerRegionEntity()
    {
    }

    internal ServerRegionEntity(Guid id, ServerRegionTypeId typeId) : base(id)
    {
        Name = typeId.ToString();
        TypeId = typeId;
    }
}