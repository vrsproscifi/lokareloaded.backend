﻿namespace Loka.Inventory.Interfaces.Presets.Models;

[GenerateSerializer]
public sealed class SavePresetAsTemplateForm
{
    [Id(0)]
    public string Name { get; init; } = null!;
    [Id(1)]
    public Guid PresetId { get; init; }
}