﻿using Loka.Common.Authentication;
using Orleans;

namespace Loka.Common.Orleans.Grains;

public interface IWithPlayerActor 
{
    PlayerActor Actor { get; set; }
}

public interface IClusterGrain : IGrainWithGuidKey
{
        
}