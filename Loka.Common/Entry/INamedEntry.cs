namespace Loka.Common.Entry;

public interface INamedEntry
{
    string Name { get; }
}