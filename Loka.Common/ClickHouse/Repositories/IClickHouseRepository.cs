﻿namespace Loka.Common.ClickHouse.Repositories;

public interface IClickHouseRepository
{
    bool DropTable();
    bool CreateTable();
}