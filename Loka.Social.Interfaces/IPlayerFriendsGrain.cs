﻿using Loka.Common.Operations.Results;
using Loka.Game.Interfaces.Players.Dtos;
using Loka.Social.Interfaces.Forms;
using Orleans;

namespace Loka.Social.Interfaces;

public interface IPlayerFriendsGrain : IGrainWithGuidKey
{
    Task<SearchPlayers.Result> SearchPlayers(SearchPlayers.Request request);
    Task<GetPlayerFriendsList.Result> GetPlayerFriendsList(GetPlayerFriendsList.Request request);

    Task<OperationResult<PlayerModel>> AddPlayerToFriendList(AddPlayerToFriendListRequest form);

    Task<OperationResult<RespondForInviteResponse>> RespondForInvite(RespondForInviteRequest form);
    Task<OperationResult<SearchPlayersResponse>> SearchPlayers(SearchPlayersRequest form);

    Task<OperationResult<PlayerModel>> RemoveFriend(RemoveFriendRequest form);
}