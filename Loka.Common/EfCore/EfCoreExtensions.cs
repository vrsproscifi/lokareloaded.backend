﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Loka.Common.EfCore;

public static class EfCoreExtensions
{
    public static async Task<T> AddAndSaveAsync<T>(this DbContext InDbContext, T InEntity) where T : class
    {
        InDbContext.Add(InEntity);
        await InDbContext.SaveChangesAsync();
        return InEntity;
    }
        
    public static IQueryable<T> HasTracking<T>(this IQueryable<T> query, bool hasTracking) where T : class
    {
        if (hasTracking)
        {
            return query.AsTracking();
        }

        return query.AsNoTracking();
    }
}