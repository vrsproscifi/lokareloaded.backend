﻿using Loka.Api.Features.Identity;

namespace Loka.Api.IntegrationTests.Generators;

public sealed class PlayerIdentityGenerators
{
    public const string Password = "qazwsx23";

    public SignUpQuery GenerateSignUp()
    {
        return new SignUpQuery()
        {
            PlayerName = $"Name_{Guid.NewGuid():N}",
            Password = Password
        };
    }
}