﻿namespace Loka.Common.Random;

public sealed class RandomGenerator : IRandomGenerator
{
    private const long MaxRandomValue = 1000000000000000000;

    public bool CheckChance(double chance)
    {
        var chancedMaxRandomValue = GetChancedMaxRandomValue(chance);
        var value = System.Random.Shared.NextInt64(0, MaxRandomValue);
        var reached = value <= chancedMaxRandomValue;
        return reached;
    }

    public int InRange(int min, int max)
    {
        return System.Random.Shared.Next(min, max);
    }

    private static long GetChancedMaxRandomValue(double chance)
    {
        var chancedMaxRandomValueDouble = MaxRandomValue * chance;
        var chancedMaxRandomValueRound = Math.Round(chancedMaxRandomValueDouble);
        var chancedMaxRandomValue = (long) chancedMaxRandomValueRound;
        return chancedMaxRandomValue;
    }
}