﻿namespace Loka.Game.Database.Entities.Awards;

public sealed class EveryDayAwardEntity : BaseEntity, IAdminCommentedEntity
{
    public int Day { get; set; }

    public Guid CaseId { get; set; }

    [MaxLength(256)]
    public string? AdminComment { get; set; }
}