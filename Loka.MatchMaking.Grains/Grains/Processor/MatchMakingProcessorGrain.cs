﻿using Loka.MatchMaking.DataBase;
using Loka.MatchMaking.DataBase.Entities.Queue;
using Loka.MatchMaking.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Loka.MatchMaking.Grains.Grains.Processor;

public sealed class MatchMakingProcessorGrain : Grain, IMatchMakingProcessorGrain
{
    private MatchMakingDbContext MatchMakingDbContext { get; }
    private ILogger<MatchMakingProcessorGrain> Logger { get; }

    public MatchMakingProcessorGrain
    (
        ILogger<MatchMakingProcessorGrain> logger,
        MatchMakingDbContext matchMakingDbContext
    )
    {
        Logger = logger;
        MatchMakingDbContext = matchMakingDbContext;
    }

    public async Task Process()
    {
        var players = await MatchMakingDbContext.PlayerQueueEntity
            .Include(entity => entity.Members)
            .Where(entity => entity.State == PlayerQueueState.Ready)
            .ToArrayAsync();

        Logger.LogInformation($"Process: {players.Length} players");
    }
}