﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Loka.Admin.Common.Models.Accounts;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Extensions;
using Loka.Game.Database;
using Microsoft.EntityFrameworkCore;

namespace Loka.Admin.Common.Services.Roles;

internal sealed class AccountRoleService : IAccountRoleService
{
    private IMapper Mapper { get; }
    private SystemDbContext SystemDbContext { get; }

    public AccountRoleService(SystemDbContext systemDbContext, IMapper mapper)
    {
        SystemDbContext = systemDbContext;
        Mapper = mapper;
    }

    public async Task<OperationResult<AccountRoleInformationModel>> GetById(Guid id, CancellationToken cancellationToken)
    {
        var entity = await SystemDbContext.AccountRoleEntity.GetById(id, cancellationToken);
        if (entity == null)
            return OperationResultCode.PlayerNotFound;

        var model = Mapper.Map<AccountRoleInformationModel>(entity);
        return model;
    }

    public Task<AccountRoleInformationModel[]> GetList(CancellationToken cancellationToken = default)
    {
        return SystemDbContext.AccountRoleEntity
            .OrderBy(q => q.EntityId)
            .ProjectTo<AccountRoleInformationModel>(Mapper.ConfigurationProvider)
            .ToArrayAsync(cancellationToken);
    }
}