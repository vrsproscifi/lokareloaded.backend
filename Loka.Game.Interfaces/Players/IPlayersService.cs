﻿using Loka.Game.Interfaces.Players.Dtos;

namespace Loka.Game.Interfaces.Players;

public interface IPlayersService
{
    Task<PlayerModel?> GetById(Guid playerId);
    Task<IReadOnlyList<PlayerModel>> GetByIds(IReadOnlyList<Guid> playerIds);
    Task<IReadOnlyList<PlayerModel>> GetByIdsAndName(IReadOnlyList<Guid> playerIds, string name);
    Task<IReadOnlyList<PlayerModel>> SearchByName(string name);
}