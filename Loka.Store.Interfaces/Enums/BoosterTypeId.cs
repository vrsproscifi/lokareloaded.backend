﻿namespace Loka.Store.Interfaces.Enums;

public enum BoosterTypeId
{
    None,

    PremiumAccount,

    Experience,
    Reputation,

    Money,
    Resources,

    //RespawnTime,
    //Stamina,
    //Accuracy,
    //MovementSpeed,
    //HealthRegeneration,
}