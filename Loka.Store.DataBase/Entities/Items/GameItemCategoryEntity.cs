﻿namespace Loka.Store.DataBase.Entities.Items;

[Index(nameof(Name))]
[Index(nameof(TypeId), IsUnique = true)]
public sealed class GameItemCategoryEntity : BaseEntity, INamedEntity, ITypedEntity<ItemCategoryTypeId>
{
    [Required]
    [MaxLength(64)]
    public string Name { get; set; } = null!;

    public ItemCategoryTypeId TypeId { get; set; }

    public List<GameItemEntity>? ItemEntities { get; set; }

        
    public GameItemCategoryEntity()
    {
    }

    internal GameItemCategoryEntity(Guid id, ItemCategoryTypeId typeId) : base(id)
    {
        EntityId = id;
        Name = typeId.ToString();
        TypeId = typeId;
    }
}