﻿using Loka.Common.Operations.Results;
using Loka.Store.Interfaces;
using Loka.Store.Interfaces.Enums;
using Loka.Store.Interfaces.Models.Boosters;
using Loka.Store.Interfaces.Models.Cases;
using Loka.Store.Interfaces.Models.Fractions;
using Loka.Store.Interfaces.Models.Items;
using Loka.Store.Interfaces.Models.Resources;
using Loka.Store.Interfaces.Repositories;

namespace Loka.Store.Grains;

internal sealed class GameStoreGrain : Grain, IGameStoreGrain
{
    private IGameResourcesRepository ResourcesRepository { get; }
    private IGameStoreFractionsRepository FractionsRepository { get; }
    private IGameStoreItemCategoriesRepository ItemCategoriesRepository { get; }
    private IGameStoreItemsRepository ItemsRepository { get; }
    private IGameCasesRepository GameCasesRepository { get; }
    private IGameBoostersRepository GameBoostersRepository { get; }

    public GameStoreGrain
    (
        IGameResourcesRepository resourcesRepository,
        IGameStoreItemsRepository itemsRepository,
        IGameStoreItemCategoriesRepository itemCategoriesRepository,
        IGameStoreFractionsRepository fractionsRepository, IGameCasesRepository gameCasesRepository, IGameBoostersRepository gameBoostersRepository)
    {
        ResourcesRepository = resourcesRepository;
        ItemsRepository = itemsRepository;
        ItemCategoriesRepository = itemCategoriesRepository;
        FractionsRepository = fractionsRepository;
        GameCasesRepository = gameCasesRepository;
        GameBoostersRepository = gameBoostersRepository;
    }


    public Task<IReadOnlyList<StoreItemModel>> GetStandardSetOfEquipment()
    {
        return ItemsRepository.FilterBy
        (
            nameof(ItemEntityFlags.IssueAfterRegistration),
            item => item.Flags.HasFlag(ItemEntityFlags.IssueAfterRegistration) && item.Category.TypeId != ItemCategoryTypeId.Profile
        );
    }

    public Task<IReadOnlyList<StoreItemModel>> GetInventoryPresets()
    {
        return ItemsRepository.FilterBy
        (
            nameof(ItemCategoryTypeId.Profile),
            item => item.Category.TypeId == ItemCategoryTypeId.Profile
        );
    }

    public Task<IReadOnlyList<StoreItemModel>> GetInventoryTemplates()
    {
        return ItemsRepository.FilterBy
        (
            nameof(ItemCategoryTypeId.Profile),
            item => item.Category.TypeId == ItemCategoryTypeId.Profile
        );
    }

    public Task<IReadOnlyList<GameResourceModel>> GetGameResources()
    {
        return ResourcesRepository.GetAll();
    }

    public async Task<IReadOnlyList<StoreItemModel>> GetStoreItems()
    {
        var items = await ItemsRepository.GetAll();
        return items;
    }

    public Task<OperationResult<StoreItemModel>> GetStoreItem(Guid id)
    {
        return ItemsRepository.GetById(id);
    }

    public Task<IReadOnlyList<StoreItemModel>> GetStoreItems(IReadOnlyList<Guid> ids)
    {
        return ItemsRepository.GetByIds(ids);
    }

    public Task<IReadOnlyList<StoreItemCategoryModel>> GetStoreCategories()
    {
        return ItemCategoriesRepository.GetAll();
    }

    public Task<IReadOnlyList<StoreFractionModel>> GetGameFractions()
    {
        return FractionsRepository.GetAll();
    }

    public Task<IReadOnlyList<StoreCaseModel>> GetGameStoreCases()
    {
        return GameCasesRepository.GetAll();
    }

    public Task<OperationResult<StoreCaseModel>> GetGameStoreCase(Guid id)
    {
        return GameCasesRepository.GetById(id);
    }

    public Task<OperationResult<GameBoosterModel>> GetGameStoreBooster(Guid id)
    {
        return GameBoostersRepository.GetById(id);
    }

    public Task<IReadOnlyList<GameBoosterModel>> GetGameStoreBoosters()
    {
        return GameBoostersRepository.GetAll();
    }
}