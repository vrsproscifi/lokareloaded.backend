﻿namespace Loka.Identity.Database.Entities.Player;

public sealed class PlayerAccountEntity : BaseEntity
{
    [Required, MaxLength(128)]
    public string PlayerName { get; set; } = null!;

    public short PrimeMatchesCount { get; set; }
    public DateTimeOffset PrimeMatchesNextDate { get; set; }
    public DateTimeOffset LastActivityDate { get; set; }

    public PlayerStatisticsEntity? Statistics { get; set; }

    public PlayerAccountEntity()
    {
    }

    public PlayerAccountEntity(string playerName)
    {
        PlayerName = playerName;
        CreatedAt = DateTimeOffset.UtcNow;

        PrimeMatchesCount = 10;
        PrimeMatchesNextDate = DateTimeOffset.UtcNow;

        Statistics = new PlayerStatisticsEntity();
    }
}