﻿using Loka.Api.Features.Chat;
using Loka.Api.Features.Identity;
using Loka.Api.Features.Inventory.Boosters;
using Loka.Api.Features.Inventory.Cases;
using Loka.Api.Features.Inventory.Items;
using Loka.Api.Features.Inventory.Presets;
using Loka.Api.Features.Inventory.Resources;
using Loka.Api.Features.Inventory.Templates;
using Loka.Api.Features.MatchMaking;
using Loka.Api.Features.Social;
using Loka.Api.Features.Store;
using Loka.Api.Features.Workbench;

namespace Loka.Api.Features;

public static class ServiceExtensions
{
    private static IEndpointRouteBuilder AddPlayerInventoryControllers(this IEndpointRouteBuilder builder, IHostEnvironment environment)
    {
        builder.MapGrpcService<PlayerInventoryItemsController>();
        builder.MapGrpcService<PlayerInventoryCasesController>();
        builder.MapGrpcService<PlayerInventoryBoostersController>();
        builder.MapGrpcService<PlayerInventoryResourcesController>();
        builder.MapGrpcService<PlayerInventoryTemplatesController>();
        builder.MapGrpcService<PlayerInventoryPresetsController>();
        return builder;
    }

    public static IEndpointRouteBuilder AddApiControllers(this IEndpointRouteBuilder builder, IHostEnvironment environment)
    {
        builder.AddPlayerInventoryControllers(environment);

        builder.MapGrpcService<IdentityController>();

        builder.MapGrpcService<MatchMakingSquadController>();
        builder.MapGrpcService<MatchMakingQueueController>();
        builder.MapGrpcService<PlayerFriendsController>();
        builder.MapGrpcService<PlayerWorkbenchController>();
        builder.MapGrpcService<GameStoreController>();
        builder.MapGrpcService<GameChatController>();
        return builder;
    }
}