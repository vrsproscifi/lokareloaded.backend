﻿using System;
using Loka.Database.Common.Entities;
using Loka.Database.Common.Entities.Store;
using Microsoft.EntityFrameworkCore;

namespace Loka.Inventory.DataBase.Entities.Currencies;

[Index(nameof(PlayerId))]
public sealed class PlayerResourceEntity: BaseEntity, IPlayerChildEntity, ICurrencyChildEntity
{
    public Guid PlayerId { get; set; }
    public Guid ModelId { get; set; }
    public long Amount { get; set; }
}