﻿using System;

namespace Loka.Database.Common.Entities.Store;

public interface IStoreCaseChildEntity
{
    Guid CaseId { get; }
}