﻿using Loka.Database.Common.Entities;
using Loka.Database.Common.Entities.Store;
using Microsoft.EntityFrameworkCore;

namespace Loka.Inventory.DataBase.Entities.Items;

[Index(nameof(PlayerId))]
public sealed class PlayerItemEntity : BaseEntity, IPlayerChildEntity, IStoreItemChildEntity
{
    public Guid PlayerId { get; set; }
    public Guid ModelId { get; set; }

    public int Level { get; set; }
    public long Amount { get; set; }

    //public IReadOnlyList<PlayerItemModuleEntity>? Modules { get; set; }

    public DateTimeOffset? LastUseDate { get; set; }
}