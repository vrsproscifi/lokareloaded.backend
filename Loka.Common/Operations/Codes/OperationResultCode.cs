﻿namespace Loka.Common.Operations.Codes;

public enum OperationResultCode
{
    UnknownError = 0,

    Successfully = 1,

    ExceptionError = 2,

    PlayerNotFound = 10000,
    PlayerBanned = 10001,
    PlayerHasActiveSession = 10002,

    MembersInSquadNotReady = 10003,
    MembersAlreadyInMatch = 10004,
    MembersHasDifferentVersions = 10005,

    CountryNotFound = 10006,
    LanguageNotFound = 10007,
    WrongPassword = 10008,
    EntityNotFound = 10009,
    PlayerEntityNotFound = 10010,
    GameCurrencyNotFound = 10011,
    PlayerCurrencyNotFound = 10012,
    ResourceNotEnough = 10013,
    PresetAlreadyBought = 10014,
    ItemDoesntHaveAnyBlueprints,
    PlayerDoesntHavePermissions,
    BadRequest,
    CaseNotEnough,
    TooMuchActivatedBoosters,
}