﻿using Loka.Inventory.Interfaces.Cases;

namespace Loka.Inventory.Interfaces.Store.Requests;

public static class BuyStoreCase
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<Result>
    {
        [Id(0)]
        public Guid ModelId { get; init; }
    }
    
    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public PlayerInventoryCaseModel Case { get; init; }
    }
}