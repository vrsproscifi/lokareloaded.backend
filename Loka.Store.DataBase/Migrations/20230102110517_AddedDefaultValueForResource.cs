﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Loka.Store.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class AddedDefaultValueForResource : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "DefaultValue",
                schema: "Store",
                table: "GameResourceEntity",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "DefaultValue",
                value: 1000);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"),
                column: "DefaultValue",
                value: 100);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"),
                column: "DefaultValue",
                value: 0);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"),
                column: "DefaultValue",
                value: 10000);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe202"),
                column: "DefaultValue",
                value: 5000);

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe203"),
                column: "DefaultValue",
                value: 10000);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DefaultValue",
                schema: "Store",
                table: "GameResourceEntity");
        }
    }
}
