﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Loka.Store.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class FirstInit : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Store");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:pgcrypto", ",,")
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", ",,");

            migrationBuilder.CreateTable(
                name: "BonusCaseEntity",
                schema: "Store",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false),
                    Delay = table.Column<TimeSpan>(type: "interval", nullable: true),
                    FirstNotifyDelay = table.Column<TimeSpan>(type: "interval", nullable: true),
                    NextNotifyDelay = table.Column<TimeSpan>(type: "interval", nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BonusCaseEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "CaseEntity",
                schema: "Store",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    RandomMaxDropItems = table.Column<int>(type: "integer", nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "FractionEntity",
                schema: "Store",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    TypeId = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FractionEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "GameCurrencyEntity",
                schema: "Store",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    TypeId = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameCurrencyEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "GameItemCategoryEntity",
                schema: "Store",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    TypeId = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameItemCategoryEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "GameItemEntity",
                schema: "Store",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    FractionId = table.Column<Guid>(type: "uuid", nullable: false),
                    Level = table.Column<int>(type: "integer", nullable: false),
                    CategoryId = table.Column<Guid>(type: "uuid", nullable: false),
                    SellCurrencyId = table.Column<Guid>(type: "uuid", nullable: false),
                    SellPrice = table.Column<int>(type: "integer", nullable: false),
                    SubscribeCurrencyId = table.Column<Guid>(type: "uuid", nullable: false),
                    SubscribePrice = table.Column<int>(type: "integer", nullable: false),
                    Flags = table.Column<byte>(type: "smallint", nullable: false),
                    Duration = table.Column<TimeSpan>(type: "interval", nullable: true),
                    DisplayPosition = table.Column<int>(type: "integer", nullable: false),
                    AmountDefault = table.Column<long>(type: "bigint", nullable: false),
                    AmountInStack = table.Column<long>(type: "bigint", nullable: false),
                    AmountMinimum = table.Column<long>(type: "bigint", nullable: false),
                    AmountMaximum = table.Column<long>(type: "bigint", nullable: true),
                    AdminComment = table.Column<string>(type: "character varying(1024)", maxLength: 1024, nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_GameItemEntity_FractionEntity_FractionId",
                        column: x => x.FractionId,
                        principalSchema: "Store",
                        principalTable: "FractionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameItemEntity_GameCurrencyEntity_SellCurrencyId",
                        column: x => x.SellCurrencyId,
                        principalSchema: "Store",
                        principalTable: "GameCurrencyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameItemEntity_GameCurrencyEntity_SubscribeCurrencyId",
                        column: x => x.SubscribeCurrencyId,
                        principalSchema: "Store",
                        principalTable: "GameCurrencyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameItemEntity_GameItemCategoryEntity_CategoryId",
                        column: x => x.CategoryId,
                        principalSchema: "Store",
                        principalTable: "GameItemCategoryEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CaseItemEntity",
                schema: "Store",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    ModelId = table.Column<Guid>(type: "uuid", nullable: false),
                    ItemId = table.Column<Guid>(type: "uuid", nullable: false),
                    MinimumCount = table.Column<int>(type: "integer", nullable: false),
                    MaximumCount = table.Column<int>(type: "integer", nullable: false),
                    ChanceOfDrop = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_CaseItemEntity_CaseEntity_ModelId",
                        column: x => x.ModelId,
                        principalSchema: "Store",
                        principalTable: "CaseEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CaseItemEntity_GameItemEntity_ItemId",
                        column: x => x.ItemId,
                        principalSchema: "Store",
                        principalTable: "GameItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "Store",
                table: "FractionEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 555, DateTimeKind.Unspecified).AddTicks(5708), new TimeSpan(0, 0, 0, 0, 0)), null, "Keepers", 1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 555, DateTimeKind.Unspecified).AddTicks(6708), new TimeSpan(0, 0, 0, 0, 0)), null, "Keepers_Friend", 2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 555, DateTimeKind.Unspecified).AddTicks(6919), new TimeSpan(0, 0, 0, 0, 0)), null, "RIFT", 3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 555, DateTimeKind.Unspecified).AddTicks(7114), new TimeSpan(0, 0, 0, 0, 0)), null, "RIFT_Friend", 4 }
                });

            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameCurrencyEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 556, DateTimeKind.Unspecified).AddTicks(3829), new TimeSpan(0, 0, 0, 0, 0)), null, "Coin", 3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 556, DateTimeKind.Unspecified).AddTicks(4650), new TimeSpan(0, 0, 0, 0, 0)), null, "Donate", 2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 556, DateTimeKind.Unspecified).AddTicks(4863), new TimeSpan(0, 0, 0, 0, 0)), null, "Money", 1 }
                });

            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 556, DateTimeKind.Unspecified).AddTicks(9014), new TimeSpan(0, 0, 0, 0, 0)), null, "Weapon", 1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 556, DateTimeKind.Unspecified).AddTicks(9667), new TimeSpan(0, 0, 0, 0, 0)), null, "Armour", 2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 556, DateTimeKind.Unspecified).AddTicks(9874), new TimeSpan(0, 0, 0, 0, 0)), null, "Service", 3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 557, DateTimeKind.Unspecified).AddTicks(66), new TimeSpan(0, 0, 0, 0, 0)), null, "Character", 4 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 557, DateTimeKind.Unspecified).AddTicks(252), new TimeSpan(0, 0, 0, 0, 0)), null, "Profile", 5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 557, DateTimeKind.Unspecified).AddTicks(438), new TimeSpan(0, 0, 0, 0, 0)), null, "Material", 6 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 557, DateTimeKind.Unspecified).AddTicks(619), new TimeSpan(0, 0, 0, 0, 0)), null, "Grenade", 7 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 557, DateTimeKind.Unspecified).AddTicks(800), new TimeSpan(0, 0, 0, 0, 0)), null, "Kits", 8 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 557, DateTimeKind.Unspecified).AddTicks(981), new TimeSpan(0, 0, 0, 0, 0)), null, "Currency", 9 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"), new DateTimeOffset(new DateTime(2022, 12, 17, 23, 42, 29, 557, DateTimeKind.Unspecified).AddTicks(1162), new TimeSpan(0, 0, 0, 0, 0)), null, "Resource", 10 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_BonusCaseEntity_Name",
                schema: "Store",
                table: "BonusCaseEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_BonusCaseEntity_TypeId",
                schema: "Store",
                table: "BonusCaseEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CaseEntity_Name",
                schema: "Store",
                table: "CaseEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_CaseItemEntity_ItemId",
                schema: "Store",
                table: "CaseItemEntity",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseItemEntity_ModelId",
                schema: "Store",
                table: "CaseItemEntity",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_FractionEntity_Name",
                schema: "Store",
                table: "FractionEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_FractionEntity_TypeId",
                schema: "Store",
                table: "FractionEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameCurrencyEntity_Name",
                schema: "Store",
                table: "GameCurrencyEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_GameCurrencyEntity_TypeId",
                schema: "Store",
                table: "GameCurrencyEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameItemCategoryEntity_Name",
                schema: "Store",
                table: "GameItemCategoryEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemCategoryEntity_TypeId",
                schema: "Store",
                table: "GameItemCategoryEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_CategoryId",
                schema: "Store",
                table: "GameItemEntity",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_DisplayPosition",
                schema: "Store",
                table: "GameItemEntity",
                column: "DisplayPosition");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_FractionId",
                schema: "Store",
                table: "GameItemEntity",
                column: "FractionId");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_Level",
                schema: "Store",
                table: "GameItemEntity",
                column: "Level");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_SellCurrencyId",
                schema: "Store",
                table: "GameItemEntity",
                column: "SellCurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_SubscribeCurrencyId",
                schema: "Store",
                table: "GameItemEntity",
                column: "SubscribeCurrencyId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BonusCaseEntity",
                schema: "Store");

            migrationBuilder.DropTable(
                name: "CaseItemEntity",
                schema: "Store");

            migrationBuilder.DropTable(
                name: "CaseEntity",
                schema: "Store");

            migrationBuilder.DropTable(
                name: "GameItemEntity",
                schema: "Store");

            migrationBuilder.DropTable(
                name: "FractionEntity",
                schema: "Store");

            migrationBuilder.DropTable(
                name: "GameCurrencyEntity",
                schema: "Store");

            migrationBuilder.DropTable(
                name: "GameItemCategoryEntity",
                schema: "Store");
        }
    }
}
