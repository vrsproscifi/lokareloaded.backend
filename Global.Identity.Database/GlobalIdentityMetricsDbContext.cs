﻿using Global.Identity.Database.Entities;

namespace Global.Identity.Database;

public sealed class GlobalIdentityMetricsDbContext : MongoDbContext
{
    public IMongoCollection<UserAccountSessionEntry> UserAccountSessionEntry { get; set; } = null!;
}