﻿using Global.Identity.Database.Entities;

namespace Global.Identity.Database;

public sealed class GlobalIdentityDbContext : BaseGameDbContext<GlobalIdentityDbContext>
{
    public DbSet<UserAccountEntity> UserAccountEntity { get; init; } = null!;

    public GlobalIdentityDbContext(DbContextOptions<GlobalIdentityDbContext> options) : base(options)
    {
        
    }
}