﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Loka.Admin.Common.Models.Store;
using Loka.Common.Cache.Single;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Extensions;
using Loka.Database.Common.Specifications;
using Loka.Game.Database;
using Loka.Game.Database.Entities.Store.Items;
using Microsoft.EntityFrameworkCore;

namespace Loka.Admin.Common.Services.Store.ItemCategories;

internal sealed class GameItemCategoryService : IGameItemCategoryService
{
    private IMapper Mapper { get; }
    private SystemDbContext SystemDbContext { get; }

    public GameItemCategoryService(SystemDbContext systemDbContext, IMapper mapper)
    {
        SystemDbContext = systemDbContext;
        Mapper = mapper;
    }

    public async Task<OperationResult<GameItemCategoryInformationModel>> GetById(Guid id, CancellationToken cancellationToken)
    {
        var entity = await SystemDbContext.GameItemCategoryEntity.GetById(id, cancellationToken);
        if (entity == null)
            return OperationResultCode.PlayerNotFound;

        var model = Mapper.Map<GameItemCategoryInformationModel>(entity);
        /*     new GameItemCategoryInformationModel
         {
             Id = entity.EntityId,
             Name = entity.Name,
             TypeId = entity.TypeId
         };*/

        return model;
    }

    public async Task<OperationResult<GameItemCategoryInformationModel[]>> GetByIds(IReadOnlyList<Guid> ids, CancellationToken cancellationToken = default)
    {
        var models = await SystemDbContext.GameItemCategoryEntity
            .Where(new EntityIdsRangeSpecification<GameItemCategoryEntity>(ids))
            .ProjectTo<GameItemCategoryInformationModel>(Mapper.ConfigurationProvider)
            .ToArrayAsync(cancellationToken);

        return models;
    }
}