﻿namespace Loka.Store.DataBase.Entities.Cases;

[Index(nameof(Name))]
[Index(nameof(TypeId), IsUnique = true)]
public sealed class BonusCaseEntity : BaseEntity, INamedEntity
{
    [Required]
    [MaxLength(64)]
    public string Name { get; set; } = null!;

    public BonusCaseTypeId TypeId { get; set; }

    public TimeSpan? Delay { get; set; }

    public TimeSpan? FirstNotifyDelay { get; set; }
    public TimeSpan? NextNotifyDelay { get; set; }

    public BonusCaseEntity()
    {
    }

    internal BonusCaseEntity(Guid id, BonusCaseTypeId typeId) : base(id)
    {
        Name = typeId.ToString();
        TypeId = typeId;
    }
}