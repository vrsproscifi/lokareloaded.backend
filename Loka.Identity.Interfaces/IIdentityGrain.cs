﻿using Loka.Common.Operations.Results;
using Loka.Identity.Interfaces.Forms;
using Loka.Identity.Interfaces.Models;
using Orleans;

namespace Loka.Identity.Interfaces;

public interface IIdentityGrain : IGrainWithStringKey
{
    Task<OperationResult<PlayerAuthenticationModel>> SignUp(SignUpForm form);
    Task<OperationResult<PlayerAuthenticationModel>> SignIn(SignInForm form);
    Task<OperationResult<RefreshTokenModel>> RefreshToken(RefreshTokenForm form);
}