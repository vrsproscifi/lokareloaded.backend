﻿using System;
using AutoMapper;
using Loka.Common.Pagination;

namespace Loka.Admin.Api.Mapping.Shared;

public sealed class PagingMapping : Profile
{
    public PagingMapping()
    {
        CreateMap<PagingQueryModel, PagingQuery>()
            .ForMember(q => q.Take, q => q.MapFrom(p => p.Count))
            .ForMember(q => q.Offset, q => q.MapFrom(p => p.Offset));
    }
}