﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Loka.Common.ClickHouse.Attributes;

namespace Loka.Common.ClickHouse;

public class ClickhouseTable
{
    public ClickhouseTableEngineAttribute Engine { get; }
    public IReadOnlyList<ClickhouseTableColumn> Columns { get; set; }
    public IReadOnlyList<ClickhouseTableColumn> ColumnsForInsert { get; set; }
        
    internal string[] ColumnsNames { get; }
    internal string[] ColumnsNamesForInsert { get; }
        
    internal string InsertQuery { get; }
    internal string DropQuery { get; }
    internal string CreateQuery { get; }

    public ClickhouseTable(Type tableType)
    {
        var properties = tableType.GetProperties();

        Engine = tableType.GetCustomAttribute<ClickhouseTableEngineAttribute>();

        Columns = properties
            .Where(p => CustomAttributeExtensions.GetCustomAttribute<ClickhouseTableColumnAttribute>((MemberInfo) p) != null)
            .Select(p =>
            {
                var attribute = p.GetCustomAttribute<ClickhouseTableColumnAttribute>();
                return new ClickhouseTableColumn(p, attribute!.Name, attribute.Type, attribute.Nullable, attribute.IgnoreInsert);
            }).ToArray();

        ColumnsForInsert = Columns
            .Where(q => !q.IgnoreInsert)
            .ToArray();

        ColumnsNamesForInsert = ColumnsForInsert
            .Select(q => q.Name)
            .ToArray();
            
        ColumnsNames = Columns
            .Select(q => q.Name)
            .ToArray();

        InsertQuery = $"Insert INTO {Engine!.Name} ({ColumnsNamesForInsert.CommaEnumeration()}) VALUES @bulk";
        DropQuery = $"DROP TABLE IF EXISTS {Engine!.Name}";
        CreateQuery = GenerateCreateQuery();
    }

    private string GenerateCreateQuery()
    {
        var sb = new StringBuilder(256);
        sb.AppendLine($"CREATE TABLE IF NOT EXISTS {Engine!.Name} ");

        sb.AppendLine("( ");

        foreach (var column in Columns)
        {
            sb.Append($"{column.Name} {column.Type},");
        }

        // Remove ,
        sb.Remove(sb.Length - 1, 1);
            
        //  
        sb.AppendLine(" ) ");

        sb.AppendLine($"ENGINE = {Engine.Engine} ");

        if (!string.IsNullOrWhiteSpace(Engine.Partition))
            sb.AppendLine($"PARTITION BY ({Engine.Partition}) ");

        if (!string.IsNullOrWhiteSpace(Engine.OrderBy))
            sb.AppendLine($"ORDER BY ({Engine.OrderBy}) ");

        if (!string.IsNullOrWhiteSpace(Engine.Settings))
            sb.AppendLine($"SETTINGS {Engine.Settings} ");

        return sb.ToString();
    }
}