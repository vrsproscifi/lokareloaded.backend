﻿using Loka.Common.Orleans;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Admin.Api.Extensions;

public static class OrleansServiceExtensions
{
    public static IServiceCollection AddAdminOrleansServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddOrleansClusterClient(configuration, new OrleansClientApplicationParts()
        {
                
        });
            
        return services;
    }
}