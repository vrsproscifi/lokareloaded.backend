﻿using Loka.Common.MassTransit.MessageBrokers;

namespace Loka.Identity.Interfaces.Events;

public sealed class AfterSignUpEvent : IClusterEvent
{
    public Guid PlayerId { get; init; }
    public string PlayerName { get; init; }= null!;
}