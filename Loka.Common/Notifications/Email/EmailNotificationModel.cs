﻿namespace Loka.Common.Notifications.Email;

public sealed class EmailNotificationModel
{
    public string Email { get; init; } = null!;

    public string Message { get; init; } = null!;
}