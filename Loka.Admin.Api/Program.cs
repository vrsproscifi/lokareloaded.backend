using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;

namespace Loka.Admin.Api;

public class Program
{
    public static void Main(string[] args)
    {
        AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);

        CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.ConfigureAppConfiguration(builder =>
                {
                    builder.AddEnvironmentVariables();
                    builder.AddJsonFile("appsettings.DataBase.json", optional: false);
                });
                webBuilder.CaptureStartupErrors(true);
                webBuilder.UseStartup<Startup>();
                    
                /* webBuilder.UseUrls(new[]
                 {
                     //"http://localhost:13852",
                     "http://localhost:5001",
                 });*/

                webBuilder.UseIISIntegration();
                webBuilder.UseSockets();
                webBuilder.UseKestrel(options =>
                {
                    options.ListenLocalhost(13852, o => { o.Protocols = HttpProtocols.Http1AndHttp2; });

                    options.ListenLocalhost(5555, o => { o.Protocols = HttpProtocols.Http2; });
                });
            });
}