﻿using Loka.Store.Interfaces.Models.Items;

namespace Loka.Inventory.Interfaces.Inventory;

public static class PlayerInventoryItemModel
{
    [GenerateSerializer]
    public sealed class RPC : IEntry
    {
        [Id(0)]
        public Guid Id { get; init; }

        [Id(1)]
        public Guid ModelId { get; init; }

        [Id(2)]
        public int Level { get; init; }

        [Id(3)]
        public long Amount { get; init; }

        [Id(4)]
        public IReadOnlyList<Module> Modules { get; init; } = ArraySegment<Module>.Empty;

        [Id(5)]
        public DateTimeOffset? LastUseDate { get; init; }
        
        [GenerateSerializer]
        public sealed class Module
        {
            [Id(0)]
            public Guid Id { get; init; }

            [Id(1)]
            public Guid ModelId { get; init; }

            [Id(2)]
            public int Level { get; init; }
        }
    }

    [GenerateSerializer]
    public sealed class Repository : IEntry
    {
        [Id(0)]
        public Guid Id { get; init; }

        [Id(1)]
        public StoreItemModel Model { get; set; } = null!;

        [Id(2)]
        public int Level { get; init; }

        [Id(3)]
        public long Amount { get; init; }

        [Id(4)]
        public IReadOnlyList<Module> Modules { get; init; } = ArraySegment<Module>.Empty;

        [Id(5)]
        public DateTimeOffset? LastUseDate { get; init; }


        [GenerateSerializer]
        public sealed class Module
        {
            [Id(0)]
            public Guid Id { get; init; }

            [Id(1)]
            public StoreItemModel Model { get; init; } = null!;

            [Id(2)]
            public int Level { get; init; }
        }
    }
}