﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Entities;
using Loka.MatchMaking.DataBase.Enums.Match;

namespace Loka.MatchMaking.DataBase.Entities.Match;

public sealed class MatchMemberEntity : BaseEntity
{
    public int Squad { get; set; }

    public Guid PlayerId { get; set; }

    public DateTimeOffset? StartedAt { get; set; }
    public DateTimeOffset? DesertedAt { get; set; }
        
    [ForeignKey(nameof(TeamEntity))]
    public Guid TeamId { get; set; }
    public MatchTeamEntity? TeamEntity { get; set; }
        
    public MatchMemberState State { get; set; }

}