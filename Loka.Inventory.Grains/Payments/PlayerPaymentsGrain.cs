﻿using Loka.Common.Authentication;
using Loka.Common.Orleans.Grains;
using Loka.Inventory.DataBase;
using Loka.Inventory.DataBase.Entities.Payments;
using Loka.Inventory.Interfaces.Payments;
using Loka.Inventory.Interfaces.Resources;
using MongoDB.Bson;

namespace Loka.Inventory.Grains.Payments;

internal sealed class PlayerPaymentsGrain : Grain, IPlayerPaymentsGrain, IWithPlayerActor
{
    public PlayerActor Actor { get; set; } = null!;
    private IMapper Mapper { get; }
    private InventoryPaymentsDbContext InventoryPaymentsDbContext { get; }

    public PlayerPaymentsGrain(InventoryPaymentsDbContext inventoryPaymentsDbContext, IMapper mapper)
    {
        InventoryPaymentsDbContext = inventoryPaymentsDbContext;
        Mapper = mapper;
    }

    public async Task<OperationResult<PlayerPaymentTransactionModel>> PayFor(PayForItem.Request request)
    {
        var inventory = GrainFactory.GetGrain<IPlayerInventoryResourcesGrain>(Actor.PlayerId);
        var playerResource = await inventory.GetResourceByType(request.ResourceId);

        var giveResourceResult = await inventory.GiveInventoryResource(new GiveInventoryResource.Request()
        {
            EntityId = playerResource.Id,
            Amount = request.Price
        });

        if (giveResourceResult.GetValueIfSucceeded(out var newResource))
        {
            var transaction = new PlayerPaymentTransactionEntity()
            {
                Id = ObjectId.GenerateNewId(),
                ModelId = request.ModelId,
                TypeId = request.TypeId,
                Count = 1,
                Resources = new[]
                {
                    new PlayerPaymentTransactionResource()
                    {
                        ModelId = request.ResourceId,
                        Amount = request.Price
                    }
                }
            };

            await InventoryPaymentsDbContext.PlayerPaymentTransactionEntity.InsertOneAsync(transaction);
            return Mapper.Map<PlayerPaymentTransactionModel>(transaction);
        }

        return giveResourceResult.Error!;
    }

    public async Task<OperationResult<PlayerPaymentTransactionModel>> PayFor(MultiplePayForItem.Request request)
    {
        var inventory = GrainFactory.GetGrain<IPlayerInventoryResourcesGrain>(Actor.PlayerId);
        var playerResources = await Task.WhenAll(request.Resources
            .Select(resource => inventory.GetResourceByType(resource.ModelId))
            .ToArray());

        var expendResult = await inventory.ExpendInventoryResources(new ExpendInventoryResources.Request()
        {
            Resources = request.Resources.Select(resource => new ExpendInventoryResources.Request.Entry()
            {
                EntityId = playerResources.First(q => q.Resource.Id == resource.ModelId).Id,
                Amount = resource.Amount * request.Count
            }).ToArray()
        });

        if (expendResult.GetValueIfSucceeded(out var resources))
        {
            var transaction = new PlayerPaymentTransactionEntity()
            {
                Id = ObjectId.GenerateNewId(),
                ModelId = request.ModelId,
                TypeId = PlayerPaymentTransactionTypeId.Craft,
                Count = request.Count,
                Resources = request.Resources.Select(resource => new PlayerPaymentTransactionResource()
                {
                    ModelId = resource.ModelId,
                    Amount = resource.Amount * request.Count
                }).ToArray()
            };

            await InventoryPaymentsDbContext.PlayerPaymentTransactionEntity.InsertOneAsync(transaction);
            return Mapper.Map<PlayerPaymentTransactionModel>(transaction);
        }

        return expendResult.Error!;
    }

    public async Task<OperationResult<RollBackPayment.Result>> RollBackPayment(RollBackPayment.Request request)
    {
        throw new NotImplementedException();
    }
}