﻿using AutoMapper;
using Loka.Common.Cache.DistributedCache;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Repositories;
using Loka.Store.DataBase;
using Loka.Store.DataBase.Entities.Items;
using Loka.Store.Interfaces.Models.Items;
using Loka.Store.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Loka.Store.Grains.Items;

internal sealed class GameStoreItemBlueprintsRepository : BaseRepository<StoreDbContext, GameItemBlueprintEntity, StoreItemBlueprintModel>, IGameStoreItemBlueprintsRepository
{
    private IGameResourcesRepository ResourcesRepository { get; }

    public GameStoreItemBlueprintsRepository
    (
        ICacheProvider cacheProvider,
        StoreDbContext dbContext,
        IMapper mapper,
        IGameResourcesRepository resourcesRepository
    ) : base(cacheProvider, dbContext, mapper)
    {
        ResourcesRepository = resourcesRepository;
    }

    protected override async Task<OperationResult> AfterMap(GameItemBlueprintEntity entity, StoreItemBlueprintModel model)
    {
        var entities = await DbContext.GameItemBlueprintDependEntity
            .Where(q => q.BlueprintId == entity.EntityId)
            .ToArrayAsync();

        var models = new List<StoreItemBlueprintDependsModel>(entities.Length);
        foreach (var dependEntity in entities)
        {
            var getResourceResult = await ResourcesRepository.GetById(dependEntity.ResourceId);
            if (getResourceResult.GetValueIfSucceeded(out var resource))
            {
                models.Add(new StoreItemBlueprintDependsModel()
                {
                    Id = dependEntity.EntityId,
                    Resource = resource,
                    Amount = dependEntity.Amount
                });
            }
            else
            {
                return getResourceResult.Error!;
            }
        }

        model.Depends = models;
        return await base.AfterMap(entity, model);
    }
}