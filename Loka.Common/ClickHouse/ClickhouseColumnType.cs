﻿namespace Loka.Common.ClickHouse;

public enum ClickhouseColumnType
{
    UInt8,
    UInt16,
    UInt32,
    UInt64,
    Int8,
    Int16,
    Int32,
    Int64,
    Float32,
    Float64,
    Single,
    Double,
    Date,
    DateTime,
    DateTime64,
    String,
    Null,
    UUID,
}