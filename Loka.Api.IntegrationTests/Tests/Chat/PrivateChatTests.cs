﻿using Loka.Api.Features;

namespace Loka.Api.IntegrationTests.Tests.Chat;

internal sealed class PrivateChatTests : BaseTest
{
    [Test]
    public async Task GetChatMessages()
    {
        var playerSession = await Services.PlayerIdentity.QuickSignUp();

        var channel = await Services.GameChat.CreatePrivateChannel(playerSession);
        var sentMessage = await Services.GameChat.SendChatMessage(playerSession, channel);
        var messages = await Services.GameChat.GetChatMessages
        (
            playerSession,
            channel,
            sentMessage.CreatedAt.ToDateTime().AddMinutes(-1),
            sentMessage.CreatedAt.ToDateTime().AddMinutes(+1)
        );

        var messageInMessages = messages.Messages.FirstOrDefault(q => q.Id.Equals(sentMessage.Id));
        Assert.That(messageInMessages, Is.Not.Null, $"Sent messages {sentMessage.Id} not found in messages");
        messageInMessages!.AssertEqualModel(sentMessage);
    }

    [Test]
    public async Task GetChatMessagesLaterThenMessage()
    {
        var playerSession = await Services.PlayerIdentity.QuickSignUp();

        var channel = await Services.GameChat.CreatePrivateChannel(playerSession);
        var sentMessage = await Services.GameChat.SendChatMessage(playerSession, channel);
        var messages = await Services.GameChat.GetChatMessages
        (
            playerSession,
            channel,
            sentMessage.CreatedAt.ToDateTime().AddMinutes(+1),
            sentMessage.CreatedAt.ToDateTime().AddMinutes(+2)
        );
        
        Assert.Multiple(() =>
        {
            Assert.That(messages.Messages, Is.Empty, $"Messages should not be received because they are later than the requested interval.");
            Assert.That(messages.HasMessagesBefore, Is.True, $"Messages should not be received because they are later than the requested interval.");
        });
    }

    [Test]
    public async Task GetChatMessagesEarlyThenMessage()
    {
        var playerSession = await Services.PlayerIdentity.QuickSignUp();

        var channel = await Services.GameChat.CreatePrivateChannel(playerSession);
        var sentMessage = await Services.GameChat.SendChatMessage(playerSession, channel);
        var messages = await Services.GameChat.GetChatMessages
        (
            playerSession,
            channel,
            sentMessage.CreatedAt.ToDateTime().AddMinutes(-3),
            sentMessage.CreatedAt.ToDateTime().AddMinutes(-2)
        );
        
        Assert.Multiple(() =>
        {
            Assert.That(messages.Messages, Is.Empty, $"Messages should not be received because they are after than the requested interval.");
            Assert.That(messages.HasMessagesBefore, Is.False, $"Messages should not be received because they are later than the requested interval.");
        });
    }

    [Test]
    public async Task InviteToChatAndSendMessage()
    {
        var playerSession = await Services.PlayerIdentity.QuickSignUp();
        var playerSession2 = await Services.PlayerIdentity.QuickSignUp();

        var channel = await Services.GameChat.CreatePrivateChannel(playerSession);
        await Services.GameChat.AssignRole(playerSession, channel, playerSession2.PlayerId, ChatMemberPermissions.SendMessages);

        var members = await Services.GameChat.GetChatMembers(playerSession, channel);
        Assert.That(members, Has.Count.EqualTo(2), $"Invalid number of {nameof(members)} | Expected owner & invited");

        var ownerMember = members.FirstOrDefault(q => q.PlayerId.Equals(playerSession.PlayerId.ToUUID()));
        var invitedMember = members.FirstOrDefault(q => q.PlayerId.Equals(playerSession2.PlayerId.ToUUID()));
        Assert.Multiple(() =>
        {
            Assert.That(ownerMember, Is.Not.Null, $"Owner member not found in channel members");
            Assert.That(invitedMember, Is.Not.Null, $"Owner member not found in channel members");
        });
        var sentMessage = await Services.GameChat.SendChatMessage(playerSession2, channel);
        var messages = await Services.GameChat.GetChatMessages
        (
            playerSession2,
            channel,
            sentMessage.CreatedAt.ToDateTime().AddMinutes(-1),
            sentMessage.CreatedAt.ToDateTime().AddMinutes(+1)
        );

        var messageInMessages = messages.Messages.FirstOrDefault(q => q.Id.Equals(sentMessage.Id));
        Assert.That(messageInMessages, Is.Not.Null, $"Sent messages {sentMessage.Id} not found in messages");
        messageInMessages!.AssertEqualModel(sentMessage);
    }
}