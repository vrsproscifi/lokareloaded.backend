﻿using MediatR;
using Loka.Common.Operations.Results;

namespace Loka.Common.MediatR.Requests;

public interface IOperationRequest : IRequest<OperationResult>
{

}

public interface IOperationRequest<TResult> : IRequest<OperationResult<TResult>>
{

}