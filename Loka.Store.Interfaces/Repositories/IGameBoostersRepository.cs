﻿using Loka.Database.Common.Repositories;
using Loka.Store.Interfaces.Models.Boosters;

namespace Loka.Store.Interfaces.Repositories;

public interface IGameBoostersRepository : IBaseRepository<GameBoosterModel>
{

}