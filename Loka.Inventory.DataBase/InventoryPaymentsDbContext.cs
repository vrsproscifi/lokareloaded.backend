﻿using Loka.Common.Mongo;
using Loka.Inventory.DataBase.Entities.Payments;
using MongoDB.Driver;

namespace Loka.Inventory.DataBase;

public sealed class InventoryPaymentsDbContext : MongoDbContext
{
    public IMongoCollection<PlayerPaymentTransactionEntity> PlayerPaymentTransactionEntity { get; init; } = null!;
}