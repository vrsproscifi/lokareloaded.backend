﻿using MongoDB.Bson;

namespace Loka.Identity.Interfaces.Models;

[GenerateSerializer]
public sealed class PlayerAuthenticationModel
{
    [Id(0)]
    public Guid PlayerId { get; init; }

    [Id(1)]
    public ObjectId SessionId { get; init; }

    [Id(2)]
    public string Login { get; init; } = null!;

    [Id(3)]
    public RefreshTokenModel Token { get; init; } = null!;
}