﻿using Loka.Api.Features;
using Loka.Api.Features.Inventory.Items;

namespace Loka.Api.IntegrationTests.Asserts;

public static class CraftWorkbenchItemAsserts
{
    public static void AssertEqual(this PlayerInventoryItem inventoryItem, CraftWorkbenchItem.Types.Response craftedItem)
    {
        Assert.That(craftedItem, Is.Not.Null, $"Invalid {nameof(craftedItem)}");
        Assert.That(craftedItem.Item, Is.Not.Null, $"Invalid {nameof(craftedItem)}");
        craftedItem.Item.AssertEqual(inventoryItem);
    }

    public static void AssertEqual(this PlayerInventoryItem craftedItem, PlayerInventoryItem inventoryItem)
    {
        Assert.That(craftedItem, Is.Not.Null, $"Invalid {nameof(craftedItem)}");
        Assert.That(inventoryItem, Is.Not.Null, $"Invalid {nameof(inventoryItem)}");
        Assert.Multiple(() =>
        {
            Assert.That(craftedItem!.Id, Is.EqualTo(inventoryItem.Id), $"Invalid {nameof(craftedItem)}.{nameof(craftedItem.Id)}");
            Assert.That(craftedItem.ModelId, Is.EqualTo(inventoryItem.ModelId), $"Invalid {nameof(craftedItem)}.{nameof(craftedItem.ModelId)}");
            Assert.That(craftedItem.Level, Is.EqualTo(inventoryItem.Level), $"Invalid {nameof(craftedItem)}.{nameof(craftedItem.Level)}");
        });
    }

    public static void AssertModel(this PlayerInventoryItem item)
    {
        Assert.That(item, Is.Not.Null, $"Invalid {nameof(item)}");
        Assert.Multiple(() =>
        {
            Assert.That(item!.Id, Is.Not.Null, $"Invalid {nameof(item)}.{nameof(item.Id)}");
            Assert.That(item.ModelId, Is.Not.Null, $"Invalid {nameof(item)}.{nameof(item.ModelId)}");
            Assert.That(item.Amount, Is.GreaterThanOrEqualTo(0), $"Invalid {nameof(item)}.{nameof(item.Amount)}");
            Assert.That(item.Level, Is.GreaterThanOrEqualTo(0), $"Invalid {nameof(item)}.{nameof(item.Level)}");

            item!.Id.AssertModel();
            item!.ModelId.AssertModel();
        });
    }
}