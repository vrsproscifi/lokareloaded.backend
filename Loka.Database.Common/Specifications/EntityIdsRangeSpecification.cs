﻿using System;
using System.Collections.Generic;
using System.Linq;
using Loka.Common;
using Loka.Common.Extensions;
using Loka.Database.Common.Entities;

namespace Loka.Database.Common.Specifications;

public sealed class EntityIdsRangeSpecification<TEntity> : Specification.IQueryableSpecification<TEntity>
    where TEntity : BaseEntity
{
    private bool FetchAllIfEmpty { get; }
    private IReadOnlyList<Guid> EntityIds { get; }

    public EntityIdsRangeSpecification(IReadOnlyList<Guid> entityIds, bool fetchAllIfEmpty = false)
    {
        EntityIds = entityIds;
        FetchAllIfEmpty = fetchAllIfEmpty;
    }

    public IQueryable<TEntity> Apply(IQueryable<TEntity> query)
    {
        if (FetchAllIfEmpty && EntityIds.IsEmpty())
            return query;

        return query.Where(q => EntityIds.Contains(q.EntityId));
    }
}