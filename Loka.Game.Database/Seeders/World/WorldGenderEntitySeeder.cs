﻿using Loka.Game.Database.Entities.World;
using Loka.Game.Database.Enums.World;

namespace Loka.Game.Database.Seeders.World;

public static class WorldGenderEntitySeeder
{
    public static Guid Male { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe101");
    public static Guid Female { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-200c8dffe102");

    public static WorldGenderEntity[] Entities { get; } = new[]
    {
        new WorldGenderEntity(Male, WorldGenderTypeId.Male),
        new WorldGenderEntity(Female, WorldGenderTypeId.Female),
    };
}