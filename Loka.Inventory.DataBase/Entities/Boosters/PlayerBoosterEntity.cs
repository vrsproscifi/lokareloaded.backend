﻿using Loka.Database.Common.Entities;

namespace Loka.Inventory.DataBase.Entities.Boosters;

public sealed class PlayerBoosterEntity : BaseEntity
{
    public Guid PlayerId { get; init; }
    public Guid BoosterId { get; init; }
    public long Amount { get; set; }
    public bool IsActive { get; set; }
}