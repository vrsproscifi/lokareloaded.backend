﻿using Loka.Database.Common.Repositories;
using Loka.Store.Interfaces.Models;
using Loka.Store.Interfaces.Models.Items;

namespace Loka.Store.Interfaces.Repositories;

public interface IGameStoreItemBlueprintsRepository : IBaseRepository<StoreItemBlueprintModel>
{

}