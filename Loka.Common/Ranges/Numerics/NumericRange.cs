﻿using System;

namespace Loka.Common.Ranges.Numerics;

public readonly struct NumericRangeOptions
{
    public NumericRangeBeginOptions Begin { get; }
    public NumericRangeEndOptions End { get; }
        
    public NumericRangeOptions(NumericRangeBeginOptions begin, NumericRangeEndOptions end)
    {
        Begin = begin;
        End = end;
    }
}
    
public readonly struct NumericRange<TBeginValue, TEndValue>
{
    public NumericRange(TBeginValue begin, TEndValue end)
    {
        Begin = begin;
        End = end;
    }

    public TBeginValue Begin { get; }
    public TEndValue End { get; }
}