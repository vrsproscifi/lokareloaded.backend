﻿using Google.Protobuf.Collections;
using Google.Protobuf.WellKnownTypes;
using Loka.Api.Features;
using Loka.Api.Features.Shared;
using Loka.Api.IntegrationTests.Clients;
using Loka.Api.IntegrationTests.Services.PlayerIdentity;

namespace Loka.Api.IntegrationTests.Services.GameChat;

public sealed class GameChatService
{
    private Loka.Api.Features.GameChat.GameChatClient GameChatClient { get; }

    public GameChatService(GameAPIClients client)
    {
        GameChatClient = client.GameChatClient;
    }

    public async Task<ChannelEntry> CreatePrivateChannel(PlayerSession playerSession)
    {
        var channelName = $"private channel for {playerSession.PlayerId}";
        var response = await GameChatClient.CreatePrivateChannelAsync(new CreatePrivateChannel.Types.Request()
        {
            Name = channelName,
        }, playerSession.UseJWT());

        response.AssertResponse();
        Assert.That(response.Channel.Name, Is.EqualTo(channelName), $"Failed {nameof(CreatePrivateChannel)} | Invalid {nameof(response.Channel.Name)}");
        return response.Channel;
    }

    #region AssignRole

    public Task<AssignRoleForChannelMember.Types.Response> AssignRole(PlayerSession playerSession, ChannelEntry channel, Guid playerId, ChatMemberPermissions permissions)
    {
        return AssignRole(playerSession, channel.Id, playerId.ToUUID(), permissions);
    }

    public Task<AssignRoleForChannelMember.Types.Response> AssignRole(PlayerSession playerSession, ChannelEntry channel, UUID playerId, ChatMemberPermissions permissions)
    {
        return AssignRole(playerSession, channel.Id, playerId, permissions);
    }

    public async Task<AssignRoleForChannelMember.Types.Response> AssignRole(PlayerSession playerSession, ObjID channelId, UUID playerId, ChatMemberPermissions permissions)
    {
        var response = await GameChatClient.AssignRoleAsync(new AssignRoleForChannelMember.Types.Request()
        {
            ChannelId = channelId,
            PlayerId = playerId,
            Permissions = permissions
        }, playerSession.UseJWT());

        response.AssertResponse();
        return response;
    }

    #endregion


    #region GetChatMembers

    public Task<RepeatedField<ChatMemberEntry>> GetChatMembers(PlayerSession playerSession, ChannelEntry channel)
    {
        return GetChatMembers(playerSession, channel.Id);
    }

    public async Task<RepeatedField<ChatMemberEntry>> GetChatMembers(PlayerSession playerSession, ObjID channelId)
    {
        var response = await GameChatClient.GetChatMembersAsync(new GetChatMembers.Types.Request()
        {
            ChannelId = channelId
        }, playerSession.UseJWT());

        response.AssertResponse();
        return response.Entries;
    }

    #endregion

    #region GetChatMember

    public Task<GetChatMember.Types.Response> GetChatMember(PlayerSession playerSession, ChannelEntry channel, Guid playerId)
    {
        return GetChatMember(playerSession, channel.Id, playerId.ToUUID());
    }

    public Task<GetChatMember.Types.Response> GetChatMember(PlayerSession playerSession, ChannelEntry channel, UUID playerId)
    {
        return GetChatMember(playerSession, channel.Id, playerId);
    }

    public async Task<GetChatMember.Types.Response> GetChatMember(PlayerSession playerSession, ObjID channelId, UUID playerId)
    {
        var response = await GameChatClient.GetChatMemberAsync(new GetChatMember.Types.Request()
        {
            ChannelId = channelId,
            PlayerId = playerId
        }, playerSession.UseJWT());

        response.AssertResponse();
        return response;
    }

    #endregion


    #region GetChatMessages

    public Task<GetChatMessages.Types.Response> GetChatMessages(PlayerSession playerSession, ChannelEntry channel, DateTime laterThen, DateTime earlyThen)
    {
        return GetChatMessages(playerSession, channel, request =>
        {
            request.LaterThen = Timestamp.FromDateTime(laterThen);
            request.EarlyThen = Timestamp.FromDateTime(earlyThen);
        });
    }

    public Task<GetChatMessages.Types.Response> GetChatMessages(PlayerSession playerSession, ChannelEntry channel, Timestamp laterThen, Timestamp earlyThen)
    {
        return GetChatMessages(playerSession, channel, request =>
        {
            request.LaterThen = laterThen;
            request.EarlyThen = earlyThen;
        });
    }

    public async Task<GetChatMessages.Types.Response> GetChatMessages(PlayerSession playerSession, ChannelEntry channel, Action<GetChatMessages.Types.Request> customize)
    {
        var request = new GetChatMessages.Types.Request()
        {
            ChannelId = channel.Id,
        };
        customize.Invoke(request);

        var response = await GameChatClient.GetChatMessagesAsync(request, playerSession.UseJWT());

        response.AssertResponse();
        return response;
    }

    #endregion

    #region SendChatMessage

    public Task<ChatMessageEntry> SendChatMessage(PlayerSession playerSession, ChannelEntry channel)
    {
        return SendChatMessage(playerSession, channel.Id);
    }

    public async Task<ChatMessageEntry> SendChatMessage(PlayerSession playerSession, ObjID channelId)
    {
        var message = $"Message_{Guid.NewGuid():N}";
        var response = await GameChatClient.SendChatMessageAsync(new SendChatMessage.Types.Request()
        {
            ChannelId = channelId,
            Message = message
        }, playerSession.UseJWT());

        response.AssertResponse();
        Assert.That(response.Message.Message, Is.EqualTo(message), $"Failed {nameof(CreatePrivateChannel)} | Invalid {nameof(response.Message.Message)}");

        return response.Message;
    }

    #endregion
}