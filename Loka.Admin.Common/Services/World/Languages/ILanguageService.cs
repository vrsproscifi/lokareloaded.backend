﻿using Loka.Admin.Common.Models.Countries;
using Loka.Database.Common.Storage;

namespace Loka.Admin.Common.Services.World.Languages;

public interface ILanguageService : IEntryStorage<LanguageInformationModel>
{

}