﻿using Loka.Common.Entry;
using Loka.Common.Operations.Results;

namespace Loka.Database.Common.Repositories;

public interface IBaseChildRepository<TModel> where TModel : class, IEntry
{
    Task<OperationResult<TModel>> GetById(Guid parentId, Guid id);
    Task<IReadOnlyList<TModel>> GetByIds(Guid parentId, IReadOnlyList<Guid> ids);
    Task<IReadOnlyList<TModel>> GetAll(Guid parentId);
    
    public interface ICached
    {
        Task ResetCache(string key);
        Task ResetCache(Guid parentId, Guid id);
        Task ResetCache(Guid parentId, IReadOnlyList<Guid> ids);
    }
}