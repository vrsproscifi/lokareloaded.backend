﻿namespace Loka.Api.IntegrationTests.Tests.Inventory;

internal sealed class PlayerInventoryCasesTests : BaseTest
{
    [Test]
    public async Task GetPlayerCases()
    {
        var playerSession = await Services.PlayerIdentity.SignUp();
        var cases = await Services.PlayerInventoryCases.GetPlayerCases(playerSession);
        Assert.That(cases, Is.Empty, $"Failed execute {nameof(Services.PlayerInventoryCases.GetPlayerCases)} for {playerSession.PlayerId}");
    }

    [Test]
    public async Task BuyGameStoreCase()
    {
        var playerSession = await Services.PlayerIdentity.QuickSignUp();

        var playerCase1 = await Services.PlayerInventoryCases.BuyGameStoreCase(playerSession, Storages.GameStore.Cases[0]);
        Assert.That(playerCase1.Amount, Is.EqualTo(1));

        var playerCase2 = await Services.PlayerInventoryCases.BuyGameStoreCase(playerSession, Storages.GameStore.Cases[0]);
        Assert.That(playerCase2.Amount, Is.EqualTo(2));

        var cases = await Services.PlayerInventoryCases.GetPlayerCases(playerSession);
        Assert.That(cases, Has.Count.EqualTo(1));

        var playerCase = cases.FirstOrDefault(q => q.Id.Equals(playerCase1.Id));
        Assert.That(playerCase, Is.Not.Null, $"Purchased case {playerCase1.Id} not found");
    }

    [Test]
    public async Task OpenPlayerCase()
    {
        foreach (var @case in Storages.GameStore.Cases)
        {
            Assert.Multiple(async () =>
            {
                await TestContext.Progress.WriteLineAsync($"=========================================");
                await TestContext.Progress.WriteLineAsync($"OpenPlayerCase: {@case.Id}");

                foreach (var item in @case.Items)
                {
                    await TestContext.Progress.WriteLineAsync($"Item: {item.ModelId} | Count: [{item.MinimumCount}, {item.MinimumCount}] | Chance: {item.ChanceOfDrop}");
                }

                foreach (var resource in @case.Resources)
                {
                    await TestContext.Progress.WriteLineAsync($"Resource: {resource.ModelId} | Count: [{resource.MinimumCount}, {resource.MinimumCount}] | Chance: {resource.ChanceOfDrop}");
                }

                await TestContext.Progress.WriteLineAsync($"OpenPlayerCase: {@case.Id}");
                await OpenPlayerCase(@case);
            });
        }
    }

    private async Task OpenPlayerCase(StoreCase.Types.Model @case)
    {
        var playerSession = await Services.PlayerIdentity.SignUp();

        var itemsBefore = await Services.PlayerInventoryItems.GetPlayerPurchasedItems(playerSession);
        var resourcesBefore = await Services.PlayerInventoryResources.GetPlayerResources(playerSession);

        var playerCase = await Services.PlayerInventoryCases.BuyGameStoreCase(playerSession, @case);
        Assert.That(playerCase.Amount, Is.EqualTo(1));

        var response = await Services.PlayerInventoryCases.OpenPlayerCase(playerSession, playerCase);
        Assert.That(response.Case.Amount, Is.EqualTo(0), $"Wrong number of cases after open case");

        var itemsAfter = await Services.PlayerInventoryItems.GetPlayerPurchasedItems(playerSession);
        var resourcesAfter = await Services.PlayerInventoryResources.GetPlayerResources(playerSession);

        foreach (var issuedItem in response.Items)
        {
            var model = Storages.GameStore.GetProduct(issuedItem.ModelId);
            if (model.Category.Type is StoreItemCategory.Types.Type.Grenade or StoreItemCategory.Types.Type.Kits)
            {
                var itemBefore = itemsBefore.FirstOrDefault(q => q.ModelId.Equals(issuedItem.ModelId));
                var itemAfter = itemsAfter.First(q => q.ModelId.Equals(issuedItem.ModelId));

                var delta = itemAfter?.Amount ?? 0 - itemBefore.Amount;
                Assert.That(delta, Is.EqualTo(issuedItem.Amount), $"Wrong number of {issuedItem.ModelId} item after open case");
            }
            else
            {
                var hadItem = itemsBefore.Any(q => q.ModelId.Equals(issuedItem.ModelId));
                if (hadItem)
                {
                    Assert.That(itemsAfter.Count(q => q.ModelId.Equals(issuedItem.ModelId)), Is.EqualTo(2), $"Wrong number of {issuedItem.ModelId} item after open case");
                }
                else
                {
                    Assert.That(itemsAfter.Count(q => q.ModelId.Equals(issuedItem.ModelId)), Is.EqualTo(1), $"Wrong number of {issuedItem.ModelId} item after open case");
                }
            }
        }

        foreach (var issuedResource in response.Resources)
        {
            var resourceBefore = resourcesBefore.First(q => q.ModelId.Equals(issuedResource.ModelId));
            var resourceAfter = resourcesAfter.First(q => q.ModelId.Equals(issuedResource.ModelId));

            var delta = resourceAfter.Amount - resourceBefore.Amount;
            Assert.That(delta, Is.EqualTo(issuedResource.Amount), $"Wrong number of {issuedResource.ModelId} resource after open case");
        }
    }
}