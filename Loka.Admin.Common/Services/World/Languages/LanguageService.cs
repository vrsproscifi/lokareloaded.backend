﻿using Loka.Admin.Common.Models.Countries;
using Loka.Common.Operations.Codes;
using Loka.Database.Common.Storage.Constructors;
using Loka.Database.Common.Storage.Impls;
using Loka.Game.Database;
using Loka.Game.Database.Entities.World;
using Microsoft.EntityFrameworkCore;

namespace Loka.Admin.Common.Services.World.Languages;

internal sealed class LanguageService : CacheableEntryStorage<LanguageInformationModel, WorldLanguageEntity, SystemDbContext>, ILanguageService
{
    protected override OperationResultCode EntityNotFoundErrorCode => OperationResultCode.LanguageNotFound;
    protected override DbSet<WorldLanguageEntity> Table => DbContext.WorldLanguageEntity;

    public LanguageService(CacheableEntryStorageConstructor<SystemDbContext, LanguageInformationModel> constructor) : base(constructor)
    {
    }
}