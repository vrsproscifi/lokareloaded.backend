﻿using Loka.Common.Pagination;
using MongoDB.Bson;

namespace Loka.Chat.Interfaces.Models;

public static class GetChatMessages
{
    [GenerateSerializer]
    public sealed class Request
    {
        [Id(0)]
        public ObjectId ChannelId { get; init; }

        [Id(1)]
        public DateTime LaterThen { get; init; }
        
        [Id(2)]
        public DateTime EarlyThen { get; init; }
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public DateTime LaterThen { get; init; }
        
        [Id(1)]
        public DateTime EarlyThen { get; init; }
        
        [Id(2)]
        public IReadOnlyList<ChatMessageEntry> Messages { get; init; } = ArraySegment<ChatMessageEntry>.Empty;

        [Id(3)]
        public bool HasMessagesBefore { get; init; }

        [Id(4)]
        public IReadOnlyList<ChatEventEntry> Events { get; init; } = ArraySegment<ChatEventEntry>.Empty;

        [Id(5)]
        public bool HasEventsBefore { get; init; }
    }
}