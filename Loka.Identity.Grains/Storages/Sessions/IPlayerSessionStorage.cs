﻿namespace Loka.Identity.Grains.Storages.Sessions;

public interface IPlayerSessionStorage
{
    public Task<PlayerSession> Create(Guid playerId);
    public Task Remove(Guid playerId);
    public Task<PlayerSession> Refresh(Guid playerId);
    public Task<PlayerSession?> Get(Guid playerId);
    public Task<bool> HasActive(Guid playerId);
    public Task<PlayerSession?> GetActive(Guid playerId);
}