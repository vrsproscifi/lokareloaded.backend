﻿using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Interfaces;
using Loka.Deployment.DataBase.Enums;

namespace Loka.Deployment.DataBase.Entities;

[Index(nameof(Channel))]
[Index(nameof(State))]
[Index(nameof(Version), IsUnique = true)]
public sealed class GameVersionEntity : BaseEntity, IAdminCommentedEntity
{
    public int Version { get; set; }

    #region Server

    [ForeignKey(nameof(ServerVersionEntity))]
    public Guid ServerVersionId { get; set; }

    public ServerVersionEntity? ServerVersionEntity { get; set; }

    #endregion


    public DeployChannelTypeId Channel { get; set; }

    [MaxLength(8192)]
    public string? AdminComment { get; set; }

    public DeployState State { get; set; }
}