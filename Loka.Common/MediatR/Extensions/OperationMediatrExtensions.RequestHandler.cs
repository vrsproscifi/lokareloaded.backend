﻿using System;
using System.Collections.Generic;
using System.Text;
using Loka.Common.MediatR.Handlers;
using Loka.Common.MediatR.Requests;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Loka.Common.Operations;
using Loka.Common.Operations.Results;

namespace Loka.Common.MediatR.Extensions;

public static partial class OperationMediatrExtensions
{
    public static IServiceCollection AddRequestHandler<TRequest, THandler>(this IServiceCollection serviceCollection)
        where TRequest : IOperationRequest
        where THandler : class, IOperationHandler<TRequest>
    {
        serviceCollection.AddScoped<IRequestHandler<TRequest, OperationResult>, THandler>();
        return serviceCollection;
    }

    public static IServiceCollection AddRequestHandler<TRequest, TResult, THandler>(this IServiceCollection serviceCollection)
        where TRequest : IOperationRequest<TResult>
        where THandler : class, IOperationHandler<TRequest, TResult>
    {
        serviceCollection.AddScoped<IRequestHandler<TRequest, OperationResult<TResult>>, THandler>();
        return serviceCollection;
    }
        
    public static IServiceCollection AddRequestHandler<TRequest, THandler>(this IServiceCollection serviceCollection, Func<IServiceProvider, THandler> implementationFactory)
        where TRequest : IOperationRequest
        where THandler : class, IOperationHandler<TRequest>
    {
        serviceCollection.AddScoped<IRequestHandler<TRequest, OperationResult>, THandler>(implementationFactory);
        return serviceCollection;
    }

    public static IServiceCollection AddRequestHandler<TRequest, TResult, THandler>(this IServiceCollection serviceCollection, Func<IServiceProvider, THandler> implementationFactory)
        where TRequest : IOperationRequest<TResult>
        where THandler : class, IOperationHandler<TRequest, TResult>
    {
        serviceCollection.AddScoped<IRequestHandler<TRequest, OperationResult<TResult>>, THandler>(implementationFactory);
        return serviceCollection;
    }
}