﻿using System;
using System.Threading.Tasks;
using Loka.Common.Operations.Results;

namespace Loka.Common.Operations;

public static class PredicatedOperationSequence
{
    public static async Task<OperationResult<TResult>> ExecuteSequence<TResult>(this TResult result, bool predicate, Func<Task<OperationResult>> action)
    {
        if (predicate)
        {
            var actionResult = await action.Invoke();
            if (actionResult.IsFailed)
                return actionResult.Error;
        }

        return result;
    }  
        
    public static async Task<OperationResult<TResult>> ExecuteSequence<TResult>(this Task<OperationResult<TResult>> getResultTask, bool predicate, Func<Task<OperationResult>> action)
    {
        var result = await getResultTask;
            
        if (result.IsSucceeded &&  predicate)
        {
            var actionResult = await action.Invoke();
            if (actionResult.IsFailed)
                return actionResult.Error;
        }

        return result;
    }

    public static async Task<OperationResult<TResult>> ExecuteSequence<TResult>(this Task<OperationResult<TResult>> getResultTask, Func<bool> predicate, Func<Task<OperationResult>> action)
    {
        var result = await getResultTask;
        if (result.IsSucceeded)
        {
            if (predicate.Invoke())
            {
                var actionResult = await action.Invoke();
                if (actionResult.IsFailed)
                    return actionResult.Error;
            }
        }

        return result;
    }

    public static async Task<OperationResult<TResult>> ExecuteSequence<TResult>(this Task<OperationResult<TResult>> getResultTask, Func<TResult, bool> predicate, Func<Task<OperationResult>> action)
    {
        var result = await getResultTask;
        if (result.IsSucceeded)
        {
            if (predicate.Invoke(result.Result))
            {
                var actionResult = await action.Invoke();
                if (actionResult.IsFailed)
                    return actionResult.Error;
            }
        }

        return result;
    }

}