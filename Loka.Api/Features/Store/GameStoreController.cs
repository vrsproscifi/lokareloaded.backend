﻿using Loka.Store.Interfaces;

namespace Loka.Api.Features.Store;

public sealed class GameStoreController : GameStore.GameStoreBase
{
    private PlayerActor PlayerActor { get; }
    private ControllerServices<GameStoreController> Services { get; }

    public GameStoreController(ControllerServices<GameStoreController> services, PlayerActor playerActor)
    {
        Services = services;
        PlayerActor = playerActor;
    }
    
    [AllowAnonymous]
    public override async Task<GetStandardSetOfEquipment.Types.Response> GetStandardSetOfEquipment(GetStandardSetOfEquipment.Types.Query request, ServerCallContext context)
    {
        var storeGrain = Services.ClusterClient.GetGameStoreGrain();
        var response = new GetStandardSetOfEquipment.Types.Response
        {
            Products =
            {
                Services.Mapper.Map<IReadOnlyList<StoreItem.Types.Model>>(await storeGrain.GetStandardSetOfEquipment())
            },
        };
        return response;
    }

    [AllowAnonymous]
    public override async Task<GetStoreItems.Types.Response> GetStoreItems(GetStoreItems.Types.Query request, ServerCallContext context)
    {
        var storeGrain = Services.ClusterClient.GetGameStoreGrain();
        var response = new GetStoreItems.Types.Response
        {
            Fractions =
            {
                Services.Mapper.Map<IReadOnlyList<GameFraction.Types.Model>>(await storeGrain.GetGameFractions())
            },
            Resources =
            {
                Services.Mapper.Map<IReadOnlyList<GameResource.Types.Model>>(await storeGrain.GetGameResources())
            },
            Categories =
            {
                Services.Mapper.Map<IReadOnlyList<StoreItemCategory.Types.Model>>(await storeGrain.GetStoreCategories())
            },
            Products =
            {
                Services.Mapper.Map<IReadOnlyList<StoreItem.Types.Model>>(await storeGrain.GetStoreItems())
            },
            Cases =
            {
                Services.Mapper.Map<IReadOnlyList<StoreCase.Types.Model>>(await storeGrain.GetGameStoreCases())
            },
            Boosters =
            {
                Services.Mapper.Map<IReadOnlyList<GameBooster.Types.Model>>(await storeGrain.GetGameStoreBoosters())
            }
        };
        return response;
    }
}