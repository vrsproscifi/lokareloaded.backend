﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Common.Models.GamePlay;
using Loka.Common.Operations.Results;

namespace Loka.Admin.Common.Services.GamePlay.GameVersion;

public interface IGameVersionService
{
    Task<OperationResult<GameVersionInformationModel>> GetById(Guid id, CancellationToken cancellationToken = default);
}