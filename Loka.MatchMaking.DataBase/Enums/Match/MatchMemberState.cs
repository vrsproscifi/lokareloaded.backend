﻿namespace Loka.MatchMaking.DataBase.Enums.Match;

public enum MatchMemberState : byte
{
    /// <summary>
    /// Player has been added to the game the match but has not yet entered the game
    /// </summary>
    Created,

    /// <summary>
    /// Player has been added to the game the match and started to play
    /// </summary>
    Started,

    /// <summary>
    /// Player has been added to the game the match but left the game
    /// </summary>
    Deserted,

    Finished,

    Notified,

    End
}