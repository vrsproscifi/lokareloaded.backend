﻿using Loka.Api.Features;
using Loka.Api.Features.Inventory.Items;
using Loka.Api.Features.Shared;
using Loka.Api.IntegrationTests.Clients;
using Loka.Api.IntegrationTests.Services.PlayerIdentity;

namespace Loka.Api.IntegrationTests.Services.PlayerInventory;

public sealed class PlayerInventoryItemsService
{
    private PlayerWorkbench.PlayerWorkbenchClient PlayerWorkbenchClient { get; }
    private PlayerInventoryItems.PlayerInventoryItemsClient PlayerInventoryItemsClient { get; }

    public PlayerInventoryItemsService(GameAPIClients client)
    {
        PlayerWorkbenchClient = client.PlayerWorkbenchClient;
        PlayerInventoryItemsClient = client.PlayerInventoryItemsClient;
    }

    public async Task<PlayerInventoryItem> BuyItem(PlayerSession playerSession, StoreItem.Types.Model item)
    {
        await TestContext.Progress.WriteLineAsync($"BuyItem: {item.Id} | PlayerId: {playerSession.PlayerId}");
        var response = await PlayerInventoryItemsClient.BuyItemAsync(new BuyGameStoreItem.Types.Request
        {
            ModelId = item.Id
        }, playerSession.UseJWT());

        Assert.That(response, Is.Not.Null, $"Invalid {response}");
        response.Item.AssertModel();
        Assert.That(response.Item.ModelId, Is.EqualTo(item.Id), $"Invalid {response} | wrong {nameof(response.Item.ModelId)}");

        return response.Item!;
    }

    public async Task<IReadOnlyList<PlayerInventoryItem>> GetPlayerPurchasedItems(PlayerSession playerSession)
    {
        var response = await PlayerInventoryItemsClient.GetPlayerPurchasedItemsAsync(new GetPlayerPurchasedItems.Types.Query(), playerSession.UseJWT());
        Assert.That(response, Is.Not.Null, $"Invalid {response}");
        Assert.That(response.Entries, Is.Not.Null, $"Invalid {response}");
        Assert.That(response.Entries, Is.Not.Empty, $"Invalid {nameof(response)}");

        foreach (var item in response.Entries)
        {
            Assert.That(item, Is.Not.Null, $"Invalid {item} in {nameof(response)}");
            Assert.That(item.Id, Is.Not.Null, $"Invalid {nameof(item.Id)} in {nameof(item)}");
            Assert.That(item.ModelId, Is.Not.Null, $"Invalid {nameof(item.ModelId)} in {nameof(item)}");
        }

        return response!.Entries;
    }

    public async Task<PlayerInventoryItem> CraftWorkbenchItem(PlayerSession playerSession, UUID productId, int count = 1)
    {
        var response = await PlayerWorkbenchClient.CraftWorkbenchItemAsync(new CraftWorkbenchItem.Types.Query()
        {
            ModelId = productId,
            Count = count
        }, playerSession.UseJWT());

        Assert.That(response, Is.Not.Null, $"Invalid {response}");
        Assert.That(response.Item.ModelId, Is.EqualTo(productId), $"Invalid {nameof(response)}.{nameof(response.Item.ModelId)}");
        await TestContext.Progress.WriteLineAsync($"CraftWorkbenchItem: {productId} / {count} | PlayerId: {playerSession.PlayerId}");

        return response!.Item;
    }
}