﻿using Loka.Common.Authentication;
using Loka.Common.MediatR.Handlers;
using Loka.Common.Random;
using Loka.Inventory.DataBase;
using Loka.Inventory.Interfaces.Cases;
using Loka.Inventory.Interfaces.Inventory;
using Loka.Inventory.Interfaces.Inventory.Requests;
using Loka.Inventory.Interfaces.Resources;
using Loka.Store.Interfaces.Models.Cases;
using Loka.Store.Interfaces.Repositories;

namespace Loka.Inventory.Grains.Cases.Handlers;

internal sealed class OpenCaseRequestHandler : IOperationHandler<OpenPlayerCase.Request, OpenPlayerCase.Result>
{
    private IClusterClient ClusterClient { get; }
    private IMapper Mapper { get; }
    public PlayerActor Actor { get; set; } = null!;
    private InventoryDbContext InventoryDbContext { get; }
    private IGameCasesRepository GameCasesRepository { get; }
    private IRandomGenerator RandomGenerator { get; }

    public OpenCaseRequestHandler(IMapper mapper, PlayerActor actor, InventoryDbContext inventoryDbContext, IGameCasesRepository gameCasesRepository, IClusterClient clusterClient, IRandomGenerator randomGenerator)
    {
        Mapper = mapper;
        Actor = actor;
        InventoryDbContext = inventoryDbContext;
        GameCasesRepository = gameCasesRepository;
        ClusterClient = clusterClient;
        RandomGenerator = randomGenerator;
    }

    public async Task<OperationResult<OpenPlayerCase.Result>> Handle(OpenPlayerCase.Request request, CancellationToken cancellationToken)
    {
        var playerCaseEntity = await InventoryDbContext.PlayerCaseEntity
            .FirstOrDefaultAsync(q => q.PlayerId == Actor.PlayerId && q.EntityId == request.Id, cancellationToken: cancellationToken);
        if (playerCaseEntity == null)
            return OperationResultCode.EntityNotFound;

        if (playerCaseEntity.Amount > 0)
        {
            var getGameCaseResult = await GameCasesRepository.GetById(playerCaseEntity.CaseId);
            if (getGameCaseResult.GetValueIfSucceeded(out var @case))
            {
                var itemsInventory = ClusterClient.GetGrain<IPlayerInventoryItemsGrain>(Actor.PlayerId);
                var resourcesInventory = ClusterClient.GetGrain<IPlayerInventoryResourcesGrain>(Actor.PlayerId);
                var items = GetItemsForIssue(@case.Items).ToArray();
                var resources = GetItemsForIssue(@case.Resources).ToArray();

                foreach (var item in items)
                {
                    await itemsInventory.AddOrStackInventoryItem(new AddOrStackInventoryItem.Request()
                    {
                        ModelId = item.Entry.Model.Id,
                        Amount = item.Amount
                    });
                }

                foreach (var resource in resources)
                {
                    var playerResource = await resourcesInventory.GetResourceByType(resource.Entry.Model.Id);
                    await resourcesInventory.GiveInventoryResource(new GiveInventoryResource.Request()
                    {
                        EntityId = playerResource.Id,
                        Amount = resource.Amount
                    });
                }

                playerCaseEntity.Amount--;
                await InventoryDbContext.SaveChangesAsync(cancellationToken);
                return new OpenPlayerCase.Result()
                {
                    Case = new PlayerInventoryCaseModel()
                    {
                        Id = playerCaseEntity.EntityId,
                        ModelId = playerCaseEntity.CaseId,
                        Amount = playerCaseEntity.Amount,
                    },
                    Resources = resources.Select(resource => new OpenPlayerCase.ResourceEntry
                    {
                        ModelId = resource.Entry.Model.Id,
                        Amount = resource.Amount
                    }).ToArray(),
                    Items = items.Select(item => new OpenPlayerCase.ItemsEntry()
                    {
                        ModelId = item.Entry.Model.Id,
                        Amount = item.Amount
                    }).ToArray(),
                };
            }

            return getGameCaseResult.Error!;
        }

        return OperationResultCode.CaseNotEnough;
    }

    private IEnumerable<ItemForIssue<TEntry>> GetItemsForIssue<TEntry>(IEnumerable<TEntry> entries) where TEntry : class, ICaseDropEntry
    {
        foreach (var item in entries)
        {
            var issue = RandomGenerator.CheckChance(item.ChanceOfDrop);
            if (issue)
            {
                var count = RandomGenerator.InRange(item.MinimumCount, item.MaximumCount);
                if (count > 0)
                {
                    yield return new ItemForIssue<TEntry>()
                    {
                        Entry = item,
                        Amount = count
                    };
                }
            }
        }
    }

    private sealed class ItemForIssue<TEntry> where TEntry : class, ICaseDropEntry
    {
        public TEntry Entry { get; init; } = null!;
        public int Amount { get; init; }
    }
}