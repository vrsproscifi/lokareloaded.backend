﻿using Loka.Inventory.Interfaces.Boosters;
using Loka.Inventory.Interfaces.Store.Requests;

namespace Loka.Api.Features.Inventory.Boosters;

public sealed class PlayerInventoryBoostersMappingProfiles : Profile
{
    public PlayerInventoryBoostersMappingProfiles()
    {
        //==========================================================
        CreateMap<BuyStoreBooster.Result, BuyGameStoreBooster.Types.Response>()
            .ForMember(q => q.Booster, q => q.MapFrom(w => w.Booster));

        //==========================================================
        CreateMap<PlayerInventoryBoosterModel, ActivatePlayerInventoryBooster.Types.Response>()
            .ForMember(q => q.Booster, q => q.MapFrom(w => w));

        //==========================================================
        CreateMap<PlayerInventoryBoosterModel, DeactivatePlayerInventoryBooster.Types.Response>()
            .ForMember(q => q.Booster, q => q.MapFrom(w => w));

        //==========================================================
        CreateMap<PlayerInventoryBoosterModel, PlayerInventoryBooster>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.ModelId))
            .ForMember(q => q.Amount, q => q.MapFrom(w => w.Amount))
            .ForMember(q => q.IsActivated, q => q.MapFrom(w => w.IsActive));

        CreateMap<GetPlayerBoosters.Result, GetPlayerInventoryBoosters.Types.Response>()
            .ForMember(q => q.Entries, q => q.MapFrom(w => w.Entries));
    }
}