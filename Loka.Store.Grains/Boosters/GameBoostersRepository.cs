﻿using AutoMapper;
using Loka.Common.Cache.DistributedCache;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Repositories;
using Loka.Store.DataBase;
using Loka.Store.DataBase.Entities.Booster;
using Loka.Store.Interfaces.Models.Boosters;
using Loka.Store.Interfaces.Repositories;

namespace Loka.Store.Grains.Boosters;

internal sealed class GameBoostersRepository : BaseRepository<StoreDbContext, BoosterEntity, GameBoosterModel>, IGameBoostersRepository
{
    private IGameResourcesRepository GameResourcesRepository { get; }

    public GameBoostersRepository(ICacheProvider cacheProvider, StoreDbContext dbContext, IMapper mapper, IGameResourcesRepository gameResourcesRepository) : base(cacheProvider, dbContext, mapper)
    {
        GameResourcesRepository = gameResourcesRepository;
    }

    protected override async Task<OperationResult> AfterMap(BoosterEntity entity, GameBoosterModel model)
    {
        if (entity.SellCurrencyId.HasValue)
        {
            var getResourceResult = await GameResourcesRepository.GetById(entity.SellCurrencyId.Value);
            if (!getResourceResult.GetValueIfSucceeded(out var resource))
                return getResourceResult.Error!;

            model.SellCurrency = resource;
        }

        return await base.AfterMap(entity, model);
    }
}