﻿using Loka.Common.Authentication;
using Loka.Common.MediatR.Executors;
using Loka.Common.Orleans.Grains;
using Loka.Inventory.DataBase;
using Loka.Inventory.Interfaces.Inventory;
using Loka.Inventory.Interfaces.Inventory.Requests;

namespace Loka.Inventory.Grains.Inventory;

public sealed class PlayerInventoryItemsGrain : Grain, IPlayerInventoryItemsGrain, IWithPlayerActor
{
    public PlayerActor Actor { get; set; } = null!;
    private InventoryDbContext InventoryDbContext { get; }
    private IOperationExecutor OperationExecutor { get; }
    private IPlayerInventoryItemsRepository PlayerInventoryRepository { get; }
    private IMapper Mapper { get; }

    public PlayerInventoryItemsGrain(InventoryDbContext inventoryDbContext, IOperationExecutor operationExecutor, IPlayerInventoryItemsRepository playerInventoryRepository, IMapper mapper)
    {
        InventoryDbContext = inventoryDbContext;
        OperationExecutor = operationExecutor;
        PlayerInventoryRepository = playerInventoryRepository;
        Mapper = mapper;
    }

    public async Task<OperationResult<AddOrStackInventoryItem.Result>> AddItemToInventory(AddOrStackInventoryItem.Request request)
    {
        var result = await PlayerInventoryRepository.AddInventoryItem(Actor.PlayerId, new AddOrStackInventoryItem.Request()
        {
            ModelId = request.ModelId,
            Amount = request.Amount
        });

        if (result.GetValueIfSucceeded(out var model))
        {
            return new AddOrStackInventoryItem.Result()
            {
                Item = Mapper.Map<PlayerInventoryItemModel.RPC>(model)
            };
        }

        return result.Error!;
    }

    public async Task<OperationResult<AddOrStackInventoryItem.Result>> AddOrStackInventoryItem(AddOrStackInventoryItem.Request request)
    {
        var result = await PlayerInventoryRepository.AddOrStackInventoryItem(Actor.PlayerId, new AddOrStackInventoryItem.Request()
        {
            ModelId = request.ModelId,
            Amount = request.Amount
        });

        if (result.GetValueIfSucceeded(out var model))
        {
            return new AddOrStackInventoryItem.Result()
            {
                Item = Mapper.Map<PlayerInventoryItemModel.RPC>(model)
            };
        }

        return result.Error!;
    }

    public async Task<OperationResult<GetPlayerPurchasedItems.Result>> GetPurchasedItems()
    {
        var items = await PlayerInventoryRepository.GetAll(Actor.PlayerId);
        return new GetPlayerPurchasedItems.Result()
        {
            Entries = Mapper.Map<PlayerInventoryItemModel.RPC[]>(items)
        };
    }
}