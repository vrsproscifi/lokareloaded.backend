﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Entities;

namespace Loka.MatchMaking.DataBase.Entities.Server;

public class ClusterRegionEntity : BaseEntity
{
    #region Cluster

    [ForeignKey(nameof(ClusterEntity))]
    public Guid ClusterId { get; set; }

    public ClusterEntity? ClusterEntity { get; set; }

    #endregion

    #region Region

    [ForeignKey(nameof(RegionEntity))]
    public Guid RegionId { get; set; }

    public ServerRegionEntity? RegionEntity { get; set; }

    #endregion
}