﻿using System;
using Loka.Common.Ranges.MinMaxAvg;

namespace Loka.MatchMaking.History.Entities;

public class MatchHistoryProcessInformation
{
    public int ShutdownCode { get; set; }
    public Guid ClusterId { get; set; }
    public Guid RegionId { get; set; }
    public DateTimeOffset StartedAt { get; set; }
    public DateTimeOffset ShutdownAt { get; set; }

    public MinMaxAvg<byte> Processor { get; set; }
    public MinMaxAvg<short> Memory { get; set; }
}