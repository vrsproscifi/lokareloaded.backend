﻿using Loka.Database.Common.Interfaces;
using Loka.Deployment.DataBase.Enums;

namespace Loka.Deployment.DataBase.Entities;

[Index(nameof(State))]
public sealed class ServerVersionEntity : BaseEntity, IAdminCommentedEntity
{
    [MaxLength(8192)]
    public string? AdminComment { get; set; }
    public DeployState State { get; set; }
        
    public List<GameVersionEntity>? GameVersions { get; set; }
}