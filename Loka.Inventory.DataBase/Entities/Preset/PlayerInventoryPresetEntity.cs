﻿using Loka.Database.Common.Entities;

namespace Loka.Inventory.DataBase.Entities.Preset;

public sealed class PlayerInventoryPresetEntity : BaseEntity
{
    public bool IsActive { get; set; } = true;
    public Guid ModelId { get; set; }

    public Guid PlayerId { get; set; }
    public IReadOnlyList<PlayerInventoryPresetItemEntity>? Items { get; set; }
}