﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Entities;

namespace Loka.MatchMaking.DataBase.Entities.Game.Prime;

public sealed class GamePrimeTimeDayEntity : BaseEntity
{
    [ForeignKey(nameof(PrimeTimeEntity))]
    public Guid PrimeTimeId { get; set; }

    public GamePrimeTimeEntity? PrimeTimeEntity { get; set; }

    public Guid DayOfWeekId { get; set; }
}