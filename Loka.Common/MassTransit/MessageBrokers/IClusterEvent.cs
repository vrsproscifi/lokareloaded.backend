namespace Loka.Common.MassTransit.MessageBrokers;

public interface IClusterEvent
{
    Guid PlayerId { get; }
    string PlayerName { get; }
}