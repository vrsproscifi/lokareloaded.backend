﻿using MongoDB.Bson;
using Orleans.Serialization.Codecs;
using Orleans.Serialization.Serializers;

namespace Loka.Common.Orleans.Extensions;

[RegisterSerializer]
internal sealed class OrleansMongoDbSerializer : GeneralizedValueTypeSurrogateCodec<ObjectId, OrleansMongoDbSerializer.ObjectIdSurrogate>
{
    public OrleansMongoDbSerializer(IValueSerializer<ObjectIdSurrogate> surrogateSerializer) : base(surrogateSerializer)
    {
    }

    public override ObjectId ConvertFromSurrogate(ref ObjectIdSurrogate surrogate)
    {
        return new ObjectId(surrogate.Value);
    }

    public override void ConvertToSurrogate(ObjectId value, ref ObjectIdSurrogate surrogate)
    {
        surrogate.Value = value.ToByteArray();
    }

    [GenerateSerializer]
    internal struct ObjectIdSurrogate
    {
        [Id(0)]
        public byte[] Value { get; set; }
    }
}