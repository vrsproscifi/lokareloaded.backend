﻿using Loka.Common.MassTransit.MessageBrokers;
using Microsoft.Extensions.Logging;

namespace Loka.Identity.Consumers.AfterSignUp;

public sealed class AfterSignUpEventConsumer : ClusterConsumer<AfterSignUpEvent>
{
    public AfterSignUpEventConsumer(ClusterConsumerServices<ClusterConsumer<AfterSignUpEvent>> services) : base(services)
    {
        
    }

    public override async Task Consume(ConsumeContext<AfterSignUpEvent> context)
    {
        await base.Consume(context);
        // Logger.LogInformation($"Begin {nameof(AfterSignUpEventConsumer)} for player {context.Message.PlayerId}");
    }

    public sealed class ConsumerDefinition : ConsumerDefinition<AfterSignUpEventConsumer>
    {
        public ConsumerDefinition()
        {
            EndpointName = nameof(AfterSignUpEventConsumer);
        }
    }


}