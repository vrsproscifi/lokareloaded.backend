﻿namespace Loka.Common.Operations;

public static class NullableOperationSequence
{
    public delegate Task<OperationResult<TResult>> ExecuteSequenceSelector<TResult, in TTarget>(TTarget value, CancellationToken cancellationToken);

    public delegate void ExecuteSequenceAction<TModel>(TModel model);

    public static async Task<OperationResult<TResult>> ExecuteSequence<TResult, TPredicateResult, TTarget>
    (
        this TResult result,
        TTarget? predicate,
        ExecuteSequenceSelector<TPredicateResult, TTarget> selector,
        ExecuteSequenceAction<TPredicateResult> action,
        CancellationToken cancellationToken = default
    )
        where TTarget : struct
    {
        if (predicate.HasValue)
        {
            var actionResult = await selector.Invoke(predicate.Value, cancellationToken);
            if (actionResult.IsSucceeded)
            {
                action(actionResult.Result);
            }
            else
            {
                return actionResult.Error!;
            }
        }

        return result;
    }

    public static async Task<OperationResult<TResult>> ExecuteSequence<TResult, TPredicateResult, TTarget>
    (
        this Task<OperationResult<TResult>> getResultTask,
        TTarget? predicate,
        ExecuteSequenceSelector<TPredicateResult, TTarget> selector,
        ExecuteSequenceAction<TPredicateResult> action,
        CancellationToken cancellationToken = default
    )
        where TTarget : struct
    {
        var result = await getResultTask;

        if (result.IsSucceeded && predicate.HasValue)
        {
            var actionResult = await selector.Invoke(predicate.Value, cancellationToken);
            if (actionResult.IsSucceeded)
            {
                action(actionResult.Result);
            }
            else
            {
                return actionResult.Error!;
            }
        }

        return result;
    }
        
    public static async Task<OperationResult<TResult>> ExecuteSequence<TResult, TPredicateResult, TTarget>
    (
        this Task<OperationResult<TResult>> getResultTask,
        TTarget value,
        ExecuteSequenceSelector<TPredicateResult, TTarget> selector,
        ExecuteSequenceAction<TPredicateResult> action,
        CancellationToken cancellationToken = default
    )
    {
        var result = await getResultTask;

        if (result.IsSucceeded )
        {
            var actionResult = await selector.Invoke(value, cancellationToken);
            if (actionResult.IsSucceeded)
            {
                action(actionResult.Result);
            }
            else
            {
                return actionResult.Error!;
            }
        }

        return result;
    }
}