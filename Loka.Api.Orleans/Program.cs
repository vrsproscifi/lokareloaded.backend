using System.Net;
using Loka.Api.Orleans.Extensions;
using Orleans.Configuration;
using Orleans.Runtime.Development;

namespace Loka.Api.Orleans;

public class Program
{
    public static void Main(string[] args)
    {
        CreateHostBuilder(args)
            .Build()
            .Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args)
    {
        return Host.CreateDefaultBuilder(args)
            .UseOrleans(ConfigureDelegate)
            .UseConsoleLifetime()
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.ConfigureAppConfiguration(builder =>
                {
                    builder.AddEnvironmentVariables();
                    builder.AddJsonFile("appsettings.DataBase.json", optional: false);
                });

                webBuilder.CaptureStartupErrors(true);
                webBuilder.UseStartup<Startup>();
                webBuilder.UseUrls("http://localhost:10030");
                webBuilder.UseSockets();
                /*webBuilder.UseKestrel(options =>
                {
                    options.ListenLocalhost(10000, o => { o.Protocols = HttpProtocols.Http1AndHttp2; });
                });*/
            });
    }


    private static void ConfigureDelegate(HostBuilderContext context, ISiloBuilder builder)
    {
        builder
            .AddIncomingGrainCallFilter<PlayerActorCallFilter>()
            .AddIncomingGrainCallFilter<ExceptionCallFilter>()
            .AddOutgoingGrainCallFilter<ExceptionCallFilter>()
            //.UsePerfCounterEnvironmentStatistics()
            .UseDashboard(options =>
            {
                options.Port = 10010;
            })
            .UseLocalhostClustering()
            .UseInMemoryReminderService()
            .UseInMemoryLeaseProvider()
            .Configure<ClusterOptions>(options =>
            {
                options.ClusterId = "dev";
                options.ServiceId = "HelloWorldApp";
            })         
            .Configure<ReminderOptions>(options =>
            {
                options.MinimumReminderPeriod = TimeSpan.FromSeconds(10);
            })
            .Configure<EndpointOptions>(options => { options.AdvertisedIPAddress = IPAddress.Loopback; });
    }
}