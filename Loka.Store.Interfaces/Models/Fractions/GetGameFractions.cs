﻿namespace Loka.Store.Interfaces.Models.Fractions;

public sealed class GetGameFractions
{
    [GenerateSerializer]
    public sealed class Request
    {
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public IReadOnlyList<StoreFractionModel> Entries { get; set; } = ArraySegment<StoreFractionModel>.Empty;
    }
}