﻿using Loka.Api.IntegrationTests.Clients;
using Loka.Api.IntegrationTests.Services.GameChat;
using Loka.Api.IntegrationTests.Services.PlayerIdentity;
using Loka.Api.IntegrationTests.Services.PlayerInventory;
using Loka.Api.IntegrationTests.Services.Store;

namespace Loka.Api.IntegrationTests.Services;

public sealed class GameAPIServices
{
    public GameAPIServices(GameAPIClients gameAPIClient)
    {
        PlayerIdentity = new PlayerIdentityService(gameAPIClient);
        GameStore = new GameStoreService(gameAPIClient);
        GameChat = new GameChatService(gameAPIClient);
        PlayerInventoryItems = new PlayerInventoryItemsService(gameAPIClient);
        PlayerInventoryCases = new PlayerInventoryCasesService(gameAPIClient);
        PlayerInventoryPresets = new PlayerInventoryPresetsService(gameAPIClient);
        PlayerInventoryTemplates = new PlayerInventoryTemplatesService(gameAPIClient);
        PlayerInventoryResources = new PlayerInventoryResourcesService(gameAPIClient);
        PlayerInventoryBoosters = new PlayerInventoryBoostersService(gameAPIClient);
    }

    public PlayerInventoryItemsService PlayerInventoryItems { get; }
    public PlayerInventoryCasesService PlayerInventoryCases { get; }
    public PlayerInventoryPresetsService PlayerInventoryPresets { get; }
    public PlayerInventoryTemplatesService PlayerInventoryTemplates { get; }
    public PlayerInventoryResourcesService PlayerInventoryResources { get; }
    public PlayerInventoryBoostersService PlayerInventoryBoosters { get; }
    public PlayerIdentityService PlayerIdentity { get; }
    public GameStoreService GameStore { get; }
    public GameChatService GameChat { get; }
}