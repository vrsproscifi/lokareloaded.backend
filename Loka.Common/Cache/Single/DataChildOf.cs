﻿namespace Loka.Common.Cache.Single;

public class DataChildOf<TKey, TData> where TData : class
{
    public TKey Key { get; }
    public TData Data { get; }
        
    public DataChildOf(TKey key, TData data)
    {
        Key = key;
        Data = data;
    }

    public static implicit operator TData(DataChildOf<TKey, TData> data) => data.Data;
}