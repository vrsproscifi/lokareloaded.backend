﻿namespace Loka.Store.Interfaces.Enums;

[Flags]
public enum ItemEntityFlags : byte
{
    None = 0,

    Hidden = 1,
    Subscribe = 2,
    IssueAfterRegistration = 4,
    IssueAfterLogin = 8,
    AllowSelling = 32,
}