﻿using Loka.Api.Features.Identity;
using MongoDB.Bson;

namespace Loka.Api.IntegrationTests.Services.PlayerIdentity;

public sealed class PlayerSession
{
    public SignUpQuery Query { get; }
    public PlayerAuthenticationResult Result { get; }
    public SessionRefreshTokenResult Token => Result.Token;

    public Guid PlayerId { get; }
    public ObjectId SessionId { get; }
    public string DisplayName { get; }

    public PlayerSession(SignUpQuery query, PlayerAuthenticationResult authenticationResult)
    {
        Query = query;
        Result = authenticationResult;
        DisplayName = authenticationResult.DisplayName;
        PlayerId = Guid.Parse(authenticationResult.PlayerId);
        SessionId = ObjectId.Parse(authenticationResult.SessionId);
    }

    public SignInQuery GetSignInQuery()
    {
        return new SignInQuery
        {
            UserNameOrEmail = Query.PlayerName,
            Password = Query.Password
        };
    }
}