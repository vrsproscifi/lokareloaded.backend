﻿using Loka.Chat.Interfaces.Enums;
using MongoDB.Bson;

namespace Loka.Chat.Interfaces.Models;

[GenerateSerializer]
public sealed class ChannelEntry
{
    [Id(0)]
    public ObjectId Id { get; init; }
            
    [Id(1)]
    public string? Name { get; init; }
    
    [Id(2)]
    public ChatChanelType TypeId { get; init; }
}