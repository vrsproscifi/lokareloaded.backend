﻿using Loka.Api.Features.Inventory.Boosters;
using Loka.Common.Operations.Codes;

namespace Loka.Api.IntegrationTests.Tests.Inventory;

internal sealed class PlayerInventoryBoostersTests : BaseTest
{
    [Test]
    public async Task GetPlayerInventoryBoosters()
    {
        var playerSession = await Services.PlayerIdentity.SignUp();
        var boosters = await Services.PlayerInventoryBoosters.GetBoosters(playerSession);
        Assert.That(boosters, Is.Empty, $"Failed execute {nameof(Services.PlayerInventoryBoosters.GetBoosters)} for {playerSession.PlayerId}");
    }

    [Test]
    public async Task ActivateDeactivateBooster()
    {
        var playerSession = await Services.PlayerIdentity.QuickSignUp();

        var playerBooster1 = await Services.PlayerInventoryBoosters.BuyBooster(playerSession, Storages.GameStore.Boosters[0]);
        var activatedBooster = await Services.PlayerInventoryBoosters.ActivateBooster(playerSession, playerBooster1);
        var deactivatedBooster = await Services.PlayerInventoryBoosters.DeactivateBooster(playerSession, playerBooster1);
    }

    [Test]
    public async Task ActivateTooMuchBoosters()
    {
        var playerSession = await Services.PlayerIdentity.QuickSignUp();

        var playerBooster1 = await Services.PlayerInventoryBoosters.BuyBooster(playerSession, Storages.GameStore.Boosters[0]);
        var playerBooster2 = await Services.PlayerInventoryBoosters.BuyBooster(playerSession, Storages.GameStore.Boosters[1]);
        var playerBooster3 = await Services.PlayerInventoryBoosters.BuyBooster(playerSession, Storages.GameStore.Boosters[2]);
        var playerBooster4 = await Services.PlayerInventoryBoosters.BuyBooster(playerSession, Storages.GameStore.Boosters[3]);

        await Services.PlayerInventoryBoosters.ActivateBooster(playerSession, playerBooster1);
        await Services.PlayerInventoryBoosters.ActivateBooster(playerSession, playerBooster2);
        await Services.PlayerInventoryBoosters.ActivateBooster(playerSession, playerBooster3);

        var exception = Assert.ThrowsAsync<RpcException>(() => Services.PlayerInventoryBoosters.ActivateBooster(playerSession, playerBooster4));
        exception.AssertResultCode(OperationResultCode.TooMuchActivatedBoosters);
    }
    
    [Test]
    public async Task ActivateDeactivateTooMuchBoosters()
    {
        var playerSession = await Services.PlayerIdentity.QuickSignUp();

        var playerBooster1 = await Services.PlayerInventoryBoosters.BuyBooster(playerSession, Storages.GameStore.Boosters[0]);
        var playerBooster2 = await Services.PlayerInventoryBoosters.BuyBooster(playerSession, Storages.GameStore.Boosters[1]);
        var playerBooster3 = await Services.PlayerInventoryBoosters.BuyBooster(playerSession, Storages.GameStore.Boosters[2]);
        var playerBooster4 = await Services.PlayerInventoryBoosters.BuyBooster(playerSession, Storages.GameStore.Boosters[3]);

        await Services.PlayerInventoryBoosters.ActivateBooster(playerSession, playerBooster1);
        await Services.PlayerInventoryBoosters.ActivateBooster(playerSession, playerBooster2);
        await Services.PlayerInventoryBoosters.ActivateBooster(playerSession, playerBooster3);
        await Services.PlayerInventoryBoosters.DeactivateBooster(playerSession, playerBooster3);
        await Services.PlayerInventoryBoosters.ActivateBooster(playerSession, playerBooster4);
    }

    [Test]
    public async Task BuyTwoSameBoosters()
    {
        var playerSession = await Services.PlayerIdentity.QuickSignUp();

        var playerBooster1 = await Services.PlayerInventoryBoosters.BuyBooster(playerSession, Storages.GameStore.Boosters[0]);
        Assert.That(playerBooster1.Amount, Is.EqualTo(1));

        var playerBooster2 = await Services.PlayerInventoryBoosters.BuyBooster(playerSession, Storages.GameStore.Boosters[0]);
        Assert.That(playerBooster2.Amount, Is.EqualTo(2));

        var boosters = await Services.PlayerInventoryBoosters.GetBoosters(playerSession);
        Assert.That(boosters, Has.Count.EqualTo(1));

        var playerBooster = boosters.FirstOrDefault(q => q.Id.Equals(playerBooster1.Id));
        Assert.That(playerBooster, Is.Not.Null, $"Purchased booster {playerBooster1.Id} not found");
    }

    [Test]
    public async Task BuyTwoDifferentBoosters()
    {
        var playerSession = await Services.PlayerIdentity.QuickSignUp();

        var playerBooster1 = await Services.PlayerInventoryBoosters.BuyBooster(playerSession, Storages.GameStore.Boosters[0]);
        Assert.That(playerBooster1.Amount, Is.EqualTo(1));

        var playerBooster2 = await Services.PlayerInventoryBoosters.BuyBooster(playerSession, Storages.GameStore.Boosters[1]);
        Assert.That(playerBooster2.Amount, Is.EqualTo(1));

        var boosters = await Services.PlayerInventoryBoosters.GetBoosters(playerSession);
        Assert.That(boosters, Has.Count.EqualTo(2));

        Assert.That(boosters[1].Id, Is.Not.EqualTo(boosters[0].Id), $"Invalid {nameof(PlayerInventoryBooster.Id)}");
        Assert.That(boosters[1].ModelId, Is.Not.EqualTo(boosters[0].ModelId), $"Invalid {nameof(PlayerInventoryBooster.ModelId)}");
    }
}