﻿namespace Global.Identity.Database.Entities;

public sealed class UserAccountEntity : BaseEntity
{
    [Required]
    [MaxLength(128)]
    public string Login { get; set; } = null!;

    public bool IsNameConfirmed { get; set; } = false;

    [MaxLength(64)]
    public string? Email { get; set; }

    public bool IsEmailConfirmed { get; set; } = false;

    [MaxLength(512)]
    public string? PasswordHash { get; set; } = null;

    public Guid? RoleId { get; set; }
    public Guid? CountryId { get; set; }
    public Guid? LanguageId { get; set; }
    public Guid? GenderId { get; set; }

    public DateTimeOffset? PremiumEndDate { get; set; }
    public DateTimeOffset? Birthdate { get; set; }
}