﻿using MongoDB.Bson;

namespace Loka.Chat.Interfaces.Models;

public static class KickFromChannel
{
    [GenerateSerializer]
    public sealed class Request
    {
        [Id(0)]
        public ObjectId ChannelId { get; init; }

        [Id(1)]
        public Guid PlayerId { get; init; }
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public ChannelEntry Channel { get; init; } = null!;

        [Id(0)]
        public ChatMemberEntry Member { get; init; } = null!;
    }
}