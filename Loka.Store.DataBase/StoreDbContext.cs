﻿using Loka.Store.DataBase.Entities;
using Loka.Store.DataBase.Entities.Booster;
using Loka.Store.DataBase.Entities.Cases;
using Loka.Store.DataBase.Entities.Items;
using Loka.Store.DataBase.Seeders;

namespace Loka.Store.DataBase;

public sealed class StoreDbContext : BaseGameDbContext<StoreDbContext>
{
    public StoreDbContext(DbContextOptions<StoreDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<GameItemEntity>().HasData(GameItemEntitySeed.Entities);
        modelBuilder.Entity<GameItemBlueprintEntity>().HasData(GameItemEntitySeed.GameItemBlueprintEntities);
        modelBuilder.Entity<GameItemBlueprintDependEntity>().HasData(GameItemEntitySeed.GameItemBlueprintDependEntities);
        modelBuilder.Entity<FractionEntity>().HasData(FractionEntitySeed.Entities);
        modelBuilder.Entity<GameResourceEntity>().HasData(GameResourceEntitySeed.Entities);
        modelBuilder.Entity<GameItemCategoryEntity>().HasData(GameItemCategoryEntitySeed.Entities);

        modelBuilder.Entity<CaseEntity>().HasData(CaseEntitySeed.Entities);
        modelBuilder.Entity<CaseItemEntity>().HasData(CaseEntitySeed.CaseItemEntities);
        modelBuilder.Entity<CaseResourceEntity>().HasData(CaseEntitySeed.CaseResourceEntities);

        modelBuilder.Entity<BoosterEntity>().HasData(BoosterEntitySeed.Entities);
    }

    public DbSet<BoosterEntity> BoosterEntity { get; set; } = null!;

    public DbSet<CaseEntity> CaseEntity { get; set; } = null!;
    public DbSet<CaseItemEntity> CaseItemEntity { get; set; } = null!;
    public DbSet<CaseResourceEntity> CaseResourceEntity { get; set; } = null!;
    public DbSet<BonusCaseEntity> BonusCaseEntity { get; set; } = null!;


    public DbSet<FractionEntity> FractionEntity { get; set; } = null!;
    public DbSet<GameResourceEntity> GameResourceEntity { get; set; } = null!;

    public DbSet<GameItemEntity> GameItemEntity { get; set; } = null!;
    public DbSet<GameItemCategoryEntity> GameItemCategoryEntity { get; set; } = null!;
    public DbSet<GameItemBlueprintEntity> GameItemBlueprintEntity { get; set; } = null!;
    public DbSet<GameItemBlueprintDependEntity> GameItemBlueprintDependEntity { get; set; } = null!;
}