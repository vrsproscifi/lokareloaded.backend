﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
#pragma warning disable CS8625

namespace Loka.Game.Database.Migrations
{
    public partial class FirstInit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "System");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:pgcrypto", ",,")
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", ",,");

            migrationBuilder.CreateTable(
                name: "AccountRoleEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AccountRoleEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "AchievementEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    TypeId = table.Column<short>(type: "smallint", nullable: false),
                    MethodId = table.Column<byte>(type: "smallint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AchievementEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "BonusCaseEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false),
                    Delay = table.Column<TimeSpan>(type: "interval", nullable: true),
                    FirstNotifyDelay = table.Column<TimeSpan>(type: "interval", nullable: true),
                    NextNotifyDelay = table.Column<TimeSpan>(type: "interval", nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BonusCaseEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "CaseEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    RandomMaxDropItems = table.Column<int>(type: "integer", nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "FractionEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FractionEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "GameCurrencyEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: true),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameCurrencyEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "GameItemCategoryEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameItemCategoryEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "WorldCountryEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorldCountryEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "WorldDayOfWeekEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(24)", maxLength: 24, nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorldDayOfWeekEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "WorldGenderEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(8)", maxLength: 8, nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorldGenderEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "WorldLanguageEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    Code = table.Column<string>(type: "character varying(3)", maxLength: 3, nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorldLanguageEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "EveryDayAwardEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Day = table.Column<int>(type: "integer", nullable: false),
                    CaseId = table.Column<Guid>(type: "uuid", nullable: false),
                    AdminComment = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_EveryDayAwardEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_EveryDayAwardEntity_CaseEntity_CaseId",
                        column: x => x.CaseId,
                        principalSchema: "System",
                        principalTable: "CaseEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "NewLevelAwardEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Level = table.Column<int>(type: "integer", nullable: false),
                    CaseId = table.Column<Guid>(type: "uuid", nullable: false),
                    AdminComment = table.Column<string>(type: "character varying(256)", maxLength: 256, nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewLevelAwardEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_NewLevelAwardEntity_CaseEntity_CaseId",
                        column: x => x.CaseId,
                        principalSchema: "System",
                        principalTable: "CaseEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GameItemEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    FractionId = table.Column<Guid>(type: "uuid", nullable: false),
                    Level = table.Column<int>(type: "integer", nullable: false),
                    CategoryId = table.Column<Guid>(type: "uuid", nullable: false),
                    CurrencyId = table.Column<Guid>(type: "uuid", nullable: false),
                    Cost = table.Column<int>(type: "integer", nullable: false),
                    Flags = table.Column<byte>(type: "smallint", nullable: false),
                    Duration = table.Column<TimeSpan>(type: "interval", nullable: true),
                    DisplayPosition = table.Column<int>(type: "integer", nullable: false),
                    AdminComment = table.Column<string>(type: "character varying(1024)", maxLength: 1024, nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_GameItemEntity_FractionEntity_FractionId",
                        column: x => x.FractionId,
                        principalSchema: "System",
                        principalTable: "FractionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameItemEntity_GameCurrencyEntity_CurrencyId",
                        column: x => x.CurrencyId,
                        principalSchema: "System",
                        principalTable: "GameCurrencyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameItemEntity_GameItemCategoryEntity_CategoryId",
                        column: x => x.CategoryId,
                        principalSchema: "System",
                        principalTable: "GameItemCategoryEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CaseItemEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    CaseId = table.Column<Guid>(type: "uuid", nullable: false),
                    ItemId = table.Column<Guid>(type: "uuid", nullable: false),
                    MinimumCount = table.Column<int>(type: "integer", nullable: false),
                    MaximumCount = table.Column<int>(type: "integer", nullable: false),
                    ChanceOfDrop = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_CaseItemEntity_CaseEntity_CaseId",
                        column: x => x.CaseId,
                        principalSchema: "System",
                        principalTable: "CaseEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CaseItemEntity_GameItemEntity_ItemId",
                        column: x => x.ItemId,
                        principalSchema: "System",
                        principalTable: "GameItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "System",
                table: "AccountRoleEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe605"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 700, DateTimeKind.Unspecified).AddTicks(7867), new TimeSpan(0, 0, 0, 0, 0)), null, "BetaTester", (byte)5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe604"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 700, DateTimeKind.Unspecified).AddTicks(7694), new TimeSpan(0, 0, 0, 0, 0)), null, "Developer", (byte)4 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe603"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 700, DateTimeKind.Unspecified).AddTicks(7487), new TimeSpan(0, 0, 0, 0, 0)), null, "Moderator", (byte)2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe602"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 700, DateTimeKind.Unspecified).AddTicks(7248), new TimeSpan(0, 0, 0, 0, 0)), null, "Partner", (byte)1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 700, DateTimeKind.Unspecified).AddTicks(6589), new TimeSpan(0, 0, 0, 0, 0)), null, "Administrator", (byte)3 }
                });

            migrationBuilder.InsertData(
                schema: "System",
                table: "AchievementEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "MethodId", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe120"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(207), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "MatchesTotal", (short)21 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe121"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(379), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "MatchesWins", (short)22 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe122"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(596), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "MatchesLoses", (short)23 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe123"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(779), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "MatchesDefeats", (short)24 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe124"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(941), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "HeadShot", (short)25 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe127"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(1415), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)1, "BestKills", (short)28 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe126"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(1258), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)1, "BestScore", (short)27 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe119"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(9997), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "SeriesKillsByGrenade", (short)20 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe128"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(1573), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "FirstInMatch", (short)29 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe129"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(1730), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "SecondInMatch", (short)30 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe130"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(1887), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "ThirdInMatch", (short)31 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe125"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 711, DateTimeKind.Unspecified).AddTicks(1099), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "TimeInGame", (short)26 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe118"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(9840), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "SeriesKillsByKnife", (short)19 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe100"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(6338), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "Kills", (short)1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe116"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(9522), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "SeriesKillsBySniperRifle", (short)17 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe102"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(7269), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "Score", (short)3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe117"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(9679), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "SeriesKillsByRifle", (short)18 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe101"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(7086), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "Deaths", (short)2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe105"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(7768), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "FirstBlood", (short)6 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe106"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(7928), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "Assist", (short)7 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe107"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(8088), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "KillsByPistol", (short)8 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe103"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(7436), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "Money", (short)4 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe108"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(8248), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "KillsByShotGun", (short)9 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe110"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(8568), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "KillsByRifle", (short)11 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe111"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(8729), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "KillsByKnife", (short)12 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe112"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(8886), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "KillsByGrenade", (short)13 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe113"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(9044), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "SeriesKills", (short)14 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe114"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(9206), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "SeriesKillsByPistol", (short)15 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe115"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(9362), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "SeriesKillsByShotGun", (short)16 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe109"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(8409), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "KillsBySniperRifle", (short)10 },
                    { new Guid("a607b39d-6e02-45ce-9b77-900c8dffe104"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 710, DateTimeKind.Unspecified).AddTicks(7604), new TimeSpan(0, 0, 0, 0, 0)), null, (byte)0, "Experience", (short)5 }
                });

            migrationBuilder.InsertData(
                schema: "System",
                table: "FractionEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 697, DateTimeKind.Unspecified).AddTicks(6351), new TimeSpan(0, 0, 0, 0, 0)), null, "Keepers_Friend", (byte)2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 697, DateTimeKind.Unspecified).AddTicks(6540), new TimeSpan(0, 0, 0, 0, 0)), null, "RIFT", (byte)3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 697, DateTimeKind.Unspecified).AddTicks(5582), new TimeSpan(0, 0, 0, 0, 0)), null, "Keepers", (byte)1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 697, DateTimeKind.Unspecified).AddTicks(6709), new TimeSpan(0, 0, 0, 0, 0)), null, "RIFT_Friend", (byte)4 }
                });

            migrationBuilder.InsertData(
                schema: "System",
                table: "GameCurrencyEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 698, DateTimeKind.Unspecified).AddTicks(5029), new TimeSpan(0, 0, 0, 0, 0)), null, "Coin", (byte)3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 698, DateTimeKind.Unspecified).AddTicks(5766), new TimeSpan(0, 0, 0, 0, 0)), null, "Donate", (byte)2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 698, DateTimeKind.Unspecified).AddTicks(5950), new TimeSpan(0, 0, 0, 0, 0)), null, "Money", (byte)1 }
                });

            migrationBuilder.InsertData(
                schema: "System",
                table: "GameItemCategoryEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 699, DateTimeKind.Unspecified).AddTicks(938), new TimeSpan(0, 0, 0, 0, 0)), null, "Currency", (byte)9 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 699, DateTimeKind.Unspecified).AddTicks(778), new TimeSpan(0, 0, 0, 0, 0)), null, "Kits", (byte)8 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 699, DateTimeKind.Unspecified).AddTicks(615), new TimeSpan(0, 0, 0, 0, 0)), null, "Grenade", (byte)7 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 699, DateTimeKind.Unspecified).AddTicks(288), new TimeSpan(0, 0, 0, 0, 0)), null, "Profile", (byte)5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 699, DateTimeKind.Unspecified).AddTicks(451), new TimeSpan(0, 0, 0, 0, 0)), null, "Material", (byte)6 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 698, DateTimeKind.Unspecified).AddTicks(9954), new TimeSpan(0, 0, 0, 0, 0)), null, "Service", (byte)3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 698, DateTimeKind.Unspecified).AddTicks(9767), new TimeSpan(0, 0, 0, 0, 0)), null, "Armour", (byte)2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 698, DateTimeKind.Unspecified).AddTicks(9118), new TimeSpan(0, 0, 0, 0, 0)), null, "Weapon", (byte)1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 699, DateTimeKind.Unspecified).AddTicks(122), new TimeSpan(0, 0, 0, 0, 0)), null, "Character", (byte)4 }
                });

            migrationBuilder.InsertData(
                schema: "System",
                table: "WorldCountryEntity",
                columns: new[] { "EntityId", "Code", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe256"), "NZ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(8550), new TimeSpan(0, 0, 0, 0, 0)), null, "New Zealand", (byte)171 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe257"), "NI", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(8707), new TimeSpan(0, 0, 0, 0, 0)), null, "Nicaragua", (byte)165 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe258"), "NE", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(8866), new TimeSpan(0, 0, 0, 0, 0)), null, "Niger", (byte)162 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe259"), "NG", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(9025), new TimeSpan(0, 0, 0, 0, 0)), null, "Nigeria", (byte)164 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe260"), "NU", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(9183), new TimeSpan(0, 0, 0, 0, 0)), null, "Niue", (byte)170 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe261"), "NF", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(9342), new TimeSpan(0, 0, 0, 0, 0)), null, "Norfolk Island", (byte)163 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe262"), "MP", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(9498), new TimeSpan(0, 0, 0, 0, 0)), null, "Northern Mariana Islands", (byte)149 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe263"), "NO", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(9655), new TimeSpan(0, 0, 0, 0, 0)), null, "Norway", (byte)167 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe264"), "OM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(9813), new TimeSpan(0, 0, 0, 0, 0)), null, "Oman", (byte)172 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe265"), "PK", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(9971), new TimeSpan(0, 0, 0, 0, 0)), null, "Pakistan", (byte)178 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe266"), "PW", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(129), new TimeSpan(0, 0, 0, 0, 0)), null, "Palau", (byte)185 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe268"), "PA", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(286), new TimeSpan(0, 0, 0, 0, 0)), null, "Panama", (byte)173 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe270"), "PY", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(600), new TimeSpan(0, 0, 0, 0, 0)), null, "Paraguay", (byte)186 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe271"), "PE", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(757), new TimeSpan(0, 0, 0, 0, 0)), null, "Peru", (byte)174 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe272"), "PH", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(957), new TimeSpan(0, 0, 0, 0, 0)), null, "Philippines", (byte)177 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe273"), "PN", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(1177), new TimeSpan(0, 0, 0, 0, 0)), null, "Pitcairn", (byte)181 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe274"), "PL", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(1353), new TimeSpan(0, 0, 0, 0, 0)), null, "Poland", (byte)179 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe275"), "PT", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(1510), new TimeSpan(0, 0, 0, 0, 0)), null, "Portugal", (byte)184 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe276"), "PR", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(1669), new TimeSpan(0, 0, 0, 0, 0)), null, "Puerto Rico", (byte)182 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe277"), "QA", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(1828), new TimeSpan(0, 0, 0, 0, 0)), null, "Qatar", (byte)187 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe278"), "RE", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(1986), new TimeSpan(0, 0, 0, 0, 0)), null, "Reunion", (byte)188 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe279"), "RO", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(2148), new TimeSpan(0, 0, 0, 0, 0)), null, "Romania", (byte)189 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe280"), "RU", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(2304), new TimeSpan(0, 0, 0, 0, 0)), null, "Russian Federation", (byte)191 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe281"), "RW", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(2463), new TimeSpan(0, 0, 0, 0, 0)), null, "RWANDA", (byte)192 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe255"), "NC", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(8393), new TimeSpan(0, 0, 0, 0, 0)), null, "New Caledonia", (byte)161 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe269"), "PG", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(443), new TimeSpan(0, 0, 0, 0, 0)), null, "Papua New Guinea", (byte)176 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe254"), "AN", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(8234), new TimeSpan(0, 0, 0, 0, 0)), null, "Netherlands Antilles", (byte)215 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe228"), "MO", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(3948), new TimeSpan(0, 0, 0, 0, 0)), null, "Macao", (byte)148 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe252"), "NP", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(7918), new TimeSpan(0, 0, 0, 0, 0)), null, "Nepal", (byte)168 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe226"), "LT", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(3629), new TimeSpan(0, 0, 0, 0, 0)), null, "Lithuania", (byte)133 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe227"), "LU", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(3787), new TimeSpan(0, 0, 0, 0, 0)), null, "Luxembourg", (byte)134 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe282"), "SH", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(2621), new TimeSpan(0, 0, 0, 0, 0)), null, "Saint Helena", (byte)199 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe229"), "MK", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(4173), new TimeSpan(0, 0, 0, 0, 0)), null, "Macedonia, The Former Yugoslav Republic of", (byte)144 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe230"), "MG", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(4358), new TimeSpan(0, 0, 0, 0, 0)), null, "Madagascar", (byte)142 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe231"), "MW", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(4517), new TimeSpan(0, 0, 0, 0, 0)), null, "Malawi", (byte)156 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe232"), "MY", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(4674), new TimeSpan(0, 0, 0, 0, 0)), null, "Malaysia", (byte)158 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe233"), "MV", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(4832), new TimeSpan(0, 0, 0, 0, 0)), null, "Maldives", (byte)155 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe234"), "ML", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(4988), new TimeSpan(0, 0, 0, 0, 0)), null, "Mali", (byte)145 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe235"), "MT", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(5146), new TimeSpan(0, 0, 0, 0, 0)), null, "Malta", (byte)153 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe236"), "MH", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(5362), new TimeSpan(0, 0, 0, 0, 0)), null, "Marshall Islands", (byte)143 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe237"), "MQ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(5538), new TimeSpan(0, 0, 0, 0, 0)), null, "Martinique", (byte)150 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe253"), "NL", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(8078), new TimeSpan(0, 0, 0, 0, 0)), null, "Netherlands", (byte)166 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe238"), "MR", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(5696), new TimeSpan(0, 0, 0, 0, 0)), null, "Mauritania", (byte)151 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe240"), "YT", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(6014), new TimeSpan(0, 0, 0, 0, 0)), null, "Mayotte", (byte)248 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe241"), "MX", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(6171), new TimeSpan(0, 0, 0, 0, 0)), null, "Mexico", (byte)157 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe242"), "FM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(6330), new TimeSpan(0, 0, 0, 0, 0)), null, "Micronesia, Federated States of", (byte)73 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe243"), "MD", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(6486), new TimeSpan(0, 0, 0, 0, 0)), null, "Moldova, Republic of", (byte)139 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe244"), "MC", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(6642), new TimeSpan(0, 0, 0, 0, 0)), null, "Monaco", (byte)138 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe245"), "MN", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(6801), new TimeSpan(0, 0, 0, 0, 0)), null, "Mongolia", (byte)147 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe246"), "MS", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(6962), new TimeSpan(0, 0, 0, 0, 0)), null, "Montserrat", (byte)152 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe247"), "MA", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(7121), new TimeSpan(0, 0, 0, 0, 0)), null, "Morocco", (byte)137 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe248"), "MZ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(7281), new TimeSpan(0, 0, 0, 0, 0)), null, "Mozambique", (byte)159 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe249"), "MM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(7440), new TimeSpan(0, 0, 0, 0, 0)), null, "Myanmar", (byte)146 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe250"), "NA", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(7599), new TimeSpan(0, 0, 0, 0, 0)), null, "Namibia", (byte)160 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe251"), "NR", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(7758), new TimeSpan(0, 0, 0, 0, 0)), null, "Nauru", (byte)169 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe239"), "MU", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(5856), new TimeSpan(0, 0, 0, 0, 0)), null, "Mauritius", (byte)154 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe283"), "KN", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(2788), new TimeSpan(0, 0, 0, 0, 0)), null, "Saint Kitts and Nevis", (byte)120 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe309"), "CH", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(6990), new TimeSpan(0, 0, 0, 0, 0)), null, "Switzerland", (byte)43 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe285"), "PM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(3106), new TimeSpan(0, 0, 0, 0, 0)), null, "Saint Pierre and Miquelon", (byte)180 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe316"), "TG", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(8178), new TimeSpan(0, 0, 0, 0, 0)), null, "Togo", (byte)219 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe317"), "TK", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(8335), new TimeSpan(0, 0, 0, 0, 0)), null, "Tokelau", (byte)222 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe318"), "TO", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(8531), new TimeSpan(0, 0, 0, 0, 0)), null, "Tonga", (byte)226 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe319"), "TT", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(8705), new TimeSpan(0, 0, 0, 0, 0)), null, "Trinidad and Tobago", (byte)228 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe320"), "TN", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(8865), new TimeSpan(0, 0, 0, 0, 0)), null, "Tunisia", (byte)225 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe321"), "TR", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(9026), new TimeSpan(0, 0, 0, 0, 0)), null, "Turkey", (byte)227 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe322"), "TM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(9185), new TimeSpan(0, 0, 0, 0, 0)), null, "Turkmenistan", (byte)224 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe323"), "TC", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(9345), new TimeSpan(0, 0, 0, 0, 0)), null, "Turks and Caicos Islands", (byte)214 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe324"), "TV", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(9505), new TimeSpan(0, 0, 0, 0, 0)), null, "Tuvalu", (byte)229 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe325"), "UG", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(9662), new TimeSpan(0, 0, 0, 0, 0)), null, "Uganda", (byte)233 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe326"), "UA", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(9816), new TimeSpan(0, 0, 0, 0, 0)), null, "Ukraine", (byte)232 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe327"), "AE", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(9973), new TimeSpan(0, 0, 0, 0, 0)), null, "United Arab Emirates", (byte)2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe328"), "GB", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(131), new TimeSpan(0, 0, 0, 0, 0)), null, "United Kingdom", (byte)77 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe329"), "US", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(288), new TimeSpan(0, 0, 0, 0, 0)), null, "United States", (byte)235 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe330"), "UM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(444), new TimeSpan(0, 0, 0, 0, 0)), null, "United States Minor Outlying Islands", (byte)234 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe331"), "UY", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(599), new TimeSpan(0, 0, 0, 0, 0)), null, "Uruguay", (byte)236 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe332"), "UZ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(758), new TimeSpan(0, 0, 0, 0, 0)), null, "Uzbekistan", (byte)237 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe333"), "VU", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(919), new TimeSpan(0, 0, 0, 0, 0)), null, "Vanuatu", (byte)244 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe334"), "VE", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(1077), new TimeSpan(0, 0, 0, 0, 0)), null, "Venezuela", (byte)240 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe335"), "VN", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(1236), new TimeSpan(0, 0, 0, 0, 0)), null, "Viet Nam", (byte)243 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe338"), "WF", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(1394), new TimeSpan(0, 0, 0, 0, 0)), null, "Wallis and Futuna", (byte)245 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe339"), "EH", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(1553), new TimeSpan(0, 0, 0, 0, 0)), null, "Western Sahara", (byte)66 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe340"), "YE", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(1709), new TimeSpan(0, 0, 0, 0, 0)), null, "Yemen", (byte)247 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe341"), "ZM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(1868), new TimeSpan(0, 0, 0, 0, 0)), null, "Zambia", (byte)250 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe342"), "ZW", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(2027), new TimeSpan(0, 0, 0, 0, 0)), null, "Zimbabwe", (byte)251 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe315"), "TL", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(8020), new TimeSpan(0, 0, 0, 0, 0)), null, "Timor-Leste", (byte)223 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe314"), "TH", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(7860), new TimeSpan(0, 0, 0, 0, 0)), null, "Thailand", (byte)220 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe313"), "TZ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(7704), new TimeSpan(0, 0, 0, 0, 0)), null, "Tanzania, United Republic of", (byte)231 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe312"), "TJ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(7544), new TimeSpan(0, 0, 0, 0, 0)), null, "Tajikistan", (byte)221 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe286"), "VC", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(3265), new TimeSpan(0, 0, 0, 0, 0)), null, "Saint Vincent and the Grenadines", (byte)239 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe287"), "WS", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(3424), new TimeSpan(0, 0, 0, 0, 0)), null, "Samoa", (byte)246 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe288"), "SM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(3581), new TimeSpan(0, 0, 0, 0, 0)), null, "San Marino", (byte)204 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe289"), "ST", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(3738), new TimeSpan(0, 0, 0, 0, 0)), null, "Sao Tome and Principe", (byte)209 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe290"), "SA", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(3894), new TimeSpan(0, 0, 0, 0, 0)), null, "Saudi Arabia", (byte)193 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe291"), "SN", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(4052), new TimeSpan(0, 0, 0, 0, 0)), null, "Senegal", (byte)205 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe292"), "CS", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(4257), new TimeSpan(0, 0, 0, 0, 0)), null, "Serbia and Montenegro", (byte)216 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe293"), "SC", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(4422), new TimeSpan(0, 0, 0, 0, 0)), null, "Seychelles", (byte)195 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe294"), "SL", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(4582), new TimeSpan(0, 0, 0, 0, 0)), null, "Sierra Leone", (byte)203 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe295"), "SG", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(4740), new TimeSpan(0, 0, 0, 0, 0)), null, "Singapore", (byte)198 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe296"), "SK", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(4899), new TimeSpan(0, 0, 0, 0, 0)), null, "Slovakia", (byte)202 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe297"), "SI", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(5057), new TimeSpan(0, 0, 0, 0, 0)), null, "Slovenia", (byte)200 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe284"), "LC", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(2947), new TimeSpan(0, 0, 0, 0, 0)), null, "Saint Lucia", (byte)128 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe298"), "SB", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(5243), new TimeSpan(0, 0, 0, 0, 0)), null, "Solomon Islands", (byte)194 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe300"), "ZA", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(5573), new TimeSpan(0, 0, 0, 0, 0)), null, "South Africa", (byte)249 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe301"), "GS", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(5730), new TimeSpan(0, 0, 0, 0, 0)), null, "South Georgia and the South Sandwich Islands", (byte)90 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe302"), "ES", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(5889), new TimeSpan(0, 0, 0, 0, 0)), null, "Spain", (byte)68 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe303"), "LK", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(6046), new TimeSpan(0, 0, 0, 0, 0)), null, "Sri Lanka", (byte)130 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe304"), "SD", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(6204), new TimeSpan(0, 0, 0, 0, 0)), null, "Sudan", (byte)196 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe305"), "SR", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(6362), new TimeSpan(0, 0, 0, 0, 0)), null, "Suriname", (byte)207 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe306"), "SJ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(6519), new TimeSpan(0, 0, 0, 0, 0)), null, "Svalbard and Jan Mayen", (byte)201 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe307"), "SZ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(6676), new TimeSpan(0, 0, 0, 0, 0)), null, "Swaziland", (byte)213 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe308"), "SE", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(6835), new TimeSpan(0, 0, 0, 0, 0)), null, "Sweden", (byte)197 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe225"), "LI", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(3450), new TimeSpan(0, 0, 0, 0, 0)), null, "Liechtenstein", (byte)129 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe310"), "SY", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(7209), new TimeSpan(0, 0, 0, 0, 0)), null, "Syrian Arab Republic", (byte)212 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe311"), "TW", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(7385), new TimeSpan(0, 0, 0, 0, 0)), null, "Taiwan, Province of China", (byte)230 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe299"), "SO", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 708, DateTimeKind.Unspecified).AddTicks(5414), new TimeSpan(0, 0, 0, 0, 0)), null, "Somalia", (byte)206 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe224"), "LY", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(3212), new TimeSpan(0, 0, 0, 0, 0)), null, "Libyan Arab Jamahiriya", (byte)136 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe200"), "IN", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(54), new TimeSpan(0, 0, 0, 0, 0)), null, "India", (byte)105 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe222"), "LS", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(2896), new TimeSpan(0, 0, 0, 0, 0)), null, "Lesotho", (byte)132 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe129"), "BV", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(9273), new TimeSpan(0, 0, 0, 0, 0)), null, "Bouvet Island", (byte)34 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe130"), "BR", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(9432), new TimeSpan(0, 0, 0, 0, 0)), null, "Brazil", (byte)31 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe131"), "IO", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(9594), new TimeSpan(0, 0, 0, 0, 0)), null, "British Indian Ocean Territory", (byte)106 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe132"), "BN", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(9755), new TimeSpan(0, 0, 0, 0, 0)), null, "Brunei Darussalam", (byte)28 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe133"), "BG", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(9914), new TimeSpan(0, 0, 0, 0, 0)), null, "Bulgaria", (byte)22 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe134"), "BF", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(74), new TimeSpan(0, 0, 0, 0, 0)), null, "Burkina Faso", (byte)21 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe135"), "BI", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(233), new TimeSpan(0, 0, 0, 0, 0)), null, "Burundi", (byte)24 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe136"), "KH", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(391), new TimeSpan(0, 0, 0, 0, 0)), null, "Cambodia", (byte)117 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe137"), "CM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(549), new TimeSpan(0, 0, 0, 0, 0)), null, "Cameroon", (byte)47 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe138"), "CA", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(709), new TimeSpan(0, 0, 0, 0, 0)), null, "Canada", (byte)38 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe139"), "CV", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(867), new TimeSpan(0, 0, 0, 0, 0)), null, "Cape Verde", (byte)52 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe140"), "KY", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(1025), new TimeSpan(0, 0, 0, 0, 0)), null, "Cayman Islands", (byte)124 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe141"), "CF", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(1186), new TimeSpan(0, 0, 0, 0, 0)), null, "Central African Republic", (byte)41 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe142"), "TD", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(1345), new TimeSpan(0, 0, 0, 0, 0)), null, "Chad", (byte)217 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe143"), "CL", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(1503), new TimeSpan(0, 0, 0, 0, 0)), null, "Chile", (byte)46 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe144"), "CN", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(1661), new TimeSpan(0, 0, 0, 0, 0)), null, "China", (byte)48 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe145"), "CX", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(1817), new TimeSpan(0, 0, 0, 0, 0)), null, "Christmas Island", (byte)54 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe147"), "CO", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(1974), new TimeSpan(0, 0, 0, 0, 0)), null, "Colombia", (byte)49 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe148"), "KM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(2134), new TimeSpan(0, 0, 0, 0, 0)), null, "Comoros", (byte)119 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe149"), "CG", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(2292), new TimeSpan(0, 0, 0, 0, 0)), null, "Congo", (byte)42 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe151"), "CK", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(2448), new TimeSpan(0, 0, 0, 0, 0)), null, "Cook Islands", (byte)45 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe223"), "LR", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(3054), new TimeSpan(0, 0, 0, 0, 0)), null, "Liberia", (byte)131 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe154"), "HR", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(2766), new TimeSpan(0, 0, 0, 0, 0)), null, "Croatia", (byte)98 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe155"), "CU", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(2955), new TimeSpan(0, 0, 0, 0, 0)), null, "Cuba", (byte)51 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe156"), "CY", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(3168), new TimeSpan(0, 0, 0, 0, 0)), null, "Cyprus", (byte)55 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe128"), "BW", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(9109), new TimeSpan(0, 0, 0, 0, 0)), null, "Botswana", (byte)35 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe127"), "BA", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(8949), new TimeSpan(0, 0, 0, 0, 0)), null, "Bosnia and Herzegovina", (byte)17 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe126"), "BO", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(8788), new TimeSpan(0, 0, 0, 0, 0)), null, "Bolivia", (byte)29 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe125"), "BT", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(8628), new TimeSpan(0, 0, 0, 0, 0)), null, "Bhutan", (byte)33 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe343"), "KR", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(3286), new TimeSpan(0, 0, 0, 0, 0)), null, "Korea", (byte)122 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe100"), "AF", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(4391), new TimeSpan(0, 0, 0, 0, 0)), null, "Afghanistan", (byte)3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"), "AX", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(4575), new TimeSpan(0, 0, 0, 0, 0)), null, "Aland Islands", (byte)15 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"), "AL", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(4747), new TimeSpan(0, 0, 0, 0, 0)), null, "Albania", (byte)6 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"), "DZ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(4923), new TimeSpan(0, 0, 0, 0, 0)), null, "Algeria", (byte)62 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe104"), "AS", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(5090), new TimeSpan(0, 0, 0, 0, 0)), null, "American Samoa", (byte)11 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe105"), "AD", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(5252), new TimeSpan(0, 0, 0, 0, 0)), null, "AndorrA", (byte)1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe106"), "AO", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(5413), new TimeSpan(0, 0, 0, 0, 0)), null, "Angola", (byte)8 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe107"), "AI", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(5575), new TimeSpan(0, 0, 0, 0, 0)), null, "Anguilla", (byte)5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe108"), "AQ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(5737), new TimeSpan(0, 0, 0, 0, 0)), null, "Antarctica", (byte)9 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe109"), "AG", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(5899), new TimeSpan(0, 0, 0, 0, 0)), null, "Antigua and Barbuda", (byte)4 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe110"), "AR", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(6061), new TimeSpan(0, 0, 0, 0, 0)), null, "Argentina", (byte)10 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe157"), "CZ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(3361), new TimeSpan(0, 0, 0, 0, 0)), null, "Czech Republic", (byte)56 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe111"), "AM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(6224), new TimeSpan(0, 0, 0, 0, 0)), null, "Armenia", (byte)7 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe113"), "AU", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(6550), new TimeSpan(0, 0, 0, 0, 0)), null, "Australia", (byte)13 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe114"), "AT", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(6713), new TimeSpan(0, 0, 0, 0, 0)), null, "Austria", (byte)12 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe115"), "AZ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(6877), new TimeSpan(0, 0, 0, 0, 0)), null, "Azerbaijan", (byte)16 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe116"), "BS", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(7036), new TimeSpan(0, 0, 0, 0, 0)), null, "Bahamas", (byte)32 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe117"), "BH", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(7267), new TimeSpan(0, 0, 0, 0, 0)), null, "Bahrain", (byte)23 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe118"), "BD", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(7450), new TimeSpan(0, 0, 0, 0, 0)), null, "Bangladesh", (byte)19 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe119"), "BB", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(7610), new TimeSpan(0, 0, 0, 0, 0)), null, "Barbados", (byte)18 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe120"), "BY", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(7770), new TimeSpan(0, 0, 0, 0, 0)), null, "Belarus", (byte)36 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe121"), "BE", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(7930), new TimeSpan(0, 0, 0, 0, 0)), null, "Belgium", (byte)20 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe122"), "BZ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(8125), new TimeSpan(0, 0, 0, 0, 0)), null, "Belize", (byte)37 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe123"), "BJ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(8305), new TimeSpan(0, 0, 0, 0, 0)), null, "Benin", (byte)25 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe124"), "BM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(8468), new TimeSpan(0, 0, 0, 0, 0)), null, "Bermuda", (byte)27 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe112"), "AW", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 705, DateTimeKind.Unspecified).AddTicks(6388), new TimeSpan(0, 0, 0, 0, 0)), null, "Aruba", (byte)14 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe158"), "DK", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(3525), new TimeSpan(0, 0, 0, 0, 0)), null, "Denmark", (byte)59 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe152"), "CR", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(2608), new TimeSpan(0, 0, 0, 0, 0)), null, "Costa Rica", (byte)50 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe160"), "DM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(3844), new TimeSpan(0, 0, 0, 0, 0)), null, "Dominica", (byte)60 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe191"), "GW", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(8707), new TimeSpan(0, 0, 0, 0, 0)), null, "Guinea-Bissau", (byte)93 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe192"), "GY", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(8872), new TimeSpan(0, 0, 0, 0, 0)), null, "Guyana", (byte)94 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe193"), "HT", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(9028), new TimeSpan(0, 0, 0, 0, 0)), null, "Haiti", (byte)99 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe194"), "HM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(9244), new TimeSpan(0, 0, 0, 0, 0)), null, "Heard Island and Mcdonald Islands", (byte)96 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe159"), "DJ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(3686), new TimeSpan(0, 0, 0, 0, 0)), null, "Djibouti", (byte)58 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe197"), "HK", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(9577), new TimeSpan(0, 0, 0, 0, 0)), null, "Hong Kong", (byte)95 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe198"), "HU", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(9734), new TimeSpan(0, 0, 0, 0, 0)), null, "Hungary", (byte)100 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe199"), "IS", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(9893), new TimeSpan(0, 0, 0, 0, 0)), null, "Iceland", (byte)109 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe201"), "ID", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(211), new TimeSpan(0, 0, 0, 0, 0)), null, "Indonesia", (byte)101 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe203"), "IQ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(369), new TimeSpan(0, 0, 0, 0, 0)), null, "Iraq", (byte)107 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe204"), "IE", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(528), new TimeSpan(0, 0, 0, 0, 0)), null, "Ireland", (byte)102 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe205"), "IM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(687), new TimeSpan(0, 0, 0, 0, 0)), null, "Isle of Man", (byte)104 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe206"), "IL", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(845), new TimeSpan(0, 0, 0, 0, 0)), null, "Israel", (byte)103 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe207"), "IT", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(1001), new TimeSpan(0, 0, 0, 0, 0)), null, "Italy", (byte)110 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe208"), "JM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(1158), new TimeSpan(0, 0, 0, 0, 0)), null, "Jamaica", (byte)112 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe209"), "JP", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(1314), new TimeSpan(0, 0, 0, 0, 0)), null, "Japan", (byte)114 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe210"), "JE", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(1473), new TimeSpan(0, 0, 0, 0, 0)), null, "Jersey", (byte)111 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe211"), "JO", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(1631), new TimeSpan(0, 0, 0, 0, 0)), null, "Jordan", (byte)113 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe212"), "KZ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(1788), new TimeSpan(0, 0, 0, 0, 0)), null, "Kazakhstan", (byte)125 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe213"), "KE", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(1945), new TimeSpan(0, 0, 0, 0, 0)), null, "Kenya", (byte)115 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe214"), "KI", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(2103), new TimeSpan(0, 0, 0, 0, 0)), null, "Kiribati", (byte)118 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe217"), "KW", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(2261), new TimeSpan(0, 0, 0, 0, 0)), null, "Kuwait", (byte)123 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe218"), "KG", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(2422), new TimeSpan(0, 0, 0, 0, 0)), null, "Kyrgyzstan", (byte)116 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe220"), "LV", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(2581), new TimeSpan(0, 0, 0, 0, 0)), null, "Latvia", (byte)135 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe221"), "LB", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 707, DateTimeKind.Unspecified).AddTicks(2739), new TimeSpan(0, 0, 0, 0, 0)), null, "Lebanon", (byte)127 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe190"), "GN", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(8548), new TimeSpan(0, 0, 0, 0, 0)), null, "Guinea", (byte)86 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe189"), "GG", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(8389), new TimeSpan(0, 0, 0, 0, 0)), null, "Guernsey", (byte)81 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe196"), "HN", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(9418), new TimeSpan(0, 0, 0, 0, 0)), null, "Honduras", (byte)97 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe187"), "GU", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(8072), new TimeSpan(0, 0, 0, 0, 0)), null, "Guam", (byte)92 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe161"), "DO", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(4004), new TimeSpan(0, 0, 0, 0, 0)), null, "Dominican Republic", (byte)61 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe162"), "EC", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(4208), new TimeSpan(0, 0, 0, 0, 0)), null, "Ecuador", (byte)63 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe163"), "EG", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(4377), new TimeSpan(0, 0, 0, 0, 0)), null, "Egypt", (byte)65 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe164"), "SV", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(4534), new TimeSpan(0, 0, 0, 0, 0)), null, "El Salvador", (byte)210 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe165"), "GQ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(4692), new TimeSpan(0, 0, 0, 0, 0)), null, "Equatorial Guinea", (byte)88 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe166"), "ER", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(4847), new TimeSpan(0, 0, 0, 0, 0)), null, "Eritrea", (byte)67 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe167"), "EE", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(5007), new TimeSpan(0, 0, 0, 0, 0)), null, "Estonia", (byte)64 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe188"), "GT", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(8231), new TimeSpan(0, 0, 0, 0, 0)), null, "Guatemala", (byte)91 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe170"), "FO", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(5323), new TimeSpan(0, 0, 0, 0, 0)), null, "Faroe Islands", (byte)74 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe171"), "FJ", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(5479), new TimeSpan(0, 0, 0, 0, 0)), null, "Fiji", (byte)71 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe172"), "FI", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(5638), new TimeSpan(0, 0, 0, 0, 0)), null, "Finland", (byte)70 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe173"), "FR", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(5841), new TimeSpan(0, 0, 0, 0, 0)), null, "France", (byte)75 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe174"), "GF", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(6014), new TimeSpan(0, 0, 0, 0, 0)), null, "French Guiana", (byte)80 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe168"), "ET", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(5167), new TimeSpan(0, 0, 0, 0, 0)), null, "Ethiopia", (byte)69 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe176"), "TF", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(6331), new TimeSpan(0, 0, 0, 0, 0)), null, "French Southern Territories", (byte)218 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe177"), "GA", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(6489), new TimeSpan(0, 0, 0, 0, 0)), null, "Gabon", (byte)76 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe178"), "GM", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(6649), new TimeSpan(0, 0, 0, 0, 0)), null, "Gambia", (byte)85 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe179"), "GE", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(6809), new TimeSpan(0, 0, 0, 0, 0)), null, "Georgia", (byte)79 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe180"), "DE", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(6967), new TimeSpan(0, 0, 0, 0, 0)), null, "Germany", (byte)57 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe181"), "GH", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(7126), new TimeSpan(0, 0, 0, 0, 0)), null, "Ghana", (byte)82 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe182"), "GI", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(7281), new TimeSpan(0, 0, 0, 0, 0)), null, "Gibraltar", (byte)83 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe183"), "GR", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(7439), new TimeSpan(0, 0, 0, 0, 0)), null, "Greece", (byte)89 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe184"), "GL", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(7594), new TimeSpan(0, 0, 0, 0, 0)), null, "Greenland", (byte)84 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe185"), "GD", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(7755), new TimeSpan(0, 0, 0, 0, 0)), null, "Grenada", (byte)78 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe186"), "GP", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(7912), new TimeSpan(0, 0, 0, 0, 0)), null, "Guadeloupe", (byte)87 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe175"), "PF", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 706, DateTimeKind.Unspecified).AddTicks(6171), new TimeSpan(0, 0, 0, 0, 0)), null, "French Polynesia", (byte)175 }
                });

            migrationBuilder.InsertData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe108"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(9613), new TimeSpan(0, 0, 0, 0, 0)), null, "Sunday", (byte)8 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe107"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(9450), new TimeSpan(0, 0, 0, 0, 0)), null, "Saturday", (byte)7 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe106"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(9271), new TimeSpan(0, 0, 0, 0, 0)), null, "Friday", (byte)6 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe105"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(9060), new TimeSpan(0, 0, 0, 0, 0)), null, "Thursday", (byte)5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(7978), new TimeSpan(0, 0, 0, 0, 0)), null, "Everyday", (byte)1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(8728), new TimeSpan(0, 0, 0, 0, 0)), null, "Tuesday", (byte)3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(8550), new TimeSpan(0, 0, 0, 0, 0)), null, "Monday", (byte)2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe104"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(8896), new TimeSpan(0, 0, 0, 0, 0)), null, "Wednesday", (byte)4 }
                });

            migrationBuilder.InsertData(
                schema: "System",
                table: "WorldGenderEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(4513), new TimeSpan(0, 0, 0, 0, 0)), null, "Male", (byte)1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"), new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 709, DateTimeKind.Unspecified).AddTicks(5118), new TimeSpan(0, 0, 0, 0, 0)), null, "Female", (byte)2 }
                });

            migrationBuilder.InsertData(
                schema: "System",
                table: "WorldLanguageEntity",
                columns: new[] { "EntityId", "Code", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"), "en", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 701, DateTimeKind.Unspecified).AddTicks(675), new TimeSpan(0, 0, 0, 0, 0)), null, "English", (byte)2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"), "ru", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 700, DateTimeKind.Unspecified).AddTicks(9985), new TimeSpan(0, 0, 0, 0, 0)), null, "Russian", (byte)1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"), "de", new DateTimeOffset(new DateTime(2021, 5, 4, 20, 14, 50, 701, DateTimeKind.Unspecified).AddTicks(858), new TimeSpan(0, 0, 0, 0, 0)), null, "German", (byte)3 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_AccountRoleEntity_Name",
                schema: "System",
                table: "AccountRoleEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_AccountRoleEntity_TypeId",
                schema: "System",
                table: "AccountRoleEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AchievementEntity_MethodId",
                schema: "System",
                table: "AchievementEntity",
                column: "MethodId");

            migrationBuilder.CreateIndex(
                name: "IX_AchievementEntity_Name",
                schema: "System",
                table: "AchievementEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_AchievementEntity_TypeId",
                schema: "System",
                table: "AchievementEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_BonusCaseEntity_Name",
                schema: "System",
                table: "BonusCaseEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_BonusCaseEntity_TypeId",
                schema: "System",
                table: "BonusCaseEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CaseEntity_Name",
                schema: "System",
                table: "CaseEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_CaseItemEntity_CaseId",
                schema: "System",
                table: "CaseItemEntity",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseItemEntity_ItemId",
                schema: "System",
                table: "CaseItemEntity",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_EveryDayAwardEntity_CaseId",
                schema: "System",
                table: "EveryDayAwardEntity",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_FractionEntity_Name",
                schema: "System",
                table: "FractionEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_FractionEntity_TypeId",
                schema: "System",
                table: "FractionEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameCurrencyEntity_Name",
                schema: "System",
                table: "GameCurrencyEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_GameCurrencyEntity_TypeId",
                schema: "System",
                table: "GameCurrencyEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameItemCategoryEntity_Name",
                schema: "System",
                table: "GameItemCategoryEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemCategoryEntity_TypeId",
                schema: "System",
                table: "GameItemCategoryEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_CategoryId",
                schema: "System",
                table: "GameItemEntity",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_CurrencyId",
                schema: "System",
                table: "GameItemEntity",
                column: "CurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_DisplayPosition",
                schema: "System",
                table: "GameItemEntity",
                column: "DisplayPosition");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_FractionId",
                schema: "System",
                table: "GameItemEntity",
                column: "FractionId");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_Level",
                schema: "System",
                table: "GameItemEntity",
                column: "Level");

            migrationBuilder.CreateIndex(
                name: "IX_NewLevelAwardEntity_CaseId",
                schema: "System",
                table: "NewLevelAwardEntity",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_WorldCountryEntity_Code",
                schema: "System",
                table: "WorldCountryEntity",
                column: "Code");

            migrationBuilder.CreateIndex(
                name: "IX_WorldCountryEntity_Name",
                schema: "System",
                table: "WorldCountryEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_WorldCountryEntity_TypeId",
                schema: "System",
                table: "WorldCountryEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorldDayOfWeekEntity_Name",
                schema: "System",
                table: "WorldDayOfWeekEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_WorldDayOfWeekEntity_TypeId",
                schema: "System",
                table: "WorldDayOfWeekEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorldGenderEntity_Name",
                schema: "System",
                table: "WorldGenderEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_WorldGenderEntity_TypeId",
                schema: "System",
                table: "WorldGenderEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_WorldLanguageEntity_Code",
                schema: "System",
                table: "WorldLanguageEntity",
                column: "Code");

            migrationBuilder.CreateIndex(
                name: "IX_WorldLanguageEntity_Name",
                schema: "System",
                table: "WorldLanguageEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_WorldLanguageEntity_TypeId",
                schema: "System",
                table: "WorldLanguageEntity",
                column: "TypeId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AccountRoleEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "AchievementEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "BonusCaseEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "CaseItemEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "EveryDayAwardEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "NewLevelAwardEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "WorldCountryEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "WorldDayOfWeekEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "WorldGenderEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "WorldLanguageEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "GameItemEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "CaseEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "FractionEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "GameCurrencyEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "GameItemCategoryEntity",
                schema: "System");
        }
    }
}
