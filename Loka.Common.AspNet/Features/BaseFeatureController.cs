﻿using AutoMapper;
using Loka.Common.MediatR.Executors;

namespace Loka.Common.AspNet.Features;

public sealed class ControllerServices<TController, TGrain> : ControllerServices<TController>
    where TController : class
    where TGrain : IGrainWithGuidKey
{
    public TGrain Grain { get; }


    public ControllerServices(ILogger<TController> logger, IClusterClient orleansClient, IMapper mapper, TGrain grain) : base(logger, orleansClient, mapper)
    {
        Grain = grain;
    }
}

public class ControllerServices<TController>
    where TController : class
{
    public ILogger<TController> Logger { get; }
    public IClusterClient ClusterClient { get; }
    public IMapper Mapper { get; }

    public ControllerServices
    (
        ILogger<TController> logger,
        IClusterClient orleansClient,
        IMapper mapper
    )
    {
        Logger = logger;
        ClusterClient = orleansClient;
        Mapper = mapper;
    }
}