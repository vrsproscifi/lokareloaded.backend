﻿using Loka.Inventory.Interfaces.Workbench;

namespace Loka.Api.Features.Workbench;

internal sealed class PlayerWorkbenchController : PlayerWorkbench.PlayerWorkbenchBase
{
    private ControllerServices<PlayerWorkbenchController> Services { get; }
    private PlayerActor PlayerActor { get; }

    public PlayerWorkbenchController(ControllerServices<PlayerWorkbenchController> services, PlayerActor playerActor)
    {
        Services = services;
        PlayerActor = playerActor;
    }

    public override Task<CraftWorkbenchItem.Types.Response> CraftWorkbenchItem(CraftWorkbenchItem.Types.Query request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerWorkbenchGrain>(PlayerActor.PlayerId);
        return grain.CraftWorkbenchItem(new Loka.Inventory.Interfaces.Workbench.CraftWorkbenchItem.Request()
        {
            ModelId =request.ModelId.ToGuid(),
            Count = request.Count
        }).ReturnResult<Loka.Inventory.Interfaces.Workbench.CraftWorkbenchItem.Result, CraftWorkbenchItem.Types.Response>(Services.Mapper);
    }
}