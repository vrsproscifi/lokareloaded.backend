﻿using System.Linq.Expressions;
using Loka.Database.Common.Repositories;
using Loka.Inventory.DataBase;
using Loka.Inventory.DataBase.Entities.Currencies;
using Loka.Inventory.Interfaces.Resources;
using Loka.Store.Interfaces.Repositories;

namespace Loka.Inventory.Grains.Resources;

internal sealed class PlayerResourcesRepository : BaseChildRepository<InventoryDbContext, PlayerResourceEntity, PlayerResourceModel.Lightweight>.Raw, IPlayerResourcesRepository
{
    private IGameResourcesRepository GameResourcesRepository { get; }

    public PlayerResourcesRepository
    (
        InventoryDbContext dbContext,
        IMapper mapper,
        IGameResourcesRepository gameResourcesRepository
    ) : base( dbContext, mapper)
    {
        GameResourcesRepository = gameResourcesRepository;
    }

    protected override Func<Guid, Expression<Func<PlayerResourceEntity, bool>>> Predicate { get; } = playerId => entity => entity.PlayerId == playerId;

    protected override async Task<OperationResult> AfterMap(Guid playerId, PlayerResourceEntity entity, PlayerResourceModel.Lightweight model)
    {
        var getResourceResult = await GameResourcesRepository.GetById(entity.ModelId);
        if (!getResourceResult.GetValueIfSucceeded(out var resource))
            return getResourceResult.Error!;

        model.Resource = resource;
        return await base.AfterMap(playerId, entity, model);
    }

    public async Task<OperationResult<PlayerResourceModel.Lightweight>> GetByType(Guid playerId, Guid typeId)
    {
        var entity = await DbSet
            .FirstOrDefaultAsync(q => q.PlayerId == playerId && q.ModelId == typeId);

        if (entity == null)
            return OperationResultCode.EntityNotFound;

        var model = Mapper.Map<PlayerResourceModel.Lightweight>(entity);
        await AfterMap(playerId, entity, model);

        return model;
    }

    public async Task<PlayerResourceModel.Lightweight> GetOrCreateByType(Guid playerId, Guid typeId)
    {
        var entity = await DbSet
            .FirstOrDefaultAsync(q => q.PlayerId == playerId && q.ModelId == typeId);

        if (entity == null)
        {
            var resource = await GameResourcesRepository.GetById(typeId);
            var e = await DbSet.AddAsync(new PlayerResourceEntity()
            {
                EntityId = Guid.NewGuid(),
                CreatedAt = DateTimeOffset.UtcNow,
                ModelId = typeId,
                PlayerId = playerId,
                Amount = resource.IsSucceeded ? resource.Result.DefaultValue : 0
            });

            await SaveChanges();
            entity = e.Entity;
        }

        var model = Mapper.Map<PlayerResourceModel.Lightweight>(entity);
        await AfterMap(playerId, entity, model);

        return model;
    }

    public async Task<PlayerResourceModel.Delta> Update(Guid playerId, UpdatePlayerResourceRequest request)
    {
        var entity = await DbSet
            .FirstAsync(q => q.PlayerId == playerId && q.EntityId == request.ResourceId);

        var getResourceResult = await GameResourcesRepository.GetById(entity.ModelId);
        var result = new PlayerResourceModel.Delta
        {
            Id = entity.EntityId,
            PreviousBalance = entity.Amount,
            CurrentBalance = request.Value,
            Resource = getResourceResult.Result!
        };

        entity.Amount = result.CurrentBalance;
        await SaveChanges();
        return result;
    }

    public async Task<IReadOnlyList<PlayerResourceModel.Delta>> Update(Guid playerId, IReadOnlyList<UpdatePlayerResourceRequest> requests)
    {
        var ids = requests.Select(q => q.ResourceId).ToArray();

        var entities = await DbSet
            .Where(q => q.PlayerId == playerId && ids.Contains(q.EntityId))
            .ToArrayAsync();

        var results = new List<PlayerResourceModel.Delta>(entities.Length);
        foreach (var request in requests)
        {
            var entity = entities.First(q => q.EntityId == request.ResourceId);
            var getResourceResult = await GameResourcesRepository.GetById(entity.ModelId);
            var result = new PlayerResourceModel.Delta
            {
                Id = entity.EntityId,
                PreviousBalance = entity.Amount,
                CurrentBalance = request.Value,
                Resource = getResourceResult.Result!
            };

            entity.Amount = result.CurrentBalance;
            results.Add(result);
        }

        await SaveChanges();
        return results;
    }
}