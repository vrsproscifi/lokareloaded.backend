﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Common.Models.GamePlay;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Extensions;
using Loka.Deployment.DataBase;
using Loka.MatchMaking.DataBase;

namespace Loka.Admin.Common.Services.GamePlay.GameVersion;

internal sealed class GameVersionService : IGameVersionService
{
    private DeploymentDbContext DeploymentDbContext { get; }

    public GameVersionService(DeploymentDbContext deploymentDbContext)
    {
        DeploymentDbContext = deploymentDbContext;
    }

    public async Task<OperationResult<GameVersionInformationModel>> GetById(Guid id, CancellationToken cancellationToken)
    {
        var entity = await DeploymentDbContext.GameVersionEntity.GetById(id, cancellationToken);
        if (entity == null)
            return OperationResultCode.PlayerNotFound;

        var model = new GameVersionInformationModel
        {
            Id = entity.EntityId,
            CreatedAt = entity.CreatedAt,
            Channel = entity.Channel,
            GameVersion = entity.Version,
            ServerVersion = entity.ServerVersionEntity.EntityId,
        };

        return model;
    }
}