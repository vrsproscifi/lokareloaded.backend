﻿using System.Reflection;

namespace Loka.Common.ClickHouse;

public class ClickhouseTableColumn
{
    public ClickhouseTableColumn(PropertyInfo property, string name, ClickhouseColumnType type, bool ignoreInsert = false, bool nullable = false)
    {
        Name = name;
        Type = type;
        Property = property;
        IgnoreInsert = ignoreInsert;
        Nullable = nullable;
    }

    public PropertyInfo Property { get; }
    public string Name { get; }
    public ClickhouseColumnType Type { get; }
    public bool Nullable { get; }
    public bool IgnoreInsert { get; }
}