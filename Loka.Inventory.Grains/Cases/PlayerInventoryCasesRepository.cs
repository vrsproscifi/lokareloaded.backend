﻿using System.Linq.Expressions;
using Loka.Database.Common.Repositories;
using Loka.Inventory.DataBase;
using Loka.Inventory.DataBase.Entities.Cases;
using Loka.Inventory.Interfaces.Cases;

namespace Loka.Inventory.Grains.Cases;

internal sealed class PlayerInventoryCasesRepository : BaseChildRepository<InventoryDbContext, PlayerCaseEntity, PlayerInventoryCaseModel>.Raw, IPlayerInventoryCasesRepository
{
    protected override Func<Guid, Expression<Func<PlayerCaseEntity, bool>>> Predicate { get; } = playerId => entity => entity.PlayerId == playerId;

    public PlayerInventoryCasesRepository(InventoryDbContext dbContext, IMapper mapper) : base(dbContext, mapper)
    {
    }

    public async Task<PlayerInventoryCaseModel> AddOrStack(Guid playerId, AddOrStackInventoryCase.Request request)
    {
        var entity = await DbSet
            .Where(Predicate(playerId))
            .FirstOrDefaultAsync(q => q.CaseId == request.ModelId);

        if (entity == null)
        {
            entity = new PlayerCaseEntity()
            {
                EntityId = Guid.NewGuid(),
                PlayerId = playerId,
                CaseId = request.ModelId,
                Amount = request.Amount,
                CreatedAt = DateTimeOffset.UtcNow,
            };
            await DbSet.AddAsync(entity);
            await SaveChanges();
        }
        else
        {
            entity.Amount += request.Amount;
            await SaveChanges();
        }
        
        return new PlayerInventoryCaseModel()
        {
            Id = entity.EntityId,
            ModelId = entity.CaseId,
            Amount = entity.Amount,
        };
    }
}