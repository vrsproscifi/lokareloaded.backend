﻿using Loka.Inventory.Interfaces.Resources;

namespace Loka.Api.Features.Inventory.Resources;

internal sealed class PlayerInventoryResourcesController : PlayerInventoryResources.PlayerInventoryResourcesBase
{
    private ControllerServices<PlayerInventoryResourcesController> Services { get; }
    private PlayerActor PlayerActor { get; }

    public PlayerInventoryResourcesController(ControllerServices<PlayerInventoryResourcesController> services, PlayerActor playerActor)
    {
        Services = services;
        PlayerActor = playerActor;
    }

    public override Task<GetPlayerInventoryResources.Types.Response> GetPlayerInventoryResources(GetPlayerInventoryResources.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerInventoryResourcesGrain>(PlayerActor.PlayerId);
        return grain.GetResources().ReturnResult<Loka.Inventory.Interfaces.Resources.GetPlayerResources.Result, GetPlayerInventoryResources.Types.Response>(Services.Mapper);
    }
}