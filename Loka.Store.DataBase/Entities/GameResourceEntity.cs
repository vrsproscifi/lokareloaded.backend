﻿namespace Loka.Store.DataBase.Entities;

[Index(nameof(Name))]
[Index(nameof(TypeId), IsUnique = true)]
public class GameResourceEntity : BaseEntity, INamedEntity, ITypedEntity<ResourceTypeId>
{
    public string Name { get; set; } = null!;
    public ResourceTypeId TypeId { get; set; }
    public int DefaultValue { get; init; }

    public GameResourceEntity(){}

    internal GameResourceEntity(Guid id, ResourceTypeId typeId, int defaultValue) : base(id)
    {
        EntityId = id;
        TypeId = typeId;
        DefaultValue = defaultValue;
        Name = TypeId.ToString();
    }
}