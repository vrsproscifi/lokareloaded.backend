﻿namespace Loka.Inventory.Interfaces.Payments;

public static class PayForItem
{
    [GenerateSerializer]
    public sealed class Request
    {
        [Id(0)]
        public Guid ModelId { get; init; }

        [Id(1)]
        public Guid ResourceId { get; init; }

        [Id(2)]
        public int Price { get; init; }

        [Id(3)]
        public PlayerPaymentTransactionTypeId TypeId { get; init; }
    }
}