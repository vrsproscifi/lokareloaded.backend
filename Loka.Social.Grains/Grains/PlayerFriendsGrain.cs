﻿using Loka.Common.Authentication;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;
using Loka.Common.Orleans.Grains;
using Loka.Database.Common.Extensions;
using Loka.Game.Interfaces.Players;
using Loka.Game.Interfaces.Players.Dtos;
using Loka.Identity.Database;
using Loka.Social.DataBase;
using Loka.Social.Interfaces;
using Loka.Social.Interfaces.Forms;
using Microsoft.EntityFrameworkCore;
using Orleans;

namespace Loka.Social.Grains.Grains;

internal sealed class PlayerFriendsGrain : Grain, IPlayerFriendsGrain, IWithPlayerActor
{
    public PlayerActor Actor { get; set; }

    private GameIdentityDbContext IdentityDbContext { get; }
    private SocialDbContext SocialDbContext { get; }
    private IPlayersService PlayersService { get; }

    public PlayerFriendsGrain
    (
        GameIdentityDbContext identityDbContext,
        SocialDbContext socialDbContext,
        IPlayersService playersService)
    {
        IdentityDbContext = identityDbContext;
        SocialDbContext = socialDbContext;
        PlayersService = playersService;
    }

    public async Task<SearchPlayers.Result> SearchPlayers(SearchPlayers.Request request)
    {
        throw new NotImplementedException();
    }

    public async Task<GetPlayerFriendsList.Result> GetPlayerFriendsList(GetPlayerFriendsList.Request request)
    {
        var availableFriends = await SocialDbContext.PlayerFriendEntity
            .Where(friend => friend.PlayerId == Actor.PlayerId)
            .ToArrayAsync();

        var availableFriendsIds = availableFriends.SelectEntityIds();

        var players = await PlayersService.GetByIdsAndName(availableFriendsIds, request.Term);
        var Responses = players.Select(player =>
        {
            var friend = availableFriends.First(f => f.FriendId == player.Id);

            return new PlayerFriendEntryModel
            {
                FriendId = friend.EntityId,
                //Group = friend.State == PlayerFriendState.Blocked ? AddPlayerToFriendListGroup.Block : AddPlayerToFriendListGroup.Friend,
                Player = player,
            };
        }).ToArray();

        return new GetPlayerFriendsList.Result()
        {
            Entries = Responses
        };
    }

    public async Task<OperationResult<PlayerModel>> AddPlayerToFriendList(AddPlayerToFriendListRequest request)
    {
        var player = await PlayersService.GetById(request.PlayerId);
        if (player == null)
            return OperationResultCode.PlayerNotFound;

        /*if (request.Group == AddPlayerToFriendListGroup.Block)
        {
            await SocialDbContext.PlayerFriendEntity.AddAsync(new PlayerFriendEntity(Actor.PlayerId, Guid.Parse(request.PlayerId), PlayerFriendState.Blocked));
        }
        else
        {
            await SocialDbContext.PlayerFriendEntity.AddAsync(new PlayerFriendEntity(Guid.Parse(request.PlayerId), Actor.PlayerId, PlayerFriendState.ConfirmedWaiting));
            await SocialDbContext.PlayerFriendEntity.AddAsync(new PlayerFriendEntity(Actor.PlayerId, Guid.Parse(request.PlayerId), PlayerFriendState.Waiting));
        }*/

        return player;
    }

    public async Task<OperationResult<RespondForInviteResponse>> RespondForInvite(RespondForInviteRequest request)
    {
        throw new System.NotImplementedException();
    }

    public async Task<OperationResult<SearchPlayersResponse>> SearchPlayers(SearchPlayersRequest request)
    {
        var players = await PlayersService.SearchByName(request.Term);

        return new SearchPlayersResponse()
        {
            Players = players
        };
    }

    public async Task<OperationResult<PlayerModel>> RemoveFriend(RemoveFriendRequest request)
    {
        throw new System.NotImplementedException();
    }
}