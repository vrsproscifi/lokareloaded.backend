﻿using Loka.Database.Common.Repositories;
using Loka.Store.Interfaces.Models;
using Loka.Store.Interfaces.Models.Fractions;

namespace Loka.Store.Interfaces.Repositories;

public interface IGameStoreFractionsRepository : IBaseRepository<StoreFractionModel>
{

}