﻿using Loka.Common.Authentication;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Errors;
using Loka.Common.Operations.Results;
using Loka.Common.Orleans.Grains;
using Orleans.Runtime;

namespace Loka.Api.Orleans.Extensions;

public class PlayerActorCallFilter : IIncomingGrainCallFilter
{
    public Task Invoke(IIncomingGrainCallContext context)
    {
        if (context.Grain is IWithPlayerActor actorGrainInterface && context.Grain is IGrainWithGuidKey gain)
        {
            var playerName = RequestContext.Get(nameof(PlayerActor.PlayerName)) as string;

            var actorStorage = context.TargetContext.ActivationServices.GetRequiredService<PlayerActorStorage>();

            actorGrainInterface.Actor = actorStorage.Actor = new PlayerActor
            (
                gain.GetPrimaryKey(),
                playerName
            );
        }

        return context.Invoke();
    }
}

public class ExceptionCallFilter : IIncomingGrainCallFilter, IOutgoingGrainCallFilter
{
    public async Task Invoke(IIncomingGrainCallContext context)
    {
        try
        {
            await context.Invoke();
        }
        catch (Exception e)
        {
            context.Result = new OperationResult(new OperationError(OperationResultCode.ExceptionError, e.ToString()));
        }
    }

    public async Task Invoke(IOutgoingGrainCallContext context)
    {
        try
        {
            await context.Invoke();
        }
        catch (Exception e)
        {
            context.Result = new OperationResult(new OperationError(OperationResultCode.ExceptionError, e.ToString()));
        }
    }
}