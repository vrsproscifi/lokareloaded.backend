﻿using Loka.Store.DataBase.Entities.Items;

namespace Loka.Store.DataBase.Seeders;

public static class GameItemCategoryEntitySeed
{
    public static Guid Weapon { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe901");
    public static Guid Armour { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe902");
    public static Guid Character { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe904");
    public static Guid Profile { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe905");
    public static Guid Material { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe906");
    public static Guid Grenade { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe907");
    public static Guid Kits { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe908");

    public static GameItemCategoryEntity[] Entities { get; } = new[]
    {
        new GameItemCategoryEntity(Weapon, ItemCategoryTypeId.Weapon),
        new GameItemCategoryEntity(Armour, ItemCategoryTypeId.Armour),
        new GameItemCategoryEntity(Character, ItemCategoryTypeId.Character),
        new GameItemCategoryEntity(Profile, ItemCategoryTypeId.Profile),
        new GameItemCategoryEntity(Material, ItemCategoryTypeId.Material),
        new GameItemCategoryEntity(Grenade, ItemCategoryTypeId.Grenade),
        new GameItemCategoryEntity(Kits, ItemCategoryTypeId.Kits),
    };
}