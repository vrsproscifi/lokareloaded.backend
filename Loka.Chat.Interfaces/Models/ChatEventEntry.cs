﻿using Loka.Chat.Interfaces.Enums;
using MongoDB.Bson;

namespace Loka.Chat.Interfaces.Models;

[GenerateSerializer]
public sealed class ChatEventEntry
{
    [Id(0)]
    public ObjectId Id { get; set; }

    [Id(1)]
    public ChatMemberEntry Sender { get; set; } = null!;

    [Id(2)]
    public ChatChannelEventType TypeId { get; set; }

    [Id(3)]
    public DateTime CreatedAt { get; set; }
    
    [Id(4)]
    public string? Value { get; set; }
}