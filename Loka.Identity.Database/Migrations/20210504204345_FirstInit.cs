﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Loka.Identity.Database.Migrations
{
    public partial class FirstInit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Identity");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:pgcrypto", ",,")
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", ",,");

            migrationBuilder.CreateTable(
                name: "PlayerAccountEntity",
                schema: "Identity",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    GooglePlayPlayerId = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    Email = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: true),
                    EmailConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    RoleId = table.Column<Guid>(type: "uuid", nullable: true),
                    CountryId = table.Column<Guid>(type: "uuid", nullable: true),
                    LanguageId = table.Column<Guid>(type: "uuid", nullable: true),
                    PremiumEndDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerAccountEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "PlayerProfileEntity",
                schema: "Identity",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    DisplayName = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    Level = table.Column<int>(type: "integer", nullable: false),
                    LastLevel = table.Column<int>(type: "integer", nullable: false),
                    Experience = table.Column<long>(type: "bigint", nullable: false),
                    IsLevelUpNotified = table.Column<bool>(type: "boolean", nullable: false),
                    IsNameConfirmed = table.Column<bool>(type: "boolean", nullable: false),
                    LastActivityDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    PrimeMatchesCount = table.Column<short>(type: "smallint", nullable: false),
                    PrimeMatchesNextDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerProfileEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PlayerProfileEntity_PlayerAccountEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "Identity",
                        principalTable: "PlayerAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "PlayerStatisticsEntity",
                schema: "Identity",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Elo = table.Column<short>(type: "smallint", nullable: false),
                    Score = table.Column<long>(type: "bigint", nullable: false),
                    Kills = table.Column<long>(type: "bigint", nullable: false),
                    Deaths = table.Column<long>(type: "bigint", nullable: false),
                    Supports = table.Column<long>(type: "bigint", nullable: false),
                    Wins = table.Column<long>(type: "bigint", nullable: false),
                    Loses = table.Column<long>(type: "bigint", nullable: false),
                    Draws = table.Column<long>(type: "bigint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerStatisticsEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PlayerStatisticsEntity_PlayerAccountEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "Identity",
                        principalTable: "PlayerAccountEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_PlayerAccountEntity_Email",
                schema: "Identity",
                table: "PlayerAccountEntity",
                column: "Email");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerAccountEntity_GooglePlayPlayerId",
                schema: "Identity",
                table: "PlayerAccountEntity",
                column: "GooglePlayPlayerId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PlayerProfileEntity_DisplayName",
                schema: "Identity",
                table: "PlayerProfileEntity",
                column: "DisplayName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlayerProfileEntity",
                schema: "Identity");

            migrationBuilder.DropTable(
                name: "PlayerStatisticsEntity",
                schema: "Identity");

            migrationBuilder.DropTable(
                name: "PlayerAccountEntity",
                schema: "Identity");
        }
    }
}
