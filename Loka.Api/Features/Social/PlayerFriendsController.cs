﻿using Loka.Social.Interfaces;
using Loka.Social.Interfaces.Forms;

namespace Loka.Api.Features.Social;

internal sealed class PlayerFriendsController : PlayerFriends.PlayerFriendsBase
{
    private ControllerServices<PlayerFriendsController> Services { get; }
    private PlayerActor PlayerActor { get; }

    public PlayerFriendsController(ControllerServices<PlayerFriendsController> services, PlayerActor playerActor)
    {
        Services = services;
        PlayerActor = playerActor;
    }

    public override async Task<ListFriendsModel> ListFriends(ListFriendsQuery request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerFriendsGrain>(PlayerActor.PlayerId);
        var result = await grain.GetPlayerFriendsList(new GetPlayerFriendsList.Request()
        {
            Term = request.Name
        });
        return Services.Mapper.Map<ListFriendsModel>(result);
    }

    /*public override Task<PlayeRes> AddPlayerToFriendList(AddPlayerToFriendListQuery request, ServerCallContext context)
    {
        return Services.Grain.AddPlayerToFriendList(new AddPlayerToFriendListForm()
        {
            PlayerId = Guid.Parse(request.PlayerId),
            //Group = AddPlayerToFriendListGroup.Block
        }).ReturnResult<PlayerResponseDto, PlayerResponse>(Services.Mapper);
    }*/

    public override Task<RespondForInviteModel> RespondForInvite(RespondForInviteQuery request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerFriendsGrain>(PlayerActor.PlayerId);
        return grain.RespondForInvite(new RespondForInviteRequest()
        {
        }).ReturnResult<RespondForInviteResponse, RespondForInviteModel>(Services.Mapper);
    }

    public override Task<SearchPlayersModel> SearchPlayers(SearchPlayersQuery request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerFriendsGrain>(PlayerActor.PlayerId);
        return grain.SearchPlayers(new SearchPlayersRequest()).ReturnResult<SearchPlayersResponse, SearchPlayersModel>(Services.Mapper);
    }
/*
        public override Task<PlayerResponse> RemoveFriend(RemoveFriendQuery request, ServerCallContext context)
        {
            return Services.Grain.RemoveFriend(new RemoveFriendForm()).ReturnResult<PlayerResponseDto, PlayerResponse>(Services.Mapper);
        }*/
}