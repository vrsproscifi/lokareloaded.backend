﻿using System.ComponentModel.DataAnnotations;
using System.Diagnostics;

namespace Loka.Common.Pagination;

[GenerateSerializer]
[DebuggerDisplay("Offset: {Offset}, Count: {Take}")]
public readonly struct PagingQuery
{
    /// <summary>
    /// <para>Num pages offset</para>
    /// </summary>
    [Id(0)]
    [Range(0, int.MaxValue)]
    public int Offset { get; }

    /// <summary>
    /// <para>Num items per page</para>
    /// </summary>
    [Id(1)]
    [Range(10, int.MaxValue)]
    public int Take { get; }

    public PagingQuery(int take, int offset)
    {
        Take = take;
        Offset = offset;
    }
        
    public PagingQuery(uint count, uint offset)
    {
        Take = (int) count;
        Offset = (int) offset;
    }
}