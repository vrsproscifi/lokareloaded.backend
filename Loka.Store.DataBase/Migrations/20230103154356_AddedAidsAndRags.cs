﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Loka.Store.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class AddedAidsAndRags : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "FractionId",
                value: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"));

            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameItemEntity",
                columns: new[] { "EntityId", "AdminComment", "AmountDefault", "AmountInStack", "AmountMaximum", "AmountMinimum", "CategoryId", "CreatedAt", "DeletedAt", "DisplayPosition", "Duration", "Flags", "FractionId", "Level", "Name", "SellCurrencyId", "SellPrice", "SubscribeCurrencyId", "SubscribePrice" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe911"), null, 1L, -1L, null, 1L, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 0, null, (byte)0, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"), 0, "AID 1", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 500, null, 0 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe912"), null, 1L, -1L, null, 1L, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, 0, null, (byte)0, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"), 0, "AID 2", new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), 750, null, 0 }
                });

            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameResourceEntity",
                columns: new[] { "EntityId", "CreatedAt", "DefaultValue", "DeletedAt", "Name", "TypeId" },
                values: new object[] { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe204"), new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), 1000, null, "Rags", 7 });

            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameItemBlueprintEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "ItemModelId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe911"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe911") },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe912"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe912") }
                });

            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                columns: new[] { "EntityId", "Amount", "BlueprintId", "CreatedAt", "DeletedAt", "ResourceId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe915"), 10, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe911"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe204") },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe916"), 25, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe912"), new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe204") }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe915"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe916"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemBlueprintEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe911"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemBlueprintEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe912"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe204"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe911"));

            migrationBuilder.DeleteData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe912"));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "FractionId",
                value: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"));
        }
    }
}
