﻿using System;
using Loka.MatchMaking.DataBase.Entities.Queue;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Loka.MatchMaking.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class Migrationzz : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<PlayerQueueOptions>(
                name: "Options",
                schema: "MatchMaking",
                table: "PlayerQueueEntity",
                type: "jsonb",
                nullable: false,
                oldClrType: typeof(PlayerQueueOptions),
                oldType: "jsonb",
                oldNullable: true);

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameMapEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe602"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe603"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe604"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeMapEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeMapEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeMapEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe301"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeMapEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe401"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe602"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe603"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe604"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe605"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe606"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe607"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe608"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<PlayerQueueOptions>(
                name: "Options",
                schema: "MatchMaking",
                table: "PlayerQueueEntity",
                type: "jsonb",
                nullable: true,
                oldClrType: typeof(PlayerQueueOptions),
                oldType: "jsonb");

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameMapEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 491, DateTimeKind.Unspecified).AddTicks(4329), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 491, DateTimeKind.Unspecified).AddTicks(7495), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe602"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 491, DateTimeKind.Unspecified).AddTicks(8348), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe603"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 491, DateTimeKind.Unspecified).AddTicks(8535), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe604"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 491, DateTimeKind.Unspecified).AddTicks(8703), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeMapEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 492, DateTimeKind.Unspecified).AddTicks(623), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeMapEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 492, DateTimeKind.Unspecified).AddTicks(994), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeMapEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe301"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 492, DateTimeKind.Unspecified).AddTicks(1000), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "GameModeMapEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe401"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 492, DateTimeKind.Unspecified).AddTicks(1002), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 490, DateTimeKind.Unspecified).AddTicks(4728), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe602"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 490, DateTimeKind.Unspecified).AddTicks(5476), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe603"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 490, DateTimeKind.Unspecified).AddTicks(5665), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe604"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 490, DateTimeKind.Unspecified).AddTicks(5836), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe605"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 490, DateTimeKind.Unspecified).AddTicks(6001), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe606"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 490, DateTimeKind.Unspecified).AddTicks(6162), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe607"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 490, DateTimeKind.Unspecified).AddTicks(6323), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe608"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2021, 5, 9, 16, 32, 45, 490, DateTimeKind.Unspecified).AddTicks(6481), new TimeSpan(0, 0, 0, 0, 0)));
        }
    }
}
