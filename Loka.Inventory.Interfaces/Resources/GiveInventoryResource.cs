﻿namespace Loka.Inventory.Interfaces.Resources;

public static class GiveInventoryResource
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<PlayerResourceModel.Delta>
    {
        [Id(0)]
        public Guid EntityId { get; init; }

        [Id(1)]
        public long Amount { get; init; }
    }
}