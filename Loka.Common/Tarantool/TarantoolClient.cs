﻿using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using ProGaudi.MsgPack.Light;
using ProGaudi.Tarantool.Client;
using ProGaudi.Tarantool.Client.Model;
using ProGaudi.Tarantool.Client.Model.Responses;

namespace Loka.Common.Tarantool;

public class TarantoolClient
{
    public TarantoolClient(IConfiguration configuration)
    {
        MsgPackContext = new MsgPackContext();
        ConnectionString = configuration.GetConnectionString(nameof(Tarantool));
        Options = new ClientOptions(ConnectionString, context: MsgPackContext);
        Client = new Box(Options);
    }

    internal string ConnectionString { get; }
    internal MsgPackContext MsgPackContext { get; }
    internal ClientOptions Options { get; }
    public IBox Client { get; }

    public ISpace this[string name] => Client.Schema[name];

    public ISpace this[uint id] => Client.Schema[id];

    public bool IsConnected => Client.IsConnected;

    public Metrics Metrics => Client.Metrics;

    public ISchema Schema => Client.Schema;

    public BoxInfo Info => Client.Info;

    public Task ReloadSchema => Client.ReloadSchema();

    public Task ReloadBoxInfo => Client.ReloadBoxInfo();

    public Task Call(string functionName) => Client.Call(functionName);

    public Task Call<TTuple>(string functionName, TTuple parameters) => Client.Call(functionName, parameters);

    public Task<DataResponse<TResponse[]>> Call<TResponse>(string functionName) => Client.Call<TResponse>(functionName);

    public Task<DataResponse<TResponse[]>> Call<TTuple, TResponse>(string functionName, TTuple parameters) => Client.Call<TTuple, TResponse>(functionName, parameters);

    public Task<DataResponse<TResponse[]>> Eval<TTuple, TResponse>(string expression, TTuple parameters) => Client.Eval<TTuple, TResponse>(expression, parameters);

    public Task<DataResponse<TResponse[]>> Eval<TResponse>(string expression) => Client.Eval<TResponse>(expression);

    public Task<DataResponse<TResponse[]>> ExecuteSql<TResponse>(string query, params SqlParameter[] parameters) => Client.ExecuteSql<TResponse>(query, parameters);

    public Task<DataResponse> ExecuteSql(string query, params SqlParameter[] parameters) => Client.ExecuteSql(query, parameters);
}