﻿using AutoMapper;
using AutoMapper.Extensions.EnumMapping;
using Loka.Admin.Api.Features.Players;
using Loka.Admin.Players.Handlers.Players.Listing.GetPlayersList;
using Loka.Admin.Players.Handlers.Players.Listing.GetPlayersList.Filters;
using Loka.Admin.Players.Handlers.Players.Listing.GetPlayersList.OrderBy;

namespace Loka.Admin.Api.Mapping.Players;

internal sealed class GetPlayersListRequestMapping : Profile
{
    public GetPlayersListRequestMapping()
    {
        CreateMap<GetPlayersListQuery, GetPlayersListRequest>()
            .ForMember(q => q.Paging, q => q.MapFrom(p => p.Paging))
            .ForMember(q => q.Filter, q => q.MapFrom(p => p.Filter))
            .ForMember(q => q.OrderBy, q => q.MapFrom(p => p.OrderBy))
            .ForMember(q => q.OrderByDirection, q => q.MapFrom(p => p.OrderByDirection));

        CreateMap<GetPlayersListFilter, GetPlayersListRequestFilter>()
            .ForMember(q => q.Term, q => q.MapFrom(p => p.Term))
            .ForMember(q => q.CountriesIds, q => q.MapFrom(p => p.Countries))
            .ForMember(q => q.LanguagesIds, q => q.MapFrom(p => p.Languages))
            .ForMember(q => q.EmailConfirmed, q => q.MapFrom(p => p.EmailConfirmed))
            .ForMember(q => q.NameConfirmed, q => q.MapFrom(p => p.NameConfirmed))
            .ForMember(q => q.HasActivePremium, q => q.MapFrom(p => p.HasActivePremium))
            .ForMember(q => q.Statistics, q => q.MapFrom(p => p.Statistics));

        CreateMap<GetPlayersListStatisticsFilter, GetPlayersListRequestStatisticsFilter>()
            .ForMember(q => q.Kills, q => q.MapFrom(p => p.Kills))
            .ForMember(q => q.Deaths, q => q.MapFrom(p => p.Deaths))
            .ForMember(q => q.Wins, q => q.MapFrom(p => p.Wins))
            .ForMember(q => q.Loses, q => q.MapFrom(p => p.Loses))
            .ForMember(q => q.Level, q => q.MapFrom(p => p.Level))
            .ForMember(q => q.Elo, q => q.MapFrom(p => p.Elo));

        CreateMap<GetPlayersListRequestOrderBy, GetPlayersListOrderBy>()
            .ConvertUsingEnumMapping(ConfigureEnumMapping)
            .ReverseMap();
    }

    private void ConfigureEnumMapping(IEnumConfigurationExpression<GetPlayersListRequestOrderBy, GetPlayersListOrderBy> configuration)
    {
        configuration
            .MapValue(GetPlayersListRequestOrderBy.Country, GetPlayersListOrderBy.Country)
            .MapValue(GetPlayersListRequestOrderBy.Language, GetPlayersListOrderBy.Language)
            .MapValue(GetPlayersListRequestOrderBy.Level, GetPlayersListOrderBy.Level)
            .MapValue(GetPlayersListRequestOrderBy.Email, GetPlayersListOrderBy.Email)
            .MapValue(GetPlayersListRequestOrderBy.Name, GetPlayersListOrderBy.Name);
    }
}