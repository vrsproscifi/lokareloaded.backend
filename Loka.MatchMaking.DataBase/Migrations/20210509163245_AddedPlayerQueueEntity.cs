﻿using System;
using Loka.MatchMaking.DataBase.Entities.Queue;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Loka.MatchMaking.DataBase.Migrations
{
    public partial class AddedPlayerQueueEntity : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PlayerQueueEntity",
                schema: "MatchMaking",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    QueueId = table.Column<Guid>(type: "uuid", nullable: true),
                    MatchId = table.Column<Guid>(type: "uuid", nullable: true),
                    State = table.Column<byte>(type: "smallint", nullable: false),
                    Options = table.Column<PlayerQueueOptions>(type: "jsonb", nullable: true),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PlayerQueueEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_PlayerQueueEntity_PlayerQueueEntity_QueueId",
                        column: x => x.QueueId,
                        principalSchema: "MatchMaking",
                        principalTable: "PlayerQueueEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Restrict);
                });


            migrationBuilder.CreateIndex(
                name: "IX_PlayerQueueEntity_QueueId",
                schema: "MatchMaking",
                table: "PlayerQueueEntity",
                column: "QueueId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PlayerQueueEntity",
                schema: "MatchMaking");
       }
    }
}
