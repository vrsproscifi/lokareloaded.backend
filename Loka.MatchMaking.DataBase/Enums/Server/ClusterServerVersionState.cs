﻿namespace Loka.MatchMaking.DataBase.Enums.Server;

public enum ClusterServerVersionState : byte
{
    None,

    Downloading,
    Downloaded,
    Deleted
}