﻿using System;
using Loka.Database.Common.Entities;
using Loka.Social.DataBase.Enums;
using Microsoft.EntityFrameworkCore;

namespace Loka.Social.DataBase.Entities.Friends;

[Index(nameof(PlayerId))]
[Index(nameof(FriendId))]
public sealed class PlayerFriendEntity : BaseEntity, IPlayerChildEntity
{
    public Guid PlayerId { get; set; }
    public Guid FriendId { get; set; }
    public PlayerFriendState State { get; set; }

    public PlayerFriendEntity()
    {
            
    }

    public PlayerFriendEntity(Guid playerId, Guid friendId, PlayerFriendState state) : base(Guid.NewGuid())
    {
        PlayerId = playerId;
        FriendId = friendId;
        State = state;
    }
}