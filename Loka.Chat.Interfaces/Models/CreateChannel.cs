﻿using Loka.Chat.Interfaces.Enums;
using MongoDB.Bson;

namespace Loka.Chat.Interfaces.Models;

public static class CreateChannel
{
    [GenerateSerializer]
    public sealed class Request
    {
        [Id(0)]
        public string? Name { get; init; }

        [Id(1)]
        public ChatChanelType TypeId { get; init; }

        [Id(2)]
        public Guid? RegionId { get; init; }

        [Id(3)]
        public Guid? TeamId { get; init; }

        [Id(4)]
        public Guid? MatchId { get; init; }

        [Id(5)]
        public Guid? ClanId { get; init; }
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public ChannelEntry Channel { get; init; } = null!;
    }
}