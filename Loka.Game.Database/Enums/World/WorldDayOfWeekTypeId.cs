﻿namespace Loka.Game.Database.Enums.World;

public enum WorldDayOfWeekTypeId : byte
{
    None,
        
    /// <summary>
    /// Каждый день
    /// </summary>
    Everyday,
        
    /// <summary>
    /// Понедельник
    /// </summary>
    Monday,
        
    /// <summary>
    /// Вторник
    /// </summary>
    Tuesday,
        
    /// <summary>
    /// Среда
    /// </summary>
    Wednesday,
        
    /// <summary>
    /// Четверг
    /// </summary>
    Thursday,
        
    /// <summary>
    /// Пятница
    /// </summary>
    Friday,
        
    /// <summary>
    /// Суббота
    /// </summary>
    Saturday,
        
    /// <summary>
    /// Воскресенье
    /// </summary>
    Sunday,
}