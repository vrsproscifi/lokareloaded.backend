﻿using System.Collections.Generic;

namespace Loka.Common.Cache.Collection;

public interface ICollectionCache<TData>
{
    bool TryGet(out TData[]? entries);
    TData[] Put(TData[] entries);
}