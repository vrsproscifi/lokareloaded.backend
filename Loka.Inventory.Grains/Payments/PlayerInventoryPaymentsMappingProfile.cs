﻿using Loka.Inventory.DataBase.Entities.Payments;
using Loka.Inventory.Interfaces.Payments;

namespace Loka.Inventory.Grains.Payments;

internal sealed class PlayerInventoryPaymentsMappingProfile : Profile
{
    public PlayerInventoryPaymentsMappingProfile()
    {
        CreateMap<PlayerPaymentTransactionEntity, PlayerPaymentTransactionModel>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.ModelId))
            .ForMember(q => q.TypeId, q => q.MapFrom(w => w.TypeId))
            .ForMember(q => q.Count, q => q.MapFrom(w => w.Count))
            .ForMember(q => q.Resources, q => q.MapFrom(w => w.Resources));

        CreateMap<PlayerPaymentTransactionResource, PlayerPaymentTransactionModel.ResourceEntry>()
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.ModelId))
            .ForMember(q => q.Amount, q => q.MapFrom(w => w.Amount));
    }
}