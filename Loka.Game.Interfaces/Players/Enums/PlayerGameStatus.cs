﻿namespace Loka.Game.Interfaces.Players.Enums;

public enum PlayerGameStatus
{
    Offline,
    Online,
    SearchGame,
    PlayGame,
}