﻿using System.Linq.Expressions;
using AutoMapper;
using Loka.Common.Cache.DistributedCache;
using Loka.Common.Entry;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;

namespace Loka.Database.Common.Repositories;

public static class BaseChildRepository<TDbContext, TEntity, TModel>
    where TModel : class, IEntry
    where TEntity : BaseEntity
    where TDbContext : DbContext
{
    private static Type ModelType { get; } = typeof(TModel);
    private static string ModelTypeName { get; } = typeof(TModel).Name;

    public abstract class Cached : Raw, IBaseChildRepository<TModel>.ICached
    {
        private ICacheProvider CacheProvider { get; }
        protected abstract TimeSpan DefaultLifeTime { get; }

        protected Cached(TDbContext dbContext, IMapper mapper, ICacheProvider cacheProvider) : base(dbContext, mapper)
        {
            CacheProvider = cacheProvider;
        }

        private DistributedCacheEntryOptions CacheEntryOptions => new DistributedCacheEntryOptions()
        {
            AbsoluteExpirationRelativeToNow = DefaultLifeTime
        };


        public override Task<OperationResult<TModel>> GetById(Guid playerId, Guid id)
        {
            return CacheProvider.GetOrAddExt(CacheKey_GetById(id), key => base.GetById(playerId, id), CacheEntryOptions);
        }

        public override async Task<IReadOnlyList<TModel>> GetByIds(Guid parentId, IReadOnlyList<Guid> ids)
        {
            var models = new List<TModel>(ids.Count);
            foreach (var id in ids)
            {
                var model = await GetById(parentId, id);
                if (model.IsSucceeded)
                {
                    models.Add(model.Result);
                }
            }

            return models;
        }

        public override Task<IReadOnlyList<TModel>> GetAll(Guid playerId)
        {
            return CacheProvider.GetOrAdd(CacheKey_GetAll(playerId), key => base.GetAll(playerId), CacheEntryOptions);
        }

        public Task ResetCache(string key)
        {
            return CacheProvider.Remove(key);
        }

        public Task ResetCache(Guid parentId, Guid id)
        {
            return ResetCache(parentId, new[]
            {
                id
            });
        }

        public Task ResetCache(Guid parentId, IReadOnlyList<Guid> ids)
        {
            return Task.WhenAll(ids.Select(id => CacheProvider.Remove(CacheKey_GetById(id))).Append(CacheProvider.Remove(CacheKey_GetAll(parentId))));
        }

        protected string CacheKey_GetById(Guid id)
        {
            return $"${ModelTypeName}_{id:N}";
        }

        protected string CacheKey_GetAll(Guid id)
        {
            return $"${ModelTypeName}_{id:N}_all";
        }
    }

    public abstract class Raw : IBaseChildRepository<TModel>
    {
        protected IMapper Mapper { get; }
        protected DbSet<TEntity> DbSet { get; }
        protected TDbContext DbContext { get; }

        protected Raw(TDbContext dbContext, IMapper mapper)
        {
            DbContext = dbContext;
            Mapper = mapper;
            DbSet = DbContext.Set<TEntity>();
        }


        protected abstract Func<Guid, Expression<Func<TEntity, bool>>> Predicate { get; }

        protected virtual Task<OperationResult> AfterMap(Guid parentId, TEntity entity, TModel model)
        {
            return Task.FromResult(new OperationResult(OperationResultCode.Successfully));
        }

        public virtual async Task<OperationResult<TModel>> GetById(Guid parentId, Guid id)
        {
            var entity = await DbSet
                .Where(Predicate(parentId))
                .FirstOrDefaultAsync(q => q.EntityId == id);

            if (entity != null)
            {
                var model = Mapper.Map<TModel>(entity);
                await AfterMap(parentId, entity, model);
                return model;
            }

            return (OperationResultCode.EntityNotFound, id);
        }

        public virtual async Task<IReadOnlyList<TModel>> GetByIds(Guid parentId, IReadOnlyList<Guid> ids)
        {
            var entities = await DbSet
                .Where(Predicate(parentId))
                .Where(q => ids.Contains(q.EntityId))
                .ToArrayAsync();

            var models = new List<TModel>(entities.Length);
            foreach (var entity in entities)
            {
                var model = Mapper.Map<TModel>(entity);
                await AfterMap(parentId, entity, model);
                models.Add(model);
            }

            return models;
        }

        public virtual async Task<IReadOnlyList<TModel>> GetAll(Guid parentId)
        {
            var entities = await DbSet
                .Where(Predicate(parentId))
                //.Where(q => q.DeletedAt == null)
                .ToArrayAsync();

            var models = new List<TModel>(entities.Length);
            foreach (var entity in entities)
            {
                var model = Mapper.Map<TModel>(entity);
                await AfterMap(parentId, entity, model);
                models.Add(model);
            }

            return models;
        }

        protected Task<int> SaveChanges()
        {
            return DbContext.SaveChangesAsync();
        }
    }
}