﻿using Loka.Common.Authentication;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Common.Authentication;

public class PlayerActorStorage
{
    public PlayerActor Actor { get; set; } = PlayerActor.NotAuthorized;
}

public static class PlayerActorStorageExtensions
{
    public static IServiceCollection AddIdentityApiServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddScoped<PlayerActorStorage>();
        services.AddScoped<PlayerActor>(provider => provider.GetRequiredService<PlayerActorStorage>().Actor);

        return services;
    }

    public static ServiceCollectionInitializer AddIdentityApiServices(this ServiceCollectionInitializer initializer)
    {
        initializer.Services.AddIdentityApiServices(initializer.Configuration);
        return initializer;
    }
}