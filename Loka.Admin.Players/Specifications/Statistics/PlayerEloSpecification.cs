﻿using System.Linq;
using Loka.Common.Ranges.Numerics;
using Loka.Identity.Database.Entities.Player;

namespace Loka.Admin.Players.Specifications.Statistics;

internal sealed class PlayerEloSpecification : PlayerStatisticsSpecification<int>
{
    public PlayerEloSpecification
    (
        NumericRange<int?, int?> range,
        NumericRangeBeginOptions beginOptions = NumericRangeBeginOptions.GreatOrEqual,
        NumericRangeEndOptions endOptions = NumericRangeEndOptions.LessOrEqual
    ) : base(range, beginOptions, endOptions)
    {
    }

    public override IQueryable<PlayerAccountEntity> Apply(IQueryable<PlayerAccountEntity> query)
    {
        var resultQuery = query;

        if (Range.Begin.HasValue)
        {
            if (Options.Begin == NumericRangeBeginOptions.Great)
                resultQuery = query.Where(player => player.Statistics!.Elo >= Range.Begin.Value);
            else if (Options.Begin == NumericRangeBeginOptions.GreatOrEqual)
                resultQuery = query.Where(player => player.Statistics!.Elo >= Range.Begin.Value);
        }

        if (Range.End.HasValue)
        {
            if (Options.End == NumericRangeEndOptions.Less)
                resultQuery = query.Where(player => player.Statistics!.Elo < Range.Begin.Value);
            else if (Options.End == NumericRangeEndOptions.LessOrEqual)
                resultQuery = query.Where(player => player.Statistics!.Elo <= Range.Begin.Value);
        }

        return resultQuery;
    }
}