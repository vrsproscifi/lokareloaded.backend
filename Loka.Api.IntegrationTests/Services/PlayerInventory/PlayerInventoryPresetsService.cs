﻿using Loka.Api.Features.Inventory.Items;
using Loka.Api.Features.Inventory.Presets;
using Loka.Api.IntegrationTests.Clients;
using Loka.Api.IntegrationTests.Services.PlayerIdentity;

namespace Loka.Api.IntegrationTests.Services.PlayerInventory;

public sealed class PlayerInventoryPresetsService
{
    private PlayerInventoryPresets.PlayerInventoryPresetsClient PlayerInventoryPresetsClient { get; }

    public PlayerInventoryPresetsService(GameAPIClients client)
    {
        PlayerInventoryPresetsClient = client.PlayerInventoryPresetsClient;
    }

    public async Task<PlayerInventoryPreset> BuyInventoryPreset(PlayerSession playerSession, StoreItem.Types.Model item)
    {
        await TestContext.Progress.WriteLineAsync($"BuyInventoryPreset: {item.Id} | PlayerId: {playerSession.PlayerId}");
        Assert.That(item.Category.Type, Is.EqualTo(StoreItemCategory.Types.Type.Profile), $"Invalid profile item");

        var response = await PlayerInventoryPresetsClient.BuyInventoryPresetAsync(new BuyInventoryPreset.Types.Request()
        {
            ModelId = item.Id
        }, playerSession.UseJWT());
        Assert.That(response, Is.Not.Null, $"Invalid {response}");
        Assert.Multiple(() =>
        {
            Assert.That(response.Preset, Is.Not.Null, $"Invalid {response}");
            Assert.That(item.Id, Is.EqualTo(response.Preset.ModelId), $"Invalid {nameof(response.Preset.ModelId)}");
            response.Preset.AssertModel();
        });

        return response.Preset;
    }

    public async Task<IReadOnlyList<PlayerInventoryPreset>> GetPlayerInventoryPresets(PlayerSession playerSession)
    {
        var response = await PlayerInventoryPresetsClient.GetPlayerInventoryPresetsAsync(new GetPlayerInventoryPresets.Types.Request(), playerSession.UseJWT());
        response.AssertResponse();
        return response.Entities;
    }

    public async Task<EquipInventoryItem.Types.Response> EquipInventoryItem(PlayerSession playerSession, PlayerInventoryPreset preset, PlayerInventoryItem item)
    {
        var response = await PlayerInventoryPresetsClient.EquipInventoryItemAsync(new EquipInventoryItem.Types.Request()
        {
            Id = item.Id,
            PresetId = preset.Id,
        }, playerSession.UseJWT());
        response.AssertResponse();
        return response;
    }
}