﻿namespace Loka.Store.DataBase.Entities.Cases;

[Index(nameof(Name))]
public sealed class CaseEntity : BaseEntity, INamedEntity
{
    [Required]
    [MaxLength(64)]
    public string Name { get; init; } = null!;

    public int? RandomMaxDropItems { get; set; } = 5;
    public int? RandomMaxDropResources { get; set; } = 5;

    public IReadOnlyList<CaseItemEntity>? Items { get; set; }
    public IReadOnlyList<CaseResourceEntity>? Resources { get; set; }

    #region Cost

    [ForeignKey(nameof(SellCurrencyEntity))]
    public Guid? SellCurrencyId { get; set; }

    public GameResourceEntity? SellCurrencyEntity { get; set; }
    public int SellPrice { get; set; }

    #endregion

    public CaseEntity()
    {
    }

    public CaseEntity(Guid id, string name, Guid? sellCurrencyId, int sellPrice) : base(id)
    {
        Name = name;
        SellCurrencyId = sellCurrencyId;
        SellPrice = sellPrice;
    }
}