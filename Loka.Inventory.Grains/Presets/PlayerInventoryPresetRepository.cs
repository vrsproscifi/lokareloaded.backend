﻿using System.Linq.Expressions;
using Loka.Database.Common.Repositories;
using Loka.Inventory.DataBase;
using Loka.Inventory.DataBase.Entities.Preset;
using Loka.Inventory.Interfaces.Inventory;
using Loka.Inventory.Interfaces.Presets;
using Loka.Inventory.Interfaces.Presets.Models;

namespace Loka.Inventory.Grains.Presets;

internal sealed class PlayerInventoryPresetRepository : BaseChildRepository<InventoryDbContext, PlayerInventoryPresetEntity, PlayerInventoryPresetModel.Repository>.Raw, IPlayerInventoryPresetRepository
{
    private IPlayerInventoryItemsRepository PlayerInventoryItemsRepository { get; }
    protected override Func<Guid, Expression<Func<PlayerInventoryPresetEntity, bool>>> Predicate { get; } = playerId => entity => entity.PlayerId == playerId;

    public PlayerInventoryPresetRepository(InventoryDbContext dbContext, IMapper mapper, IPlayerInventoryItemsRepository playerInventoryItemsRepository) : base(dbContext, mapper)
    {
        PlayerInventoryItemsRepository = playerInventoryItemsRepository;
    }

    protected override async Task<OperationResult> AfterMap(Guid parentId, PlayerInventoryPresetEntity entity, PlayerInventoryPresetModel.Repository model)
    {
        var itemEntities = await DbContext.PlayerInventoryPresetItemEntity
            .Where(q => q.PresetId == entity.EntityId)
            .Select(item => new
            {
                Id = item.EntityId,
                ItemId = item.ItemId,
            }).ToArrayAsync();

        var itemIds = itemEntities
            .Select(q => q.ItemId)
            .ToArray();

        var items = await PlayerInventoryItemsRepository.GetByIds(parentId, itemIds);
        model.Items = itemEntities.Select(q => new PlayerInventoryPresetModel.Repository.ItemEntry()
        {
            Id = q.Id,
            Item = items.First(i => i.Id == q.ItemId)
        }).ToArray();

        return await base.AfterMap(parentId, entity, model);
    }


    public async Task<PlayerInventoryPresetModel.Repository> CreatePreset(Guid playerId, Guid modelId)
    {
        var entity = await DbSet.AddAsync(new PlayerInventoryPresetEntity()
        {
            EntityId = Guid.NewGuid(),
            PlayerId = playerId,
            CreatedAt = DateTimeOffset.UtcNow,
            ModelId = modelId
        });
        await SaveChanges();

        return new PlayerInventoryPresetModel.Repository()
        {
            Id = entity.Entity.EntityId,
            ModelId = entity.Entity.ModelId,
            IsActive = entity.Entity.IsActive,
            Items = ArraySegment<PlayerInventoryPresetModel.Repository.ItemEntry>.Empty
        };
    }

    public Task<bool> HasPreset(Guid playerId, Guid modelId)
    {
        return DbSet
            .Where(Predicate(playerId))
            .Where(q => q.ModelId == modelId)
            .AnyAsync(q => q.ModelId == modelId);
    }

    public async Task<Guid> AddSlotItem(Guid presetId, Guid itemId)
    {
        var entity = await DbContext.PlayerInventoryPresetItemEntity.AddAsync(new PlayerInventoryPresetItemEntity()
        {
            EntityId = Guid.NewGuid(),
            PresetId = presetId,
            ItemId = itemId,
            CreatedAt = DateTimeOffset.UtcNow
        });
        await SaveChanges();

        return entity.Entity.EntityId;
    }

    public async Task<Guid> SetSlotItem(Guid presetId, Guid slotId, Guid itemId)
    {
        var entity = await DbContext.PlayerInventoryPresetItemEntity
            .FirstAsync(q => q.PresetId == presetId && q.EntityId == slotId);

        entity.ItemId = itemId;
        await SaveChanges();

        return entity.EntityId;
    }
}