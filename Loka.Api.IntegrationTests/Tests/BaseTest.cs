﻿using Loka.Api.IntegrationTests.Clients;
using Loka.Api.IntegrationTests.Generators;
using Loka.Api.IntegrationTests.Services;
using Loka.Api.IntegrationTests.Storages;

[assembly: LevelOfParallelism(32)]
namespace Loka.Api.IntegrationTests.Tests;

[TestFixture]
internal abstract class BaseTest
{
    protected PlayerIdentityGenerators PlayerIdentityGenerators { get; }
    protected GameAPIClients Clients { get; }
    protected GameAPIServices Services { get; }
    protected GameAPIStorages Storages { get; }

    protected BaseTest()
    {
        PlayerIdentityGenerators = new PlayerIdentityGenerators();
        AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
        var channel = GrpcChannel.ForAddress("https://localhost:5555", new GrpcChannelOptions());
        Clients = new GameAPIClients(channel);
        Services = new GameAPIServices(Clients);
        Storages = new GameAPIStorages(Services);
    }

    [SetUp]
    public virtual async Task Setup()
    {
        await Clients.GrpcChannel.ConnectAsync();
    }
    
    [OneTimeSetUp]
    public virtual async Task OneTimeSetUp()
    {
        await Storages.Initialize();
    }
}