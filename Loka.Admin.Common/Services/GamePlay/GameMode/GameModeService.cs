﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Common.Models.GamePlay;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Extensions;
using Loka.MatchMaking.DataBase;

namespace Loka.Admin.Common.Services.GamePlay.GameMode;

internal sealed class GameModeService : IGameModeService
{
    private MatchMakingDbContext MatchMakingDbContext { get; }

    public GameModeService(MatchMakingDbContext matchMakingDbContext)
    {
        MatchMakingDbContext = matchMakingDbContext;
    }
        
    public async Task<OperationResult<GameModeInformationModel>> GetById(Guid id, CancellationToken cancellationToken)
    {
        var entity = await MatchMakingDbContext.GameModeEntity.GetById(id, cancellationToken);
        if (entity == null)
            return OperationResultCode.PlayerNotFound;

        var model = new GameModeInformationModel
        {
            Id = entity.EntityId,
            Name = entity.Name
        };

        return model;
    }
}