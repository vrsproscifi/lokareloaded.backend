﻿namespace Loka.Chat.Interfaces.Enums;

public enum ChatChanelType
{
    Announcements = 0,
    Global = 1,
    Private = 2,
    Squad = 3,
    Match = 4,
    Clan = 5,
}