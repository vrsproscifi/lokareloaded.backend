﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Loka.Identity.Database.Migrations
{
    /// <inheritdoc />
    public partial class GlobalIdentityChanges : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_PlayerAccountEntity_DeviceId",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropIndex(
                name: "IX_PlayerAccountEntity_Email",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropIndex(
                name: "IX_PlayerAccountEntity_PlayerName",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropColumn(
                name: "CountryId",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropColumn(
                name: "DeviceId",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropColumn(
                name: "Email",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropColumn(
                name: "IsEmailConfirmed",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropColumn(
                name: "IsNameConfirmed",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropColumn(
                name: "LanguageId",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropColumn(
                name: "LastActivityDate",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropColumn(
                name: "PremiumEndDate",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.DropColumn(
                name: "RoleId",
                schema: "Identity",
                table: "PlayerAccountEntity");

            migrationBuilder.EnsureSchema(
                name: "GameIdentity");

            migrationBuilder.RenameTable(
                name: "PlayerStatisticsEntity",
                schema: "Identity",
                newName: "PlayerStatisticsEntity",
                newSchema: "GameIdentity");

            migrationBuilder.RenameTable(
                name: "PlayerAccountEntity",
                schema: "Identity",
                newName: "PlayerAccountEntity",
                newSchema: "GameIdentity");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Identity");

            migrationBuilder.RenameTable(
                name: "PlayerStatisticsEntity",
                schema: "GameIdentity",
                newName: "PlayerStatisticsEntity",
                newSchema: "Identity");

            migrationBuilder.RenameTable(
                name: "PlayerAccountEntity",
                schema: "GameIdentity",
                newName: "PlayerAccountEntity",
                newSchema: "Identity");

            migrationBuilder.AddColumn<Guid>(
                name: "CountryId",
                schema: "Identity",
                table: "PlayerAccountEntity",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DeviceId",
                schema: "Identity",
                table: "PlayerAccountEntity",
                type: "character varying(128)",
                maxLength: 128,
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Email",
                schema: "Identity",
                table: "PlayerAccountEntity",
                type: "character varying(64)",
                maxLength: 64,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsEmailConfirmed",
                schema: "Identity",
                table: "PlayerAccountEntity",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "IsNameConfirmed",
                schema: "Identity",
                table: "PlayerAccountEntity",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "LanguageId",
                schema: "Identity",
                table: "PlayerAccountEntity",
                type: "uuid",
                nullable: true);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LastActivityDate",
                schema: "Identity",
                table: "PlayerAccountEntity",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "PremiumEndDate",
                schema: "Identity",
                table: "PlayerAccountEntity",
                type: "timestamp with time zone",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "RoleId",
                schema: "Identity",
                table: "PlayerAccountEntity",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_PlayerAccountEntity_DeviceId",
                schema: "Identity",
                table: "PlayerAccountEntity",
                column: "DeviceId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PlayerAccountEntity_Email",
                schema: "Identity",
                table: "PlayerAccountEntity",
                column: "Email");

            migrationBuilder.CreateIndex(
                name: "IX_PlayerAccountEntity_PlayerName",
                schema: "Identity",
                table: "PlayerAccountEntity",
                column: "PlayerName",
                unique: true);
        }
    }
}
