﻿using Loka.MatchMaking.Interfaces.Forms.Queue;

namespace Loka.Api.Features.MatchMaking;

internal sealed class MatchMakingMappingProfiles : Profile
{
    public MatchMakingMappingProfiles()
    {
        CreateMap<JoinToMatchMakingQueueResponse, JoinToMatchMakingQueueModel>()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter();
        CreateMap<GetMatchMakingQueueInformationResponse, GetMatchMakingQueueInformationModel>()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter();
    }
}