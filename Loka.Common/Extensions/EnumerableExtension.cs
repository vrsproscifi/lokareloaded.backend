using System.Text;

namespace Loka.Common.Extensions;

public static class EnumerableExtension
{
    public static IEnumerable<(T item, int index)> WithIndex<T>(this IEnumerable<T> source)
    {
        return source.Select((item, index) => (item, index));
    }


    public static IEnumerable<IEnumerable<T>> GetAllCombinations<T>(IReadOnlyList<T> list, int length)
    {
        if (length == 1)
            return list.Select(t => new T[]
            {
                t
            });

        return GetAllCombinations(list, length - 1)
            .SelectMany(t => list, (t1, t2) => t1.Concat(new T[]
            {
                t2
            }));
    }

    public static T[] GetEnumValues<T>()
    {
        return Enum.GetValues(typeof(T)).Cast<T>().ToArray();
    }


    public static StringBuilder AppendRange(this StringBuilder stringBuilder, IEnumerable<char> chars)
    {
        foreach (var @char in chars)
        {
            stringBuilder.Append(@char);
        }

        return stringBuilder;
    }

    public static bool IsEmpty<T>(this IEnumerable<T> source)
    {
        return !source.Any();
    }

    public static bool IsNullOrEmpty<T>(this IEnumerable<T> source)
    {
        return source == null || !source.Any();
    }

    public static T PickRandom<T>(this IEnumerable<T> source)
    {
        return source.PickRandom(1).Single();
    }

    public static IEnumerable<T> PickRandom<T>(this IEnumerable<T> source, int count)
    {
        return source.Shuffle().Take(count);
    }

    public static IEnumerable<T> Shuffle<T>(this IEnumerable<T> source)
    {
        return source.OrderBy(x => Guid.NewGuid());
    }

    public static IEnumerable<IEnumerable<TSource>> Batch<TSource>(
        this IEnumerable<TSource> source, int size)
    {
        return source.Select((item, index) => new
            {
                item,
                index
            })
            .GroupBy(x => x.index / size)
            .Select(g => g.Select(x => x.item));
    }

    public static async Task<List<T>> ToListAsync<T>(this IAsyncEnumerable<T> items, CancellationToken cancellationToken = default)
    {
        var results = new List<T>();
        await foreach (var item in items.WithCancellation(cancellationToken)
                           .ConfigureAwait(false))
            results.Add(item);
        return results;
    }
}