﻿using AutoMapper;
using Loka.Common.Cache.DistributedCache;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Repositories;
using Loka.Store.DataBase;
using Loka.Store.DataBase.Entities.Cases;
using Loka.Store.Interfaces.Models.Cases;
using Loka.Store.Interfaces.Repositories;

namespace Loka.Store.Grains.Cases;

internal sealed class GameCaseItemsRepository : BaseRepository<StoreDbContext, CaseItemEntity, StoreCaseModel.Item>
{
    private IGameStoreItemsRepository GameStoreItemsRepository { get; }

    public GameCaseItemsRepository(ICacheProvider cacheProvider, StoreDbContext dbContext, IMapper mapper, IGameStoreItemsRepository gameStoreItemsRepository) : base(cacheProvider, dbContext, mapper)
    {
        GameStoreItemsRepository = gameStoreItemsRepository;
    }
    
    protected override async Task<OperationResult> AfterMap(CaseItemEntity entity, StoreCaseModel.Item model)
    {
        var getResourceResult = await GameStoreItemsRepository.GetById(entity.ModelId);
        if (!getResourceResult.GetValueIfSucceeded(out var resource))
            return getResourceResult.Error!;

        model.Model = resource;
        return await base.AfterMap(entity, model);
    }
}