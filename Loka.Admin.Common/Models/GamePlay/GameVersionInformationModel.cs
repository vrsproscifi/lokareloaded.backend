﻿using System;
using Loka.Common.Entry;
using Loka.Deployment.DataBase.Enums;

namespace Loka.Admin.Common.Models.GamePlay;

public sealed class GameVersionInformationModel : IEntry
{
    public Guid Id { get; set; }
    public DateTimeOffset CreatedAt { get; set; }

    public int GameVersion { get; set; }
    public Guid ServerVersion { get; set; }
    public DeployChannelTypeId Channel { get; set; }

    public override string ToString()
    {
        return $"Id: {Id} | Version: {GameVersion} | Channel: {Channel} | CreatedAt: {CreatedAt}";
    }
}