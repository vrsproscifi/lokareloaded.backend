﻿using Loka.Inventory.Interfaces.Discounts;
using Loka.Inventory.Interfaces.Inventory;
using Loka.Inventory.Interfaces.Store;

namespace Loka.Api.Features.Inventory.Items;

public sealed class PlayerInventoryItemsController : PlayerInventoryItems.PlayerInventoryItemsBase
{
    private ControllerServices<PlayerInventoryItemsController> Services { get; }
    private PlayerActor PlayerActor { get; }

    public PlayerInventoryItemsController(ControllerServices<PlayerInventoryItemsController> services, PlayerActor playerActor)
    {
        Services = services;
        PlayerActor = playerActor;
    }

    //public override Task<EquipInventoryItem.Types.Response> EquipInventoryItem(EquipInventoryItem.Types.Request request, ServerCallContext context)
    //{
    //    var grain = Services.ClusterClient.GetGrain<IPlayerInventoryItemsGrain>(PlayerActor.PlayerId);
    //    return grain.Equip(new Loka.Inventory.Interfaces.Presets.Models.EquipInventoryItem.Request()
    //    {
    //        ItemId = request.Id.ToGuid(),
    //        PresetId = Guid.Parse(request.PresetId.Value),
    //    }).ReturnResult<Loka.Inventory.Interfaces.Presets.Models.EquipInventoryItem.Result, EquipInventoryItem.Types.Response>(Services.Mapper);
    //}

    public override Task<BuyGameStoreItem.Types.Response> BuyItem(BuyGameStoreItem.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerStoreGrain>(PlayerActor.PlayerId);
        return grain.BuyStoreItem(new Loka.Inventory.Interfaces.Store.Requests.BuyStoreItem.Request()
        {
            ModelId =request.ModelId.ToGuid()
        }).ReturnResult<Loka.Inventory.Interfaces.Store.Requests.BuyStoreItem.Result, BuyGameStoreItem.Types.Response>(Services.Mapper);
    }

    public override Task<GetPlayerPurchasedItems.Types.Response> GetPlayerPurchasedItems(GetPlayerPurchasedItems.Types.Query request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerInventoryItemsGrain>(PlayerActor.PlayerId);
        return grain.GetPurchasedItems().ReturnResult<Loka.Inventory.Interfaces.Inventory.Requests.GetPlayerPurchasedItems.Result, GetPlayerPurchasedItems.Types.Response>(Services.Mapper);
    }

    public override Task<GetPlayerDiscounts.Types.Response> GetPlayerDiscounts(GetPlayerDiscounts.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerDiscountsGrain>(PlayerActor.PlayerId);
        return grain.GetDiscounts().ReturnResult<Loka.Inventory.Interfaces.Discounts.GetPlayerDiscounts.Result, GetPlayerDiscounts.Types.Response>(Services.Mapper);
    }
}