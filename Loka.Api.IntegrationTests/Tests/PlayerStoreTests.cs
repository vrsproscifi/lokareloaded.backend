﻿namespace Loka.Api.IntegrationTests.Tests;

internal sealed class PlayerStoreTests : BaseTest
{
    [Test]
    public async Task BuyStoreItem()
    {
        var playerSession = await Services.PlayerIdentity.QuickSignUp();

        var product = Storages.GameStore.GetProduct(q => q.SellPrice != 0);
        var item = await Services.PlayerInventoryItems.BuyItem(playerSession, product);

        var items = await Services.PlayerInventoryItems.GetPlayerPurchasedItems(playerSession);
        var boughtItem = items.FirstOrDefault(q => q.Id.Value == item.Id.Value);
        boughtItem.AssertEqual(item);
    }
}