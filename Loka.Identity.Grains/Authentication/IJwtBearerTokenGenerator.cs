﻿using Loka.Identity.Grains.Storages.Sessions;
using Loka.Identity.Interfaces.Models;

namespace Loka.Identity.Grains.Authentication;

public interface IJwtBearerTokenGenerator
{
    JwtTokenObject GenerateToken(PlayerSession session, Claim[] claims);
    JwtTokenObject GenerateRefreshToken(PlayerSession session, Claim[] claims);
}