﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Common.Models.GamePlay;
using Loka.Common.Operations.Results;

namespace Loka.Admin.Common.Services.GamePlay.GameMode;

public interface IGameModeService
{
    Task<OperationResult<GameModeInformationModel>> GetById(Guid id, CancellationToken cancellationToken = default);
}