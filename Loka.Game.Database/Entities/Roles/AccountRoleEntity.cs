﻿using System.ComponentModel.DataAnnotations;
using Loka.Database.Common.Entities;
using Loka.Game.Database.Enums.Account;

namespace Loka.Game.Database.Entities.Roles;

[Index(nameof(Name))]
[Index(nameof(TypeId), IsUnique = true)]
public sealed class AccountRoleEntity : BaseEntity, INamedEntity
{
    [Required]
    [MaxLength(64)]
    public string Name { get; set; } = null!;

    public AccountRoleTypeId TypeId { get; set; }

    public AccountRoleEntity()
    {
    }

    internal AccountRoleEntity(Guid id, AccountRoleTypeId typeId) : base(id)
    {
        Name = typeId.ToString();
        TypeId = typeId;
    }
}