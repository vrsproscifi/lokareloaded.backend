﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Entities;
using MongoDB.Bson;

namespace Loka.Chat.DataBase.Entities;

public sealed class ChatChanelMessageEntity
{
    public ObjectId Id { get; set; }
    public ObjectId ChannelId { get; set; }

    [MaxLength(256)]
    public string Message { get; set; } = null!;

    public DateTime CreatedAt { get; set; }

    public ChatMessageSender Sender { get; set; } = null!;
}