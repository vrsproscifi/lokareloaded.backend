﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;

namespace Loka.Common.Cache.Single;

public abstract class DataCache<TKey, TData> : IDataCache<TKey, TData>
{
    protected IMemoryCache MemoryCache { get; }
    protected abstract string CachePrefix { get; }

    protected virtual TimeSpan? ExpirationTime { get; } = null;

    protected DataCache(IMemoryCache memoryCache)
    {
        this.MemoryCache = memoryCache;
    }

    public bool Exists(TKey id)
    {
        return TryGet(id, out _);
    }

    public bool Exists(IReadOnlyList<TKey> ids, out List<TKey> notFoundIds)
    {
        notFoundIds = new List<TKey>(ids.Count);

        foreach (var id in ids)
        {
            if (!Exists(id))
            {
                notFoundIds.Add(id);
            }
        }

        return notFoundIds.Count == 0;
    }

    public bool Exists(IReadOnlyList<TKey> ids)
    {
        return ids.All(Exists);
    }

    public bool TryGet(TKey id, out TData model)
    {
        return MemoryCache.TryGetValue($"{CachePrefix}{id}", out model);
    }

    public bool TryGet(IReadOnlyList<TKey> ids, out List<TData> models, out List<TKey> notFoundIds)
    {
        notFoundIds = new List<TKey>(ids.Count);
        models = new List<TData>(ids.Count);

        foreach (var id in ids)
        {
            if (TryGet(id, out var model))
            {
                models.Add(model);
            }
            else
            {
                notFoundIds.Add(id);
            }
        }

        return notFoundIds.Count == 0;
    }

    public virtual TData Put(TData model)
    {
        var id = GetModelKey(model);

        if (ExpirationTime.HasValue)
        {
            return MemoryCache.Set($"{CachePrefix}{id}", model, ExpirationTime.Value);
        }

        return MemoryCache.Set($"{CachePrefix}{id}", model);
    }

    public void Put(IReadOnlyList<TData> models)
    {
        foreach (var model in models)
        {
            Put(model);
        }
    }

    protected abstract string GetModelKey(TData model);

    public override string ToString()
    {
        return $"DataCache<{typeof(TData).Name}> | CachePrefix: {CachePrefix}";
    }
}