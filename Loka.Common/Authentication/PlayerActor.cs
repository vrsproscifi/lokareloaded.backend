﻿using System;
using System.Diagnostics;

namespace Loka.Common.Authentication;

[DebuggerDisplay("PlayerId: {_playerId}, Name: {_playerName} | SessionId: {_sessionId}")]
public sealed class PlayerActor
{
    public static PlayerActor NotAuthorized { get; } = new PlayerActor();

    private Guid? _playerId;
    private string? _playerName;

    public PlayerActor()
    {
        _playerId = null;
        _playerName = null;
    }

    public PlayerActor(Guid playerId, string playerName)
    {
        _playerId = playerId;
        _playerName = playerName;
    }

    public bool IsAuthorized => _playerId.HasValue;

    public Guid PlayerId
    {
        get
        {
            if (_playerId.HasValue)
                return _playerId.Value;
            throw new UnauthorizedAccessException();
        }
    }

    public string PlayerName
    {
        get
        {
            if (string.IsNullOrWhiteSpace(_playerName))
                throw new UnauthorizedAccessException();

            return _playerName;
        }
    }
}