﻿using Loka.Database.Common.Entities.Store;
using Loka.Store.DataBase.Entities.Items;

namespace Loka.Store.DataBase.Entities.Cases;

public sealed class CaseItemEntity : BaseEntity, IStoreCaseChildEntity //, IStoreItemChildEntity
{
    [ForeignKey(nameof(CaseEntity))]
    public Guid CaseId { get; set; }

    public CaseEntity? CaseEntity { get; set; }

    [ForeignKey(nameof(ModelEntity))]
    public Guid ModelId { get; set; }

    public GameItemEntity? ModelEntity { get; set; }

    public int MinimumCount { get; set; }
    public int MaximumCount { get; set; }
    public double ChanceOfDrop { get; set; }
    public int OrderBy { get; set; }

    public CaseItemEntity()
    {
    }

    public CaseItemEntity(Guid id, Guid caseId, int minimumCount, int maximumCount, double chanceOfDrop, Guid modelId) : base(id)
    {
        CaseId = caseId;
        MinimumCount = minimumCount;
        MaximumCount = maximumCount;
        ChanceOfDrop = chanceOfDrop;
        ModelId = modelId;
    }
}