namespace Loka.Common.Ranges.Numerics;

public enum NumericRangeBeginOptions
{
    Great,
    GreatOrEqual
}