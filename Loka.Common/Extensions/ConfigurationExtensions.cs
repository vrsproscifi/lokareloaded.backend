using Microsoft.Extensions.Configuration;

namespace Loka.Common.Extensions;

public static class ConfigurationExtensions
{
    private const string ApplicationDataBaseSectionName = "Application.DataBase";

    public static T GetApplicationDataBaseSectionValue<T>(this IConfiguration configuration, string sectionName, bool isValue)
    {
        var applicationDataBaseSection = configuration.GetSection(ApplicationDataBaseSectionName);
        if (applicationDataBaseSection == null)
            throw new NullReferenceException($"Configuration section \"{ApplicationDataBaseSectionName}\" not found");
        
        if (isValue)
        {
            var config = applicationDataBaseSection.GetValue<T>(sectionName);
            if (config == null)
                throw new NullReferenceException($"Configuration section \"{sectionName}\" invalid config value");

            return config;
        }
        else
        {
            var section = applicationDataBaseSection.GetSection(sectionName);
            if (section == null)
                throw new NullReferenceException($"Configuration section \"{sectionName}\" not found");

            var config = section.Get<T>();
            if (config == null)
                throw new NullReferenceException($"Configuration section \"{sectionName}\" invalid config");

            return config;
        }
    }
}