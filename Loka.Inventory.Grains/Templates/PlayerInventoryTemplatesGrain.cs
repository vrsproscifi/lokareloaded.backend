﻿using Loka.Common.Authentication;
using Loka.Common.MediatR.Executors;
using Loka.Common.Orleans.Grains;
using Loka.Inventory.DataBase;
using Loka.Inventory.DataBase.Entities.Preset;
using Loka.Inventory.Interfaces.Templates;

namespace Loka.Inventory.Grains.Templates;

public sealed class PlayerInventoryTemplatesGrain : Grain, IPlayerInventoryTemplatesGrain, IWithPlayerActor
{
    public PlayerActor Actor { get; set; } = null!;
    private InventoryDbContext InventoryDbContext { get; }
    private IOperationExecutor OperationExecutor { get; }

    public PlayerInventoryTemplatesGrain(InventoryDbContext inventoryDbContext, IOperationExecutor operationExecutor)
    {
        InventoryDbContext = inventoryDbContext;
        OperationExecutor = operationExecutor;
    }
    
    
    public async Task<OperationResult<GetPlayerInventoryTemplates.Result>> GetInventoryTemplates()
    {
        var entities = await InventoryDbContext.PlayerInventoryTemplateEntity
            .Where(q => q.PlayerId == Actor.PlayerId)
            .Select(q => new PlayerInventoryTemplateModel()
            {
                Id = q.EntityId,
                Name = q.Name,
                Entries = q.Items!.Select(item => new PlayerInventoryTemplateModel.ItemEntry()
                {
                    Id = item.EntityId,
                    ItemId = item.ItemId
                }).ToArray()
            })
            .ToArrayAsync();

        return new GetPlayerInventoryTemplates.Result()
        {
            Presets = entities
        };
    }


    
    public async Task<OperationResult> UseTemplateAsPreset(UseTemplateAsPresetForm form)
    {
        var preset = await InventoryDbContext.PlayerInventoryPresetEntity
            .Include(q => q.Items)
            .FirstOrDefaultAsync(q => q.EntityId == form.PresetId && q.PlayerId == Actor.PlayerId);
        if (preset == null)
            return OperationResultCode.UnknownError;

        var template = await InventoryDbContext.PlayerInventoryTemplateEntity
            .Include(q => q.Items)
            .FirstOrDefaultAsync(q => q.EntityId == form.TemplateId && q.PlayerId == Actor.PlayerId);
        if (template == null)
            return OperationResultCode.UnknownError;

        InventoryDbContext.PlayerInventoryPresetItemEntity.RemoveRange(preset.Items!);
        preset.Items = template.Items!.Select(item => new PlayerInventoryPresetItemEntity
        {
            EntityId = Guid.NewGuid(),
            ItemId = item.ItemId,
            CreatedAt = DateTimeOffset.UtcNow,
        }).ToArray();
        await InventoryDbContext.SaveChangesAsync();

        return OperationResultCode.Successfully;
    }
}