﻿namespace Loka.Inventory.Interfaces.Inventory.Requests;

public static class GetPlayerPurchasedItems
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<Result>
    {
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public IReadOnlyList<PlayerInventoryItemModel.RPC> Entries { get; init; } = null!;
    }
}