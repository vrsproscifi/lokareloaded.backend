﻿namespace Loka.Inventory.DataBase.Entities.Payments;

public sealed class PlayerPaymentTransactionResource
{
    public Guid ModelId { get; init; }
    public long Amount { get; init; }
}