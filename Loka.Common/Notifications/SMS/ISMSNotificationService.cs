﻿namespace Loka.Common.Notifications.SMS;

public interface ISMSNotificationService
{
    Task Send(SMSNotificationModel model);
}