﻿using Loka.Inventory.Interfaces.Boosters;
using Loka.Inventory.Interfaces.Store;
using Loka.Inventory.Interfaces.Store.Requests;

namespace Loka.Api.Features.Inventory.Boosters;

internal sealed class PlayerInventoryBoostersController : PlayerInventoryBoosters.PlayerInventoryBoostersBase
{
    private ControllerServices<PlayerInventoryBoostersController> Services { get; }
    private PlayerActor PlayerActor { get; }

    public PlayerInventoryBoostersController(ControllerServices<PlayerInventoryBoostersController> services, PlayerActor playerActor)
    {
        Services = services;
        PlayerActor = playerActor;
    }

    public override Task<ActivatePlayerInventoryBooster.Types.Response> ActivateBooster(ActivatePlayerInventoryBooster.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerInventoryBoostersGrain>(PlayerActor.PlayerId);
        return grain.ActivatePlayerBooster(new ActivatePlayerBooster.Request()
        {
            Id = request.Id.ToGuid()
        }).ReturnResult<PlayerInventoryBoosterModel, ActivatePlayerInventoryBooster.Types.Response>(Services.Mapper);
    }

    public override Task<DeactivatePlayerInventoryBooster.Types.Response> DeactivateBooster(DeactivatePlayerInventoryBooster.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerInventoryBoostersGrain>(PlayerActor.PlayerId);
        return grain.DeactivatePlayerBooster(new DeactivatePlayerBooster.Request()
        {
            Id = request.Id.ToGuid()
        }).ReturnResult<PlayerInventoryBoosterModel, DeactivatePlayerInventoryBooster.Types.Response>(Services.Mapper);
    }

    public override Task<BuyGameStoreBooster.Types.Response> BuyBooster(BuyGameStoreBooster.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerStoreGrain>(PlayerActor.PlayerId);
        return grain.BuyStoreBooster(new BuyStoreBooster.Request()
        {
            ModelId = request.ModelId.ToGuid()
        }).ReturnResult<BuyStoreBooster.Result, BuyGameStoreBooster.Types.Response>(Services.Mapper);
    }

    public override Task<GetPlayerInventoryBoosters.Types.Response> GetBoosters(GetPlayerInventoryBoosters.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerInventoryBoostersGrain>(PlayerActor.PlayerId);
        return grain.GetPlayerBoosters().ReturnResult<GetPlayerBoosters.Result, GetPlayerInventoryBoosters.Types.Response>(Services.Mapper);
    }
}