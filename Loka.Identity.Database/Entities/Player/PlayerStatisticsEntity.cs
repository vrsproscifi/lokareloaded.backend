﻿namespace Loka.Identity.Database.Entities.Player;

public sealed class PlayerStatisticsEntity : BaseEntity
{
    public PlayerAccountEntity? AccountEntity { get; set; }
        
    public int Level { get; set; }
    public int LastLevel { get; set; }

    public long Experience { get; set; }
        
    public bool IsLevelUpNotified { get; set; }
        
    public short Elo { get; set; }
    public long Score { get; set; }

    public long Kills { get; set; }
    public long Deaths { get; set; }
    public long Supports { get; set; }

    public long Wins { get; set; }
    public long Loses { get; set; }
    public long Draws { get; set; }
}