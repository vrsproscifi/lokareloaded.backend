﻿using System;

namespace Loka.Common.ClickHouse.Attributes;

[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
public class ClickhouseTableEngineAttribute : Attribute
{
    public ClickhouseTableEngineAttribute(string name, string engine, string? partition = default, string? orderBy = default)
    {
        Engine = engine;
        Name = name;
        Partition = partition;
        OrderBy = orderBy;
    }

    public string Name { get; }
    public string Engine { get; }
    public string? Partition { get; }
    public string? OrderBy { get; set; }
    public string Settings { get; set; }
}