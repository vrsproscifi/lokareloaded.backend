using Loka.Common.Operations.Codes;

[assembly: Parallelizable(ParallelScope.All)]

namespace Loka.Api.IntegrationTests.Tests;

internal sealed class SignInTests : BaseTest
{
    [Test]
    public async Task SingIn_Delay_SignUp()
    {
        var playerSession = await Services.PlayerIdentity.QuickSignUp();

        var delay = TimeSpan.FromSeconds(5) + (playerSession.Token.TimeoutDate.ToDateTime() - DateTime.UtcNow);
        await TestContext.Progress.WriteLineAsync($"Prepare for execute {nameof(Clients.PlayerIdentityClient.SignInAsync)} | Wait for {delay:g}");
        await Task.Delay(delay);

        await TestContext.Progress.WriteLineAsync($"Execute {nameof(Clients.PlayerIdentityClient.SignInAsync)}");
        var signInResult = await Clients.PlayerIdentityClient.SignInAsync(playerSession.GetSignInQuery());
        signInResult.AssertModel();

        Assert.Multiple(() =>
        {
            Assert.That(signInResult.PlayerId, Is.EqualTo(playerSession.Result.PlayerId), $"Invalid {signInResult.PlayerId} | Must be equal");
            Assert.That(signInResult.DisplayName, Is.EqualTo(playerSession.DisplayName), $"Invalid {signInResult.PlayerId} | Must be equal");
            Assert.That(signInResult.SessionId, Is.Not.EqualTo(playerSession.Result.SessionId), $"Invalid {signInResult.SessionId} | Must be notequal");
        });
    }

    [Test]
    public async Task SingIn_NoDelay_SignUp()
    {
        var signUpResult = await Services.PlayerIdentity.QuickSignUp();

        var exception = Assert.ThrowsAsync<RpcException>(async () =>
        {
            await TestContext.Progress.WriteLineAsync($"Execute {nameof(Clients.PlayerIdentityClient.SignInAsync)}");
            await Clients.PlayerIdentityClient.SignInAsync(signUpResult.GetSignInQuery());
        });

        exception.AssertResultCode(OperationResultCode.PlayerHasActiveSession);
    }
}