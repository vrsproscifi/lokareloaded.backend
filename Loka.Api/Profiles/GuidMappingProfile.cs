﻿using Loka.Api.Features.Shared;
using Loka.Common.Pagination;
using MongoDB.Bson;

namespace Loka.Api.Profiles;

public sealed class GuidMappingProfile : Profile
{
    public GuidMappingProfile()
    {
        //==========================================================
        CreateMap<PagingQuery, PagingQueryModel>()
            .ForMember(q => q.Count, q => q.MapFrom(w => (uint) w.Take))
            .ForMember(q => q.Offset, q => q.MapFrom(w => (uint) w.Offset));

        CreateMap<PagingQueryModel, PagingQuery>()
            .ConstructUsing(q => new PagingQuery(q.Count, q.Offset));

        //==========================================================
        CreateMap<PagingResult, PagingResultModel>()
            .ForMember(q => q.Take, q => q.MapFrom(w => (uint) w.Take))
            .ForMember(q => q.Total, q => q.MapFrom(w => (uint) w.Total))
            .ForMember(q => q.Offset, q => q.MapFrom(w => (uint) w.Offset));

        CreateMap<PagingResultModel, PagingResult>()
            .ConstructUsing(q => new PagingResult((int) q.Total, (int) q.Take, (int) q.Offset));

        //==========================================================
        CreateMap<DateTime, Timestamp>()
            .ForMember(q => q.Seconds, q => q.Ignore())
            .ForMember(q => q.Nanos, q => q.Ignore())
            .ConstructUsing(q => Timestamp.FromDateTime(q));

        CreateMap<DateTimeOffset, Timestamp>()
            .ForMember(q => q.Seconds, q => q.Ignore())
            .ForMember(q => q.Nanos, q => q.Ignore())
            .ConstructUsing(q => Timestamp.FromDateTimeOffset(q));

        CreateMap<Timestamp, DateTime>().ConstructUsing(q => q.ToDateTime());
        CreateMap<Timestamp, DateTimeOffset>().ConstructUsing(q => q.ToDateTimeOffset());

        #region GUID

        CreateMap<Guid, UUID>()
            .ForMember(q => q.Value, q => q.Ignore())
            .ConstructUsing(q => new UUID()
            {
                Value = q.ToString()
            });

        CreateMap<UUID, Guid>()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter()
            .ConstructUsing(q => Guid.Parse(q.Value));

        #endregion

        #region ObjectId

        CreateMap<ObjectId, ObjID>()
            .ForMember(q => q.Value, q => q.Ignore())
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter()
            .ConstructUsing(q => new ObjID()
            {
                Value = q.ToString()
            });
        CreateMap<ObjID, ObjectId>()
            .IgnoreAllSourcePropertiesWithAnInaccessibleSetter()
            .ConstructUsing(q => ObjectId.Parse(q.Value));

        #endregion
    }
}