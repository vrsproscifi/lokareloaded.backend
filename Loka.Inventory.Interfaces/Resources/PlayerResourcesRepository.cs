﻿using Loka.Database.Common.Repositories;

namespace Loka.Inventory.Interfaces.Resources;

public interface IPlayerResourcesRepository : IBaseChildRepository<PlayerResourceModel.Lightweight>
{
    Task<OperationResult<PlayerResourceModel.Lightweight>> GetByType(Guid playerId, Guid typeId);
    Task<PlayerResourceModel.Lightweight> GetOrCreateByType(Guid playerId, Guid typeId);
    Task<PlayerResourceModel.Delta> Update(Guid playerId, UpdatePlayerResourceRequest request);
    Task<IReadOnlyList<PlayerResourceModel.Delta>> Update(Guid playerId, IReadOnlyList<UpdatePlayerResourceRequest> request);
}

public sealed class UpdatePlayerResourceRequest
{
    public Guid ResourceId { get; set; }
    public long Value { get; set; }
}