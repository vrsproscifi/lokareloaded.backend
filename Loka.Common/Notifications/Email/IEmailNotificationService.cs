﻿namespace Loka.Common.Notifications.Email;

public interface IEmailNotificationService
{
    Task Send(EmailNotificationModel model);
}