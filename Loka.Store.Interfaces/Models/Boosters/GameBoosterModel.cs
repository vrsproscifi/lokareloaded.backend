﻿using Loka.Common.Entry;
using Loka.Store.Interfaces.Enums;
using Loka.Store.Interfaces.Models.Resources;

namespace Loka.Store.Interfaces.Models.Boosters;

[GenerateSerializer]
public sealed class GameBoosterModel : IEntry, INamedEntry
{
    [Id(0)]
    public Guid Id { get; init; }

    [Id(1)]
    public string Name { get; init; } = null!;

    [Id(2)]
    public BoosterTypeId TypeId { get; init; }

    [Id(3)]
    public int BoostPercent { get; init; } = 10;

    [Id(4)]
    public GameResourceModel? SellCurrency { get; set; }

    [Id(5)]
    public int? SellPrice { get; init; }
}