﻿using System;

namespace Loka.Database.Common.Entities.Store;

public interface ICurrencyChildEntity
{
    Guid ModelId { get; }
}