﻿using System.Linq.Expressions;
using Loka.Database.Common.Repositories;
using Loka.Inventory.DataBase;
using Loka.Inventory.DataBase.Entities.Items;
using Loka.Inventory.Interfaces.Inventory;
using Loka.Inventory.Interfaces.Inventory.Requests;
using Loka.Store.Interfaces.Repositories;

namespace Loka.Inventory.Grains.Inventory;

public sealed class PlayerInventoryItemsRepository : BaseChildRepository<InventoryDbContext, PlayerItemEntity, PlayerInventoryItemModel.Repository>.Raw, IPlayerInventoryItemsRepository
{
    private IGameStoreItemsRepository GameStoreItemsRepository { get; }

    public PlayerInventoryItemsRepository(InventoryDbContext dbContext, IMapper mapper, IGameStoreItemsRepository gameStoreItemsRepository) : base(dbContext, mapper)
    {
        GameStoreItemsRepository = gameStoreItemsRepository;
    }

    protected override Func<Guid, Expression<Func<PlayerItemEntity, bool>>> Predicate { get; } = playerId => entity => entity.PlayerId == playerId;


    protected override async Task<OperationResult> AfterMap(Guid playerId, PlayerItemEntity entity, PlayerInventoryItemModel.Repository model)
    {
        var getModelResult = await GameStoreItemsRepository.GetById(entity.ModelId);
        if (!getModelResult.GetValueIfSucceeded(out var itemModel))
            return getModelResult.Error!;

        model.Model = itemModel;
        
        //  Todo: implement Modules
        return await base.AfterMap(playerId, entity, model);
    }

    public async Task<OperationResult<PlayerInventoryItemModel.Repository>> AddInventoryItem(Guid playerId, AddOrStackInventoryItem.Request request)
    {
        var entity = new PlayerItemEntity()
        {
            EntityId = Guid.NewGuid(),
            ModelId = request.ModelId,
            Amount = request.Amount,
            Level = 0,
            CreatedAt = DateTimeOffset.UtcNow,
            PlayerId = playerId
        };

        await DbSet.AddAsync(entity);
        await SaveChanges();

        return await GetById(playerId, entity.EntityId);
    }

    public async Task<OperationResult<PlayerInventoryItemModel.Repository>> AddOrStackInventoryItem(Guid playerId, AddOrStackInventoryItem.Request request)
    {
        var getModelResult = await GameStoreItemsRepository.GetById(request.ModelId);
        if (getModelResult.GetValueIfSucceeded(out var model))
        {
            if (model.AmountInStack == 1)
                return await AddInventoryItem(playerId, request);

            var entity = await DbSet
                .FirstOrDefaultAsync(q => q.ModelId == request.ModelId && q.PlayerId == playerId);

            if (entity != null)
            {
                entity.Amount += request.Amount;
                await SaveChanges();

                return await GetById(playerId, entity.EntityId);
            }

            return await AddInventoryItem(playerId, request);
        }

        return getModelResult.Error!;
    }
}