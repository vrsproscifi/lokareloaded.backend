﻿namespace Loka.Social.DataBase.Enums;

public enum PlayerFriendState : byte
{
    Waiting,
    Confirmed,
    ConfirmedWaiting,
    Rejected,
    Blocked,
    None,
}