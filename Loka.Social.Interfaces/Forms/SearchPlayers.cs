﻿using Loka.Game.Interfaces.Players.Dtos;

namespace Loka.Social.Interfaces.Forms;

public static class SearchPlayers
{
    [GenerateSerializer]
    public class Request
    {
        [Id(0)]
        public string? Term { get; init; }
    }

    [GenerateSerializer]
    public class Result
    {
        [Id(0)]
        public IReadOnlyList<PlayerModel> Entries { get; set; } = ArraySegment<PlayerModel>.Empty;
    }
}