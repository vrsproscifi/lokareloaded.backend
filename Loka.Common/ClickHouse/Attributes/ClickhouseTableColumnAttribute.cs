﻿using System;

namespace Loka.Common.ClickHouse.Attributes;

[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
public class ClickhouseTableColumnAttribute : Attribute
{
    public ClickhouseTableColumnAttribute(string name, ClickhouseColumnType type, bool ignoreInsert = false, bool nullable = false)
    {
        Name = name;
        Type = type;
        IgnoreInsert = ignoreInsert;
        Nullable = nullable;
    }

    public string Name { get; }
    public ClickhouseColumnType Type { get; }
    public bool Nullable { get; }
    public bool IgnoreInsert { get; }
}