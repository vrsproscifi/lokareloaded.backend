﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Loka.Admin.Common.Models.GamePlay;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Extensions;
using Loka.Database.Common.Specifications;
using Loka.Game.Database;
using Loka.Game.Database.Entities.Store;
using Microsoft.EntityFrameworkCore;

namespace Loka.Admin.Common.Services.Store.Currencies;

internal sealed class GameCurrencyService : IGameCurrencyService
{
    private IMapper Mapper { get; }
    private SystemDbContext SystemDbContext { get; }

    public GameCurrencyService(SystemDbContext systemDbContext, IMapper mapper)
    {
        SystemDbContext = systemDbContext;
        Mapper = mapper;
    }

    public async Task<OperationResult<CurrencyInformationModel>> GetById(Guid id, CancellationToken cancellationToken)
    {
        var entity = await SystemDbContext.GameCurrencyEntity.GetById(id, cancellationToken);
        if (entity == null)
            return OperationResultCode.PlayerNotFound;

        var model = Mapper.Map<CurrencyInformationModel>(entity);
        return model;
    }

    public async Task<OperationResult<CurrencyInformationModel[]>> GetByIds(IReadOnlyList<Guid> ids, CancellationToken cancellationToken = default)
    {
        var models = await SystemDbContext.GameCurrencyEntity
            .Where(new EntityIdsRangeSpecification<GameCurrencyEntity>(ids))
            .ProjectTo<CurrencyInformationModel>(Mapper.ConfigurationProvider)
            .ToArrayAsync(cancellationToken);

        return models;
    }
}