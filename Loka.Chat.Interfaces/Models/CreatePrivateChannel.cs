﻿using Loka.Chat.Interfaces.Enums;

namespace Loka.Chat.Interfaces.Models;

public static class CreatePrivateChannel
{
    [GenerateSerializer]
    public sealed class Request
    {
        [Id(0)]
        public string? Name { get; init; }
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public ChannelEntry Channel { get; init; } = null!;
    }
}