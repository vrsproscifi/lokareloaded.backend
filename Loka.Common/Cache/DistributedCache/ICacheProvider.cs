﻿using Microsoft.Extensions.Caching.Distributed;

namespace Loka.Common.Cache.DistributedCache;

public interface ICacheProvider
{
    Task<T> GetOrAdd<T>(string key, Func<string, Task<T>> fetcher, DistributedCacheEntryOptions? options = default) where T : class?;
    Task<T?> Get<T>(string key) where T : class;
    Task<T> Set<T>(string key, T value, DistributedCacheEntryOptions? options = null) where T : class;
    Task Remove(string key);
}