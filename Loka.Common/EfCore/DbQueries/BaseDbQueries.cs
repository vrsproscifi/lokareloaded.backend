﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Loka.Common.EfCore.DbQueries;

public interface IBaseDbQueries
{
        
}
    
public abstract class BaseDbQueries<TDbContext> : IBaseDbQueries
    where TDbContext : DbContext
{
    private TDbContext DbContext { get; }
    protected internal BaseDbQueries(TDbContext InDbContext)
    {
        DbContext = InDbContext;
    }

    protected internal Task<T> AddAndSaveAsync<T>(T InEntity) where T : class
    {
        return DbContext.AddAndSaveAsync(InEntity);
    }
}

public abstract class BaseDbQueries<TDbContext, TDbTable> : BaseDbQueries<TDbContext>
    where TDbContext : DbContext
    where TDbTable : class
{
    protected internal abstract DbSet<TDbTable> DbTable { get; }
    protected BaseDbQueries(TDbContext InDbContext) : base(InDbContext)
    {
    }
}