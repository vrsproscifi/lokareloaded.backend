﻿namespace Loka.Inventory.Interfaces.Workbench;

public interface IPlayerWorkbenchGrain: IGrainWithGuidKey
{
    Task<OperationResult<CraftWorkbenchItem.Result>> CraftWorkbenchItem(CraftWorkbenchItem.Request request);
}