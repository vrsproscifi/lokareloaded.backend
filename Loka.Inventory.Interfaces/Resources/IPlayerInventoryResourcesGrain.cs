namespace Loka.Inventory.Interfaces.Resources;

public interface IPlayerInventoryResourcesGrain : IGrainWithGuidKey
{
    Task<OperationResult<GetPlayerResources.Result>> GetResources();
    Task<OperationResult<GetPlayerResources.Result>> GetResources(IReadOnlyList<Guid> ids);
    Task<PlayerResourceModel.Lightweight> GetResourceByType(Guid id);
    Task<OperationResult<PlayerResourceModel.Delta>> GiveInventoryResource(GiveInventoryResource.Request request);
    Task<OperationResult<ExpendInventoryResources.Result>> ExpendInventoryResources(ExpendInventoryResources.Request request);

}