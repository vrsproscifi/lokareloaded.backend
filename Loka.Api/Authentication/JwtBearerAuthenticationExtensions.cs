﻿using Loka.Identity.Interfaces.Models;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;

namespace Loka.Api.Authentication;

public static class JwtBearerAuthenticationExtensions
{
    public static IServiceCollection AddAuthenticationServices(this IServiceCollection services, IConfiguration configuration)
    {
        var settingsSection = configuration
            .GetSection(nameof(JwtBearerAuthenticationSettings));

        var settings = settingsSection
            .Get<JwtBearerAuthenticationSettings>();

        if (settings == null)
            throw new NullReferenceException($"Failed get {nameof(JwtBearerAuthenticationSettings)}");

        settings.Validate();
        services.Configure<JwtBearerAuthenticationSettings>(settingsSection);

        services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(o =>
            {
                o.RequireHttpsMetadata = false;
                o.IncludeErrorDetails = true;

                o.SecurityTokenValidators.Clear();
                o.SecurityTokenValidators.Add(new JwtTokenValidator(settings));

                o.TokenValidationParameters = new TokenValidationParameters()
                {
                    RequireSignedTokens = false,
                    RequireAudience = false,
                    RequireExpirationTime = false,

                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(settings.SigningKey!)),

                    ValidIssuer = settings.Issuer,
                    ValidAudience = settings.Audience,
                    ValidAudiences = new[]
                    {
                        settings.Audience,
                    },
                };
            });

        //services.AddAuthorization(options => { });

        return services;
    }
}