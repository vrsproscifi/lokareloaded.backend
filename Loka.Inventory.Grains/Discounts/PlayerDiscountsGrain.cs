﻿using Loka.Common.Authentication;
using Loka.Common.Orleans.Grains;
using Loka.Inventory.DataBase;
using Loka.Inventory.Interfaces.Discounts;

namespace Loka.Inventory.Grains.Discounts;

internal sealed class PlayerDiscountsGrain : Grain, IPlayerDiscountsGrain, IWithPlayerActor
{
    private IMapper Mapper { get; }
    private InventoryDbContext InventoryDbContext { get; }
    public PlayerActor Actor { get; set; } = null!;

    public PlayerDiscountsGrain(InventoryDbContext inventoryDbContext, IMapper mapper)
    {
        InventoryDbContext = inventoryDbContext;
        Mapper = mapper;
    }

    public async Task<OperationResult<GetPlayerDiscounts.Result>> GetDiscounts()
    {
        var entities = await InventoryDbContext.PlayerDiscountEntity
            .Where(q => q.PlayerId == Actor.PlayerId)
            .Where(q => q.EndDiscountDate == null || q.EndDiscountDate < DateTimeOffset.UtcNow)
            .ProjectTo<GetPlayerDiscounts.Entry>(Mapper.ConfigurationProvider)
            .ToArrayAsync();

        return new GetPlayerDiscounts.Result()
        {
            Entries = entities
        };
    }
}