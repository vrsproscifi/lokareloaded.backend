﻿using AutoMapper;
using AutoMapper.Extensions.EnumMapping;

namespace Loka.Admin.Api.Mapping.Shared;

public sealed class OrderByDirectionMapping : Profile
{
    public OrderByDirectionMapping()
    {
        CreateMap<Loka.Common.Pagination.OrderByDirection, OrderByDirection>()
            .ConvertUsingEnumMapping(ConfigureEnumMapping)
            .ReverseMap();
    }

    private void ConfigureEnumMapping(IEnumConfigurationExpression<Loka.Common.Pagination.OrderByDirection, OrderByDirection> configuration)
    {
        configuration
            .MapValue(Loka.Common.Pagination.OrderByDirection.Ascending, OrderByDirection.Ascending)
            .MapValue(Loka.Common.Pagination.OrderByDirection.Descending, OrderByDirection.Descending);
    }
}