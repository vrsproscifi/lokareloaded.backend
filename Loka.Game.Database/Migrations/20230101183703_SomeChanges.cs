﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Loka.Game.Database.Migrations
{
    /// <inheritdoc />
    public partial class SomeChanges : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 323, DateTimeKind.Unspecified).AddTicks(7719), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe602"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 323, DateTimeKind.Unspecified).AddTicks(8873), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe603"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 323, DateTimeKind.Unspecified).AddTicks(9154), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe604"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 323, DateTimeKind.Unspecified).AddTicks(9411), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe605"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 323, DateTimeKind.Unspecified).AddTicks(9662), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe100"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(936), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(2048), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(2328), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(2588), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(2837), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(3080), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(3324), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(3686), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(3993), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe109"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(4241), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe110"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(4479), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe111"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(4714), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe112"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(4954), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe113"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(5196), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe114"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(5433), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe115"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(5669), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe116"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(5901), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe117"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(6137), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe118"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(6461), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe119"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(6729), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe120"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(6964), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe121"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(7197), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe122"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(7481), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe123"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(7744), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe124"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(7983), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe125"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(8215), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe126"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(8449), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe127"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(8690), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe128"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(9006), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe129"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(9283), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe130"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 339, DateTimeKind.Unspecified).AddTicks(9525), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe100"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(1586), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(1882), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(2152), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(2408), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(2750), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(3075), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(3349), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(3593), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(3832), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe109"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(4068), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe110"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(4308), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe111"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(4544), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe112"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(4783), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe113"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(5015), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe114"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(5252), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe115"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(5489), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe116"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(5789), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe117"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(6229), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe118"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(6621), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe119"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(6885), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe120"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(7127), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe121"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(7363), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe122"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(7600), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe123"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(7837), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe124"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(8077), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe125"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(8313), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe126"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(8544), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe127"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(8776), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe128"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(9003), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe129"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(9304), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe130"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(9570), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe131"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(9810), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe132"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(132), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe133"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(408), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe134"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(648), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe135"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(884), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe136"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(1205), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe137"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(1503), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe138"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(1755), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe139"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(1986), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe140"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(2218), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe141"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(2445), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe142"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(2672), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe143"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(2898), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe144"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(3126), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe145"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(3378), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe147"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(3624), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe148"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(3853), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe149"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(4076), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe151"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(4307), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe152"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(4536), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe154"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(4764), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe155"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(4989), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe156"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(5212), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe157"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(5438), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe158"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(5665), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe159"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(5894), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe160"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(6121), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe161"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(6678), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe162"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(6973), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe163"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(7209), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe164"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(7442), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe165"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(7672), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe166"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(7902), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe167"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(8132), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe168"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(8362), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe170"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(8587), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe171"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(8815), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe172"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(9141), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe173"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(9445), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe174"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(9696), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe175"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 332, DateTimeKind.Unspecified).AddTicks(9926), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe176"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(157), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe177"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(384), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe178"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(657), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe179"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(887), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe180"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(1114), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe181"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(1340), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe182"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(1633), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe183"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(1914), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe184"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(2149), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe185"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(2384), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe186"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(2613), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe187"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(2845), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe188"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(3070), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe189"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(3301), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe190"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(3528), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe191"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(3753), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe192"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(3981), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe193"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(4207), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe194"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(4435), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe196"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(4667), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe197"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(4892), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe198"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(5118), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe199"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(5345), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe200"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(5573), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe201"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(5798), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe203"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(6071), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe204"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(6397), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe205"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(6661), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe206"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(6985), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe207"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(7286), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe208"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(7538), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe209"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(7802), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe210"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(8035), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe211"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(8264), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe212"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(8494), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe213"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(8726), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe214"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(8953), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe217"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(9181), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe218"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(9408), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe220"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(9635), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe221"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 333, DateTimeKind.Unspecified).AddTicks(9858), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe222"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(89), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe223"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(314), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe224"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(542), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe225"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(770), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe226"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(997), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe227"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(1226), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe228"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(1451), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe229"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(1682), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe230"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(1911), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe231"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(2138), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe232"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(2365), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe233"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(2593), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe234"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(2918), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe235"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(3198), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe236"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(3429), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe237"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(3657), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe238"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(3888), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe239"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(4117), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe240"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(4366), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe241"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(4739), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe242"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(5043), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe243"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(5294), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe244"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(5522), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe245"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(5752), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe246"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(5983), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe247"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(6210), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe248"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(6607), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe249"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(6867), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe250"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(7100), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe251"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(7324), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe252"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(7552), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe253"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(7821), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe254"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(8175), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe255"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(8399), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe256"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(8628), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe257"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(8857), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe258"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(9087), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe259"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(9315), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe260"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(9539), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe261"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(9762), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe262"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 334, DateTimeKind.Unspecified).AddTicks(9988), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe263"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(212), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe264"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(434), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe265"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(656), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe266"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(882), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe268"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(1111), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe269"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(1338), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe270"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(1590), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe271"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(1837), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe272"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(2064), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe273"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(2290), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe274"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(2637), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe275"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(3027), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe276"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(3292), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe277"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(3525), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe278"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(3757), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe279"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(3985), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe280"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(4212), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe281"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(4436), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe282"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(4739), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe283"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(5010), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe284"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(5244), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe285"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(5470), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe286"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(5701), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe287"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(5928), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe288"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(6153), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe289"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(6460), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe290"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(6715), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe291"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(6947), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe292"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(7179), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe293"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(7409), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe294"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(7634), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe295"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(7857), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe296"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(8081), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe297"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(8308), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe298"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(8535), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe299"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(8790), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe300"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(9041), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe301"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(9277), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe302"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(9502), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe303"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(9728), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe304"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 335, DateTimeKind.Unspecified).AddTicks(9956), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe305"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(183), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe306"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(498), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe307"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(799), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe308"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(1046), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe309"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(1274), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe310"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(1506), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe311"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(1735), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe312"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(1961), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe313"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(2185), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe314"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(2410), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe315"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(2638), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe316"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(2866), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe317"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(3091), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe318"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(3320), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe319"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(3549), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe320"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(3780), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe321"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(4010), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe322"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(4237), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe323"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(4464), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe324"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(4688), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe325"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(4912), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe326"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(5136), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe327"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(5365), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe328"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(5662), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe329"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(5938), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe330"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(6175), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe331"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(6836), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe332"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(7360), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe333"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(7790), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe334"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(8032), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe335"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(8390), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe338"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(8703), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe339"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(9147), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe340"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(9505), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe341"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 336, DateTimeKind.Unspecified).AddTicks(9815), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe342"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 337, DateTimeKind.Unspecified).AddTicks(66), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe343"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 331, DateTimeKind.Unspecified).AddTicks(127), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 337, DateTimeKind.Unspecified).AddTicks(9543), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 338, DateTimeKind.Unspecified).AddTicks(636), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 338, DateTimeKind.Unspecified).AddTicks(1046), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 338, DateTimeKind.Unspecified).AddTicks(1449), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 338, DateTimeKind.Unspecified).AddTicks(1758), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 338, DateTimeKind.Unspecified).AddTicks(2006), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 338, DateTimeKind.Unspecified).AddTicks(2248), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 338, DateTimeKind.Unspecified).AddTicks(2483), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldGenderEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 337, DateTimeKind.Unspecified).AddTicks(4143), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldGenderEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 337, DateTimeKind.Unspecified).AddTicks(4987), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 325, DateTimeKind.Unspecified).AddTicks(303), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 324, DateTimeKind.Unspecified).AddTicks(9077), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 37, 3, 325, DateTimeKind.Unspecified).AddTicks(675), new TimeSpan(0, 0, 0, 0, 0)));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 672, DateTimeKind.Unspecified).AddTicks(1803), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe602"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 672, DateTimeKind.Unspecified).AddTicks(2660), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe603"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 672, DateTimeKind.Unspecified).AddTicks(2868), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe604"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 672, DateTimeKind.Unspecified).AddTicks(3060), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe605"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 672, DateTimeKind.Unspecified).AddTicks(3246), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe100"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(6469), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(7473), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(7712), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(7966), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(8240), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(8461), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(8655), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(8846), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(9033), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe109"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(9220), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe110"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(9406), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe111"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(9588), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe112"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(9771), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe113"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(9991), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe114"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(197), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe115"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(384), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe116"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(569), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe117"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(755), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe118"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(938), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe119"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(1122), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe120"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(1306), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe121"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(1492), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe122"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(1677), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe123"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(1859), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe124"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(2041), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe125"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(2227), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe126"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(2411), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe127"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(2597), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe128"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(2785), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe129"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(2970), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe130"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(3155), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe100"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(5362), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(5661), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(5894), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(6088), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(6274), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(6459), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(6644), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(6867), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(7080), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe109"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(7268), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe110"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(7455), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe111"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(7641), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe112"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(7825), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe113"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(8027), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe114"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(8220), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe115"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(8400), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe116"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(8582), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe117"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(8761), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe118"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(8937), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe119"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(9118), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe120"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(9332), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe121"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(9534), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe122"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(9719), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe123"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(9902), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe124"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(83), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe125"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(265), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe126"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(448), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe127"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(628), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe128"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(808), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe129"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(987), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe130"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(1169), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe131"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(1348), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe132"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(1527), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe133"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(1779), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe134"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(1989), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe135"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(2172), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe136"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(2352), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe137"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(2537), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe138"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(2722), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe139"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(2907), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe140"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(3087), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe141"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(3266), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe142"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(3461), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe143"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(3654), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe144"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(3835), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe145"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(4015), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe147"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(4194), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe148"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(4377), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe149"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(4557), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe151"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(4736), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe152"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(4916), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe154"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(5096), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe155"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(5274), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe156"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(5453), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe157"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(5688), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe158"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(5892), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe159"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(6076), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe160"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(6258), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe161"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(6441), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe162"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(6625), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe163"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(6806), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe164"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(6988), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe165"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(7169), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe166"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(7352), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe167"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(7537), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe168"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(7845), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe170"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(8063), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe171"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(8250), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe172"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(8430), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe173"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(8613), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe174"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(8797), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe175"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(9003), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe176"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(9187), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe177"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(9370), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe178"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(9552), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe179"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(9735), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe180"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(9919), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe181"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(102), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe182"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(281), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe183"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(464), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe184"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(642), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe185"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(825), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe186"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(1008), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe187"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(1189), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe188"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(1366), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe189"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(1549), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe190"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(1731), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe191"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(1906), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe192"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(2172), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe193"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(2390), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe194"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(2574), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe196"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(2751), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe197"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(2934), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe198"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(3113), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe199"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(3290), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe200"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(3471), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe201"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(3652), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe203"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(3914), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe204"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(4130), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe205"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(4317), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe206"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(4554), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe207"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(4761), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe208"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(4944), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe209"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(5125), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe210"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(5304), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe211"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(5489), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe212"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(5668), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe213"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(5849), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe214"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(6029), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe217"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(6209), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe218"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(6387), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe220"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(6567), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe221"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(6746), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe222"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(6926), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe223"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(7104), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe224"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(7285), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe225"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(7464), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe226"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(7646), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe227"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(7822), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe228"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(7999), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe229"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(8179), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe230"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(8358), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe231"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(8538), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe232"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(8719), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe233"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(8898), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe234"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(9075), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe235"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(9254), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe236"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(9433), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe237"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(9628), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe238"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(9876), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe239"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(87), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe240"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(275), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe241"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(458), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe242"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(639), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe243"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(821), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe244"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(1003), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe245"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(1181), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe246"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(1361), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe247"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(1539), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe248"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(1716), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe249"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(1895), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe250"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(2076), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe251"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(2251), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe252"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(2429), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe253"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(2605), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe254"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(2783), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe255"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(2961), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe256"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(3188), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe257"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(3392), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe258"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(3575), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe259"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(3756), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe260"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(3934), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe261"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(4110), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe262"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(4289), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe263"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(4467), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe264"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(4644), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe265"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(4867), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe266"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(5082), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe268"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(5274), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe269"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(5457), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe270"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(5636), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe271"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(5881), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe272"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(6091), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe273"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(6275), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe274"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(6457), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe275"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(6636), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe276"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(6816), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe277"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(6996), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe278"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(7178), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe279"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(7360), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe280"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(7537), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe281"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(7715), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe282"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(7895), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe283"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(8075), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe284"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(8251), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe285"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(8430), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe286"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(8612), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe287"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(8793), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe288"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(8972), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe289"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(9152), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe290"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(9331), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe291"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(9509), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe292"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(9685), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe293"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(9864), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe294"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(42), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe295"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(219), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe296"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(414), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe297"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(606), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe298"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(788), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe299"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(965), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe300"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(1145), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe301"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(1324), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe302"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(1501), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe303"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(1782), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe304"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(1998), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe305"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(2187), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe306"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(2374), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe307"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(2557), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe308"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(2738), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe309"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(2916), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe310"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(3098), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe311"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(3278), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe312"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(3458), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe313"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(3635), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe314"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(3813), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe315"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(3992), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe316"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(4172), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe317"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(4355), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe318"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(4536), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe319"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(4714), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe320"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(4893), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe321"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(5072), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe322"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(5250), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe323"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(5426), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe324"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(5606), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe325"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(5784), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe326"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(5962), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe327"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(6137), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe328"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(6318), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe329"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(6497), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe330"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(6675), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe331"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(6855), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe332"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(7033), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe333"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(7208), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe334"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(7387), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe335"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(7711), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe338"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(7945), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe339"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(8129), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe340"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(8311), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe341"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(8494), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe342"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(8672), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe343"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(4182), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(5477), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(6238), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(6447), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(6641), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(6831), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(7018), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(7252), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(7526), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldGenderEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(1257), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldGenderEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(1895), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 673, DateTimeKind.Unspecified).AddTicks(1102), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 673, DateTimeKind.Unspecified).AddTicks(224), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 673, DateTimeKind.Unspecified).AddTicks(1338), new TimeSpan(0, 0, 0, 0, 0)));
        }
    }
}
