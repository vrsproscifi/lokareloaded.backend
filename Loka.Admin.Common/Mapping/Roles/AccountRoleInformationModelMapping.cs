﻿using AutoMapper;
using Loka.Admin.Common.Models.Accounts;
using Loka.Game.Database.Entities.Roles;

namespace Loka.Admin.Common.Mapping.Roles;

public class AccountRoleInformationModelMapping: Profile
{
    public AccountRoleInformationModelMapping()
    {
        CreateMap<AccountRoleEntity, AccountRoleInformationModel>()
            .ForMember(q => q.Id, q => q.MapFrom(p => p.EntityId))
            .ForMember(q => q.Name, q => q.MapFrom(p => p.Name));
    }
}