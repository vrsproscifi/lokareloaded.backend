namespace Loka.Common.Ranges.Numerics;

public enum NumericRangeEndOptions
{
    Less,
    LessOrEqual
}