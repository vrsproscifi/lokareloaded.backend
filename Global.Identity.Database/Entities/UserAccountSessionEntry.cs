﻿using System.Net;
using MongoDB.Bson;

namespace Global.Identity.Database.Entities;

public sealed class UserAccountSessionEntry
{
    public ObjectId EntityId { get; set; }
    public Guid UserAccountId { get; set; }
    public DateTimeOffset CreatedAt { get; set; }
    public IPAddress IpAddress { get; set; } = null!;
}