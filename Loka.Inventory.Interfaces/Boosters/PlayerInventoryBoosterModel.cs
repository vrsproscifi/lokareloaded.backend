﻿namespace Loka.Inventory.Interfaces.Boosters;

[GenerateSerializer]
public sealed class PlayerInventoryBoosterModel : IEntry
{
    [Id(0)]
    public Guid Id { get; init; }

    [Id(1)]
    public Guid ModelId { get; init; }

    [Id(2)]
    public long Amount { get; init; }
    
    [Id(3)]
    public bool IsActive { get; set; }

}