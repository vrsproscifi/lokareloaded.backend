﻿using AutoMapper;
using Loka.Common.Cache.DistributedCache;
using Loka.Common.Entry;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Entities;
using Microsoft.EntityFrameworkCore;

namespace Loka.Database.Common.Repositories;

public abstract class BaseRepository<TDbContext, TEntity, TModel> : IBaseRepository<TModel>
    where TModel : class, IEntry
    where TEntity : BaseEntity
    where TDbContext : DbContext
{
    private static Type ModelType { get; } = typeof(TModel);
    private static string ModelTypeName { get; } = typeof(TModel).Name;

    private IMapper Mapper { get; }
    private ICacheProvider CacheProvider { get; }
    protected TDbContext DbContext { get; }
    protected DbSet<TEntity> DbSet { get; }

    protected BaseRepository(ICacheProvider cacheProvider, TDbContext dbContext, IMapper mapper)
    {
        CacheProvider = cacheProvider;
        DbContext = dbContext;
        Mapper = mapper;
        DbSet = DbContext.Set<TEntity>();
    }

    protected virtual Task<OperationResult> AfterMap(TEntity entity, TModel model)
    {
        return Task.FromResult(new OperationResult(OperationResultCode.Successfully));
    }

    public async Task<OperationResult<TModel>> GetById(Guid id)
    {
        var model = await CacheProvider.GetOrAdd( string.Intern($"${ModelTypeName}_{id:N}_{Guid.NewGuid()}"), async key =>
        {
            var entity = await DbSet.FirstOrDefaultAsync(q => q.EntityId == id);
            if (entity != null)
            {
                var model = Mapper.Map<TModel>(entity);
                await AfterMap(entity, model);
                return model;
            }

            return null;
        });

        if (model == null)
        {
            return (OperationResultCode.EntityNotFound);
        }

        return new OperationResult<TModel>(model);
    }

    public async Task<IReadOnlyList<TModel>> GetByIds(IReadOnlyList<Guid> ids)
    {
        var models = new List<TModel>(ids.Count);
        foreach (var id in ids)
        {
            var model = await GetById(id);
            if (model.IsSucceeded)
            {
                models.Add(model.Result);
            }
        }

        return models;
    }

    public Task<IReadOnlyList<TModel>> GetByIds(string cacheKey, IReadOnlyList<Guid> ids)
    {
        return CacheProvider.GetOrAdd(string.Intern($"{ModelTypeName}_{cacheKey}_{Guid.NewGuid()}"), key => GetByIds(ids));
    }

    public Task<IReadOnlyList<TModel>> FilterBy(string cacheKey, Func<TModel, bool> predicate)
    {
        return CacheProvider.GetOrAdd(string.Intern($"{ModelTypeName}_{cacheKey}_{Guid.NewGuid()}"), key => FilterBy(predicate));
    }

    public async Task<IReadOnlyList<TModel>> FilterBy(Func<TModel, bool> predicate)
    {
        var models = await GetAll();
        var result = models
            .Where(predicate)
            .ToArray();

        return result;
    }

    public async Task<IReadOnlyList<TModel>> GetAll()
    {
        return await CacheProvider.GetOrAdd(string.Intern($"{ModelTypeName}_all_{Guid.NewGuid()}"), async key =>
        {
            var entities = await DbSet
                //.Where(q => q.DeletedAt == null)
                .ToArrayAsync();

            var models = new List<TModel>(entities.Length);
            foreach (var entity in entities)
            {
                var model = Mapper.Map<TModel>(entity);
                await AfterMap(entity, model);
                models.Add(model);
            }

            return models;
        });
    }
}