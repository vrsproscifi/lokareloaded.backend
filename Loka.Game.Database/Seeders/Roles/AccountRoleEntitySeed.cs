﻿using Loka.Game.Database.Entities.Roles;
using Loka.Game.Database.Enums.Account;

namespace Loka.Game.Database.Seeders.Roles;

public static class AccountRoleEntitySeed
{
    public static Guid Administrator { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe601");
    public static Guid Partner { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe602");
    public static Guid Moderator { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe603");
    public static Guid Developer { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe604");
    public static Guid BetaTester { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe605");

    public static AccountRoleEntity[] Entities { get; } = new[]
    {
        new AccountRoleEntity(Administrator, AccountRoleTypeId.Administrator),
        new AccountRoleEntity(Partner, AccountRoleTypeId.Partner),
        new AccountRoleEntity(Moderator, AccountRoleTypeId.Moderator),
        new AccountRoleEntity(Developer, AccountRoleTypeId.Developer),
        new AccountRoleEntity(BetaTester, AccountRoleTypeId.BetaTester),
    };
}