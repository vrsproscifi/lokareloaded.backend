﻿using System;
using Loka.MatchMaking.DataBase.Entities.Game;
using static Loka.MatchMaking.DataBase.Seeders.Game.GameMapEntitySeeder;
using static Loka.MatchMaking.DataBase.Seeders.Game.GameModeEntitySeeder;

namespace Loka.MatchMaking.DataBase.Seeders.Game;

public static class GameModeMapEntitySeeder
{
    public static GameModeMapEntity[] Entities { get; } = new[]
    {
        new GameModeMapEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe101"), TeamDeadMatch, Hangar),
        new GameModeMapEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe201"), LostDeadMatch, Hangar),
        new GameModeMapEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe301"), ResearchMatch, Hangar),
        new GameModeMapEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe401"), DuelMatch, Hangar),
    };
}