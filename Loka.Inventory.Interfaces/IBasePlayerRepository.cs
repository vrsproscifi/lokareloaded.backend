﻿namespace Loka.Inventory.Interfaces;

public interface IBasePlayerRepository<TModel> where TModel : class, IEntry
{
    Task<OperationResult<TModel>> GetById(Guid playerId, Guid id);
    Task<IReadOnlyList<TModel>> GetByIds(Guid playerId, IReadOnlyList<Guid> ids);
    Task<IReadOnlyList<TModel>> GetByIds(Guid playerId, string cacheKey, IReadOnlyList<Guid> ids);
    Task<IReadOnlyList<TModel>> FilterBy(Guid playerId, string cacheKey, Func<TModel, bool> predicate);
    Task<IReadOnlyList<TModel>> FilterBy(Guid playerId, Func<TModel, bool> predicate);
    Task<IReadOnlyList<TModel>> GetAll(Guid playerId);
}