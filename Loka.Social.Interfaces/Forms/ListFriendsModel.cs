﻿using Loka.Game.Interfaces.Players.Dtos;

namespace Loka.Social.Interfaces.Forms;

[GenerateSerializer]
public class ListFriendsResponse
{
    [Id(0)]
    public IReadOnlyList<PlayerFriendEntryModel> Players { get; set; } = ArraySegment<PlayerFriendEntryModel>.Empty;
}

[GenerateSerializer]
public class AddPlayerToFriendListRequest
{
    [Id(0)]
    public Guid PlayerId { get; set; }
}

[GenerateSerializer]
public class RemoveFriendRequest
{
    [Id(0)]
    public Guid FriendId { get; set; }
}

[GenerateSerializer]
public class SearchPlayersRequest
{
    [Id(0)]
    public string Term { get; set; }= null!;
}

[GenerateSerializer]
public class SearchPlayersResponse
{
    [Id(0)]
    public IReadOnlyList<PlayerModel> Players { get; set; } = ArraySegment<PlayerModel>.Empty;
}

[GenerateSerializer]
public class RespondForInviteResponse
{
    [Id(0)]
    public string Term { get; set; }= null!;
}

[GenerateSerializer]
public class RespondForInviteRequest
{
    [Id(0)]
    public string Term { get; set; }= null!;
}