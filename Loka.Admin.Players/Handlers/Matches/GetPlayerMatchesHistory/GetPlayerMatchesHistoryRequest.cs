﻿using System;
using Loka.Common.MediatR.Requests;
using Loka.Common.Pagination;
using Loka.Common.Ranges.Numerics;

namespace Loka.Admin.Players.Handlers.Matches.GetPlayerMatchesHistory;

public sealed class GetPlayerMatchesHistoryRequest : IOperationRequest<PagingResult<GetPlayerMatchesHistoryResponse>>
{
    public Guid PlayerId { get; set; }
    public bool? HasSquad { get; set; }

    public DateTime From { get; set; }
    public DateTime End { get; set; }

    public Guid[] GameVersionIds { get; set; }
    public Guid[] GameModeIds { get; set; }
    public Guid[] GameMapIds { get; set; }

    public NumericRange<int?, int?> Level { get; set; }
    public NumericRange<int?, int?> Kills { get; set; }
    public NumericRange<int?, int?> Deaths { get; set; }
    public NumericRange<int?, int?> Score { get; set; }

    public PagingQuery Paging { get; set; }
}