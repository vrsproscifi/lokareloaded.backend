﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Entities;

namespace Loka.MatchMaking.DataBase.Entities.Game;

public class GameModeMapEntity : BaseEntity
{
    [ForeignKey(nameof(GameModeEntity))]
    public Guid GameModeId { get; set; }
    public GameModeEntity? GameModeEntity { get; set; }
        
        
    [ForeignKey(nameof(GameMapEntity))]
    public Guid GameMapId { get; set; }
    public GameMapEntity? GameMapEntity { get; set; }
        
    public GameModeMapEntity(){}

    internal GameModeMapEntity(Guid id, Guid gameModeId, Guid gameMapId) : base(id)
    {
        GameModeId = gameModeId;
        GameMapId = gameMapId;
    }
}