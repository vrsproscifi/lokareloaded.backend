﻿using AutoMapper;
using Loka.Common.Ranges.Numerics;

namespace Loka.Admin.Api.Mapping.Shared;

public class ValueRangesMapping : Profile
{
    public ValueRangesMapping()
    {
        #region int
        CreateMap<UInt32Range, NumericRange<uint?, uint?>>()
            .ForMember(q => q.Begin, q => q.MapFrom(p => p.From))
            .ForMember(q => q.End, q => q.MapFrom(p => p.End));
                
        CreateMap<Int32Range, NumericRange<int?, int?>>()
            .ForMember(q => q.Begin, q => q.MapFrom(p => p.From))
            .ForMember(q => q.End, q => q.MapFrom(p => p.End));

        #endregion

        #region long
            
        CreateMap<UInt64Range, NumericRange<ulong?, ulong?>>()
            .ForMember(q => q.Begin, q => q.MapFrom(p => p.From))
            .ForMember(q => q.End, q => q.MapFrom(p => p.End));

        CreateMap<Int64Range, NumericRange<long?, long?>>()
            .ForMember(q => q.Begin, q => q.MapFrom(p => p.From))
            .ForMember(q => q.End, q => q.MapFrom(p => p.End));

        #endregion

        CreateMap<FloatRange, NumericRange<float?, float?>>()
            .ForMember(q => q.Begin, q => q.MapFrom(p => p.From))
            .ForMember(q => q.End, q => q.MapFrom(p => p.End));
            
        CreateMap<DoubleRange, NumericRange<double?, double?>>()
            .ForMember(q => q.Begin, q => q.MapFrom(p => p.From))
            .ForMember(q => q.End, q => q.MapFrom(p => p.End));
    }
}