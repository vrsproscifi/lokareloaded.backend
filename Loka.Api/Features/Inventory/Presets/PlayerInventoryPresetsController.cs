﻿using Loka.Api.Features.Inventory.Items;
using Loka.Inventory.Interfaces.Presets;
using Loka.Inventory.Interfaces.Store;

namespace Loka.Api.Features.Inventory.Presets;

internal sealed class PlayerInventoryPresetsController : PlayerInventoryPresets.PlayerInventoryPresetsBase
{
    private ControllerServices<PlayerInventoryPresetsController> Services { get; }
    private PlayerActor PlayerActor { get; }

    public PlayerInventoryPresetsController(ControllerServices<PlayerInventoryPresetsController> services, PlayerActor playerActor)
    {
        Services = services;
        PlayerActor = playerActor;
    }

    public override Task<EquipInventoryItem.Types.Response> EquipInventoryItem(EquipInventoryItem.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerInventoryPresetGrain>(PlayerActor.PlayerId);
        return grain.Equip(new Loka.Inventory.Interfaces.Presets.Models.EquipInventoryItem.Request()
        {
            ItemId = request.Id.ToGuid(),
            PresetId = request.PresetId.ToGuid()
        }).ReturnResult<Loka.Inventory.Interfaces.Presets.Models.EquipInventoryItem.Result, EquipInventoryItem.Types.Response>(Services.Mapper);
    }

    public override Task<BuyInventoryPreset.Types.Response> BuyInventoryPreset(BuyInventoryPreset.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerStoreGrain>(PlayerActor.PlayerId);
        return grain.BuyPreset(new BuyPlayerInventoryPresets.Request()
        {
            ModelId = request.ModelId.ToGuid()
        }).ReturnResult<BuyPlayerInventoryPresets.Result, BuyInventoryPreset.Types.Response>(Services.Mapper);
    }

    public override Task<GetPlayerInventoryPresets.Types.Response> GetPlayerInventoryPresets(GetPlayerInventoryPresets.Types.Request request, ServerCallContext context)
    {
        var grain = Services.ClusterClient.GetGrain<IPlayerInventoryPresetGrain>(PlayerActor.PlayerId);
        return grain.GetPresets().ReturnResult<Loka.Inventory.Interfaces.Presets.Models.GetPlayerInventoryPresets.Result, GetPlayerInventoryPresets.Types.Response>(Services.Mapper);
    }
}