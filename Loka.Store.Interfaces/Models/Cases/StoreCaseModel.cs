﻿using Loka.Common.Entry;
using Loka.Store.Interfaces.Models.Items;
using Loka.Store.Interfaces.Models.Resources;

namespace Loka.Store.Interfaces.Models.Cases;

public interface ICaseDropEntry
{
    int MinimumCount { get; }

    int MaximumCount { get; }

    double ChanceOfDrop { get; }
}

[GenerateSerializer]
public sealed class StoreCaseModel : IEntry, INamedEntry
{
    [Id(0)]
    public Guid Id { get; init; }

    [Id(1)]
    public string Name { get; init; } = null!;

    [Id(2)]
    public int? RandomMaxDropItems { get; init; }

    [Id(3)]
    public int? RandomMaxDropResources { get; init; }

    [Id(4)]
    public IReadOnlyList<Item> Items { get; set; } = null!;

    [Id(5)]
    public IReadOnlyList<Resource> Resources { get; set; } = null!;

    [Id(6)]
    public GameResourceModel? SellCurrency { get; set; }

    [Id(7)]
    public int? SellPrice { get; init; }

    [GenerateSerializer]
    public sealed class Item : IEntry, ICaseDropEntry
    {
        [Id(0)]
        public Guid Id { get; set; }

        [Id(1)]
        public StoreItemModel Model { get; set; } = null!;

        [Id(2)]
        public int MinimumCount { get; set; }

        [Id(3)]
        public int MaximumCount { get; set; }

        /// <summary>
        /// Chance Of Drop 1 of 
        /// </summary>
        [Id(4)]
        public double ChanceOfDrop { get; set; }
    }

    [GenerateSerializer]
    public sealed class Resource : IEntry, ICaseDropEntry
    {
        [Id(0)]
        public Guid Id { get; set; }

        [Id(1)]
        public GameResourceModel Model { get; set; } = null!;

        [Id(2)]
        public int MinimumCount { get; set; }

        [Id(3)]
        public int MaximumCount { get; set; }

        [Id(4)]
        public double ChanceOfDrop { get; set; }
    }
}