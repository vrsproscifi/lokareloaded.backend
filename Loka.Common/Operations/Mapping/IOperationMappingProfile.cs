﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Loka.Common.Operations.Results;

namespace Loka.Common.Operations.Mapping;

public interface IOperationMappingProfile<TEntity, TModel>
{
    Task<OperationResult> Map(IReadOnlyList<TEntity> entities, IReadOnlyList<TModel> models, CancellationToken cancellationToken);
}