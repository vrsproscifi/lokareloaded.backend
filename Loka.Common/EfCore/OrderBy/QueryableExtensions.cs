using System;
using System.Linq;
using System.Linq.Expressions;

namespace Loka.Common.EfCore.OrderBy;

public static class QueryableExtensions
{
    public static IOrderedQueryable<TSource> OrderBy<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, bool orderByAsc)
    {
        if (orderByAsc)
            return source.OrderBy(keySelector);

        return source.OrderByDescending(keySelector);
    }

    public static IOrderedQueryable<TSource> ThenBy<TSource, TKey>(this IOrderedQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, bool orderByAsc)
    {
        if (orderByAsc)
            return source.ThenBy(keySelector);

        return source.ThenByDescending(keySelector);
    }
        
    public static IQueryable<TEntity> ApplySort<TKey, TEntity, TOrderBy>(this IQueryable<TEntity> source, PropertySortMapping<TKey, TEntity, TOrderBy> mapping, TOrderBy orderBy)
        where TEntity : class
        where TKey : Enum
        where TOrderBy : IOrderByMethod<TKey>
    {
        if (source == null)
            throw new ArgumentNullException(nameof(source));

        if (mapping == null)
            throw new ArgumentNullException(nameof(mapping));

        return mapping.ApplySort(source, orderBy);
    }
        
    public static IQueryable<TEntity> ApplySort<TKey, TEntity>(this IQueryable<TEntity> source, PropertySortMapping<TKey, TEntity> mapping, OrderByMethod<TKey> orderBy)
        where TEntity : class
        where TKey : Enum
    {
        if (source == null)
            throw new ArgumentNullException(nameof(source));

        if (mapping == null)
            throw new ArgumentNullException(nameof(mapping));

        return mapping.ApplySort(source, orderBy);
    }
}