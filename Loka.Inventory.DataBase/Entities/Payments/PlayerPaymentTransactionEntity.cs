﻿using Loka.Inventory.Interfaces.Payments;
using MongoDB.Bson;

namespace Loka.Inventory.DataBase.Entities.Payments;

public sealed class PlayerPaymentTransactionEntity
{
    public ObjectId Id { get; init; }
    public PlayerPaymentTransactionTypeId TypeId { get; init; }
    public Guid ModelId { get; init; }
    public int Count { get; init; }
    public IReadOnlyList<PlayerPaymentTransactionResource> Resources { get; init; } = ArraySegment<PlayerPaymentTransactionResource>.Empty;
}