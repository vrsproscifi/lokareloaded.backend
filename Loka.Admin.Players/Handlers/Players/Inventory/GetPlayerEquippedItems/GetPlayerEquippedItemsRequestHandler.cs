﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Loka.Common.MediatR.Handlers;
using Loka.Common.Operations.Results;

namespace Loka.Admin.Players.Handlers.Players.Inventory.GetPlayerEquippedItems;

public class GetPlayerEquippedItemsRequestHandler : IOperationHandler<GetPlayerEquippedItemsRequest, GetPlayerEquippedItemsResponse>
{
    public Task<OperationResult<GetPlayerEquippedItemsResponse>> Handle(GetPlayerEquippedItemsRequest request, CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }
}