﻿using Loka.Inventory.Interfaces.Inventory;

namespace Loka.Inventory.Interfaces.Presets.Models;

public static class PlayerInventoryPresetModel
{
    [GenerateSerializer]
    public sealed class Repository : IEntry
    {
        [Id(0)]
        public Guid Id { get; init; }

        [Id(1)]
        public Guid ModelId { get; init; }

        [Id(2)]
        public bool IsActive { get; init; }

        [Id(3)]
        public IReadOnlyList<ItemEntry> Items { get; set; } = ArraySegment<ItemEntry>.Empty;

        [GenerateSerializer]
        public sealed class ItemEntry : IEntry
        {
            [Id(0)]
            public Guid Id { get; init; }

            [Id(1)]
            public PlayerInventoryItemModel.Repository Item { get; init; } = null!;
        }
    }

    [GenerateSerializer]
    public sealed class RPC : IEntry
    {
        [Id(0)]
        public Guid Id { get; init; }

        [Id(1)]
        public Guid ModelId { get; init; }

        [Id(2)]
        public bool IsActive { get; init; }

        [Id(3)]
        public IReadOnlyList<ItemEntry> Items { get; set; } = ArraySegment<ItemEntry>.Empty;

        [GenerateSerializer]
        public sealed class ItemEntry : IEntry
        {
            [Id(0)]
            public Guid Id { get; init; }

            [Id(1)]
            public Guid ItemId { get; init; }
        }
    }
}