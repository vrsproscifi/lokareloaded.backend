using System;

namespace Loka.MatchMaking.History.Entities.Team;

public class MatchHistoryTeamInformation
{
    public Guid TeamId { get; set; }
    public int Score { get; set; }

    public MatchHistoryTeamMember Members { get; set; }
}