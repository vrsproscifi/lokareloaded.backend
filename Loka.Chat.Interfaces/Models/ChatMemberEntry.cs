﻿namespace Loka.Chat.Interfaces.Models;

[GenerateSerializer]
public sealed class ChatMemberEntry
{
    [Id(0)]
    public Guid PlayerId { get; set; }

    [Id(1)]
    public string PlayerName { get; set; } = null!;
}