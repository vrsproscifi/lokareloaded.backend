﻿using Loka.Store.DataBase.Entities.Cases;

namespace Loka.Store.DataBase.Seeders;

internal static class CaseEntitySeed
{
    public static Guid Grenades { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe510");
    public static Guid GrenadesAndAids { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe520");
    public static Guid GrenadesAndAK47 { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe530");

    public static Guid IronAndCooper { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe540");
    public static Guid GrenadesAndIron { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe550");
    public static Guid AK47AndIron { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe560");
    public static Guid Mixed { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe570");
    public static Guid Iron { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe580");


    public static CaseEntity[] Entities { get; } = new[]
    {
        new CaseEntity(Grenades, nameof(Grenades), GameResourceEntitySeed.Money, 500),
        new CaseEntity(GrenadesAndAids, nameof(GrenadesAndAids), GameResourceEntitySeed.Money, 600),
        new CaseEntity(Iron, nameof(Iron), GameResourceEntitySeed.Money, 700),
        new CaseEntity(IronAndCooper, nameof(IronAndCooper), GameResourceEntitySeed.Money, 800),
        new CaseEntity(GrenadesAndIron, nameof(GrenadesAndIron), GameResourceEntitySeed.Money, 900),
        new CaseEntity(GrenadesAndAK47, nameof(GrenadesAndAK47), GameResourceEntitySeed.Money, 1500),
        new CaseEntity(AK47AndIron, nameof(AK47AndIron), GameResourceEntitySeed.Money, 1700),
        new CaseEntity(Mixed, nameof(Mixed), GameResourceEntitySeed.Money, 1800),
    };

    public static CaseItemEntity[] CaseItemEntities { get; } = new[]
    {
        new CaseItemEntity(Grenades, Grenades, 1, 10, 0.5, GameItemEntitySeed.Grenade1),
        new CaseItemEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe521"), GrenadesAndAids, 1, 10, 0.5, GameItemEntitySeed.Grenade1),
        new CaseItemEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe522"), GrenadesAndAids, 1, 10, 0.5, GameItemEntitySeed.Aid1),

        new CaseItemEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe531"), GrenadesAndAK47, 1, 10, 0.5, GameItemEntitySeed.Grenade1),
        new CaseItemEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe532"), GrenadesAndAK47, 1, 10, 0.5, GameItemEntitySeed.AK47),

        new CaseItemEntity(GrenadesAndIron, GrenadesAndIron, 1, 10, 0.5, GameItemEntitySeed.Grenade1),
        new CaseItemEntity(AK47AndIron, AK47AndIron, 1, 10, 0.5, GameItemEntitySeed.AK47),

        new CaseItemEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe571"), Mixed, 1, 10, 0.5, GameItemEntitySeed.Grenade1),
        new CaseItemEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe572"), Mixed, 1, 10, 0.5, GameItemEntitySeed.Grenade2),
        new CaseItemEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe573"), Mixed, 1, 10, 0.5, GameItemEntitySeed.Aid1),
        new CaseItemEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe574"), Mixed, 1, 10, 1.0, GameItemEntitySeed.Aid2),
        new CaseItemEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe575"), Mixed, 1, 10, 1.0, GameItemEntitySeed.AK47),
        new CaseItemEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe576"), Mixed, 1, 10, 0.5, GameItemEntitySeed.M16),
    };

    public static CaseResourceEntity[] CaseResourceEntities { get; } = new[]
    {
        new CaseResourceEntity(Iron, Iron, 1, 10, 0.5, GameResourceEntitySeed.Iron),
        new CaseResourceEntity(GrenadesAndIron, GrenadesAndIron, 1, 10, 0.5, GameResourceEntitySeed.Iron),
        new CaseResourceEntity(AK47AndIron, AK47AndIron, 1, 10, 0.5, GameResourceEntitySeed.Iron),

        new CaseResourceEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe561"), IronAndCooper, 1, 10, 0.5, GameResourceEntitySeed.Iron),
        new CaseResourceEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe562"), IronAndCooper, 1, 10, 0.5, GameResourceEntitySeed.Copper),

        new CaseResourceEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe571"), Mixed, 1, 10, 1.0, GameResourceEntitySeed.Steel),
        new CaseResourceEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe572"), Mixed, 1, 10, 0.5, GameResourceEntitySeed.Copper),
        new CaseResourceEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe573"), Mixed, 1, 10, 0.5, GameResourceEntitySeed.Iron),
        new CaseResourceEntity(Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe574"), Mixed, 1, 10, 1.0, GameResourceEntitySeed.Rags),
    };
}