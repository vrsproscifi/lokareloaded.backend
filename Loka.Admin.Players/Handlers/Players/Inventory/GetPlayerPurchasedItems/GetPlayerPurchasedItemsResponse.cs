﻿using System;
using Loka.Admin.Common.Models.Store;
using Loka.Common.Entry;

namespace Loka.Admin.Players.Handlers.Players.Inventory.GetPlayerPurchasedItems;

public class GetPlayerPurchasedItemsResponse : IEntry
{
    public Guid Id { get; set; }
    public GameItemInformationModel Model { get; set; }

    public int Level { get; set; }
    public long Amount { get; set; }

    public DateTimeOffset PurchasedAt { get; set; }
    public DateTimeOffset? LastUseDate { get; set; }
}