﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Loka.MatchMaking.DataBase.Migrations
{
    public partial class AddedNewTypes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "IsAllowJoinNewPlayers",
                schema: "MatchMaking",
                table: "MatchEntity",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<Guid>(
                name: "WinnerTeamId",
                schema: "MatchMaking",
                table: "MatchEntity",
                type: "uuid",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "GamePrimeTimeEntity",
                schema: "MatchMaking",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    GameModeId = table.Column<Guid>(type: "uuid", nullable: true),
                    From = table.Column<TimeSpan>(type: "interval", nullable: false),
                    End = table.Column<TimeSpan>(type: "interval", nullable: false),
                    Level = table.Column<int>(type: "integer", nullable: true),
                    BonusExperience = table.Column<int>(type: "integer", nullable: false),
                    BonusMoney = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GamePrimeTimeEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_GamePrimeTimeEntity_GameModeEntity_GameModeId",
                        column: x => x.GameModeId,
                        principalSchema: "MatchMaking",
                        principalTable: "GameModeEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_GamePrimeTimeEntity_GameModeId",
                schema: "MatchMaking",
                table: "GamePrimeTimeEntity",
                column: "GameModeId");

            migrationBuilder.CreateIndex(
                name: "IX_GamePrimeTimeEntity_Name",
                schema: "MatchMaking",
                table: "GamePrimeTimeEntity",
                column: "Name");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GamePrimeTimeEntity",
                schema: "MatchMaking");

            migrationBuilder.DropColumn(
                name: "IsAllowJoinNewPlayers",
                schema: "MatchMaking",
                table: "MatchEntity");

            migrationBuilder.DropColumn(
                name: "WinnerTeamId",
                schema: "MatchMaking",
                table: "MatchEntity");
        }
    }
}
