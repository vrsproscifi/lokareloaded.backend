﻿using AutoMapper;
using Loka.Admin.Api.Features.Players;
using Loka.Admin.Players.Handlers.Players.Listing.GetPlayersList;
using Loka.Common.Pagination;

namespace Loka.Admin.Api.Mapping.Players;

internal sealed class GetPlayersListResultMapping : Profile
{
    private static GetPlayersListStatistics Empty { get; } = new GetPlayersListStatistics()
    {
        Deaths = 0,
        Elo = 0,
        Kills = 0,
        Level = 0,
        Loses = 0,
        Wins = 0
    };
        
    public GetPlayersListResultMapping()
    {
        CreateMap<PagingResult<GetPlayersListResponse>, GetPlayersListResult>()
            .ForMember(q => q.Paging, q => q.MapFrom(p => p))
            .ForMember(q => q.Players, q => q.MapFrom(p => p.Items));

        CreateMap<GetPlayersListResponse, GetPlayersListEntry>()
            .ForMember(q => q.Id, q => q.MapFrom(p => p.Id))
            .ForMember(q => q.Name, q => q.MapFrom(p => p.Name))
            .ForMember(q => q.Email, q => q.MapFrom(p => p.Email))
            .ForMember(q => q.Country, q => q.MapFrom(p => p.Country))
            .ForMember(q => q.Language, q => q.MapFrom(p => p.Language))
            .ForMember(q => q.Role, q => q.MapFrom(p => p.Role))
            .ForMember(q => q.RegisteredAt, q => q.MapFrom(p => p.RegisteredAt))
            .ForMember(q => q.PremiumEndDate, q => q.MapFrom(p => p.PremiumEndDate))
            .ForMember(q => q.LastActivityAt, q => q.MapFrom(p => p.LastActivityAt))
            .ForMember(q => q.Statistics, q => q.MapFrom(p => Empty));
            

    }
}