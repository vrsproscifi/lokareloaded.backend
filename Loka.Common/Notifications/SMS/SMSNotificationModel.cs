﻿namespace Loka.Common.Notifications.SMS;

public sealed class SMSNotificationModel
{
    public string PhoneNumber { get; init; } = null!;

    public string Message { get; init; } = null!;
}