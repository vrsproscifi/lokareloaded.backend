﻿namespace Loka.Api.Features.Inventory.Templates;

internal sealed class PlayerInventoryTemplatesMappingProfiles : Profile
{
    public PlayerInventoryTemplatesMappingProfiles()
    {
        //==========================================================
        CreateMap<Loka.Inventory.Interfaces.Templates.GetPlayerInventoryTemplates.Result, GetPlayerInventoryTemplates.Types.Response>()
            .ForMember(q => q.Entities, q => q.MapFrom(w => w.Presets));

        CreateMap<Loka.Inventory.Interfaces.Templates.PlayerInventoryTemplateModel, PlayerInventoryTemplate>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.Name, q => q.MapFrom(w => w.Name))
            .ForMember(q => q.Items, q => q.MapFrom(w => w.Entries));

        CreateMap<Loka.Inventory.Interfaces.Templates.PlayerInventoryTemplateModel.ItemEntry, PlayerInventoryTemplate.Types.ItemEntry>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.ItemId, q => q.MapFrom(w => w.ItemId));
    }
}