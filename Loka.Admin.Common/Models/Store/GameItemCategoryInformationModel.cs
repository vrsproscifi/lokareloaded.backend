﻿using System;
using AutoMapper;
using Loka.Common.Entry;
using Loka.Game.Database.Entities.Store.Items;
using Loka.Game.Database.Enums.Store;
using Loka.Store.Interfaces.Enums;

namespace Loka.Admin.Common.Models.Store;

public sealed class GameItemCategoryInformationModel : IEntry
{
    public Guid Id { get; set; }
    public string Name { get; set; }= null!;
    public ItemCategoryTypeId TypeId { get; set; }

    public override string ToString()
    {
        return $"Id: {Id} | Name: {Name} | TypeId: {TypeId}";
    }

    internal sealed class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<GameItemCategoryEntity, GameItemCategoryInformationModel>()
                .ForMember(q => q.Id, q => q.MapFrom(p => p.EntityId))
                .ForMember(q => q.Name, q => q.MapFrom(p => p.Name))
                .ForMember(q => q.TypeId, q => q.MapFrom(p => p.TypeId));
        }
    }
}