﻿namespace Loka.Store.DataBase.Entities.Items;

public sealed class GameItemBlueprintDependEntity : BaseEntity
{
    #region Blueprient

    [ForeignKey(nameof(BlueprintEntity))]
    public Guid BlueprintId { get; set; }

    public GameItemBlueprintEntity? BlueprintEntity { get; set; }

    #endregion


    #region Resource

    [ForeignKey(nameof(ResourceEntity))]
    public Guid ResourceId { get; set; }

    public GameResourceEntity? ResourceEntity { get; set; }

    public int Amount { get; set; }

    #endregion
}