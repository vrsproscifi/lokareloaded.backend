﻿using Loka.Common.Extensions;
using Npgsql;

namespace Loka.Api.Orleans.Services;

public class MatchMakingService : BackgroundService
{
    private IClusterClient GrainFactory;

    public MatchMakingService(IClusterClient grainFactory)
    {
        GrainFactory = grainFactory;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        //var reminder = GrainFactory.GetGrain<IMatchMakingProcessorReminder>(Guid.Empty);
        //await reminder.Start();
    }
}

public class SeedOrleansService : BackgroundService
{
    private static string[] FileNames = new[]
    {
        "Main.sql", "Clustering.sql", "Persistence.sql", "Reminders.sql",
    };

    private IConfiguration Configuration { get; }
    private ILogger<SeedOrleansService> Logger { get; }

    public SeedOrleansService(IConfiguration configuration, ILogger<SeedOrleansService> logger)
    {
        Configuration = configuration;
        Logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        Logger.LogInformation($"Begin apply Orleans migrations | Begin get connection string");
        return;

        var connString = Configuration.GetConnectionString(nameof(Orleans));

        Logger.LogInformation($"Begin apply Orleans migrations | Begin open connect to {connString}");
        await using var conn = new NpgsqlConnection(connString);
        await conn.OpenAsync(stoppingToken);

        foreach (var fileName in FileNames)
        {
            Logger.LogInformation($"Apply apply Orleans migrations | begin search migrations for {fileName}");
            var query = await GetType().Assembly.ReadResourceAsync(fileName);

            await using var cmd = new NpgsqlCommand(query, conn);
            var result = await cmd.ExecuteNonQueryAsync(stoppingToken);

            Logger.LogInformation($"Apply Orleans migrations | Migration applied for {fileName} | result: {result}");
        }

        Logger.LogInformation($"Complete apply Orleans migrations");
    }
}