﻿using System;
using Loka.Admin.Common.Models.Accounts;
using Loka.Admin.Common.Models.Countries;
using Loka.Admin.Players.Models;

namespace Loka.Admin.Players.Handlers.Players.Listing.GetPlayerInformation;

public sealed class GetPlayerInformationResponse
{
    public Guid EntityId { get; set; }
        
    public PlayerNameObject Login { get; set; }
    public PlayerEmailObject Email { get; set; }

    public DetailedPlayerStatisticsModel Statistics { get; set; }
        
    public DateTimeOffset RegisteredAt { get; set; }
    public DateTimeOffset LastActivityAt { get; set; }
    public DateTimeOffset? PremiumEndDate { get; set; }

    public AccountRoleInformationModel Role { get; set; }
    public CountryInformationModel Country { get; set; }
    public LanguageInformationModel Language { get; set; }
}