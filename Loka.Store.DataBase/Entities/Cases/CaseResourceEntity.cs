﻿using Loka.Database.Common.Entities.Store;

namespace Loka.Store.DataBase.Entities.Cases;

public sealed class CaseResourceEntity : BaseEntity, IStoreCaseChildEntity //, IStoreItemChildEntity
{
    #region Case

    [ForeignKey(nameof(CaseEntity))]
    public Guid CaseId { get; set; }

    public CaseEntity? CaseEntity { get; set; }

    #endregion


    #region Resource

    [ForeignKey(nameof(ModelEntity))]
    public Guid ModelId { get; set; }

    public GameResourceEntity? ModelEntity { get; set; }

    #endregion


    public int MinimumCount { get; set; }
    public int MaximumCount { get; set; }
    public double ChanceOfDrop { get; set; }
    public int OrderBy { get; set; }
    
    
    public CaseResourceEntity()
    {
    }

    public CaseResourceEntity(Guid id, Guid caseId, int minimumCount, int maximumCount, double chanceOfDrop, Guid modelId) : base(id)
    {
        CaseId = caseId;
        MinimumCount = minimumCount;
        MaximumCount = maximumCount;
        ChanceOfDrop = chanceOfDrop;
        ModelId = modelId;
    }
}