﻿using Loka.Store.DataBase.Entities.Booster;

namespace Loka.Store.DataBase.Seeders;

internal static class BoosterEntitySeed
{
    public static Guid PremiumAccount { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe501");
    public static Guid Experience { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe502");
    public static Guid Reputation { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe503");
    public static Guid Money { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe504");
    public static Guid Resources { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe505");

    public static BoosterEntity[] Entities { get; } = new[]
    {
        new BoosterEntity(PremiumAccount, BoosterTypeId.PremiumAccount, GameResourceEntitySeed.Money, 500),
        new BoosterEntity(Experience, BoosterTypeId.Experience, GameResourceEntitySeed.Money, 500),
        new BoosterEntity(Reputation, BoosterTypeId.Reputation, GameResourceEntitySeed.Money, 500),
        new BoosterEntity(Money, BoosterTypeId.Money, GameResourceEntitySeed.Money, 500),
        new BoosterEntity(Resources, BoosterTypeId.Resources, GameResourceEntitySeed.Money, 500),
    };
}