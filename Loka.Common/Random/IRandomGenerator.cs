﻿namespace Loka.Common.Random;

public interface IRandomGenerator
{
    bool CheckChance(double chance);
    int InRange(int min, int max);
}