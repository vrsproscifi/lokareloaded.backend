﻿using System;
using AutoMapper;
using Loka.Admin.Common.Models.GamePlay;
using Loka.Common.Entry;
using Loka.Game.Database.Entities.Store.Items;
using Loka.Game.Database.Enums.Store;
using Loka.Store.Interfaces.Enums;

namespace Loka.Admin.Common.Models.Store;

public class GameItemInformationModel : IEntry
{
    public Guid Id { get; set; }
    public string Name { get; set; }= null!;

    public int Level { get; set; }

    public CurrencyInformationModel Currency { get; set; }= null!;
    public FractionInformationModel Fraction { get; set; }= null!;
    public GameItemCategoryInformationModel Category { get; set; }= null!;

    public ItemEntityFlags Flags { get; set; }
    public TimeSpan? Duration { get; set; }
    public int DisplayPosition { get; set; }

    public override string ToString()
    {
        return $"Id: {Id} | Name: {Name}";
    }
        
    internal sealed class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<GameItemEntity, GameItemInformationModel>()
                .ForMember(q => q.Id, q => q.MapFrom(p => p.EntityId))
                .ForMember(q => q.Name, q => q.MapFrom(p => p.Name))
                .ForMember(q => q.Level, q => q.MapFrom(p => p.Level))
                .ForMember(q => q.Flags, q => q.MapFrom(p => p.Flags))
                .ForMember(q => q.Duration, q => q.MapFrom(p => p.Duration))
                .ForMember(q => q.DisplayPosition, q => q.MapFrom(p => p.DisplayPosition));
        }
    }
}