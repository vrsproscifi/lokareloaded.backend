﻿namespace Loka.MatchMaking.History.Entities.Team;

public enum MatchHistoryTeamStatus : byte
{
    NotJoined = 0,
    Joined = 1,
    Appended = 2,
    Finished = 3,
    Deserted = 4,
}