﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Loka.Store.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class SomeChanges : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SellCurrencyId",
                schema: "Store",
                table: "GameItemEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SubscribeCurrencyId",
                schema: "Store",
                table: "GameItemEntity");

            migrationBuilder.DropTable(
                name: "GameCurrencyEntity",
                schema: "Store");

            migrationBuilder.CreateTable(
                name: "GameItemBlueprintEntity",
                schema: "Store",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    ItemModelId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameItemBlueprintEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_GameItemBlueprintEntity_GameItemEntity_ItemModelId",
                        column: x => x.ItemModelId,
                        principalSchema: "Store",
                        principalTable: "GameItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GameResourceEntity",
                schema: "Store",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "text", nullable: false),
                    TypeId = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameResourceEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "GameItemBlueprintDependEntity",
                schema: "Store",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    BlueprintId = table.Column<Guid>(type: "uuid", nullable: false),
                    ResourceId = table.Column<Guid>(type: "uuid", nullable: false),
                    Amount = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameItemBlueprintDependEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_GameItemBlueprintDependEntity_GameItemBlueprintEntity_Bluep~",
                        column: x => x.BlueprintId,
                        principalSchema: "Store",
                        principalTable: "GameItemBlueprintEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameItemBlueprintDependEntity_GameResourceEntity_ResourceId",
                        column: x => x.ResourceId,
                        principalSchema: "Store",
                        principalTable: "GameResourceEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(1402), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(2624), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(3091), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(3563), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(2225), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(4122), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(4629), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(5192), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(5786), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(6350), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(6727), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(7244), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(7645), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(8149), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameResourceEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"), new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(7637), new TimeSpan(0, 0, 0, 0, 0)), null, "Coin", 3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"), new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(9120), new TimeSpan(0, 0, 0, 0, 0)), null, "Donate", 2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(9128), new TimeSpan(0, 0, 0, 0, 0)), null, "Money", 1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"), new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(9447), new TimeSpan(0, 0, 0, 0, 0)), null, "Iron", 4 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe202"), new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(9747), new TimeSpan(0, 0, 0, 0, 0)), null, "Steel", 5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe203"), new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 398, DateTimeKind.Unspecified).AddTicks(41), new TimeSpan(0, 0, 0, 0, 0)), null, "Copper", 6 }
                });

            migrationBuilder.CreateIndex(
                name: "IX_GameItemBlueprintDependEntity_BlueprintId",
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                column: "BlueprintId");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemBlueprintDependEntity_ResourceId",
                schema: "Store",
                table: "GameItemBlueprintDependEntity",
                column: "ResourceId");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemBlueprintEntity_ItemModelId",
                schema: "Store",
                table: "GameItemBlueprintEntity",
                column: "ItemModelId");

            migrationBuilder.CreateIndex(
                name: "IX_GameResourceEntity_Name",
                schema: "Store",
                table: "GameResourceEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_GameResourceEntity_TypeId",
                schema: "Store",
                table: "GameResourceEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_GameItemEntity_GameResourceEntity_SellCurrencyId",
                schema: "Store",
                table: "GameItemEntity",
                column: "SellCurrencyId",
                principalSchema: "Store",
                principalTable: "GameResourceEntity",
                principalColumn: "EntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_GameItemEntity_GameResourceEntity_SubscribeCurrencyId",
                schema: "Store",
                table: "GameItemEntity",
                column: "SubscribeCurrencyId",
                principalSchema: "Store",
                principalTable: "GameResourceEntity",
                principalColumn: "EntityId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_GameItemEntity_GameResourceEntity_SellCurrencyId",
                schema: "Store",
                table: "GameItemEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_GameItemEntity_GameResourceEntity_SubscribeCurrencyId",
                schema: "Store",
                table: "GameItemEntity");

            migrationBuilder.DropTable(
                name: "GameItemBlueprintDependEntity",
                schema: "Store");

            migrationBuilder.DropTable(
                name: "GameItemBlueprintEntity",
                schema: "Store");

            migrationBuilder.DropTable(
                name: "GameResourceEntity",
                schema: "Store");

            migrationBuilder.CreateTable(
                name: "GameCurrencyEntity",
                schema: "Store",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: false),
                    TypeId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameCurrencyEntity", x => x.EntityId);
                });

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(5316), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(6051), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(6257), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(6445), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.InsertData(
                schema: "Store",
                table: "GameCurrencyEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"), new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(8528), new TimeSpan(0, 0, 0, 0, 0)), null, "Coin", 3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"), new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(9289), new TimeSpan(0, 0, 0, 0, 0)), null, "Donate", 2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(9294), new TimeSpan(0, 0, 0, 0, 0)), null, "Money", 1 }
                });

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 447, DateTimeKind.Unspecified).AddTicks(9872), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(788), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(1020), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(1211), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(1398), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(1578), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(1909), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(2268), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(2576), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 18, 23, 28, 49, 448, DateTimeKind.Unspecified).AddTicks(2835), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.CreateIndex(
                name: "IX_GameCurrencyEntity_Name",
                schema: "Store",
                table: "GameCurrencyEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_GameCurrencyEntity_TypeId",
                schema: "Store",
                table: "GameCurrencyEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SellCurrencyId",
                schema: "Store",
                table: "GameItemEntity",
                column: "SellCurrencyId",
                principalSchema: "Store",
                principalTable: "GameCurrencyEntity",
                principalColumn: "EntityId");

            migrationBuilder.AddForeignKey(
                name: "FK_GameItemEntity_GameCurrencyEntity_SubscribeCurrencyId",
                schema: "Store",
                table: "GameItemEntity",
                column: "SubscribeCurrencyId",
                principalSchema: "Store",
                principalTable: "GameCurrencyEntity",
                principalColumn: "EntityId");
        }
    }
}
