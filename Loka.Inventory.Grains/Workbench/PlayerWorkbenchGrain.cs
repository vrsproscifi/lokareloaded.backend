﻿using Loka.Common.Authentication;
using Loka.Common.MediatR.Executors;
using Loka.Common.Orleans.Grains;
using Loka.Inventory.Interfaces.Workbench;

namespace Loka.Inventory.Grains.Workbench;

public class PlayerWorkbenchGrain : Grain, IPlayerWorkbenchGrain, IWithPlayerActor
{
    private IOperationExecutor OperationExecutor { get; }
    public PlayerActor Actor { get; set; } = null!;

    public PlayerWorkbenchGrain(IOperationExecutor operationExecutor)
    {
        OperationExecutor = operationExecutor;
    }

    public Task<OperationResult<CraftWorkbenchItem.Result>> CraftWorkbenchItem(CraftWorkbenchItem.Request request)
    {
        return OperationExecutor.Execute(request);
    }
}