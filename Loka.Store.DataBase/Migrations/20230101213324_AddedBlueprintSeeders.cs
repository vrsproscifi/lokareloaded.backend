﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Loka.Store.DataBase.Migrations
{
    /// <inheritdoc />
    public partial class AddedBlueprintSeeders : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(1524), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(2408), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(2683), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(2936), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(5748), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(6948), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(7231), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(7492), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(7739), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(7979), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(8219), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(8451), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(8688), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 647, DateTimeKind.Unspecified).AddTicks(8923), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe001"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe002"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe003"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(5967), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(7114), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(7122), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(7507), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe202"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(7824), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe203"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 21, 33, 23, 648, DateTimeKind.Unspecified).AddTicks(8085), new TimeSpan(0, 0, 0, 0, 0)));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(1402), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(2624), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(3091), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "FractionEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(3563), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(2225), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(4122), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(4629), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(5192), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(5786), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(6350), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(6727), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(7244), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(7645), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemCategoryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 396, DateTimeKind.Unspecified).AddTicks(8149), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe001"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe002"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe003"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameItemEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(7637), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(9120), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(9128), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(9447), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe202"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 397, DateTimeKind.Unspecified).AddTicks(9747), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "Store",
                table: "GameResourceEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe203"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2023, 1, 1, 18, 39, 9, 398, DateTimeKind.Unspecified).AddTicks(41), new TimeSpan(0, 0, 0, 0, 0)));
        }
    }
}
