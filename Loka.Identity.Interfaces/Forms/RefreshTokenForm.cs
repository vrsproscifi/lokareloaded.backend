﻿namespace Loka.Identity.Interfaces.Forms;

[GenerateSerializer]
public sealed class RefreshTokenForm
{
    [Id(0)]
    public string DeviceId { get; set; }

    [Id(1)]
    public string RefreshToken { get; set; }
}