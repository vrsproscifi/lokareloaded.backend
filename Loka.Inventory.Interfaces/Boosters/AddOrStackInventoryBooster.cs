﻿namespace Loka.Inventory.Interfaces.Boosters;

public static class AddOrStackInventoryBooster
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<PlayerInventoryBoosterModel>
    {
        [Id(0)]
        public Guid ModelId { get; init; }

        [Id(1)]
        public long Amount { get; init; }
    }
}