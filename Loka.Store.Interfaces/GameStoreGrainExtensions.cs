﻿namespace Loka.Store.Interfaces;

public static class GameStoreGrainExtensions
{
    public static IGameStoreGrain GetGameStoreGrain(this IGrainFactory factory)
    {
        return factory.GetGrain<IGameStoreGrain>(Guid.Empty);
    }
}