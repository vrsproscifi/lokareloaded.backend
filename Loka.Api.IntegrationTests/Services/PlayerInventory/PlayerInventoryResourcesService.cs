﻿using Loka.Api.Features.Inventory.Resources;
using Loka.Api.IntegrationTests.Clients;
using Loka.Api.IntegrationTests.Services.PlayerIdentity;

namespace Loka.Api.IntegrationTests.Services.PlayerInventory;

public sealed class PlayerInventoryResourcesService
{
    private PlayerInventoryResources.PlayerInventoryResourcesClient PlayerInventoryResourcesClient { get; }

    public PlayerInventoryResourcesService(GameAPIClients client)
    {
        PlayerInventoryResourcesClient = client.PlayerInventoryResourcesClient;
    }

    public async Task<IReadOnlyList<PlayerInventoryResource>> GetPlayerResources(PlayerSession playerSession)
    {
        var resources = await PlayerInventoryResourcesClient.GetPlayerInventoryResourcesAsync(new GetPlayerInventoryResources.Types.Request(), playerSession.UseJWT());
        Assert.That(resources, Is.Not.Null, $"Invalid {resources}");
        Assert.That(resources.Entries, Is.Not.Null, $"Invalid {resources}");
        Assert.That(resources.Entries, Is.Not.Empty, $"Invalid {nameof(resources)}");

        foreach (var resource in resources.Entries)
        {
            Assert.That(resource, Is.Not.Null, $"Invalid {resource} in {nameof(resources)}");
            Assert.That(resource.Id, Is.Not.Null, $"Invalid {nameof(resource.Id)} in {nameof(resource)}");
            Assert.That(resource.ModelId, Is.Not.Null, $"Invalid {nameof(resource.ModelId)} in {nameof(resource)}");
        }

        return resources.Entries;
    }
}