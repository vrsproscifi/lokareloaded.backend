﻿using Loka.Inventory.Interfaces.Store.Requests;

namespace Loka.Inventory.Interfaces.Store;

public interface IPlayerStoreGrain : IGrainWithGuidKey
{
    Task<OperationResult<BuyPlayerInventoryPresets.Result>> BuyPreset(BuyPlayerInventoryPresets.Request request);
    Task<OperationResult<BuyStoreItem.Result>> BuyStoreItem(BuyStoreItem.Request request);
    Task<OperationResult<BuyStoreBooster.Result>> BuyStoreBooster(BuyStoreBooster.Request request);
    Task<OperationResult<BuyStoreCase.Result>> BuyStoreCase(BuyStoreCase.Request request);
}