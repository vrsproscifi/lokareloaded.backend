﻿namespace Loka.Store.Interfaces.Enums;

public enum ItemCategoryTypeId
{
    None = 0,

    Weapon,
    Armour,
    Character,
    Profile,

    Material,
    Grenade,
    Kits,
    Mines,

    End
}

public static class ItemCategoryTypeIdExtensions
{
    public static bool IsEquippable(this ItemCategoryTypeId type)
    {
        switch (type)
        {
            case ItemCategoryTypeId.Weapon:
                return true;
            case ItemCategoryTypeId.Armour:
                return true;
            case ItemCategoryTypeId.Grenade:
                return true;
            case ItemCategoryTypeId.Kits:
                return true;
            case ItemCategoryTypeId.Mines:
                return true;
            default:
                return false;
        }
    }
}