﻿using System;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ProGaudi.MsgPack.Light;

namespace Loka.Common.Tarantool;

public static class TarantoolClientServiceExtensions
{
    public static IServiceCollection AddTarantoolClientServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddSingleton<TarantoolClient>();
        services.AddHostedService<TarantoolClientService>();

        return services;
    }

    public static int GetMsgPackArrayElement<TType>(this TarantoolClient client, string propertyName)
    {
        var type = typeof(TType);
        var property = type.GetProperty(propertyName);
        if (property == null)
            throw new ArgumentNullException(nameof(propertyName), $"Property {propertyName} not founded in {type.Name}");

        var attribute = property.GetCustomAttribute<MsgPackArrayElementAttribute>();
        if (attribute == null)
            throw new ArgumentNullException(nameof(attribute), $"Attribute for {propertyName} not founded in {type.Name}");

        return attribute.Order;
    }
}