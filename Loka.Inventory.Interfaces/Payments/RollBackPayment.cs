﻿using MongoDB.Bson;

namespace Loka.Inventory.Interfaces.Payments;

public sealed class RollBackPayment
{
    [GenerateSerializer]
    public sealed class Request
    {
        [Id(0)]
        public ObjectId TransactionId { get; init; }
    }  
    
    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public Guid TransactionId { get; init; }
        
        [Id(1)]
        public Guid ModelId { get; init; }
        
        [Id(2)]
        public Guid CurrencyId { get; init; }
        
        [Id(3)]
        public int Price { get; init; }
    }
}