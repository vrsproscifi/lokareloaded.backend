﻿using System.Linq;
using Loka.Common.Ranges.Numerics;

namespace Loka.Database.Common.Specifications;

public abstract class RangeSpecification<TEntity, TRange> : Specification.IQueryableSpecification<TEntity>
    where TRange : struct where TEntity : class
{
    protected NumericRange<TRange?, TRange?> Range { get; } 
    protected NumericRangeOptions Options { get; }

    protected RangeSpecification(NumericRange<TRange?, TRange?> range, NumericRangeBeginOptions beginOptions, NumericRangeEndOptions endOptions)
        : this(range, new NumericRangeOptions(beginOptions, endOptions))
    {
    }

    protected RangeSpecification(NumericRange<TRange?, TRange?> range, NumericRangeOptions options)
    {
        Range = range;
        Options = options;
    }

    public abstract IQueryable<TEntity> Apply(IQueryable<TEntity> query);
}