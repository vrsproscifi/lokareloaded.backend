﻿using Loka.Common.Orleans.Extensions;
using Loka.MatchMaking.Grains.Grains.Queue;
using Loka.MatchMaking.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.MatchMaking.Grains;

public static class ServiceExtensions
{
    public static IServiceCollection AddMatchMakingServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddGrain<IMatchMakingQueueGrain>();
        return services;
    }
}