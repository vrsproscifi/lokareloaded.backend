﻿using System.Threading.Tasks;
using MediatR;
using Loka.Common.MediatR.Requests;
using Loka.Common.Operations.Results;

namespace Loka.Common.MediatR.Executors;

internal class OperationExecutor : IOperationExecutor
{
    private readonly IMediator mediator;

    public OperationExecutor(IMediator mediator)
    {
        this.mediator = mediator;
    }

    public Task<OperationResult> Execute(IOperationRequest request)
    {
        return mediator.Send(request);
    }

    public Task<OperationResult<TResult>> Execute<TResult>(IOperationRequest<TResult> request)
    {
        return mediator.Send(request);
    }
}