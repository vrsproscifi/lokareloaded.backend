using AutoMapper;

namespace Loka.Database.Common.Storage.Constructors;

public class EntryStorageConstructor<TDbContext>
{
    public EntryStorageConstructor(IMapper mapper, TDbContext dbContext)
    {
        Mapper = mapper;
        DbContext = dbContext;
    }

    internal IMapper Mapper { get; }
    internal TDbContext DbContext { get; }
}