﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Entities;

namespace Loka.MatchMaking.DataBase.Entities.Match;

public class MatchProcessEntity : BaseEntity
{
    public MatchEntity? MatchEntity { get; set; }
        
    public int ShutdownCode { get; set; }
    public int ProcessId { get; set; }
    public DateTimeOffset StartedAt { get; set; }
    public DateTimeOffset ShutdownAt { get; set; }
    public DateTimeOffset LastCheckDate { get; set; }
}