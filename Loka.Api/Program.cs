using Microsoft.AspNetCore.Server.Kestrel.Core;
using Orleans.Configuration;

namespace Loka.Api;

public class Program
{
    public static void Main(string[] args)
    {
        AppContext.SetSwitch("System.Net.Http.SocketsHttpHandler.Http2UnencryptedSupport", true);
        
        Thread.Sleep(7000);
        CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(string[] args) =>
        Host.CreateDefaultBuilder(args)
            .UseOrleansClient(q =>
            {
                q.UseLocalhostClustering();
                q.Configure<ClusterOptions>(options =>
                {
                    options.ClusterId = "dev";
                    options.ServiceId = "HelloWorldApp";
                });
                q.Configure<EndpointOptions>(options => options.AdvertisedIPAddress = IPAddress.Loopback);
            })
            .ConfigureWebHostDefaults(webBuilder =>
            {
                
                webBuilder.ConfigureAppConfiguration(builder => { builder.AddEnvironmentVariables(); });
                webBuilder.CaptureStartupErrors(true);
                webBuilder.UseStartup<Startup>();
                webBuilder.UseIISIntegration();
                webBuilder.UseSockets();
                webBuilder.UseKestrel(options =>
                {
                    options.ListenAnyIP(13852, o =>
                    {
                        o.Protocols = HttpProtocols.Http1AndHttp2;
                        o.UseHttps();
                    });
                    
                    options.ListenAnyIP(13853, o =>
                    {
                        o.Protocols = HttpProtocols.Http1AndHttp2;
                        
                    });

                    options.ListenAnyIP(5555, o =>
                    {
                        o.Protocols = HttpProtocols.Http2;
                        o.UseHttps();
                    });
                });
            });
}