﻿using System.Linq;
using AutoMapper;
using AutoMapper.EquivalencyExpression;
using AutoMapper.Extensions.EnumMapping;
using AutoMapper.Extensions.ExpressionMapping;
using Loka.Common.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Common;

public static class ServiceExtensions
{
    public static IServiceCollection AddAutoMapperWithProfiles(this IServiceCollection services)
    {
        var assemblies = GenericReflectionExtensions.GetAssemblies()
            .Where(assembly => assembly.FullName!.StartsWith(nameof(Loka)))
            .ToArray();

        services.AddAutoMapper(expression =>
        {
            expression.EnableEnumMappingValidation();
            expression.AddMaps(assemblies);
            expression.AddExpressionMapping();
            expression.AddCollectionMappers();
        });

       // var mapper = services.BuildServiceProvider().GetService<IMapper>();
       // mapper.ConfigurationProvider.AssertConfigurationIsValid();
        
        return services;
    }
}