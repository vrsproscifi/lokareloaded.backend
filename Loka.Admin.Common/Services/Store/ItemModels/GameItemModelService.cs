﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Loka.Admin.Common.Models.GamePlay;
using Loka.Admin.Common.Models.Store;
using Loka.Admin.Common.Services.Store.Currencies;
using Loka.Admin.Common.Services.Store.ItemCategories;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Mapping;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Extensions;
using Loka.Database.Common.Specifications;
using Loka.Game.Database;
using Loka.Game.Database.Entities.Store.Items;
using Microsoft.EntityFrameworkCore;

namespace Loka.Admin.Common.Services.Store.ItemModels;

internal sealed class GameItemModelService : IGameItemModelService
{
    private IMapper Mapper { get; }
    private SystemDbContext SystemDbContext { get; }
    private IGameItemCategoryService ItemCategoryService { get; }
    private IGameCurrencyService CurrencyService { get; }

    private IOperationMappingProfile<GameItemEntity, GameItemInformationModel>[] MappingProfiles { get; }

    public GameItemModelService
    (
        IMapper mapper,
        SystemDbContext systemDbContext,
        IGameItemCategoryService itemCategoryService,
        IGameCurrencyService currencyService
    )
    {
        SystemDbContext = systemDbContext;
        ItemCategoryService = itemCategoryService;
        Mapper = mapper;
        CurrencyService = currencyService;

        MappingProfiles = new[]
        {
            OperationMappingProfile<GameItemEntity, GameItemInformationModel, GameItemCategoryInformationModel>.Create
            (
                response => response.CategoryId,
                (response, model) => response.Category = model,
                ItemCategoryService.GetByIds
            ),
            OperationMappingProfile<GameItemEntity, GameItemInformationModel, CurrencyInformationModel>.Create
            (
                response => response.CurrencyId,
                (response, model) => response.Currency = model,
                CurrencyService.GetByIds
            ),
            /*OperationMappingProfile<GameItemEntity, GameItemInformationModel, FractionInformationModel>.Create
            (
                response => response.FractionId,
                (response, model) => response.Fraction = model,
                ItemCategoryService.GetByIds
            ),*/
        };
    }

    public async Task<OperationResult<GameItemInformationModel>> GetById(Guid id, CancellationToken cancellationToken)
    {
        var entity = await SystemDbContext.GameItemEntity.GetById(id, cancellationToken);
        if (entity == null)
            return OperationResultCode.PlayerNotFound;

        var model = Mapper.Map<GameItemInformationModel>(entity);
        return model;
    }

    public async Task<OperationResult<GameItemInformationModel[]>> GetByIds(IReadOnlyList<Guid> ids, CancellationToken cancellationToken = default)
    {
        var entities = await SystemDbContext.GameItemEntity
            .Where(new EntityIdsRangeSpecification<GameItemEntity>(ids))
            .ToArrayAsync(cancellationToken);

        var mapModelsResult = await Mapper.OperationMap(entities, MappingProfiles, cancellationToken);

        if (mapModelsResult.IsFailed)
            return mapModelsResult.Error;

        return mapModelsResult.Result.ToArray();
    }
}