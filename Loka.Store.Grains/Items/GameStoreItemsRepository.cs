﻿using AutoMapper;
using Loka.Common.Cache.DistributedCache;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Repositories;
using Loka.Store.DataBase;
using Loka.Store.DataBase.Entities.Items;
using Loka.Store.Interfaces.Enums;
using Loka.Store.Interfaces.Models.Items;
using Loka.Store.Interfaces.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Loka.Store.Grains.Items;

internal sealed class GameStoreItemsRepository : BaseRepository<StoreDbContext, GameItemEntity, StoreItemModel>, IGameStoreItemsRepository
{
    private IGameResourcesRepository ResourcesRepository { get; }
    private IGameStoreFractionsRepository FractionsRepository { get; }
    private IGameStoreItemCategoriesRepository CategoriesRepository { get; }
    private IGameStoreItemBlueprintsRepository ItemBlueprintsRepository { get; }

    public GameStoreItemsRepository
    (
        ICacheProvider cacheProvider,
        StoreDbContext dbContext,
        IMapper mapper,
        IGameResourcesRepository resourcesRepository,
        IGameStoreFractionsRepository fractionsRepository,
        IGameStoreItemCategoriesRepository categoriesRepository,
        IGameStoreItemBlueprintsRepository itemBlueprintsRepository
    ) : base(cacheProvider, dbContext, mapper)
    {
        ResourcesRepository = resourcesRepository;
        FractionsRepository = fractionsRepository;
        CategoriesRepository = categoriesRepository;
        ItemBlueprintsRepository = itemBlueprintsRepository;
    }

    protected override async Task<OperationResult> AfterMap(GameItemEntity entity, StoreItemModel model)
    {
        var getCategoryResult = await CategoriesRepository.GetById(entity.CategoryId);
        if (!getCategoryResult.GetValueIfSucceeded(out var category))
            return getCategoryResult.Error!;

        if (entity.SellCurrencyId.HasValue)
        {
            var getSellCurrencyResult = await ResourcesRepository.GetById(entity.SellCurrencyId.Value);
            if (!getSellCurrencyResult.GetValueIfSucceeded(out var sellCurrency))
                return getSellCurrencyResult.Error!;

            model.SellCurrency = sellCurrency;
        }

        if (entity.SubscribeCurrencyId.HasValue)
        {
            var getSubscribeCurrencyResult = await ResourcesRepository.GetById(entity.SubscribeCurrencyId.Value);
            if (!getSubscribeCurrencyResult.GetValueIfSucceeded(out var subscribeCurrency))
                return getSubscribeCurrencyResult.Error!;

            model.SubscribeCurrency = subscribeCurrency;
        }

        var getFractionResult = await FractionsRepository.GetById(entity.FractionId);
        if (!getFractionResult.GetValueIfSucceeded(out var fraction))
            return getFractionResult.Error!;

        model.Category = category;
        model.Fraction = fraction;

        try
        {
            if (model.Category.TypeId != ItemCategoryTypeId.Profile)
            {
                var blueprintIds = await DbContext.GameItemBlueprintEntity
                    .Where(q => q.ItemModelId == entity.EntityId)
                    .Select(q => q.EntityId)
                    .ToArrayAsync();

                model.Blueprints = await ItemBlueprintsRepository.GetByIds($"{entity.EntityId:N}_child", blueprintIds);
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
        

        return await base.AfterMap(entity, model);
    }
}