﻿namespace Loka.Inventory.Interfaces.Presets.Models;

public static class EquipInventoryItem
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<Result>
    {
        [Id(0)]
        public Guid ItemId { get; init; }

        [Id(1)]
        public Guid PresetId { get; init; }
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public Guid PresetId { get; init; }

        [Id(1)]
        public Guid ItemId { get; init; }
    }
}