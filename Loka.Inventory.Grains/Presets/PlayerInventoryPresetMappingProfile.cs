﻿using Loka.Inventory.DataBase.Entities.Preset;
using Loka.Inventory.Interfaces.Presets.Models;

namespace Loka.Inventory.Grains.Presets;

internal sealed class PlayerInventoryPresetMappingProfile : Profile
{
    public PlayerInventoryPresetMappingProfile()
    {
        CreateMap<PlayerInventoryPresetEntity, PlayerInventoryPresetModel.Repository>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.IsActive, q => q.MapFrom(w => w.IsActive))
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.ModelId))
            .ForMember(q => q.Items, q => q.Ignore());

        CreateMap<PlayerInventoryPresetModel.Repository, PlayerInventoryPresetModel.RPC>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.IsActive, q => q.MapFrom(w => w.IsActive))
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.ModelId))
            .ForMember(q => q.Items, q => q.MapFrom(w => w.Items));
        
        CreateMap<PlayerInventoryPresetModel.Repository.ItemEntry, PlayerInventoryPresetModel.RPC.ItemEntry>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.ItemId, q => q.MapFrom(w => w.Item.Id));
    }
}