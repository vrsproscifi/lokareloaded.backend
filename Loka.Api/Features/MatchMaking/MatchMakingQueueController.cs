﻿using Loka.MatchMaking.Interfaces;
using Loka.MatchMaking.Interfaces.Forms.Queue;


namespace Loka.Api.Features.MatchMaking;

[Authorize(AuthenticationSchemes = "Bearer")]
public class MatchMakingQueueController : MatchMakingQueue.MatchMakingQueueBase
{
    private ControllerServices<MatchMakingQueueController, IMatchMakingQueueGrain> Services { get; }

    public MatchMakingQueueController(ControllerServices<MatchMakingQueueController, IMatchMakingQueueGrain> services)
    {
        Services = services;
    }

    public override Task<GetMatchMakingQueueInformationModel>GetMatchMakingQueueInformation(Empty request, ServerCallContext context)
    {
        return Services.Grain.GetMatchMakingQueueInformation().ReturnResult<GetMatchMakingQueueInformationResponse, GetMatchMakingQueueInformationModel>(Services.Mapper);
    }

    public override Task<JoinToMatchMakingQueueModel>JoinToMatchMakingQueue(JoinToMatchMakingQueueQuery request, ServerCallContext context)
    {
        return Services.Grain.JoinToMatchMakingQueue(new JoinToMatchMakingQueueRequest()
        {
            RegionsIds = request.RegionsIds.Select(Guid.Parse).ToArray(),
            GameMapsIds = request.GameMapsIds.Select(Guid.Parse).ToArray(),
            GameModesIds = request.GameModesIds.Select(Guid.Parse).ToArray(),
            TargetServerVersionId = Guid.Parse(request.TargetServerVersionId),
            GameVersionId = Guid.Parse(request.GameVersionId),
        }).ReturnResult<JoinToMatchMakingQueueResponse, JoinToMatchMakingQueueModel>(Services.Mapper);
    }

    public override Task<Empty>LeaveFromMatchMakingQueue(Empty request, ServerCallContext context)
    {
        return Services.Grain.LeaveFromMatchMakingQueue().ReturnResult();
    }
}