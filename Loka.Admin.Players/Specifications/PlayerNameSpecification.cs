﻿using System.Linq;
using Global.Identity.Database.Entities;
using Loka.Database.Common.Specifications;
using Loka.Identity.Database.Entities.Player;

namespace Loka.Admin.Players.Specifications;

internal sealed class PlayerNameSpecification : Specification.IQueryableSpecification<UserAccountEntity>
{
    private string? PlayerName { get; }
    private bool? PlayerNameConfirmed { get; }

    public PlayerNameSpecification(string playerName, bool? playerNameConfirmed)
    {
        PlayerName = playerName;
        PlayerNameConfirmed = playerNameConfirmed;
    }

    public IQueryable<UserAccountEntity> Apply(IQueryable<UserAccountEntity> query)
    {
        var resultQuery = query;

        if (PlayerNameConfirmed.HasValue)
            resultQuery = query.Where(player => player.IsNameConfirmed == PlayerNameConfirmed);


        if (!string.IsNullOrWhiteSpace(PlayerName))
            resultQuery = query.Where(player => player.Login.Contains(PlayerName));

        return resultQuery;
    }
}