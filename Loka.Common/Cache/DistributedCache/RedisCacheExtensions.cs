﻿using Loka.Common.Extensions;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.DependencyInjection;
using StackExchange.Redis;

namespace Loka.Common.Cache.DistributedCache;

public static class RedisCacheExtensions
{
    public static async Task<OperationResult<T>> GetOrAddExt<T>(this ICacheProvider cacheProvider, string key, Func<string, Task<OperationResult<T>>> fetcher, DistributedCacheEntryOptions? options) where T : class
    {
        var model = await cacheProvider.Get<T>(key);
        if (model == null)
        {
            var result = await fetcher(key);
            if (result.GetValueIfSucceeded(out model))
            {
                await cacheProvider.Set(key, model);
                return model;
            }

            return result.Error!;
        }

        return model;
    }

    public static ServiceCollectionInitializer AddRedisCache(this ServiceCollectionInitializer initializer)
    {
        initializer.Services.AddMemoryCache();

        initializer.Services.AddSingleton<ICacheProvider, CacheProvider>();
        initializer.Services.AddStackExchangeRedisCache(options =>
        {
            var configuration = initializer.Configuration
                .GetApplicationDataBaseSectionValue<RedisConfig>("Redis", isValue: false);

            options.ConfigurationOptions = new ConfigurationOptions();
            options.ConfigurationOptions.EndPoints.Add(configuration.Host, configuration.Port);
            options.ConfigurationOptions.Ssl = configuration.UseSSL ?? false;

            //options.ConfigurationOptions.User = configuration.UserName;
            //options.ConfigurationOptions.Password = configuration.Password;
            options.InstanceName = configuration.InstanceName;
        });
        initializer.Services.Add(ServiceDescriptor.Singleton<IDistributedCache, RedisCache>());

        return initializer;
    }
}