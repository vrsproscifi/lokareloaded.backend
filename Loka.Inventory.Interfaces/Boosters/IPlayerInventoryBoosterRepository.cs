﻿using Loka.Database.Common.Repositories;

namespace Loka.Inventory.Interfaces.Boosters;

public interface IPlayerInventoryBoosterRepository : IBaseChildRepository<PlayerInventoryBoosterModel>
{
    Task<PlayerInventoryBoosterModel> AddOrStack(Guid playerId, AddOrStackInventoryBooster.Request request);
    Task<OperationResult<PlayerInventoryBoosterModel>> Activate(Guid playerId, ActivatePlayerBooster.Request request);
    Task<OperationResult<PlayerInventoryBoosterModel>> Deactivate(Guid playerId, DeactivatePlayerBooster.Request request);
    Task<int> CountActivated(Guid playerId);
}