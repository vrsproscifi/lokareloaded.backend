﻿using AutoMapper;
using Loka.Common.Cache.DistributedCache;
using Loka.Common.Operations.Results;
using Loka.Database.Common.Repositories;
using Loka.Store.DataBase;
using Loka.Store.DataBase.Entities.Cases;
using Loka.Store.Interfaces.Models.Cases;
using Loka.Store.Interfaces.Repositories;

namespace Loka.Store.Grains.Cases;

internal sealed class GameCaseResourcesRepository : BaseRepository<StoreDbContext, CaseResourceEntity, StoreCaseModel.Resource>
{
    private IGameResourcesRepository GameResourcesRepository { get; }

    public GameCaseResourcesRepository(ICacheProvider cacheProvider, StoreDbContext dbContext, IMapper mapper, IGameResourcesRepository gameResourcesRepository) : base(cacheProvider, dbContext, mapper)
    {
        GameResourcesRepository = gameResourcesRepository;
    }

    protected override async Task<OperationResult> AfterMap(CaseResourceEntity entity, StoreCaseModel.Resource model)
    {
        var getResourceResult = await GameResourcesRepository.GetById(entity.ModelId);
        if (!getResourceResult.GetValueIfSucceeded(out var resource))
            return getResourceResult.Error!;

        model.Model = resource;
        return await base.AfterMap(entity, model);
    }
}