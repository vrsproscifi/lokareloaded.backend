﻿using Loka.Game.Database.Entities.Achievements;
using Loka.Game.Database.Entities.Awards;
using Loka.Game.Database.Entities.Roles;
using Loka.Game.Database.Entities.World;
using Loka.Game.Database.Seeders.Achievements;
using Loka.Game.Database.Seeders.Roles;
using Loka.Game.Database.Seeders.World;

namespace Loka.Game.Database;

public sealed class SystemDbContext : BaseGameDbContext<SystemDbContext>
{
        
    public SystemDbContext(DbContextOptions<SystemDbContext> options) : base(options)
    {
            
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        modelBuilder.Entity<AccountRoleEntity>().HasData(AccountRoleEntitySeed.Entities);
        modelBuilder.Entity<WorldLanguageEntity>().HasData(WorldLanguageEntitySeeder.Entities);
        modelBuilder.Entity<WorldCountryEntity>().HasData(WorldCountryEntitySeeder.Entities);
        modelBuilder.Entity<WorldGenderEntity>().HasData(WorldGenderEntitySeeder.Entities);
        modelBuilder.Entity<WorldDayOfWeekEntity>().HasData(WorldDayOfWeekEntitySeeder.Entities);
            
        modelBuilder.Entity<AchievementEntity>().HasData(AchievementEntitySeeder.Entities);
    }

    public DbSet<AchievementEntity> AchievementEntity { get; set; } = null!;
        
    public DbSet<EveryDayAwardEntity> EveryDayAwardEntity { get; set; } = null!;
    public DbSet<NewLevelAwardEntity> NewLevelAwardEntity { get; set; } = null!;
    
    public DbSet<WorldDayOfWeekEntity> WorldDayOfWeekEntity { get; set; } = null!;
    public DbSet<WorldGenderEntity> WorldGenderEntity { get; set; } = null!;
    public DbSet<WorldLanguageEntity> WorldLanguageEntity { get; set; } = null!;
    public DbSet<WorldCountryEntity> WorldCountryEntity { get; set; } = null!;
        
    public DbSet<AccountRoleEntity> AccountRoleEntity { get; set; } = null!;

}