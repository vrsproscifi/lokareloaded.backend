﻿using Loka.Identity.Interfaces.Models;

namespace Loka.Api.Features.Identity;

internal sealed class IdentityMappingProfiles : Profile
{
    public IdentityMappingProfiles()
    {
        CreateMap<PlayerAuthenticationModel, PlayerAuthenticationResult>()
            .ForMember(q => q.PlayerId, q => q.MapFrom(w => w.PlayerId))
            .ForMember(q => q.DisplayName, q => q.MapFrom(w => w.Login))
            .ForMember(q => q.SessionId, q => q.MapFrom(w => w.SessionId))
            .ForMember(q => q.Token, q => q.MapFrom(w => w.Token));

        CreateMap<RefreshTokenModel, SessionRefreshTokenResult>()
            .ForMember(q => q.JwtToken, q => q.MapFrom(w => w.JwtToken))
            .ForMember(q => q.RefreshToken, q => q.MapFrom(w => w.RefreshToken))
            .ForMember(q => q.TimeoutDate, q => q.MapFrom(w => w.TimeoutDate));

        CreateMap<JwtTokenObject, JwtTokenValueObject>()
            .ForMember(q => q.Token, q => q.MapFrom(w => w.Token))
            .ForMember(q => q.NotBefore, q => q.MapFrom(w => w.NotBefore))
            .ForMember(q => q.ExpirationDate, q => q.MapFrom(w => w.ExpirationDate));
    }
}