﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Loka.Admin.Players.Models;
using Loka.Common.Operations.Results;

namespace Loka.Admin.Players.Services;

public interface IPlayerStatisticsService
{
    Task<OperationResult<DetailedPlayerStatisticsModel>> GetDetailedPlayerStatistics(Guid playerId, CancellationToken cancellationToken);
}