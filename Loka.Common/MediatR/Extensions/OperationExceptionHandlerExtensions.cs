using System;
using System.Linq;
using Loka.Common.MediatR.ExceptionHandlers;
using Loka.Common.MediatR.Requests;
using Loka.Common.Operations.Results;
using MediatR.Pipeline;
using Microsoft.Extensions.DependencyInjection;
using Loka.Common.Reflection;

namespace Loka.Common.MediatR.Extensions;

internal static class OperationExceptionHandlerExtensions
{
    public static IServiceCollection RegisterOperationExceptionHandlers(this IServiceCollection services)
    {
        //return services;
            
        //  TODO: Add support custom exception handling, see more in RPX-192
        return services
            .RegisterGenericOperations()
            .RegisterVoidOperations();
    }

    private static IServiceCollection RegisterGenericOperations(this IServiceCollection services)
    {
        var operationRequestTypes = GenericReflectionExtensions
            .GetLoadedTypes()
            .Where(type => type.IsImplementedGenericInterface(typeof(IOperationRequest<>)))
            .ToArray();

        foreach (var requestType in operationRequestTypes)
        {
            var resultType = requestType
                .GetGenericArguments(typeof(IOperationRequest<>))[0];

            var resultTypeInOperationResult =
                typeof(OperationResult<>)
                    .MakeGenericType(resultType);

            var exceptionHandlerBase =
                typeof(IRequestExceptionHandler<,,>)
                    .MakeGenericType(requestType, resultTypeInOperationResult, typeof(Exception));

            var exceptionHandlerImpl =
                typeof(OperationExceptionHandler<,,>)
                    .MakeGenericType(requestType, resultType, typeof(Exception));

            services.AddScoped(exceptionHandlerBase, exceptionHandlerImpl);
        }

        return services;
    }

    private static IServiceCollection RegisterVoidOperations(this IServiceCollection services)
    {
        var operationRequestTypes = GenericReflectionExtensions
            .GetLoadedTypes()
            .Where(type => type.IsImplementedGenericInterface(typeof(IOperationRequest)))
            .ToArray();

        foreach (var requestType in operationRequestTypes)
        {
            var resultType = typeof(OperationResult);

            var exceptionHandlerBase =
                typeof(IRequestExceptionHandler<,,>)
                    .MakeGenericType(requestType, resultType, typeof(Exception));

            var exceptionHandlerImpl =
                typeof(OperationExceptionHandler<,,>)
                    .MakeGenericType(requestType, resultType, typeof(Exception));

            services.AddScoped(exceptionHandlerBase, exceptionHandlerImpl);
        }

        return services;
    }
}