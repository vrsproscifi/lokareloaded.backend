﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Loka.Identity.Interfaces.Models;
using Microsoft.IdentityModel.Tokens;

namespace Loka.Api.Authentication;

public class JwtTokenValidator : ISecurityTokenValidator
{
    private TokenValidationParameters ValidationParameters { get; }
    private JwtBearerAuthenticationSettings SettingsMonitor { get; }

    public JwtTokenValidator(JwtBearerAuthenticationSettings settingsMonitor)
    {
        SettingsMonitor = settingsMonitor;
        ValidationParameters = new TokenValidationParameters()
        {
            ValidateIssuer = true,
            ValidateAudience = true,
            ValidateLifetime = true,
            ValidateIssuerSigningKey = true,
            ValidAudience = SettingsMonitor.Audience,
            ValidIssuer = SettingsMonitor.Issuer,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(SettingsMonitor.SigningKey!))
        };
    }


    public bool CanReadToken(string securityToken)
    {
        return true;
    }

    public ClaimsPrincipal ValidateToken(string securityToken, TokenValidationParameters validationParameters, out SecurityToken validatedToken)
    {
        var handler = new JwtSecurityTokenHandler();
        var claimsPrincipal = handler.ValidateToken(securityToken, ValidationParameters, out validatedToken);
        return claimsPrincipal;
    }

    public bool CanValidateToken { get; } = true;
    public int MaximumTokenSizeInBytes { get; set; } = int.MaxValue;
}