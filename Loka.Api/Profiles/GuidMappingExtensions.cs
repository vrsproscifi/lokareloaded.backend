﻿using Loka.Api.Features.Shared;
using MongoDB.Bson;

namespace Loka.Api.Profiles;

public static class GuidMappingExtensions
{
    public static Guid ToGuid(this UUID? uuid)
    {
        if (uuid == null)
            throw new ArgumentNullException(nameof(uuid));

        if (string.IsNullOrWhiteSpace(uuid.Value))
            throw new ArgumentNullException(nameof(uuid));

        return Guid.Parse(uuid.Value);
    }

    public static ObjectId ToGuid(this ObjID? uuid)
    {
        if (uuid == null)
            throw new ArgumentNullException(nameof(uuid));

        if (string.IsNullOrWhiteSpace(uuid.Value))
            throw new ArgumentNullException(nameof(uuid));

        return ObjectId.Parse(uuid.Value);
    }
}