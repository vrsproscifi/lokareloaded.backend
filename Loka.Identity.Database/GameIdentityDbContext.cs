﻿using Loka.Identity.Database.Entities.Player;

namespace Loka.Identity.Database;

public sealed class GameIdentityDbContext : BaseGameDbContext<GameIdentityDbContext>
{
    public DbSet<PlayerAccountEntity> PlayerAccountEntity { get; set; } = null!;
    public DbSet<PlayerStatisticsEntity> PlayerStatisticsEntity { get; set; } = null!;

    public GameIdentityDbContext(DbContextOptions<GameIdentityDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
            
        modelBuilder.Entity<PlayerAccountEntity>()
            .HasOne(q => q.Statistics!)
            .WithOne(q => q.AccountEntity!)
            .HasForeignKey<PlayerStatisticsEntity>(q => q.EntityId);
    }
}