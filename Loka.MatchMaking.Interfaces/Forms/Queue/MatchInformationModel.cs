﻿namespace Loka.MatchMaking.Interfaces.Forms.Queue;

[GenerateSerializer]
public class MatchInformationResponse
{
    [Id(0)]
    public int Stub { get; set; }
}

[GenerateSerializer]
public class JoinToMatchMakingQueueRequest
{

    [Id(0)]
    public Guid GameVersionId { get; set; }

    [Id(1)]
    public Guid TargetServerVersionId { get; set; }

    [Id(2)]
    public Guid[] GameModesIds { get; set; } = Array.Empty<Guid>();

    [Id(3)]
    public Guid[] GameMapsIds { get; set; } = Array.Empty<Guid>();

    [Id(4)]
    public Guid[] RegionsIds { get; set; } = Array.Empty<Guid>();
}

[GenerateSerializer]
public class JoinToMatchMakingQueueResponse
{
    [Id(0)]
    public int Stub { get; set; }
}

[GenerateSerializer]
public class GetMatchMakingQueueInformationResponse
{
    [Id(0)]
    public int Stub { get; set; }
}