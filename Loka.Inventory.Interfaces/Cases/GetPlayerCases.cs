﻿namespace Loka.Inventory.Interfaces.Cases;

public static class GetPlayerCases
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<Result>
    {
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public IReadOnlyList<PlayerInventoryCaseModel> Entries { get; init; } = null!;
    }

}