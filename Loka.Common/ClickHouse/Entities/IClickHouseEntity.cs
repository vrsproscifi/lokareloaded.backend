﻿using System;

namespace Loka.Common.ClickHouse.Entities;

public interface IClickHouseEntity
{
    public Guid Id { get; }
    public DateTime CreatedAt { get; }
}