﻿using System.Linq;
using Loka.Common.Ranges.Numerics;
using Loka.Inventory.DataBase.Entities.Items;

namespace Loka.Admin.Players.Specifications.Inventory;

internal sealed class PlayerPurchasedItemUpgradeSpecification : PlayerPurchasedItemSpecification<byte>
{
    public PlayerPurchasedItemUpgradeSpecification(NumericRange<byte?, byte?> range, NumericRangeBeginOptions beginOptions, NumericRangeEndOptions endOptions) : base(range, beginOptions, endOptions)
    {
    }

    public PlayerPurchasedItemUpgradeSpecification(NumericRange<byte?, byte?> range, NumericRangeOptions options) : base(range, options)
    {
    }

    public override IQueryable<PlayerItemEntity> Apply(IQueryable<PlayerItemEntity> query)
    {
        var resultQuery = query;

        if (Range.Begin.HasValue)
        {
            if (Options.Begin == NumericRangeBeginOptions.Great)
                resultQuery = query.Where(player => player.Level >= Range.Begin.Value);
            else if (Options.Begin == NumericRangeBeginOptions.GreatOrEqual)
                resultQuery = query.Where(player => player.Level >= Range.Begin.Value);
        }

        if (Range.End.HasValue)
        {
            if (Options.End == NumericRangeEndOptions.Less)
                resultQuery = query.Where(player => player.Level < Range.Begin.Value);
            else if (Options.End == NumericRangeEndOptions.LessOrEqual)
                resultQuery = query.Where(player => player.Level <= Range.Begin.Value);
        }

        return resultQuery;
    }
}