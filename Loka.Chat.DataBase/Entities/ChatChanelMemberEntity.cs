﻿using Loka.Chat.Interfaces.Enums;
using MongoDB.Bson;

namespace Loka.Chat.DataBase.Entities;

public sealed class ChatChanelMemberEntity
{
    public ObjectId Id { get; init; }
    public ObjectId ChannelId { get; init; }

    public Guid PlayerId { get; init; }
    public Guid AssignedBy { get; init; }
    public ChatMemberPermissions Permissions { get; init; }
}