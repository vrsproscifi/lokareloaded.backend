﻿using System;
using Loka.MatchMaking.DataBase.Entities.Game;
using Loka.MatchMaking.DataBase.Enums.Game;

namespace Loka.MatchMaking.DataBase.Seeders.Game;

public static class GameModeEntitySeeder
{
    public static Guid TeamDeadMatch { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe601");
    public static Guid LostDeadMatch { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe602");
    public static Guid ResearchMatch { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe603");
    public static Guid DuelMatch { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe604");
        
    public static GameModeEntity[] Entities { get; } = new[]
    {
        new GameModeEntity(TeamDeadMatch, GameModeTypeId.TeamDeadMatch),
        new GameModeEntity(LostDeadMatch, GameModeTypeId.LostDeadMatch),
        new GameModeEntity(ResearchMatch, GameModeTypeId.ResearchMatch),
        new GameModeEntity(DuelMatch, GameModeTypeId.DuelMatch),
    };
}