﻿using Loka.Common.Entry;
using Microsoft.Extensions.Caching.Memory;

namespace Loka.Common.Cache.Single;

public class EntryDataCache<TKey, TData> : DataCache<TKey, TData>
    where TData : IEntry
{
    protected override string CachePrefix { get; } = typeof(TData).Name;

    public EntryDataCache(IMemoryCache memoryCache) : base(memoryCache)
    {
    }
        
    protected override string GetModelKey(TData model)
    {
        return model.Id.ToString();
    }
}