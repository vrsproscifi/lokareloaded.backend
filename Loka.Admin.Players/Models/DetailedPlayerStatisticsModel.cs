﻿namespace Loka.Admin.Players.Models;

public class DetailedPlayerStatisticsModel
{
    public uint Level { get; set; }
    public ulong Elo { get; set; }

    public ulong Kills { get; set; }
    public ulong Deaths { get; set; }
    public ulong Supports { get; set; }

    public ulong Wins { get; set; }
    public ulong Loses { get; set; }
    public ulong Draws { get; set; }

    public override string ToString()
    {
        return $"Level: {Level}, Elo: {Elo} | Kills: {Kills}, Deaths: {Deaths} | Wins: {Wins}, Loses: {Loses}";
    }
}