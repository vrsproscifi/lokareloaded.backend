﻿using System.Threading.Tasks;
using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using Loka.Admin.Common.Services.Roles;
using Loka.Common.AspNet.Features;

namespace Loka.Admin.Api.Features.Storages;

public sealed class AccountRolesController : AccountRolesStorage.AccountRolesStorageBase
{
    private ControllerServices<AccountRolesController> Services { get; }
    private IAccountRoleService AccountRoleService { get; }

    public AccountRolesController(ControllerServices<AccountRolesController> services, IAccountRoleService accountRoleService)
    {
        Services = services;
        AccountRoleService = accountRoleService;
    }

    public override async Task<GetAccountRolesResult> GetAccountRoles(Empty request, ServerCallContext context)
    {
        var roles = await AccountRoleService.GetList(context.CancellationToken);
        return Services.Mapper.Map<GetAccountRolesResult>(roles);
    }
}