﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace Loka.Game.Database.Migrations
{
    /// <inheritdoc />
    public partial class MovedStoreItems : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_EveryDayAwardEntity_CaseEntity_ModelId",
                schema: "System",
                table: "EveryDayAwardEntity");

            migrationBuilder.DropForeignKey(
                name: "FK_NewLevelAwardEntity_CaseEntity_CaseId",
                schema: "System",
                table: "NewLevelAwardEntity");

            migrationBuilder.DropTable(
                name: "BonusCaseEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "CaseItemEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "CaseEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "GameItemEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "FractionEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "GameCurrencyEntity",
                schema: "System");

            migrationBuilder.DropTable(
                name: "GameItemCategoryEntity",
                schema: "System");

            migrationBuilder.DropIndex(
                name: "IX_NewLevelAwardEntity_CaseId",
                schema: "System",
                table: "NewLevelAwardEntity");

            migrationBuilder.DropIndex(
                name: "IX_EveryDayAwardEntity_ModelId",
                schema: "System",
                table: "EveryDayAwardEntity");

            migrationBuilder.RenameColumn(
                name: "ModelId",
                schema: "System",
                table: "EveryDayAwardEntity",
                newName: "CaseId");

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 672, DateTimeKind.Unspecified).AddTicks(1803), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe602"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 672, DateTimeKind.Unspecified).AddTicks(2660), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe603"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 672, DateTimeKind.Unspecified).AddTicks(2868), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe604"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 672, DateTimeKind.Unspecified).AddTicks(3060), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe605"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 672, DateTimeKind.Unspecified).AddTicks(3246), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe100"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(6469), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(7473), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(7712), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(7966), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(8240), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(8461), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(8655), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(8846), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(9033), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe109"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(9220), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe110"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(9406), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe111"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(9588), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe112"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(9771), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe113"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 684, DateTimeKind.Unspecified).AddTicks(9991), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe114"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(197), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe115"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(384), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe116"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(569), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe117"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(755), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe118"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(938), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe119"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(1122), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe120"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(1306), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe121"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(1492), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe122"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(1677), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe123"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(1859), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe124"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(2041), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe125"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(2227), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe126"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(2411), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe127"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(2597), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe128"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(2785), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe129"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(2970), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe130"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 685, DateTimeKind.Unspecified).AddTicks(3155), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe100"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(5362), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(5661), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(5894), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(6088), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(6274), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(6459), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(6644), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(6867), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(7080), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe109"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(7268), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe110"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(7455), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe111"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(7641), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe112"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(7825), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe113"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(8027), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe114"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(8220), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe115"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(8400), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe116"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(8582), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe117"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(8761), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe118"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(8937), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe119"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(9118), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe120"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(9332), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe121"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(9534), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe122"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(9719), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe123"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(9902), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe124"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(83), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe125"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(265), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe126"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(448), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe127"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(628), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe128"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(808), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe129"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(987), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe130"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(1169), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe131"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(1348), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe132"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(1527), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe133"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(1779), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe134"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(1989), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe135"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(2172), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe136"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(2352), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe137"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(2537), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe138"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(2722), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe139"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(2907), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe140"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(3087), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe141"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(3266), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe142"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(3461), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe143"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(3654), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe144"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(3835), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe145"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(4015), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe147"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(4194), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe148"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(4377), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe149"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(4557), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe151"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(4736), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe152"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(4916), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe154"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(5096), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe155"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(5274), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe156"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(5453), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe157"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(5688), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe158"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(5892), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe159"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(6076), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe160"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(6258), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe161"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(6441), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe162"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(6625), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe163"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(6806), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe164"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(6988), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe165"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(7169), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe166"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(7352), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe167"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(7537), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe168"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(7845), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe170"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(8063), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe171"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(8250), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe172"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(8430), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe173"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(8613), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe174"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(8797), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe175"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(9003), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe176"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(9187), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe177"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(9370), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe178"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(9552), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe179"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(9735), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe180"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 679, DateTimeKind.Unspecified).AddTicks(9919), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe181"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(102), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe182"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(281), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe183"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(464), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe184"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(642), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe185"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(825), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe186"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(1008), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe187"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(1189), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe188"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(1366), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe189"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(1549), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe190"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(1731), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe191"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(1906), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe192"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(2172), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe193"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(2390), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe194"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(2574), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe196"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(2751), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe197"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(2934), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe198"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(3113), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe199"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(3290), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe200"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(3471), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe201"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(3652), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe203"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(3914), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe204"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(4130), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe205"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(4317), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe206"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(4554), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe207"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(4761), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe208"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(4944), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe209"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(5125), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe210"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(5304), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe211"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(5489), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe212"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(5668), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe213"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(5849), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe214"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(6029), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe217"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(6209), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe218"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(6387), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe220"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(6567), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe221"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(6746), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe222"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(6926), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe223"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(7104), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe224"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(7285), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe225"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(7464), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe226"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(7646), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe227"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(7822), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe228"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(7999), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe229"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(8179), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe230"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(8358), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe231"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(8538), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe232"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(8719), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe233"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(8898), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe234"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(9075), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe235"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(9254), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe236"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(9433), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe237"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(9628), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe238"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 680, DateTimeKind.Unspecified).AddTicks(9876), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe239"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(87), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe240"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(275), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe241"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(458), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe242"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(639), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe243"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(821), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe244"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(1003), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe245"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(1181), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe246"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(1361), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe247"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(1539), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe248"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(1716), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe249"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(1895), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe250"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(2076), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe251"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(2251), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe252"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(2429), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe253"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(2605), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe254"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(2783), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe255"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(2961), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe256"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(3188), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe257"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(3392), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe258"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(3575), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe259"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(3756), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe260"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(3934), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe261"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(4110), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe262"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(4289), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe263"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(4467), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe264"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(4644), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe265"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(4867), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe266"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(5082), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe268"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(5274), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe269"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(5457), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe270"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(5636), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe271"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(5881), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe272"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(6091), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe273"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(6275), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe274"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(6457), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe275"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(6636), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe276"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(6816), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe277"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(6996), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe278"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(7178), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe279"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(7360), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe280"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(7537), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe281"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(7715), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe282"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(7895), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe283"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(8075), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe284"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(8251), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe285"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(8430), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe286"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(8612), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe287"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(8793), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe288"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(8972), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe289"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(9152), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe290"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(9331), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe291"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(9509), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe292"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(9685), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe293"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 681, DateTimeKind.Unspecified).AddTicks(9864), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe294"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(42), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe295"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(219), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe296"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(414), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe297"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(606), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe298"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(788), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe299"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(965), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe300"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(1145), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe301"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(1324), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe302"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(1501), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe303"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(1782), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe304"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(1998), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe305"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(2187), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe306"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(2374), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe307"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(2557), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe308"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(2738), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe309"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(2916), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe310"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(3098), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe311"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(3278), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe312"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(3458), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe313"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(3635), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe314"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(3813), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe315"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(3992), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe316"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(4172), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe317"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(4355), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe318"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(4536), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe319"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(4714), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe320"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(4893), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe321"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(5072), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe322"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(5250), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe323"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(5426), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe324"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(5606), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe325"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(5784), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe326"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(5962), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe327"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(6137), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe328"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(6318), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe329"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(6497), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe330"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(6675), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe331"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(6855), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe332"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(7033), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe333"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(7208), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe334"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(7387), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe335"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(7711), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe338"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(7945), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe339"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(8129), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe340"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(8311), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe341"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(8494), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe342"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 682, DateTimeKind.Unspecified).AddTicks(8672), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe343"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 678, DateTimeKind.Unspecified).AddTicks(4182), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(5477), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(6238), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(6447), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(6641), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(6831), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(7018), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(7252), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(7526), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldGenderEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(1257), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldGenderEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 683, DateTimeKind.Unspecified).AddTicks(1895), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 673, DateTimeKind.Unspecified).AddTicks(1102), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 673, DateTimeKind.Unspecified).AddTicks(224), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 17, 23, 43, 6, 673, DateTimeKind.Unspecified).AddTicks(1338), new TimeSpan(0, 0, 0, 0, 0)));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "CaseId",
                schema: "System",
                table: "EveryDayAwardEntity",
                newName: "ModelId");

            migrationBuilder.CreateTable(
                name: "BonusCaseEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    Delay = table.Column<TimeSpan>(type: "interval", nullable: true),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    FirstNotifyDelay = table.Column<TimeSpan>(type: "interval", nullable: true),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    NextNotifyDelay = table.Column<TimeSpan>(type: "interval", nullable: true),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BonusCaseEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "CaseEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    RandomMaxDropItems = table.Column<int>(type: "integer", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "FractionEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FractionEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "GameCurrencyEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    Name = table.Column<string>(type: "text", nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameCurrencyEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "GameItemCategoryEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameItemCategoryEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "GameItemEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    CategoryId = table.Column<Guid>(type: "uuid", nullable: false),
                    FractionId = table.Column<Guid>(type: "uuid", nullable: false),
                    SellCurrencyId = table.Column<Guid>(type: "uuid", nullable: false),
                    SubscribeCurrencyId = table.Column<Guid>(type: "uuid", nullable: false),
                    AdminComment = table.Column<string>(type: "character varying(1024)", maxLength: 1024, nullable: true),
                    AmountDefault = table.Column<long>(type: "bigint", nullable: false),
                    AmountInStack = table.Column<long>(type: "bigint", nullable: false),
                    AmountMaximum = table.Column<long>(type: "bigint", nullable: true),
                    AmountMinimum = table.Column<long>(type: "bigint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    DisplayPosition = table.Column<int>(type: "integer", nullable: false),
                    Duration = table.Column<TimeSpan>(type: "interval", nullable: true),
                    Flags = table.Column<byte>(type: "smallint", nullable: false),
                    Level = table.Column<int>(type: "integer", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    SellPrice = table.Column<int>(type: "integer", nullable: false),
                    SubscribePrice = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_GameItemEntity_FractionEntity_FractionId",
                        column: x => x.FractionId,
                        principalSchema: "System",
                        principalTable: "FractionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameItemEntity_GameCurrencyEntity_SellCurrencyId",
                        column: x => x.SellCurrencyId,
                        principalSchema: "System",
                        principalTable: "GameCurrencyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameItemEntity_GameCurrencyEntity_SubscribeCurrencyId",
                        column: x => x.SubscribeCurrencyId,
                        principalSchema: "System",
                        principalTable: "GameCurrencyEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameItemEntity_GameItemCategoryEntity_CategoryId",
                        column: x => x.CategoryId,
                        principalSchema: "System",
                        principalTable: "GameItemCategoryEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "CaseItemEntity",
                schema: "System",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    ItemId = table.Column<Guid>(type: "uuid", nullable: false),
                    ModelId = table.Column<Guid>(type: "uuid", nullable: false),
                    ChanceOfDrop = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    MaximumCount = table.Column<int>(type: "integer", nullable: false),
                    MinimumCount = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseItemEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_CaseItemEntity_CaseEntity_ModelId",
                        column: x => x.ModelId,
                        principalSchema: "System",
                        principalTable: "CaseEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CaseItemEntity_GameItemEntity_ItemId",
                        column: x => x.ItemId,
                        principalSchema: "System",
                        principalTable: "GameItemEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(8396), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe602"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(9015), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe603"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(9215), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe604"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(9403), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AccountRoleEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-260c8dffe605"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(9586), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe100"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(9288), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(123), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(332), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(523), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(709), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(891), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(1076), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(1257), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(1436), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe109"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(1617), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe110"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(1798), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe111"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(1982), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe112"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(2163), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe113"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(2346), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe114"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(2528), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe115"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(2708), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe116"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(2890), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe117"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(3073), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe118"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(3253), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe119"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(3437), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe120"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(3692), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe121"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(3910), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe122"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(4094), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe123"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(4275), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe124"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(4454), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe125"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(4685), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe126"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(4866), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe127"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(5047), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe128"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(5229), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe129"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(5407), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "AchievementEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-900c8dffe130"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 84, DateTimeKind.Unspecified).AddTicks(5588), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.InsertData(
                schema: "System",
                table: "FractionEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe501"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 70, DateTimeKind.Unspecified).AddTicks(758), new TimeSpan(0, 0, 0, 0, 0)), null, "Keepers", (byte)1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe502"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 70, DateTimeKind.Unspecified).AddTicks(1609), new TimeSpan(0, 0, 0, 0, 0)), null, "Keepers_Friend", (byte)2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe503"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 70, DateTimeKind.Unspecified).AddTicks(1818), new TimeSpan(0, 0, 0, 0, 0)), null, "RIFT", (byte)3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe504"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 70, DateTimeKind.Unspecified).AddTicks(2009), new TimeSpan(0, 0, 0, 0, 0)), null, "RIFT_Friend", (byte)4 }
                });

            migrationBuilder.InsertData(
                schema: "System",
                table: "GameCurrencyEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 70, DateTimeKind.Unspecified).AddTicks(8350), new TimeSpan(0, 0, 0, 0, 0)), null, "Coin", (byte)3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe102"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 70, DateTimeKind.Unspecified).AddTicks(9252), new TimeSpan(0, 0, 0, 0, 0)), null, "Donate", (byte)2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe103"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 70, DateTimeKind.Unspecified).AddTicks(9460), new TimeSpan(0, 0, 0, 0, 0)), null, "Money", (byte)1 }
                });

            migrationBuilder.InsertData(
                schema: "System",
                table: "GameItemCategoryEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe901"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(3367), new TimeSpan(0, 0, 0, 0, 0)), null, "Weapon", (byte)1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe902"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(4030), new TimeSpan(0, 0, 0, 0, 0)), null, "Armour", (byte)2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe903"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(4231), new TimeSpan(0, 0, 0, 0, 0)), null, "Service", (byte)3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe904"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(4418), new TimeSpan(0, 0, 0, 0, 0)), null, "Character", (byte)4 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe905"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(4601), new TimeSpan(0, 0, 0, 0, 0)), null, "Profile", (byte)5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe906"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(4780), new TimeSpan(0, 0, 0, 0, 0)), null, "Material", (byte)6 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe907"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(4956), new TimeSpan(0, 0, 0, 0, 0)), null, "Grenade", (byte)7 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe908"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(5135), new TimeSpan(0, 0, 0, 0, 0)), null, "Kits", (byte)8 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe909"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(5381), new TimeSpan(0, 0, 0, 0, 0)), null, "Currency", (byte)9 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe910"), new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 71, DateTimeKind.Unspecified).AddTicks(5594), new TimeSpan(0, 0, 0, 0, 0)), null, "Resource", (byte)10 }
                });

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe100"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(8139), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(8348), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(8545), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(8737), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(8928), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(9120), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(9309), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(9496), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(9681), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe109"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(9871), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe110"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(59), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe111"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(247), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe112"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(433), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe113"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(618), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe114"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(804), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe115"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(989), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe116"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(1173), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe117"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(1357), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe118"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(1620), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe119"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(1841), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe120"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(2033), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe121"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(2222), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe122"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(2409), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe123"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(2596), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe124"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(2780), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe125"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(2963), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe126"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(3149), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe127"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(3339), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe128"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(3542), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe129"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(3737), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe130"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(3923), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe131"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(4108), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe132"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(4342), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe133"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(4526), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe134"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(4710), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe135"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(4896), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe136"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(5083), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe137"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(5268), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe138"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(5453), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe139"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(5636), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe140"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(5822), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe141"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(6059), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe142"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(6271), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe143"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(6458), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe144"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(6642), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe145"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(6828), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe147"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(7016), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe148"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(7198), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe149"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(7384), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe151"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(7567), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe152"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(7815), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe154"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(8028), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe155"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(8219), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe156"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(8405), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe157"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(8591), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe158"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(8776), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe159"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(8960), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe160"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(9168), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe161"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(9354), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe162"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(9539), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe163"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(9724), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe164"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 78, DateTimeKind.Unspecified).AddTicks(9908), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe165"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(94), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe166"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(278), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe167"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(458), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe168"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(645), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe170"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(829), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe171"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(1012), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe172"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(1195), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe173"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(1378), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe174"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(1565), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe175"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(1745), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe176"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(1929), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe177"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(2111), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe178"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(2293), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe179"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(2616), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe180"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(2814), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe181"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(3004), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe182"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(3195), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe183"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(3381), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe184"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(3567), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe185"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(3752), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe186"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(4006), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe187"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(4223), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe188"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(4414), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe189"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(4604), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe190"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(4818), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe191"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(5049), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe192"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(5261), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe193"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(5449), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe194"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(5633), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe196"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(5818), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe197"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(6005), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe198"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(6187), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe199"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(6372), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe200"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(6556), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe201"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(6738), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe203"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(6917), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe204"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(7099), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe205"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(7285), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe206"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(7542), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe207"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(7721), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe208"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(7900), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe209"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(8079), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe210"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(8257), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe211"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(8437), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe212"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(8615), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe213"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(8798), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe214"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(8976), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe217"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(9155), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe218"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(9335), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe220"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(9514), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe221"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(9695), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe222"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 79, DateTimeKind.Unspecified).AddTicks(9876), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe223"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(125), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe224"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(334), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe225"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(517), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe226"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(697), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe227"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(878), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe228"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(1056), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe229"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(1231), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe230"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(1407), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe231"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(1588), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe232"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(1765), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe233"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(1942), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe234"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(2121), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe235"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(2299), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe236"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(2475), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe237"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(2651), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe238"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(2828), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe239"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(3014), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe240"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(3191), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe241"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(3369), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe242"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(3546), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe243"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(3773), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe244"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(3974), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe245"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(4160), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe246"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(4340), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe247"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(4515), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe248"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(4690), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe249"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(4868), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe250"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(5046), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe251"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(5222), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe252"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(5416), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe253"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(5608), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe254"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(5788), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe255"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(6033), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe256"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(6249), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe257"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(6432), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe258"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(6614), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe259"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(6794), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe260"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(6970), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe261"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(7147), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe262"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(7326), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe263"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(7536), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe264"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(7741), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe265"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(7948), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe266"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(8130), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe268"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(8314), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe269"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(8495), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe270"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(8679), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe271"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(8862), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe272"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(9044), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe273"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(9225), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe274"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(9409), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe275"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(9590), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe276"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(9770), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe277"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 80, DateTimeKind.Unspecified).AddTicks(9952), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe278"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(168), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe279"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(368), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe280"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(553), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe281"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(736), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe282"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(930), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe283"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(1122), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe284"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(1305), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe285"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(1485), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe286"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(1667), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe287"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(1850), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe288"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(2097), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe289"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(2309), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe290"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(2537), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe291"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(2749), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe292"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(2936), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe293"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(3121), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe294"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(3303), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe295"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(3485), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe296"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(3667), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe297"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(3849), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe298"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(4034), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe299"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(4215), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe300"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(4398), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe301"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(4582), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe302"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(4765), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe303"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(4945), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe304"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(5128), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe305"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(5313), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe306"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(5496), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe307"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(5677), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe308"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(5862), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe309"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(6046), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe310"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(6231), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe311"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(6432), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe312"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(6628), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe313"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(6815), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe314"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(6999), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe315"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(7184), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe316"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(7366), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe317"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(7554), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe318"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(7758), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe319"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(7938), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe320"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(8181), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe321"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(8390), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe322"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(8576), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe323"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(8755), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe324"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(8934), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe325"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(9113), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe326"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(9291), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe327"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(9468), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe328"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(9648), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe329"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 81, DateTimeKind.Unspecified).AddTicks(9823), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe330"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe331"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(178), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe332"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(354), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe333"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(529), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe334"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(706), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe335"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(884), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe338"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(1061), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe339"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(1286), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe340"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(1487), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe341"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(1668), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe342"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(1847), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldCountryEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe343"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 77, DateTimeKind.Unspecified).AddTicks(6831), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(9644), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(286), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(490), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe104"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(679), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe105"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(865), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe106"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(1048), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe107"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(1232), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldDayOfWeekEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe108"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 83, DateTimeKind.Unspecified).AddTicks(1413), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldGenderEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(4759), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldGenderEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 82, DateTimeKind.Unspecified).AddTicks(5439), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe101"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 72, DateTimeKind.Unspecified).AddTicks(2979), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe102"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 72, DateTimeKind.Unspecified).AddTicks(2218), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.UpdateData(
                schema: "System",
                table: "WorldLanguageEntity",
                keyColumn: "EntityId",
                keyValue: new Guid("a607b39d-6e02-45ce-9b77-200c8dffe103"),
                column: "CreatedAt",
                value: new DateTimeOffset(new DateTime(2022, 12, 14, 21, 14, 35, 72, DateTimeKind.Unspecified).AddTicks(3182), new TimeSpan(0, 0, 0, 0, 0)));

            migrationBuilder.CreateIndex(
                name: "IX_NewLevelAwardEntity_CaseId",
                schema: "System",
                table: "NewLevelAwardEntity",
                column: "CaseId");

            migrationBuilder.CreateIndex(
                name: "IX_EveryDayAwardEntity_ModelId",
                schema: "System",
                table: "EveryDayAwardEntity",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_BonusCaseEntity_Name",
                schema: "System",
                table: "BonusCaseEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_BonusCaseEntity_TypeId",
                schema: "System",
                table: "BonusCaseEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_CaseEntity_Name",
                schema: "System",
                table: "CaseEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_CaseItemEntity_ItemId",
                schema: "System",
                table: "CaseItemEntity",
                column: "ItemId");

            migrationBuilder.CreateIndex(
                name: "IX_CaseItemEntity_ModelId",
                schema: "System",
                table: "CaseItemEntity",
                column: "ModelId");

            migrationBuilder.CreateIndex(
                name: "IX_FractionEntity_Name",
                schema: "System",
                table: "FractionEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_FractionEntity_TypeId",
                schema: "System",
                table: "FractionEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameCurrencyEntity_Name",
                schema: "System",
                table: "GameCurrencyEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_GameCurrencyEntity_TypeId",
                schema: "System",
                table: "GameCurrencyEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameItemCategoryEntity_Name",
                schema: "System",
                table: "GameItemCategoryEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemCategoryEntity_TypeId",
                schema: "System",
                table: "GameItemCategoryEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_CategoryId",
                schema: "System",
                table: "GameItemEntity",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_DisplayPosition",
                schema: "System",
                table: "GameItemEntity",
                column: "DisplayPosition");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_FractionId",
                schema: "System",
                table: "GameItemEntity",
                column: "FractionId");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_Level",
                schema: "System",
                table: "GameItemEntity",
                column: "Level");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_SellCurrencyId",
                schema: "System",
                table: "GameItemEntity",
                column: "SellCurrencyId");

            migrationBuilder.CreateIndex(
                name: "IX_GameItemEntity_SubscribeCurrencyId",
                schema: "System",
                table: "GameItemEntity",
                column: "SubscribeCurrencyId");

            migrationBuilder.AddForeignKey(
                name: "FK_EveryDayAwardEntity_CaseEntity_ModelId",
                schema: "System",
                table: "EveryDayAwardEntity",
                column: "ModelId",
                principalSchema: "System",
                principalTable: "CaseEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_NewLevelAwardEntity_CaseEntity_CaseId",
                schema: "System",
                table: "NewLevelAwardEntity",
                column: "CaseId",
                principalSchema: "System",
                principalTable: "CaseEntity",
                principalColumn: "EntityId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
