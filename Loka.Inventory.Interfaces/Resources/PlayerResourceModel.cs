﻿using Loka.Store.Interfaces.Models.Resources;

namespace Loka.Inventory.Interfaces.Resources;

public static class PlayerResourceModel
{
    [GenerateSerializer]
    public sealed class Lightweight : IEntry
    {
        [Id(0)]
        public Guid Id { get; init; }

        [Id(1)]
        public GameResourceModel Resource { get; set; } = null!;

        [Id(2)]
        public long Amount { get; init; }
    }
    
    [GenerateSerializer]
    public sealed class Delta : IEntry
    {
        [Id(0)]
        public Guid Id { get; init; }

        [Id(1)]
        public GameResourceModel Resource { get; set; } = null!;
        
        [Id(2)]
        public long PreviousBalance { get; init; }

        [Id(3)]
        public long CurrentBalance { get; init; }
        
        public long DeltaBalance => PreviousBalance - CurrentBalance;
    }
}
