﻿using Loka.Common.MassTransit.MessageBrokers;

namespace Loka.Identity.Interfaces.Events;

public sealed class AfterSignInEvent : IClusterEvent
{
    public Guid PlayerId { get; init; }
    public Guid SessionId { get; init; }
    public string PlayerName { get; init; } = null!;
}