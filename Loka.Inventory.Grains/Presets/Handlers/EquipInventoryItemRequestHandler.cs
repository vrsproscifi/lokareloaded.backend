﻿using Loka.Common.Authentication;
using Loka.Common.MediatR.Handlers;
using Loka.Inventory.Interfaces.Inventory;
using Loka.Inventory.Interfaces.Presets;
using Loka.Inventory.Interfaces.Presets.Models;
using Loka.Store.Interfaces.Enums;
using Loka.Store.Interfaces.Repositories;

namespace Loka.Inventory.Grains.Presets.Handlers;

internal sealed class EquipInventoryItemRequestHandler : IOperationHandler<EquipInventoryItem.Request, EquipInventoryItem.Result>
{
    private PlayerActor Actor { get; }
    private IGameStoreItemsRepository StoreItemsRepository { get; }
    private IPlayerInventoryItemsRepository PlayerInventoryItemsRepository { get; }
    private IPlayerInventoryPresetRepository PlayerInventoryPresetRepository { get; }

    public EquipInventoryItemRequestHandler
    (
        PlayerActor actor,
        IGameStoreItemsRepository storeItemsRepository, IPlayerInventoryItemsRepository playerInventoryItemsRepository,
        IPlayerInventoryPresetRepository playerInventoryPresetRepository)
    {
        Actor = actor;
        StoreItemsRepository = storeItemsRepository;
        PlayerInventoryItemsRepository = playerInventoryItemsRepository;
        PlayerInventoryPresetRepository = playerInventoryPresetRepository;
    }

    public async Task<OperationResult<EquipInventoryItem.Result>> Handle(EquipInventoryItem.Request request, CancellationToken cancellationToken)
    {
        var getItemResult = await PlayerInventoryItemsRepository.GetById(Actor.PlayerId, request.ItemId);
        if (!getItemResult.GetValueIfSucceeded(out var item))
            return getItemResult.Error!;

        if (item.Model.Category.TypeId.IsEquippable())
        {
            var getPresetResult = await PlayerInventoryPresetRepository.GetById(Actor.PlayerId, request.PresetId);
            if (!getPresetResult.GetValueIfSucceeded(out var preset))
                return getPresetResult.Error!;

            var slotItem = preset.Items.FirstOrDefault(slot => slot.Item.Model.Category.Id == item.Model.Category.Id);
            if (slotItem == null)
            {
                await PlayerInventoryPresetRepository.AddSlotItem(request.PresetId, request.ItemId);
            }
            else
            {
                await PlayerInventoryPresetRepository.SetSlotItem(request.PresetId, slotItem.Id, request.ItemId);
            }

            return new EquipInventoryItem.Result()
            {
                PresetId = request.PresetId,
                ItemId = request.ItemId
            };
        }

        return OperationResultCode.UnknownError;
    }
}