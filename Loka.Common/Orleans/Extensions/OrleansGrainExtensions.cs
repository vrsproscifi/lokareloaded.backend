﻿using Loka.Common.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Orleans;

namespace Loka.Common.Orleans.Extensions;

public static class OrleansGrainExtensions
{
    public static IServiceCollection AddGrain<TGrain>(this IServiceCollection services) where TGrain : class, IGrainWithGuidKey
    {
        services.AddScoped(provider =>
        {
            var actor = provider.GetRequiredService<PlayerActor>();
            var client = provider.GetRequiredService<IClusterClient>();
            var grain = client.GetGrain<TGrain>(actor.PlayerId);
            return grain;
        });
            
        return services;
    }
}