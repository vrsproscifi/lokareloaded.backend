﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR.Pipeline;
using Microsoft.Extensions.Logging;
using Loka.Common.MediatR.Requests;
using Loka.Common.Operations.Codes;
using Loka.Common.Operations.Results;

namespace Loka.Common.MediatR.ExceptionHandlers;

public class OperationExceptionHandler<TRequest, TResult, TException>
    : IOperationExceptionHandler<TRequest, TResult, TException>
    where TRequest : IOperationRequest<TResult>
    where TException : Exception
{
    private readonly ILogger<OperationExceptionHandler<TRequest, TResult, TException>> logger;

    public OperationExceptionHandler(ILogger<OperationExceptionHandler<TRequest, TResult, TException>> logger)
    {
        this.logger = logger;
    }

    public Task Handle(TRequest request, TException exception, RequestExceptionHandlerState<OperationResult<TResult>> state,
        CancellationToken cancellationToken)
    {
        state.SetHandled(new OperationError(OperationResultCode.ExceptionError, $"Exception: {exception}"));

        this.logger.LogError(exception, exception.Message);
        return Task.CompletedTask;
    }
}

public class OperationExceptionHandler<TRequest, TException>
    : IOperationExceptionHandler<TRequest, TException>
    where TRequest : IOperationRequest
    where TException : Exception
{
    private readonly ILogger<OperationExceptionHandler<TRequest, TException>> logger;

    public OperationExceptionHandler(ILogger<OperationExceptionHandler<TRequest, TException>> logger)
    {
        this.logger = logger;
    }

    public Task Handle(TRequest request, TException exception, RequestExceptionHandlerState<OperationResult> state,
        CancellationToken cancellationToken)
    {
        state.SetHandled(new OperationError(OperationResultCode.ExceptionError, $"Exception: {exception}"));

        this.logger.LogError(exception, exception.Message);
        return Task.CompletedTask;
    }
}