﻿using Loka.Store.DataBase.Entities;

namespace Loka.Store.DataBase.Seeders;

public class FractionEntitySeed
{
    public static Guid Keepers { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe501");
    public static Guid KeepersFriend { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe502");
        
    public static Guid RIFT { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe503");
    public static Guid RIFTFriend { get; } = Guid.Parse("a607b39d-6e02-45ce-9b77-260c8dffe504");
        
    public static FractionEntity[] Entities { get; } = new[]
    {
        new FractionEntity(Keepers, FractionTypeId.Keepers),
        new FractionEntity(KeepersFriend, FractionTypeId.Keepers_Friend),

        new FractionEntity(RIFT, FractionTypeId.RIFT),
        new FractionEntity(RIFTFriend, FractionTypeId.RIFT_Friend),
    };
}