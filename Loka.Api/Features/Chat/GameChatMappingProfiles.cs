﻿using AutoMapper.Extensions.EnumMapping;

namespace Loka.Api.Features.Chat;

public sealed class GameChatMappingProfiles : Profile
{
    public GameChatMappingProfiles()
    {
        CreateMap<Loka.Chat.Interfaces.Enums.ChatChannelEventType, ChatChannelEventType>()
            .ConvertUsingEnumMapping(q => q
                .MapValue(Loka.Chat.Interfaces.Enums.ChatChannelEventType.Create, ChatChannelEventType.Create)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatChannelEventType.Close, ChatChannelEventType.Close)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatChannelEventType.ChangeName, ChatChannelEventType.ChangeName)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatChannelEventType.AssignRole, ChatChannelEventType.AssignRole)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatChannelEventType.Invite, ChatChannelEventType.Invite)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatChannelEventType.Kick, ChatChannelEventType.Kick)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatChannelEventType.Left, ChatChannelEventType.Left)
            )
            .ReverseMap();

        CreateMap<Loka.Chat.Interfaces.Enums.ChatChanelType, ChatChanelType>()
            .ConvertUsingEnumMapping(q => q
                .MapValue(Loka.Chat.Interfaces.Enums.ChatChanelType.Announcements, ChatChanelType.Announcements)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatChanelType.Global, ChatChanelType.Global)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatChanelType.Private, ChatChanelType.Private)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatChanelType.Squad, ChatChanelType.Squad)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatChanelType.Match, ChatChanelType.Match)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatChanelType.Clan, ChatChanelType.Clan)
            )
            .ReverseMap();

        CreateMap<Loka.Chat.Interfaces.Enums.ChatMemberPermissions, ChatMemberPermissions>()
            .ConvertUsingEnumMapping(q => q
                .MapValue(Loka.Chat.Interfaces.Enums.ChatMemberPermissions.ReadMessages, ChatMemberPermissions.ReadMessages)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatMemberPermissions.SendMessages, ChatMemberPermissions.SendMessages)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatMemberPermissions.InviteNewMembers, ChatMemberPermissions.InviteNewMembers)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatMemberPermissions.KickMembers, ChatMemberPermissions.KickMembers)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatMemberPermissions.AssignMemberRole, ChatMemberPermissions.AssignMemberRole)
                .MapValue(Loka.Chat.Interfaces.Enums.ChatMemberPermissions.Owner, ChatMemberPermissions.Owner)
            )
            .ReverseMap();

        //==========================================================
        CreateMap<AssignRoleForChannelMember.Types.Request, Loka.Chat.Interfaces.Models.AssignRoleForChannelMember.Request>()
            .ForMember(q => q.ChannelId, q => q.MapFrom(w => w.ChannelId))
            .ForMember(q => q.PlayerId, q => q.MapFrom(w => w.PlayerId))
            .ForMember(q => q.Permissions, q => q.MapFrom(w => w.Permissions));

        CreateMap<Loka.Chat.Interfaces.Models.AssignRoleForChannelMember.Result, AssignRoleForChannelMember.Types.Response>()
            .ForMember(q => q.Channel, q => q.MapFrom(w => w.Channel))
            .ForMember(q => q.Member, q => q.MapFrom(w => w.Member));
          //  .ForMember(q => q.Permissions, q => q.MapFrom(w => w.Permissions))

        //==========================================================
        CreateMap<Loka.Chat.Interfaces.Models.ChatMemberEntry, ChatMemberEntry>()
            .ForMember(q => q.PlayerId, q => q.MapFrom(w => w.PlayerId))
            .ForMember(q => q.PlayerName, q => q.MapFrom(w => w.PlayerName));

        //==========================================================
        CreateMap<Loka.Chat.Interfaces.Models.CreatePrivateChannel.Result, CreatePrivateChannel.Types.Response>()
            .ForMember(q => q.Channel, q => q.MapFrom(w => w.Channel));

        //==========================================================
        CreateMap<Loka.Chat.Interfaces.Models.GetChatMember.Result, GetChatMember.Types.Response>()
            .ForMember(q => q.Member, q => q.MapFrom(w => w.Member))
            .ForMember(q => q.Permissions, q => q.MapFrom(w => w.Permissions));

        //==========================================================
        CreateMap<Loka.Chat.Interfaces.Models.GetChatMembers.Result, GetChatMembers.Types.Response>()
            .ForMember(q => q.Entries, q => q.MapFrom(w => w.Entries));

        //==========================================================
        CreateMap<Loka.Chat.Interfaces.Models.GetChatMessages.Result, GetChatMessages.Types.Response>()
            .ForMember(q => q.Messages, q => q.MapFrom(w => w.Messages))
            .ForMember(q => q.Events, q => q.MapFrom(w => w.Events))
            .ForMember(q => q.LaterThen, q => q.MapFrom(w => w.LaterThen))
            .ForMember(q => q.EarlyThen, q => q.MapFrom(w => w.EarlyThen));

        //==========================================================
        CreateMap<Loka.Chat.Interfaces.Models.GetChatChannels.Result, GetChatChannels.Types.Response>()
            .ForMember(q => q.Entries, q => q.MapFrom(w => w.Entries));

        //==========================================================
        CreateMap<Loka.Chat.Interfaces.Models.SendChatMessage.Result, SendChatMessage.Types.Response>()
            .ForMember(q => q.Message, q => q.MapFrom(w => w.Message));

        //==========================================================
        CreateMap<Loka.Chat.Interfaces.Models.ChannelEntry, ChannelEntry>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.Name, q => q.MapFrom(w => w.Name))
            .ForMember(q => q.TypeId, q => q.MapFrom(w => w.TypeId));

        //==========================================================
        CreateMap<Loka.Chat.Interfaces.Models.ChatMessageEntry, ChatMessageEntry>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.Message, q => q.MapFrom(w => w.Message))
            .ForMember(q => q.Sender, q => q.MapFrom(w => w.Sender))
            .ForMember(q => q.CreatedAt, q => q.MapFrom(w => w.CreatedAt));

        //==========================================================
        CreateMap<Loka.Chat.Interfaces.Models.ChatEventEntry, ChatEventEntry>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.TypeId, q => q.MapFrom(w => w.TypeId))
            .ForMember(q => q.Sender, q => q.MapFrom(w => w.Sender))
            .ForMember(q => q.CreatedAt, q => q.MapFrom(w => w.CreatedAt));
    }
}