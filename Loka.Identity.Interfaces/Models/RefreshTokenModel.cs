﻿namespace Loka.Identity.Interfaces.Models;

[GenerateSerializer]
public sealed class RefreshTokenModel
{
    [Id(0)]
    public JwtTokenObject JwtToken { get; init; } = null!;

    [Id(1)]
    public JwtTokenObject RefreshToken { get; init; } = null!;

    [Id(2)]
    public DateTimeOffset TimeoutDate { get; init; }
}