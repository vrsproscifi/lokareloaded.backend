﻿using System;

namespace Loka.Database.Common.Entities;

public interface IPlayerChildEntity
{
    Guid PlayerId { get; }
}