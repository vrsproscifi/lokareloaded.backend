﻿using Loka.Api.Models;
using Loka.Identity.Interfaces;
using Loka.Identity.Interfaces.Forms;
using Loka.Identity.Interfaces.Models;

namespace Loka.Api.Features.Identity;

[AllowAnonymous]
public sealed class IdentityController : PlayerIdentity.PlayerIdentityBase
{
    private PlayerActor PlayerActor { get; }
    private ControllerServices<IdentityController> Services { get; }

    public IdentityController(ControllerServices<IdentityController> services, PlayerActor playerActor)
    {
        Services = services;
        PlayerActor = playerActor;
    }

    [Authorize]
    public override Task<SessionRefreshTokenResult> RefreshToken(RefreshToken.Types.Request request, ServerCallContext context)
    {
        var identity = Services.ClusterClient.GetGrain<IIdentityGrain>(request.ToString());
        return identity.RefreshToken(new RefreshTokenForm()
        {
        }).ReturnResult<RefreshTokenModel, SessionRefreshTokenResult>(Services.Mapper);
    }
    
    [AllowAnonymous]
    public override Task<PlayerAuthenticationResult> SignIn(SignInQuery request, ServerCallContext context)
    {
        var tokenLifetime = context.RequestHeaders.GetTimeSpan(HeaderExtensions.TokenLifetime, HeaderExtensions.DefaultTokenLifetime);

        var identity = Services.ClusterClient.GetGrain<IIdentityGrain>(request.UserNameOrEmail);
        return identity.SignIn(new SignInForm()
        {
            UserNameOrEmail = request.UserNameOrEmail.ToLowerInvariant(),
            Password = request.Password,
            IpAddress = IPAddress.Any,
            TokenLifetime = tokenLifetime
        }).ReturnResult<PlayerAuthenticationModel, PlayerAuthenticationResult>(Services.Mapper);
    }

    [AllowAnonymous]
    public override Task<PlayerAuthenticationResult> SignUp(SignUpQuery request, ServerCallContext context)
    {
        var tokenLifetime = context.RequestHeaders.GetTimeSpan(HeaderExtensions.TokenLifetime, HeaderExtensions.DefaultTokenLifetime);

        var identity = Services.ClusterClient.GetGrain<IIdentityGrain>(request.PlayerName);
        return identity.SignUp(new SignUpForm()
        {
            TokenLifetime = tokenLifetime,
            //------------------------------
            Email = request.Email,
            PlayerName = request.PlayerName,
            Password = request.Password,
            IpAddress = IPAddress.Any,

            //------------------------------
            Birthdate = request.Birthdate != null ? request.Birthdate.ToDateTimeOffset() : null,

            //------------------------------
            RoleId = null,
            CountryId = string.IsNullOrWhiteSpace(request.CountryId?.Value) ? null : Guid.Parse(request.CountryId.Value),
            LanguageId = string.IsNullOrWhiteSpace(request.LanguageId?.Value) ? null : Guid.Parse(request.LanguageId.Value),
            GenderId = string.IsNullOrWhiteSpace(request.GenderId?.Value) ? null : Guid.Parse(request.GenderId.Value),
        }).ReturnResult<PlayerAuthenticationModel, PlayerAuthenticationResult>(Services.Mapper);
    }
}