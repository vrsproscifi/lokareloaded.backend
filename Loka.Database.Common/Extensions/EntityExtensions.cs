﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using Loka.Database.Common.Entities;
using Microsoft.EntityFrameworkCore;

namespace Loka.Database.Common.Extensions;

public static class EntityExtensions
{
    public static async Task<T> GetOrCreate<T>(this DbSet<T> set, Expression<Func<T, bool>> expression, Func<T> factory) where T : BaseEntity
    {
        var entity = await set.FirstOrDefaultAsync(expression);
        if (entity == null)
        {
            entity = factory();
            await set.AddAsync(entity);
        }

        return entity;
    }

    public static IReadOnlyList<Guid> SelectEntityIds(this IReadOnlyList<BaseEntity> entities)
    {
        return entities.Select(entity => entity.EntityId).ToArray();
    }

    public static Dictionary<Guid, TEntity> SelectEntityDictionary<TEntity>(this IReadOnlyList<TEntity> entities)
        where TEntity : BaseEntity
    {
        return entities.ToDictionary(x => x.EntityId, x => x);
    }

    [return: NotNull]
    public static Task<TEntity> GetById<TEntity>(this DbSet<TEntity> dbSet, Guid id, CancellationToken cancellationToken = default)
        where TEntity : BaseEntity
    {
        return dbSet.FirstAsync(q => q.EntityId == id, cancellationToken);
    }

    public static Task<TEntity> GetById<TEntity>(this DbSet<TEntity> dbSet, Guid id, Func<IQueryable<TEntity>, IQueryable<TEntity>> customize, CancellationToken cancellationToken = default)
        where TEntity : BaseEntity
    {
        var query = customize(dbSet.AsQueryable());
        return query.FirstAsync(q => q.EntityId == id, cancellationToken);
    }
}