﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace Loka.Common.Mongo;

public abstract class MongoDbContext
{
    private static Type MongoCollectionType { get; } = typeof(IMongoCollection<>);
    private static Type MongoDatabaseType { get; } = typeof(IMongoDatabase);

    protected ILogger Logger { get; private init; } = null!;
    private IMongoClient Client { get; init; } = null!;
    protected IMongoDatabase Database { get; private init; } = null!;

    [MemberNotNull(nameof(Logger))]
    [MemberNotNull(nameof(Client))]
    [MemberNotNull(nameof(Database))]
    internal static T Create<T>(MongoUrl settings, ILogger logger)
        where T : MongoDbContext, new()
    {
        var client = new MongoClient(settings);

        var context = new T()
        {
            Logger = logger,
            Client = client,
            Database = client.GetDatabase(settings.DatabaseName)
        };

        context.Initialize();
        return context;
    }

    private void Initialize()
    {
        Logger.LogInformation($"Begin {nameof(Initialize)}");

        var availableProperties = GetType().GetProperties();
        var collectionProperties = availableProperties
            .Where(property => property.PropertyType.GetGenericTypeDefinition() == MongoCollectionType)
            .ToArray();

        Logger.LogInformation($"Collection properties: {collectionProperties.Length}");
        var methodInfo = MongoDatabaseType.GetMethod(nameof(IMongoDatabase.GetCollection));

        foreach (var collectionProperty in collectionProperties)
        {
            var propertyName = collectionProperty.Name;
            var collectionType = collectionProperty.PropertyType.GenericTypeArguments[0];
            Logger.LogInformation($"Collection property {propertyName}[Type: {collectionType.Name}] | CanWrite: {collectionProperty.CanWrite}");

            if (collectionProperty.CanWrite)
            {
                MethodInfo methodGeneric = methodInfo!.MakeGenericMethod(collectionType);
                var collection = methodGeneric.Invoke(Database, new object[]
                {
                    propertyName, null!
                });

                collectionProperty.SetValue(this, collection);
            }
        }

        Logger.LogInformation($"End {nameof(Initialize)}");
    }
}