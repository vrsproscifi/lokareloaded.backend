﻿namespace Loka.Store.Interfaces.Enums;

public enum ResourceTypeId 
{
    None = 0,

    #region Money

    Money,
    Donate,
    Coin,

    #endregion

    Iron,
    Steel,
    Copper,
    
    /// <summary>
    /// Тпяпки
    /// </summary>
    Rags,
}