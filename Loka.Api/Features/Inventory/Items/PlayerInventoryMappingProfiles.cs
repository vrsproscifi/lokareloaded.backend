﻿using Loka.Inventory.Interfaces.Inventory;

namespace Loka.Api.Features.Inventory.Items;

public sealed class PlayerInventoryMappingProfiles : Profile
{
    public PlayerInventoryMappingProfiles()
    {
        //==========================================================
        CreateMap<Loka.Inventory.Interfaces.Discounts.GetPlayerDiscounts.Result, GetPlayerDiscounts.Types.Response>()
            .ForMember(q => q.Entries, q => q.MapFrom(w => w.Entries));

        CreateMap<Loka.Inventory.Interfaces.Discounts.GetPlayerDiscounts.Entry, GetPlayerDiscounts.Types.Entry>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.ModelId))
            .ForMember(q => q.NextAvailableDate, q => q.MapFrom(w => w.NextAvailableDate))
            .ForMember(q => q.EndDiscountDate, q => q.MapFrom(w => w.EndDiscountDate));

        //==========================================================
        CreateMap<Loka.Inventory.Interfaces.Inventory.Requests.GetPlayerPurchasedItems.Result, GetPlayerPurchasedItems.Types.Response>()
            .ForMember(q => q.Entries, q => q.MapFrom(w => w.Entries));

        CreateMap<PlayerInventoryItemModel.RPC, PlayerInventoryItem>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.ModelId))
            .ForMember(q => q.Level, q => q.MapFrom(w => w.Level))
            .ForMember(q => q.Amount, q => q.MapFrom(w => w.Amount))
            .ForMember(q => q.LastUseDate, q => q.MapFrom(w => w.LastUseDate));
    }
}