﻿using Loka.Api.Features;
using Loka.Api.IntegrationTests.Clients;
using static NUnit.Framework.Assert;

namespace Loka.Api.IntegrationTests.Services.Store;

public sealed class GameStoreService
{
    private GameAPIClients Clients { get; }

    public GameStoreService(GameAPIClients client)
    {
        Clients = client;
    }

    public async Task<GetStoreItems.Types.Response> GetStoreItems()
    {
        var store = await Clients.GameStoreClient.GetStoreItemsAsync(new GetStoreItems.Types.Query());
        That(store, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid response");
        Multiple(() =>
        {
            That(store.Products, Is.Not.Empty, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(store.Products)}");
            That(store.Categories, Is.Not.Empty, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(store.Categories)}");
            That(store.Resources, Is.Not.Empty, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(store.Resources)}");
            That(store.Fractions, Is.Not.Empty, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(store.Fractions)}");
            That(store.Cases, Is.Not.Empty, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(store.Cases)}");
            That(store.Boosters, Is.Not.Empty, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(store.Boosters)}");
        });

        await TestContext.Progress.WriteLineAsync($"{nameof(GameStoreService)}.{nameof(GetStoreItems)}");
        await TestContext.Progress.WriteLineAsync($"Received: {store.Products.Count} {nameof(store.Products)}");
        await TestContext.Progress.WriteLineAsync($"Received: {store.Categories.Count} {nameof(store.Categories)}");
        await TestContext.Progress.WriteLineAsync($"Received: {store.Resources.Count} {nameof(store.Resources)}");
        await TestContext.Progress.WriteLineAsync($"Received: {store.Fractions.Count} {nameof(store.Fractions)}");
        await TestContext.Progress.WriteLineAsync($"Received: {store.Cases.Count} {nameof(store.Cases)}");
        await TestContext.Progress.WriteLineAsync($"Received: {store.Boosters.Count} {nameof(store.Boosters)}");

        foreach (var product in store.Products)
        {
            That(product, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(product)}");
            Multiple(() =>
            {
                That(product.Id, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(product)}.{nameof(product.Id)}");
                That(product.Name, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(product)}.{nameof(product.Name)}");
                That(product.Blueprints, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(product)}.{nameof(product.Blueprints)}");
                That(product.Category, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(product)}.{nameof(product.Category)}");
                That(product.Category.Id, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(product)}.{nameof(product.Category)}.{nameof(product.Category.Id)}");
                That(product.Category.Name, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(product)}.{nameof(product.Category)}.{nameof(product.Category.Name)}");
                That(product.Fraction, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(product)}.{nameof(product.Fraction)}");
                That(product.Fraction.Id, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(product)}.{nameof(product.Fraction)}.{nameof(product.Fraction.Id)}");
                That(product.Fraction.Name, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(product)}.{nameof(product.Fraction)}.{nameof(product.Fraction.Name)}");
            });
        }

        foreach (var category in store.Categories)
        {
            That(category, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(category)}");
            Multiple(() =>
            {
                That(category.Id, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(category)}.{nameof(category.Id)}");
                That(category.Name, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(category)}.{nameof(category.Name)}");
            });
        }

        foreach (var fraction in store.Fractions)
        {
            NotNull(fraction, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(fraction)}");
            NotNull(fraction.Id, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(fraction)}.{nameof(fraction.Id)}");
            NotNull(fraction.Name, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(fraction)}.{nameof(fraction.Name)}");
        }

        foreach (var resource in store.Resources)
        {
            NotNull(resource, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(resource)}");
            NotNull(resource.Id, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(resource)}.{nameof(resource.Id)}");
            NotNull(resource.Name, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(resource)}.{nameof(resource.Name)}");
        }

        foreach (var @case in store.Cases)
        {
            That(@case, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(@case)}");
            Assert.Multiple(() =>
            {
                That(@case.Id, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(@case)}.{nameof(@case.Id)}");
                That(@case.Name, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(@case)}.{nameof(@case.Name)}");
                That(@case.Resources, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(@case)}.{nameof(@case.Resources)}");
                That(@case.Items, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(@case)}.{nameof(@case.Items)}");
            });
        }

        foreach (var booster in store.Boosters)
        {
            That(booster, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(booster)}");
            Assert.Multiple(() =>
            {
                That(booster.Id, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(booster)}.{nameof(booster.Id)}");
                That(booster.Name, Is.Not.Null, $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(booster)}.{nameof(booster.Name)}");
                That(booster.Type, Is.Not.EqualTo(GameBooster.Types.Type.None), $"Failed {nameof(Clients.GameStoreClient.GetStoreItems)} | Invalid {nameof(booster)}.{nameof(booster.Name)}");
            });
        }

        return store;
    }
}