namespace Loka.Inventory.Interfaces.Cases;

public static class OpenPlayerCase
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<Result>
    {
        [Id(0)]
        public Guid Id { get; init; }
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public PlayerInventoryCaseModel Case { get; init; }

        [Id(1)]
        public IReadOnlyList<ResourceEntry> Resources { get; init; }

        [Id(2)]
        public IReadOnlyList<ItemsEntry> Items { get; init; }
    }

    [GenerateSerializer]
    public sealed class ResourceEntry
    {
        [Id(0)]
        public Guid ModelId { get; init; }

        [Id(1)]
        public long Amount { get; init; }
    }
    
    [GenerateSerializer]
    public sealed class ItemsEntry
    {
        [Id(0)]
        public Guid ModelId { get; init; }

        [Id(1)]
        public long Amount { get; init; }
    }
}