﻿using System;
using Loka.Database.Common.Contexts;
using Loka.Inventory.DataBase.Entities.Boosters;
using Loka.Inventory.DataBase.Entities.Cases;
using Loka.Inventory.DataBase.Entities.Currencies;
using Loka.Inventory.DataBase.Entities.Discounts;
using Loka.Inventory.DataBase.Entities.Items;
using Loka.Inventory.DataBase.Entities.Preset;
using Loka.Inventory.DataBase.Entities.Templates;
using Microsoft.EntityFrameworkCore;

namespace Loka.Inventory.DataBase;

public sealed class InventoryDbContext : BaseGameDbContext<InventoryDbContext>
{
    public InventoryDbContext(DbContextOptions<InventoryDbContext> options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);
        // modelBuilder.Entity<PlayerItemEntity>()
        //     .HasMany(q => q.Modules)
        //     .WithOne(q => q.ItemEntity!)
        //     .HasForeignKey(q => q.ItemId);
    }

    public DbSet<PlayerBoosterEntity> PlayerBoosterEntity { get; set; } = null!;
    public DbSet<PlayerCaseEntity> PlayerCaseEntity { get; set; } = null!;
    public DbSet<PlayerResourceEntity> PlayerResourceEntity { get; set; } = null!;
    public DbSet<PlayerDiscountEntity> PlayerDiscountEntity { get; set; } = null!;

    public DbSet<PlayerInventoryPresetEntity> PlayerInventoryPresetEntity { get; set; } = null!;
    public DbSet<PlayerInventoryPresetItemEntity> PlayerInventoryPresetItemEntity { get; set; } = null!;

    public DbSet<PlayerInventoryTemplateEntity> PlayerInventoryTemplateEntity { get; set; } = null!;
    public DbSet<PlayerInventoryTemplateItemEntity> PlayerInventoryTemplateItemEntity { get; set; } = null!;

    public DbSet<PlayerItemEntity> PlayerPurchasedItemEntity { get; set; } = null!;
}