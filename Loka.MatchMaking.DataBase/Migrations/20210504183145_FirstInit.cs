﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
#pragma warning disable CS8625

namespace Loka.MatchMaking.DataBase.Migrations
{
    public partial class FirstInit : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "MatchMaking");

            migrationBuilder.AlterDatabase()
                .Annotation("Npgsql:PostgresExtension:pgcrypto", ",,")
                .Annotation("Npgsql:PostgresExtension:uuid-ossp", ",,");

            migrationBuilder.CreateTable(
                name: "ClusterEntity",
                schema: "MatchMaking",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    MachineAddress = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    MachineName = table.Column<string>(type: "character varying(128)", maxLength: 128, nullable: false),
                    LimitActiveNodes = table.Column<int>(type: "integer", nullable: false),
                    LastActivityDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClusterEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "GameMapEntity",
                schema: "MatchMaking",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false),
                    MinimumMembers = table.Column<int>(type: "integer", nullable: false),
                    MaximumMembers = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameMapEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "GameModeEntity",
                schema: "MatchMaking",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false),
                    DurationTime = table.Column<TimeSpan>(type: "interval", nullable: false),
                    ScoreLimit = table.Column<int>(type: "integer", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameModeEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "ServerRegionEntity",
                schema: "MatchMaking",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Name = table.Column<string>(type: "character varying(64)", maxLength: 64, nullable: false),
                    TypeId = table.Column<byte>(type: "smallint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServerRegionEntity", x => x.EntityId);
                });

            migrationBuilder.CreateTable(
                name: "GameModeMapEntity",
                schema: "MatchMaking",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    GameModeId = table.Column<Guid>(type: "uuid", nullable: false),
                    GameMapId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GameModeMapEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_GameModeMapEntity_GameMapEntity_GameMapId",
                        column: x => x.GameMapId,
                        principalSchema: "MatchMaking",
                        principalTable: "GameMapEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_GameModeMapEntity_GameModeEntity_GameModeId",
                        column: x => x.GameModeId,
                        principalSchema: "MatchMaking",
                        principalTable: "GameModeEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MatchEntity",
                schema: "MatchMaking",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    GameVersion = table.Column<int>(type: "integer", nullable: false),
                    Duration = table.Column<TimeSpan>(type: "interval", nullable: false),
                    ScoreLimit = table.Column<int>(type: "integer", nullable: false),
                    Difficulty = table.Column<int>(type: "integer", nullable: false),
                    GameModeId = table.Column<Guid>(type: "uuid", nullable: false),
                    GameMapId = table.Column<Guid>(type: "uuid", nullable: false),
                    ClusterId = table.Column<Guid>(type: "uuid", nullable: false),
                    ClusterServerPort = table.Column<short>(type: "smallint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_MatchEntity_ClusterEntity_ClusterId",
                        column: x => x.ClusterId,
                        principalSchema: "MatchMaking",
                        principalTable: "ClusterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MatchEntity_GameMapEntity_GameMapId",
                        column: x => x.GameMapId,
                        principalSchema: "MatchMaking",
                        principalTable: "GameMapEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MatchEntity_GameModeEntity_GameModeId",
                        column: x => x.GameModeId,
                        principalSchema: "MatchMaking",
                        principalTable: "GameModeEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ClusterRegionEntity",
                schema: "MatchMaking",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    ClusterId = table.Column<Guid>(type: "uuid", nullable: false),
                    RegionId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ClusterRegionEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_ClusterRegionEntity_ClusterEntity_ClusterId",
                        column: x => x.ClusterId,
                        principalSchema: "MatchMaking",
                        principalTable: "ClusterEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ClusterRegionEntity_ServerRegionEntity_RegionId",
                        column: x => x.RegionId,
                        principalSchema: "MatchMaking",
                        principalTable: "ServerRegionEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MatchProcessEntity",
                schema: "MatchMaking",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    ShutdownCode = table.Column<int>(type: "integer", nullable: false),
                    ProcessId = table.Column<int>(type: "integer", nullable: false),
                    StartedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    ShutdownAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    LastCheckDate = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchProcessEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_MatchProcessEntity_MatchEntity_EntityId",
                        column: x => x.EntityId,
                        principalSchema: "MatchMaking",
                        principalTable: "MatchEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MatchTeamEntity",
                schema: "MatchMaking",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    MatchId = table.Column<Guid>(type: "uuid", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchTeamEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_MatchTeamEntity_MatchEntity_MatchId",
                        column: x => x.MatchId,
                        principalSchema: "MatchMaking",
                        principalTable: "MatchEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MatchMemberEntity",
                schema: "MatchMaking",
                columns: table => new
                {
                    EntityId = table.Column<Guid>(type: "uuid", nullable: false),
                    Squad = table.Column<int>(type: "integer", nullable: false),
                    PlayerId = table.Column<Guid>(type: "uuid", nullable: false),
                    StartedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    DesertedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true),
                    TeamId = table.Column<Guid>(type: "uuid", nullable: false),
                    State = table.Column<byte>(type: "smallint", nullable: false),
                    CreatedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    DeletedAt = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchMemberEntity", x => x.EntityId);
                    table.ForeignKey(
                        name: "FK_MatchMemberEntity_MatchTeamEntity_TeamId",
                        column: x => x.TeamId,
                        principalSchema: "MatchMaking",
                        principalTable: "MatchTeamEntity",
                        principalColumn: "EntityId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                schema: "MatchMaking",
                table: "GameMapEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "MaximumMembers", "MinimumMembers", "Name", "TypeId" },
                values: new object[] { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 117, DateTimeKind.Unspecified).AddTicks(4681), new TimeSpan(0, 0, 0, 0, 0)), null, 0, 0, "Hangar", (byte)1 });

            migrationBuilder.InsertData(
                schema: "MatchMaking",
                table: "GameModeEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "DurationTime", "Name", "ScoreLimit", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 117, DateTimeKind.Unspecified).AddTicks(7568), new TimeSpan(0, 0, 0, 0, 0)), null, new TimeSpan(0, 0, 7, 0, 0), "TeamDeadMatch", 100, (byte)1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe602"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 117, DateTimeKind.Unspecified).AddTicks(8555), new TimeSpan(0, 0, 0, 0, 0)), null, new TimeSpan(0, 0, 7, 0, 0), "LostDeadMatch", 100, (byte)2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe603"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 117, DateTimeKind.Unspecified).AddTicks(8745), new TimeSpan(0, 0, 0, 0, 0)), null, new TimeSpan(0, 0, 7, 0, 0), "ResearchMatch", 100, (byte)3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe604"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 117, DateTimeKind.Unspecified).AddTicks(8913), new TimeSpan(0, 0, 0, 0, 0)), null, new TimeSpan(0, 0, 7, 0, 0), "DuelMatch", 100, (byte)4 }
                });

            migrationBuilder.InsertData(
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "Name", "TypeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 116, DateTimeKind.Unspecified).AddTicks(5011), new TimeSpan(0, 0, 0, 0, 0)), null, "RuEast", (byte)1 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe602"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 116, DateTimeKind.Unspecified).AddTicks(5754), new TimeSpan(0, 0, 0, 0, 0)), null, "RuCentral", (byte)2 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe603"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 116, DateTimeKind.Unspecified).AddTicks(5944), new TimeSpan(0, 0, 0, 0, 0)), null, "RuSouth", (byte)3 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe604"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 116, DateTimeKind.Unspecified).AddTicks(6113), new TimeSpan(0, 0, 0, 0, 0)), null, "UsNorth", (byte)4 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe605"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 116, DateTimeKind.Unspecified).AddTicks(6305), new TimeSpan(0, 0, 0, 0, 0)), null, "UsSouth", (byte)5 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe606"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 116, DateTimeKind.Unspecified).AddTicks(6480), new TimeSpan(0, 0, 0, 0, 0)), null, "China", (byte)6 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe607"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 116, DateTimeKind.Unspecified).AddTicks(6714), new TimeSpan(0, 0, 0, 0, 0)), null, "Japan", (byte)7 },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe608"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 116, DateTimeKind.Unspecified).AddTicks(6898), new TimeSpan(0, 0, 0, 0, 0)), null, "Australian", (byte)8 }
                });

            migrationBuilder.InsertData(
                schema: "MatchMaking",
                table: "GameModeMapEntity",
                columns: new[] { "EntityId", "CreatedAt", "DeletedAt", "GameMapId", "GameModeId" },
                values: new object[,]
                {
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe101"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 118, DateTimeKind.Unspecified).AddTicks(883), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601") },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe201"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 118, DateTimeKind.Unspecified).AddTicks(1221), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe602") },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe301"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 118, DateTimeKind.Unspecified).AddTicks(1224), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe603") },
                    { new Guid("a607b39d-6e02-45ce-9b77-260c8dffe401"), new DateTimeOffset(new DateTime(2021, 5, 4, 18, 31, 45, 118, DateTimeKind.Unspecified).AddTicks(1227), new TimeSpan(0, 0, 0, 0, 0)), null, new Guid("a607b39d-6e02-45ce-9b77-260c8dffe601"), new Guid("a607b39d-6e02-45ce-9b77-260c8dffe604") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_ClusterEntity_MachineAddress",
                schema: "MatchMaking",
                table: "ClusterEntity",
                column: "MachineAddress",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ClusterEntity_MachineName",
                schema: "MatchMaking",
                table: "ClusterEntity",
                column: "MachineName");

            migrationBuilder.CreateIndex(
                name: "IX_ClusterEntity_Name",
                schema: "MatchMaking",
                table: "ClusterEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_ClusterRegionEntity_ClusterId",
                schema: "MatchMaking",
                table: "ClusterRegionEntity",
                column: "ClusterId");

            migrationBuilder.CreateIndex(
                name: "IX_ClusterRegionEntity_RegionId",
                schema: "MatchMaking",
                table: "ClusterRegionEntity",
                column: "RegionId");

            migrationBuilder.CreateIndex(
                name: "IX_GameMapEntity_Name",
                schema: "MatchMaking",
                table: "GameMapEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_GameMapEntity_TypeId",
                schema: "MatchMaking",
                table: "GameMapEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameModeEntity_Name",
                schema: "MatchMaking",
                table: "GameModeEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_GameModeEntity_TypeId",
                schema: "MatchMaking",
                table: "GameModeEntity",
                column: "TypeId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_GameModeMapEntity_GameMapId",
                schema: "MatchMaking",
                table: "GameModeMapEntity",
                column: "GameMapId");

            migrationBuilder.CreateIndex(
                name: "IX_GameModeMapEntity_GameModeId",
                schema: "MatchMaking",
                table: "GameModeMapEntity",
                column: "GameModeId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchEntity_ClusterId",
                schema: "MatchMaking",
                table: "MatchEntity",
                column: "ClusterId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchEntity_GameMapId",
                schema: "MatchMaking",
                table: "MatchEntity",
                column: "GameMapId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchEntity_GameModeId",
                schema: "MatchMaking",
                table: "MatchEntity",
                column: "GameModeId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchMemberEntity_TeamId",
                schema: "MatchMaking",
                table: "MatchMemberEntity",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchTeamEntity_MatchId",
                schema: "MatchMaking",
                table: "MatchTeamEntity",
                column: "MatchId");

            migrationBuilder.CreateIndex(
                name: "IX_ServerRegionEntity_Name",
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_ServerRegionEntity_TypeId",
                schema: "MatchMaking",
                table: "ServerRegionEntity",
                column: "TypeId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ClusterRegionEntity",
                schema: "MatchMaking");

            migrationBuilder.DropTable(
                name: "GameModeMapEntity",
                schema: "MatchMaking");

            migrationBuilder.DropTable(
                name: "MatchMemberEntity",
                schema: "MatchMaking");

            migrationBuilder.DropTable(
                name: "MatchProcessEntity",
                schema: "MatchMaking");

            migrationBuilder.DropTable(
                name: "ServerRegionEntity",
                schema: "MatchMaking");

            migrationBuilder.DropTable(
                name: "MatchTeamEntity",
                schema: "MatchMaking");

            migrationBuilder.DropTable(
                name: "MatchEntity",
                schema: "MatchMaking");

            migrationBuilder.DropTable(
                name: "ClusterEntity",
                schema: "MatchMaking");

            migrationBuilder.DropTable(
                name: "GameMapEntity",
                schema: "MatchMaking");

            migrationBuilder.DropTable(
                name: "GameModeEntity",
                schema: "MatchMaking");
        }
    }
}
