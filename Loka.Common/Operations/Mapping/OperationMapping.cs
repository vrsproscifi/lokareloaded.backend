﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Loka.Common.Operations.Results;

namespace Loka.Common.Operations.Mapping;

public static class OperationMapping
{
    public static async Task<OperationResult<IReadOnlyList<TModel>>> OperationMap<TEntity, TModel>
    (
        this IReadOnlyList<TEntity> entities,
        Func<TEntity, TModel> map,
        IOperationMappingProfile<TEntity, TModel>[] profiles,
        CancellationToken cancellationToken
    )
        where TModel : new()
    {
        var models = entities.Select(map).ToArray();

        foreach (var profile in profiles)
        {
            var mapResult = await profile.Map(entities, models, cancellationToken);
            if (mapResult.IsFailed)
                return mapResult.Error;
        }

        return models;
    }

    public static async Task<OperationResult<IReadOnlyList<TModel>>> OperationMap<TEntity, TModel>
    (
        this IMapper mapper,
        IReadOnlyList<TEntity> entities,
        IOperationMappingProfile<TEntity, TModel>[] profiles,
        CancellationToken cancellationToken
    )
        where TModel : new()
    {
        var models = mapper.Map<TModel[]>(entities);

        foreach (var profile in profiles)
        {
            var mapResult = await profile.Map(entities, models, cancellationToken);
            if (mapResult.IsFailed)
                return mapResult.Error;
        }

        return models;
    }
}