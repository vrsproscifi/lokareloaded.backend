﻿using Loka.Database.Common.Entities;

namespace Loka.Building.DataBase.Entities;

public sealed class IslandEntity : BaseEntity
{
    public Guid PlayerId { get; init; }
}