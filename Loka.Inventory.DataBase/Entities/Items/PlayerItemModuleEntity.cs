﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using Loka.Database.Common.Entities;

namespace Loka.Inventory.DataBase.Entities.Items;

//public sealed class PlayerItemModuleEntity : BaseEntity
//{
//    #region ItemEntity
//
//    [ForeignKey(nameof(ItemEntity))]
//    public Guid ItemId { get; set; }
//
//    public PlayerItemEntity? ItemEntity { get; set; }
//
//    #endregion
//
//    #region ModuleEntity
//
//    [ForeignKey(nameof(ModuleEntity))]
//    public Guid ModuleId { get; set; }
//
//    public PlayerItemEntity? ModuleEntity { get; set; }
//
//    #endregion
//}