﻿using Loka.Identity.Grains.Authentication;
using Loka.Identity.Grains.Storages.Sessions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Loka.Identity.Grains;

public static class ServiceExtensions
{
    public static IServiceCollection AddIdentityOrleansServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddSingleton<IJwtBearerTokenGenerator, JwtBearerTokenGenerator>();
        services.AddSingleton<IPlayerSessionStorage, PlayerSessionRedisStorage>();
        return services;
    }
}