﻿using AutoMapper;
using Loka.Common.Cache.DistributedCache;
using Loka.Database.Common.Repositories;
using Loka.Store.DataBase;
using Loka.Store.DataBase.Entities;
using Loka.Store.Interfaces.Models;
using Loka.Store.Interfaces.Models.Resources;
using Loka.Store.Interfaces.Repositories;

namespace Loka.Store.Grains.Repositories;

internal sealed class GameResourcesRepository : BaseRepository<StoreDbContext, GameResourceEntity, GameResourceModel>, IGameResourcesRepository
{
    public GameResourcesRepository(ICacheProvider cacheProvider, StoreDbContext dbContext, IMapper mapper) : base(cacheProvider, dbContext, mapper)
    {
    }
}