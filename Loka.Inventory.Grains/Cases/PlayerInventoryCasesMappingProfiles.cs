﻿using Loka.Inventory.DataBase.Entities.Cases;
using Loka.Inventory.DataBase.Entities.Discounts;
using Loka.Inventory.Interfaces.Cases;
using Loka.Inventory.Interfaces.Discounts;

namespace Loka.Inventory.Grains.Cases;

internal sealed class PlayerInventoryCasesMappingProfiles : Profile
{
    public PlayerInventoryCasesMappingProfiles()
    {
        CreateMap<PlayerDiscountEntity, GetPlayerDiscounts.Entry>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.ModelId))
            .ForMember(q => q.NextAvailableDate, q => q.MapFrom(w => w.NextAvailableDate))
            .ForMember(q => q.EndDiscountDate, q => q.MapFrom(w => w.EndDiscountDate));

        CreateMap<PlayerCaseEntity, PlayerInventoryCaseModel>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.EntityId))
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.CaseId))
            .ForMember(q => q.Amount, q => q.MapFrom(w => w.Amount));
    }
}