﻿using System;
using Loka.Common.ClickHouse.Entities;

namespace Loka.MatchMaking.DataBase.Entities.Server.Statistics;

public abstract class ClusterStatisticsEntity<TTable> : ClickHouseEntity<TTable> where TTable : ClickHouseEntity<TTable>
{
    public Guid ClusterId { get; set; }

    public byte ProcessorUsage { get; set; }
    public short MemoryUsage { get; set; }
    public short ActiveNodes { get; set; }
}