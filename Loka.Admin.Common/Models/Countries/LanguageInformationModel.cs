﻿using System;
using Loka.Common.Entry;

namespace Loka.Admin.Common.Models.Countries;

public sealed class LanguageInformationModel : IEntry, INamedEntry
{
    public Guid Id { get; set; }
    public string Name { get; set; }

    public override string ToString()
    {
        return $"Id: {Id} | Name: {Name}";
    }
}