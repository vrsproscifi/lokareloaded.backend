﻿using System;
using Loka.Common.MediatR.Requests;

namespace Loka.Admin.Players.Handlers.Players.Inventory.GetPlayerEquippedItems;

public class GetPlayerEquippedItemsRequest : IOperationRequest<GetPlayerEquippedItemsResponse>
{
    public Guid PlayerId { get; set; }
}