﻿using Loka.Inventory.Interfaces.Inventory;

namespace Loka.Inventory.Interfaces.Workbench;

public static class CraftWorkbenchItem
{
    [GenerateSerializer]
    public sealed class Request : IOperationRequest<Result>
    {
        [Id(0)]
        public Guid ModelId { get; init; }

        [Id(1)]
        public int Count { get; init; }
    }

    [GenerateSerializer]
    public sealed class Result
    {
        [Id(0)]
        public PlayerInventoryItemModel.RPC Item { get; init; } = null!;
    }
}