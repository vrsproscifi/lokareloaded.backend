﻿using Loka.Common.Entry;
using Loka.Common.Operations.Results;

namespace Loka.Database.Common.Repositories;

public interface IBaseRepository<TModel> where TModel : class, IEntry
{
    Task<OperationResult<TModel>> GetById(Guid id);
    Task<IReadOnlyList<TModel>> GetByIds(IReadOnlyList<Guid> ids);
    Task<IReadOnlyList<TModel>> GetByIds(string cacheKey, IReadOnlyList<Guid> ids);
    Task<IReadOnlyList<TModel>> FilterBy(string cacheKey, Func<TModel, bool> predicate);
    Task<IReadOnlyList<TModel>> FilterBy(Func<TModel, bool> predicate);
    Task<IReadOnlyList<TModel>> GetAll();
}


/*
public interface IBaseRepository<out TEntity, TModel> : IBaseRepository<TModel>
    where TModel : class, IEntry
    where TEntity : BaseEntity
{
    Task<IReadOnlyList<TModel>> FilterBy(string cacheKey, Func<TEntity, bool> predicate);
    Task<IReadOnlyList<TModel>> FilterBy(Func<TEntity, bool> predicate);

    Task<IReadOnlyList<TModel>> FilterBy(string cacheKey, Func<TEntity, bool> entityPredicate, Func<TModel, bool> predicate);
    Task<IReadOnlyList<TModel>> FilterBy(Func<TEntity, bool> entityPredicate, Func<TModel, bool> predicate);
}*/