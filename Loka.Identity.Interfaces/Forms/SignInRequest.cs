﻿using System.Net;

namespace Loka.Identity.Interfaces.Forms;

[GenerateSerializer]
public sealed class SignUpForm
{
    //public  string SteamId { get; init; } = null!;
    [Id(0)]
    public string PlayerName { get; init; } = null!;

    [Id(1)]
    public string? Email { get; init; }

    [Id(2)]
    public string? Password { get; init; }

    [Id(3)]
    public DateTimeOffset? Birthdate { get; init; }

    [Id(4)]
    public Guid? RoleId { get; init; }

    [Id(5)]
    public Guid? CountryId { get; init; }

    [Id(6)]
    public Guid? LanguageId { get; init; }

    [Id(7)]
    public Guid? GenderId { get; init; }

    [Id(8)]
    public IPAddress IpAddress { get; init; } = null!;

    [Id(9)]
    public TimeSpan TokenLifetime { get; init; }
}