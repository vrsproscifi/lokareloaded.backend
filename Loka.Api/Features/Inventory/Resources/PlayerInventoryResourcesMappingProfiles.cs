﻿using Loka.Inventory.Interfaces.Resources;

namespace Loka.Api.Features.Inventory.Resources;

internal sealed class PlayerInventoryResourcesMappingProfiles : Profile
{
    public PlayerInventoryResourcesMappingProfiles()
    {
        //==========================================================
        CreateMap<Loka.Inventory.Interfaces.Resources.GetPlayerResources.Result, GetPlayerInventoryResources.Types.Response>()
            .ForMember(q => q.Entries, q => q.MapFrom(w => w.Entries));

        CreateMap<PlayerResourceModel.Lightweight, PlayerInventoryResource>()
            .ForMember(q => q.Id, q => q.MapFrom(w => w.Id))
            .ForMember(q => q.ModelId, q => q.MapFrom(w => w.Resource.Id))
            .ForMember(q => q.Amount, q => q.MapFrom(w => w.Amount));
    }
}