﻿using Loka.Database.Common.Repositories;
using Loka.Inventory.Interfaces.Inventory.Requests;

namespace Loka.Inventory.Interfaces.Inventory;

public interface IPlayerInventoryItemsRepository : IBaseChildRepository<PlayerInventoryItemModel.Repository>
{
    Task<OperationResult<PlayerInventoryItemModel.Repository>> AddInventoryItem(Guid playerId, AddOrStackInventoryItem.Request request);
    Task<OperationResult<PlayerInventoryItemModel.Repository>> AddOrStackInventoryItem(Guid playerId, AddOrStackInventoryItem.Request request);
}