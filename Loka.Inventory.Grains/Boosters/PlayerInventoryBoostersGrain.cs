﻿using Loka.Common.Authentication;
using Loka.Common.Orleans.Grains;
using Loka.Inventory.Interfaces.Boosters;

namespace Loka.Inventory.Grains.Boosters;

public sealed class PlayerInventoryBoostersGrain : Grain, IPlayerInventoryBoostersGrain, IWithPlayerActor
{
    private IPlayerInventoryBoosterRepository PlayerInventoryRepository { get; }

    public PlayerActor Actor { get; set; } = null!;

    public PlayerInventoryBoostersGrain(IPlayerInventoryBoosterRepository playerInventoryRepository)
    {
        PlayerInventoryRepository = playerInventoryRepository;
    }

    public async Task<OperationResult<PlayerInventoryBoosterModel>> ActivatePlayerBooster(ActivatePlayerBooster.Request request)
    {
        var activated = await PlayerInventoryRepository.CountActivated(Actor.PlayerId);
        if (activated >= 3)
        {
            return OperationResultCode.TooMuchActivatedBoosters;
        }

        return await PlayerInventoryRepository.Activate(Actor.PlayerId, request);
    }

    public Task<OperationResult<PlayerInventoryBoosterModel>> DeactivatePlayerBooster(DeactivatePlayerBooster.Request request)
    {
        return PlayerInventoryRepository.Deactivate(Actor.PlayerId, request);
    }

    public Task<PlayerInventoryBoosterModel> AddOrStackInventoryBooster(AddOrStackInventoryBooster.Request request)
    {
        return PlayerInventoryRepository.AddOrStack(Actor.PlayerId, request);
    }

    public async Task<OperationResult<GetPlayerBoosters.Result>> GetPlayerBoosters()
    {
        return new GetPlayerBoosters.Result()
        {
            Entries = await PlayerInventoryRepository.GetAll(Actor.PlayerId)
        };
    }
}